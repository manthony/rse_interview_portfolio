#include <ExportTrees/Export.h>


void  ExportTrees::GetxAODJets(Jets*& jets, std::string name){
			   
  if(!jets)
    jets=new Jets();
  else{
    std::cout << name << " are not null pointer!!! not retrieving..." << std::endl;
    return;
  }


  bool isFat=true;
  if(name=="STCalibAntiKt4EMTopoJets") isFat=false;

  const xAOD::JetContainer* xAOD_jets(0);
  //const xAOD::ShallowAuxContainer* xAOD_jets_aux(0);
  evtStore()->retrieve(xAOD_jets, name);
  // evtStore()->retrieve(xAOD_jets_aux, "STCalibAntiKt4EMTopoJetsAux.");

  
  if(isFat){
    //m_jetRecTool_kt2->execute();
    //const xAOD::JetContainer* subJets(0);
    //evtStore()->retrieve( subJets,"MySubJets" );
    //std::cout << "size=" << xAOD_jets->size() << " " << subJets->size() << std::endl;


    /*
      PseudoJetGetter* plcget = new PseudoJetGetter("test");
      plcget->setProperty("InputContainer", name);
      plcget->setProperty("OutputContainer", "MySubJets");
      plcget->setProperty("Label", "Tower");
      plcget->setProperty("SkipNegativeEnergy", true);
      plcget->setProperty("GhostScale", 0.0);
      plcget->initialize();
      plcget->get();
      plcget->print();
      
      const jet::PseudoJetVector* subJets(0);
      evtStore()->retrieve( subJets,"MySubJets" );
      std::cout << "size=" << xAOD_jets->size() << " " << subJets->size() << std::endl;
    */
    
    //for(const auto& jet: *xAOD_jets){
     
      // }




  }


  //const DataVector<xAOD::TrackParticle>* track_particles(0);
  //evtStore()->retrieve(track_particles,"InDetTrackParticles");

  /*
    Particles* InDetTracks = new Particles();
    for (const auto& track : *track_particles){
    Particle* theTrack = new Particle(track->p4()*0.001);
    //std::cout << theTrack->Pt() << std::endl;
    InDetTracks->push_back(theTrack);
    }
  */
  


  for(const auto& jet: *xAOD_jets){

    bool isSignal = true; 
    bool passOR = true;
    bool isBjet = false;
    
    if(!isFat){
      isSignal = jet->auxdata<char>("signal");
      passOR = jet->auxdata<char>("passOR");
      isBjet = jet->auxdata<char>("bjet");
    }
    
   
    if(isSignal && passOR)
      {

	TLorentzVector tmpjet=jet->p4();
	Jet* theJet = new Jet(tmpjet.Pt()/1000.,tmpjet.Eta()/1000.,tmpjet.Phi()/1000.,tmpjet.E()/1000.);
	if(isBjet) theJet->flav=5;
	
	//get associated tracks
	//	Particles* tracks = InDetTracks;//JSS::GetJetGhostTracks(jet);
	Particles* tracks = JSS::GetJetGhostTracks(jet);
	//get calo constituents
	Particles* caloCells = JSS::GetJetConstituents(jet);

	double charge = JSS::charge(tracks, theJet);
	std::cout << "charge=" << charge << " " << theJet->Pt() << std::endl;

	theJet->charge=charge;


	//jet pull with tracks
	//TVector2 pull_tracks;// = JSS::CalcJetPull(tracks,theJet);
	
	

	/*
	if(isFat){
	  //for (const auto &cell: *caloCells){
	  //for (const auto &cell: *tracks){
	  //double dphi=TVector2::Phi_mpi_pi(theJet->Phi() - cell->Phi());
	  //double dy  =theJet->Rapidity() - cell->Rapidity(); 
	  //double pt=cell->Pt();
	  //std::cout << dphi << " " << dy << std::endl;
	  //hJet->Fill(dy,dphi,pt);
	
	  theJet->caloCells=caloCells;
	  theJet->tracks=tracks;

	  Particles* fatSubJets = new Particles();
	  std::vector<fastjet::PseudoJet> subjets = m_subjetTool->result(*jet);
	  for(auto subjet : subjets){
	    fatSubJets->push_back(new Particle(subjet.px(),subjet.py(),subjet.pz(),subjet.e()));
	  }
	  theJet->subJets=fatSubJets;


	  //theJet->tracks=InDetTracks;
	}

	//jet pull with calorimeter cells
	TVector2 pull_caloCells = JSS::CalcJetPull(caloCells,theJet);

	TVector2 pull_test= JSS::CalcJetPull(jet);



	//calculate the jet pull angle
	double theta_tracks(-99),theta_calo(-99);
	if(tracks->size()>0){
	  //theta_tracks = pull_tracks.Phi();//acos(pull_tracks.Px() / pull_tracks.Mod()  );

	  theta_tracks= atan2(pull_tracks.Py(),pull_tracks.Px() ) ;
	 
	  //std::cout << theta_tracks << std::endl;
	  //continue;
	  
	  //std::cout << theta_tracks << " " << theta_2 << " " << atan2(pull_tracks.Py(),pull_tracks.Px() ) << std::endl;
	  //std::cout << pull_tracks.Phi() << std::endl;
	  //std::cout << theta_tracks << " " << pow(pow(pull_tracks.Px(),2)+pow(pull_tracks.Py(),2),0.5) << " " << pull_tracks.Mod() <<   std::endl;
	}
	if(caloCells->size()>0){
	  //std::cout << pull_test.Mod() << " " << pull_caloCells.Mod() << std::endl;
	  pull_caloCells=pull_test;
	  if(pull_caloCells.Py()!=0 && pull_caloCells.Px()!=0)
	    theta_calo = atan2(pull_caloCells.Py(),pull_caloCells.Px() );
	}


	theJet->jpa_tracks=theta_tracks;
	
	theJet->jpa_calo=theta_calo;
	
	theJet->ntracks=tracks->size();	
	*/
	
	//cleanup the memory
	if(!isFat){
	  delete tracks;
	  delete caloCells;
	}
	//let's play with the jet;
	//m_subjetTool->modifyJet(*jet);



	//double y_jet=jet->p4().Rapidity();
	//double phi_jet=jet->p4().Phi();
	//double pt_jet=jet->pt();
	
	//TVector2 pull_vector(0,0);
	
	/*
	for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
	  const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
	  if(!pTrk)
	    continue;
	  
	  double dy=pTrk->p4().Rapidity() - y_jet;
	  double dphi=pTrk->p4().Phi() - phi_jet;
	  
	  TVector2 rvector(dy,dphi);
	  TVector2 ptvector(pTrk->p4().Px(),pTrk->p4().Py());
	  pull_vector = pull_vector + (rvector*ptvector)/pt_jet;
  

	  //std::cout << "item=" << pTrk->pt() << " " << pTrk->p4().X()   << " " << pTrk->p4().Y()  << std::endl;
	  //htracks->Fill(pTrk->phi(), pTrk->eta(), pTrk->pt() );
	}
	//std::cout << pull_vector.Mod() << std::endl;
	theJet->pull=pull_vector;
	*/
	jets->push_back(theJet);
	


      }  
  }

  // std::sort(m_jets->begin(),m_jets->end(),ptsorter);

  
  // for(unsigned int i=0;i<m_jets->size();i++){
  //   Jet* jet = (*m_jets)[i];
  //   std::sort(m_jets->begin(),m_jets->end(),dRsorter(jet));
   
  //   for(unsigned int j=1;j<m_jets->size();j++){
  //     Jet* jet2 = (*m_jets)[j];
      
  //     double dy=jet->Rapidity() - jet2->Rapidity();
  //     double dphi=jet->Phi() - jet2->Phi();
  //     TVector2 axis(dy,dphi);


  //     double product = (jet->pull.Px()*axis.Px() +  jet->pull.Py()*axis.Py())/(axis.Mod()*jet->pull.Mod())  ;
  //     std::cout << i << " " << product << std::endl;



  //     break;
  //   }


  //   std::sort(m_jets->begin(),m_jets->end(),ptsorter);
  // }

  // //outputntuple->pull_1jet=smallest_product;

 
  std::sort(m_jets->begin(),m_jets->end(),ptsorter);


  
}
