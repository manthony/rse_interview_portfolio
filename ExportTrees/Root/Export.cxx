
 #include "CalibrationDataInterface/CalibrationDataContainer.h"
 #include "TH2D.h"




 #include "TF1.h"
 #include "TH1F.h"
 #include "TFile.h"
 #include "TTree.h"
 #include "TChain.h"
 #include <TROOT.h>
 #include "TLorentzVector.h"
 #include "TVector2.h"



 #include <iostream>
 #include <fstream>
 #include <string>
 #include <cmath>
 #include <cstdlib>
 #include <sstream>
 #include <vector>
 #include <iomanip>
 #include <regex>
 #include <iterator>
 #include <algorithm>


 #include "TKey.h"
 #include "TIterator.h"
 #include "TClass.h"
 #include "TList.h"
 #include "TEntryList.h"
 #include "TEventList.h"

 #include "ExportTrees/Export.h"


 #include <map>

 using namespace std;

using asg::ToolStore;


 bool pairCompare(const std::pair<TLorentzVector, double>& firstElem, const std::pair<TLorentzVector,double>& secondElem) {
   return firstElem.second > secondElem.second;
 }


 double ExportTrees::getXsec(int runnumber,string xsecfile)
 {
   ifstream xseclist;
   xseclist.open(xsecfile.c_str());

   bool found = false;
   double xsec, kfact, filteff;
   xsec=kfact=filteff=0;
   string line;
   while (std::getline(xseclist, line) && !found)
     {
       std::istringstream iss(line);
       int a;
       string desc = "";
       float b, c, d;
       if (!(iss >> a >> desc >> b >> c >> d)) { 
	 continue; } // error

       if (a==runnumber)
	 {
	   cout << " x-sec, k-factor and filt eff for the run from SUSYTools file " <<b << " " << c  << " " << d << endl;
	   xsec = b;
	   kfact = c;
	   filteff = d;

	   found = true;
	 }
     }

   std::cout << xsec << " " << kfact << " " << filteff << std::endl;
   xseclist.close();
   return xsec*kfact*filteff;
 }



 void ExportTrees::Tokenize(const string& str,
			    vector<string>& tokens,
			    const string& delimiters )
 {
   // Skip delimiters at beginning.
   string::size_type lastPos = str.find_first_not_of(delimiters, 0);
   // Find first "non-delimiter".
   string::size_type pos     = str.find_first_of(delimiters, lastPos);

   while (string::npos != pos || string::npos != lastPos)
     {
       // Found a token, add it to the vector.
       tokens.push_back(str.substr(lastPos, pos - lastPos));
       // Skip delimiters.  Note the "not_of"
       lastPos = str.find_first_not_of(delimiters, pos);
       // Find next "non-delimiter"
       pos = str.find_first_of(delimiters, lastPos);
     }
 }


 void ExportTrees::resetKt8Jets(){

   if(m_fatjets_kt8){
     for(const auto &ptr: *m_fatjets_kt8){
       if(ptr)
	 delete ptr;
     }
     m_fatjets_kt8->shrink_to_fit();
     m_fatjets_kt8->clear();
     delete m_fatjets_kt8;
     m_fatjets_kt8=NULL;
   }


 }


 void ExportTrees::resetKt12Jets(){

   if(m_fatjets_kt12){
     for(const auto &ptr: *m_fatjets_kt12){
       if(ptr)
	 delete ptr;
     }
     m_fatjets_kt12->shrink_to_fit();
     m_fatjets_kt12->clear();
     delete m_fatjets_kt12;
     m_fatjets_kt12=NULL;
   }


 }

void ExportTrees::resetJets(){

  if(m_jets){
     for(const auto &ptr: *m_jets){
       if(ptr)
	 delete ptr;
     }
     m_jets->shrink_to_fit();
     m_jets->clear();
     delete m_jets;
     m_jets=NULL;
   }


   if(m_bjets){
     m_bjets->shrink_to_fit();
     m_bjets->clear();
     delete m_bjets;
     m_bjets=NULL;
   }
   if(m_bjets60){
     m_bjets60->shrink_to_fit();
     m_bjets60->clear();
     delete m_bjets60;
     m_bjets60=NULL;
   }
    if(m_bjets85){
     m_bjets85->shrink_to_fit();
     m_bjets85->clear();
     delete m_bjets85;
     m_bjets85=NULL;
   }
   if(m_ljets){
     m_ljets->shrink_to_fit();
     m_ljets->clear();
     delete m_ljets;
     m_ljets=NULL;
   }


   //RESET TRACK JETS
   if(m_trackjets){
     for(const auto &ptr: *m_trackjets){
       if(ptr)
	 delete ptr;
     }
     m_trackjets->shrink_to_fit();
     m_trackjets->clear();
     delete m_trackjets;
     m_trackjets=NULL;
   }


   if(m_trackbjets60){
     m_trackbjets60->shrink_to_fit();
     m_trackbjets60->clear();
     delete m_trackbjets60;
     m_trackbjets60=NULL;
   }
   if(m_trackbjets70){
     m_trackbjets70->shrink_to_fit();
     m_trackbjets70->clear();
     delete m_trackbjets70;
     m_trackbjets70=NULL;
   }
   if(m_trackbjets77){
     m_trackbjets77->shrink_to_fit();
     m_trackbjets77->clear();
     delete m_trackbjets77;
     m_trackbjets77=NULL;
   }
   if(m_trackbjets85){
     m_trackbjets85->shrink_to_fit();
     m_trackbjets85->clear();
     delete m_trackbjets85;
     m_trackbjets85=NULL;
   }
   

   if(m_trackljets){
     m_trackljets->shrink_to_fit();
     m_trackljets->clear();
     delete m_trackljets;
     m_trackljets=NULL;
   }




 }

void ExportTrees::resetFatJets(){

  if(m_fatjets_st){
    for(const auto &ptr: *m_fatjets_st){
      if(ptr)
	delete ptr;
    }
    m_fatjets_st->shrink_to_fit();
    m_fatjets_st->clear();
    delete m_fatjets_st;
    m_fatjets_st=NULL;
  }
}


 void ExportTrees::reset(){


   if(m_photons){
     for(const auto &ptr: *m_photons){
       if(ptr)
	 delete ptr;
     }
     m_photons->shrink_to_fit();
     m_photons->clear();
     delete m_photons;
     m_photons=NULL;
   }

   if(m_truthphotons){
     for(const auto &ptr: *m_truthphotons){
       if(ptr)
	 delete ptr;
     }
     m_truthphotons->shrink_to_fit();
     m_truthphotons->clear();
     delete m_truthphotons;
     m_truthphotons=NULL;
   }



   if(m_muons){
     for(const auto &ptr: *m_muons){
       if(ptr)
	 delete ptr;
     }
     m_muons->shrink_to_fit();
     m_muons->clear();
     delete m_muons;
     m_muons=NULL;
   }



   if(m_electrons){
     for(const auto &ptr: *m_electrons){
       if(ptr)
	 delete ptr;
     }
     m_electrons->shrink_to_fit();
     m_electrons->clear();
     delete m_electrons;
     m_electrons=NULL;
   }

   
   if(m_taus){ // removing all traces of taus
     for(const auto &ptr:*m_taus){
       if(ptr)
	 delete ptr;
     }
     m_taus->shrink_to_fit();
     m_taus->clear();
     delete m_taus;
     m_taus=NULL;
   }



   resetJets();
   resetFatJets();
   resetKt8Jets();
   resetKt12Jets();


   //free up the container
   if(m_leptons){
     m_leptons->shrink_to_fit();
     m_leptons->clear();
     delete m_leptons;
     m_leptons=NULL;
   }

   if(m_Z)
     delete m_Z;

   if(m_photon)
     delete m_photon;

   if(m_met)
     delete m_met;
   if(m_met_simple)
     delete m_met_simple;
   //if(m_metlep)
   //  delete m_metlep;
   //if(m_metph){
   //  delete m_metph;
   // }
   if(m_met_fake)
     delete m_met_fake;
   if(m_met_track )
     delete m_met_track;



   if(m_met_NonInt)
     delete m_met_NonInt;
   
 }

ExportTrees::ExportTrees(string in_file_name, string out_file_name, bool useProcessedNev, string filter, bool nosys, bool smr):
  asg::AsgTool("ExportTrees"),
  in_file_name(in_file_name),
  out_file_name(out_file_name),
  isPythia(useProcessedNev),
  filter(filter),
  nosys(nosys),
  smr(smr){

  GetJetToolRunner(m_jetRecTool_kt2,0.5,"STCalibAntiKt10LCTopoTrimmedPtFrac5SmallR20Jets","MySubJets");

  m_subjetTool = new JetSubStructureUtils::SubjetFinder(fastjet::antikt_algorithm,0.2);

}




ExportTrees::~ExportTrees(){
  delete m_subjetTool;
}


//-------------------------------------------
// Run a simple test on the input file!
//--------------------------------------------
void ExportTrees::runTest(){
  
  //open up the infile
  std::unique_ptr<TFile> f_in = std::make_unique<TFile>(in_file_name.c_str());
  //check the file is good
  if(f_in->IsZombie())
   {
     cout << "file failed! IsZombie " << endl;
     return;
   }
 

  string tree_name = "Nominal";
  TTree *t_old = (TTree*)f_in->Get(tree_name.c_str());
  if (!t_old){
    std::cout << tree_name << " is no good!!!  " <<std::endl;
    abort();
  }
  
 

  std::cout << "\n \n" << std::endl;
  //inputntuple->Show(0);
  std::cout << "\n \n" << std::endl;
  t_old->Show(0);
  std::cout << "\n \n" << std::endl;


  t_old->SetScanField(0);
  t_old->Scan("jet_pt/1000.:jet_eta:jet_phi:jet_e/1000.:jet_flav:jet_MV2c10","","",5);
  std::cout << "\n \n" << std::endl;


  TObjArray* array=(TObjArray*)t_old->GetListOfBranches();
  int nbranches = array->GetEntries();
  
  std::cout << "Number of branches=" << nbranches << std::endl;

  for(int i=0;i<nbranches;i++){
   
    //std::cout << i << " " <<  temp->GetName() << std::endl;

    std::string names;
    int j=0;
    while(j<5){
      TObject* temp = array->At(i);
      if(j<4)names+=std::string(temp->GetName())+":";
      else names+=std::string(temp->GetName());
      i++;
      j++;
    }
    std::cout << names << std::endl;
    t_old->Scan(names.c_str(),"","",5);
    std::cout << "\n" << std::endl;
  }

  if (inputntuple) delete inputntuple;
  inputntuple = new MiniNtuple(t_old); 
  
  return;
 }


void ExportTrees::execute(){
  
  cout << "------------------------------" << endl;
  cout << "Starting export of file : " << in_file_name << endl;
  cout << "Out file name : " << out_file_name << endl;
  cout << "use procssed number of events (useage with Pythia samples) : " << isPythia << endl;
  cout << "skimming : " << filter << endl;
  cout << "nosystematics : " << nosys << endl;
  cout << "make smr ntuple : " << smr << endl;
  
  cout << "------------------------------" << endl;
 
  //open up the infile
  std::unique_ptr<TFile> f_in = std::make_unique<TFile>(in_file_name.c_str());
  //check the file is good
  if(f_in->IsZombie())
   {
     cout << "file failed! IsZombie " << endl;
     return;
   }
  TTree* xAODTree(0);
  std::unique_ptr<xAOD::TEvent> xAODevent;
  
  if(m_usexAODJets){
    xAOD::Init();
    xAODTree=(TTree*)f_in->Get("CollectionTree;1");
    std::unique_ptr<xAOD::TEvent> tmpEvent = std::make_unique<xAOD::TEvent>(xAOD::TEvent::kClassAccess);
    xAODevent = std::move(tmpEvent);
    xAODevent->readFrom(xAODTree);
  }
 
  //only export certain trees and counter histograms
  vector<string> out_trees;
  vector<string> out_counters;
  
  TIter nextkey(f_in->GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)nextkey())) 
    {
      cout << key->GetName() << endl;
      TClass *cl = gROOT->GetClass(key->GetClassName());
      if (cl->InheritsFrom("TTree"))
	{
	  string tmp = key->GetName();
	  //only get Nominal
	  if(tmp=="Nominal" && !(std::find(out_trees.begin(), out_trees.end(), tmp) != out_trees.end()))
	    {
	      if(nosys && tmp!="Nominal")
		continue;
	      out_trees.push_back(tmp);
	      //Nominal and systematic trees will have associated histogram
	      string tmp2 = "H"+tmp;
	      out_counters.push_back(tmp2);
       	    }
	}
    }
  //check which trees and associated counters have been found
  cout << endl;
  cout << "To do..... " << endl;
  for(unsigned int i=0 ; i<out_trees.size() ; i++ ) {
    cout <<  out_trees[i] << " " << out_counters[i] << endl;
  }
  cout << endl;
  

  


 
 for(unsigned int k=0 ; k<out_trees.size() ; k++ ) {
   cout << "Now doing..... " <<  out_trees[k] << " " << out_counters[k] << endl;
   
   string prefix="Fixed"; 
   string tree_name = out_trees[k];
   string tree_name_new = tree_name+prefix;
   //if(smr)
   //  tree_name_new="SMR"+prefix;
   
   string counter_name = out_counters[k]+"_Wgt";
   string counter_name_new = counter_name+prefix;
   string entries_name = out_counters[k];
   string entries_name_new = "HNominal"+prefix;
  
   //check the new names
   std::cout <<":::::::::::::::::::::::::::::::::::::::::::::::::::::" <<std::endl;
   std::cout << "Tree Name \t " << tree_name_new << std::endl;
   std::cout << "Counter Name \t " << counter_name_new << std::endl;
   std::cout << "Entries Name \t " << entries_name_new << std::endl;
   std::cout <<":::::::::::::::::::::::::::::::::::::::::::::::::::::" <<std::endl;

   
   //load up the current tree to be exported
   TTree *t_old = (TTree*)f_in->Get(tree_name.c_str());
   if (!t_old){
     std::cout << tree_name << " is no good!!! continuing.... " <<std::endl;
     continue;
   }
   

   string path = getenv("ROOTCOREBIN");
   
 
   //TFile* m_file=new TFile( out_file_name.c_str(), "RECREATE" );
   if (inputntuple) delete inputntuple;
   inputntuple = new MiniNtuple(t_old); 
   if (outputntuple) delete outputntuple;
   outputntuple = new myNtupleDumper(out_file_name, tree_name_new, k);
   //if(nullptr != &inputntuple->jet_npartons) outputntuple->m_ExtraTruth=true;
   outputntuple->init();


 

   if (MVAFlag){
     cout<<"*************************** TMVA CONFIGURATION**************************************"<<endl;
     //add in the TMVA reader
     cout<<"Processing BDT trained on SIGNAL = (1000,1)"<<endl;
     m_reader_odd = new TMVA::Reader( "!Color:!Silent" );
     m_reader_even = new TMVA::Reader( "!Color:!Silent" );
     m_reader_BDTG_odd = new TMVA::Reader( "!Color:!Silent" );
     m_reader_BDTG_even = new TMVA::Reader( "!Color:!Silent" );     

      m_reader_odd->AddVariable("Tau32_1fatjet",  &m_BDTvar1); // FIX TO REMOVE THE NAN errors
      m_reader_odd->AddVariable("Tau32_2fatjet",  &m_BDTvar2); // FIX TO REMOVE THE NAN errors, fixed at ntuple level.
      m_reader_odd->AddVariable( "m_1fatjet_st",   &m_BDTvar3);
      m_reader_odd->AddVariable( "m_2fatjet_st",   &m_BDTvar4);
      m_reader_odd->AddVariable( "Split23_1fatjet", &m_BDTvar5);
      m_reader_odd->AddVariable( "Split23_2fatjet", &m_BDTvar6);
      m_reader_odd->AddVariable( "eT_miss_orig"   ,&m_BDTvar7);
      m_reader_odd->AddVariable( "MTbmin_orig"   ,&m_BDTvar8);
      m_reader_odd->AddVariable( "dRb1b2"   ,&m_BDTvar9);
      m_reader_odd->AddVariable("nj_good",&m_BDTvar10);
      m_reader_odd->AddVariable("pt_1fatjet_st", &m_BDTvar11);
      m_reader_odd->AddVariable("pt_2fatjet_st",&m_BDTvar12);
      m_reader_odd->AddVariable("MT2Chi2",&m_BDTvar13);
      m_reader_odd->AddVariable("NFatJetsSt",&m_BDTvar14);

      m_reader_even->AddVariable("Tau32_1fatjet",  &m_BDTvar1); // FIX TO REMOVE THE NAN errors                                                                                                    
      m_reader_even->AddVariable("Tau32_2fatjet",  &m_BDTvar2); // FIX TO REMOVE THE NAN errors, fixed at ntuple level.                                                                             
      m_reader_even->AddVariable( "m_1fatjet_st",   &m_BDTvar3);
      m_reader_even->AddVariable( "m_2fatjet_st",   &m_BDTvar4);
      m_reader_even->AddVariable( "Split23_1fatjet", &m_BDTvar5);
      m_reader_even->AddVariable( "Split23_2fatjet", &m_BDTvar6);
      m_reader_even->AddVariable( "eT_miss_orig"   ,&m_BDTvar7);
      m_reader_even->AddVariable( "MTbmin_orig"   ,&m_BDTvar8);
      m_reader_even->AddVariable( "dRb1b2"   ,&m_BDTvar9);
      m_reader_even->AddVariable("nj_good",&m_BDTvar10);
      m_reader_even->AddVariable("pt_1fatjet_st", &m_BDTvar11);
      m_reader_even->AddVariable("pt_2fatjet_st",&m_BDTvar12);
      m_reader_even->AddVariable("MT2Chi2",&m_BDTvar13);
      m_reader_even->AddVariable("NFatJetsSt",&m_BDTvar14);

      m_reader_BDTG_odd->AddVariable("Tau32_1fatjet",  &m_BDTvar1); // FIX TO REMOVE THE NAN errors                                                                                                
      m_reader_BDTG_odd->AddVariable("Tau32_2fatjet",  &m_BDTvar2); // FIX TO REMOVE THE NAN errors, fixed at ntuple level.                                                                        
      m_reader_BDTG_odd->AddVariable( "m_1fatjet_st",   &m_BDTvar3);
      m_reader_BDTG_odd->AddVariable( "m_2fatjet_st",   &m_BDTvar4);
      m_reader_BDTG_odd->AddVariable( "Split23_1fatjet", &m_BDTvar5);
      m_reader_BDTG_odd->AddVariable( "Split23_2fatjet", &m_BDTvar6);
      m_reader_BDTG_odd->AddVariable( "eT_miss_orig"   ,&m_BDTvar7);
      m_reader_BDTG_odd->AddVariable( "MTbmin_orig"   ,&m_BDTvar8);
      m_reader_BDTG_odd->AddVariable( "dRb1b2"   ,&m_BDTvar9);
      m_reader_BDTG_odd->AddVariable("nj_good",&m_BDTvar10);
      m_reader_BDTG_odd->AddVariable("pt_1fatjet_st", &m_BDTvar11);
      m_reader_BDTG_odd->AddVariable("pt_2fatjet_st",&m_BDTvar12);
      m_reader_BDTG_odd->AddVariable("MT2Chi2",&m_BDTvar13);
      m_reader_BDTG_odd->AddVariable("NFatJetsSt",&m_BDTvar14);

      m_reader_BDTG_even->AddVariable("Tau32_1fatjet",  &m_BDTvar1); // FIX TO REMOVE THE NAN errors                                                                                               
      m_reader_BDTG_even->AddVariable("Tau32_2fatjet",  &m_BDTvar2); // FIX TO REMOVE THE NAN errors, fixed at ntuple level.                                                                        
      m_reader_BDTG_even->AddVariable( "m_1fatjet_st",   &m_BDTvar3);
      m_reader_BDTG_even->AddVariable( "m_2fatjet_st",   &m_BDTvar4);
      m_reader_BDTG_even->AddVariable( "Split23_1fatjet", &m_BDTvar5);
      m_reader_BDTG_even->AddVariable( "Split23_2fatjet", &m_BDTvar6);
      m_reader_BDTG_even->AddVariable( "eT_miss_orig"   ,&m_BDTvar7);
      m_reader_BDTG_even->AddVariable( "MTbmin_orig"   ,&m_BDTvar8);
      m_reader_BDTG_even->AddVariable( "dRb1b2"   ,&m_BDTvar9);
      m_reader_BDTG_even->AddVariable("nj_good",&m_BDTvar10);
      m_reader_BDTG_even->AddVariable("pt_1fatjet_st", &m_BDTvar11);
      m_reader_BDTG_even->AddVariable("pt_2fatjet_st",&m_BDTvar12);
      m_reader_BDTG_even->AddVariable("MT2Chi2",&m_BDTvar13);
      m_reader_BDTG_even->AddVariable("NFatJetsSt",&m_BDTvar14);



     //Might want to incorporate some parts of the macro at some point?
     // m_reader->BookMVA( "BDTG method",(path+"/data/ExportTrees/weights/TMVAClassification_BDTG_Jet1.weights.xml").c_str());
     //m_reader->BookMVA( "BDT method",(path+"/data/ExportTrees/weights/TMVAClassification_BDT_EXTENDED.weights.xml").c_str());
   
      m_reader_odd->BookMVA( "BDT method",(path+"/data/ExportTrees/weights/TMVAClassification_BDT_ODD.weights.xml").c_str());
      m_reader_even->BookMVA( "BDT method",(path+"/data/ExportTrees/weights/TMVAClassification_BDT_EVEN.weights.xml").c_str());

      m_reader_BDTG_odd->BookMVA( "BDTG method",(path+"/data/ExportTrees/weights/TMVAClassification_BDTG_ODD.weights.xml").c_str());
      m_reader_BDTG_even->BookMVA( "BDTG method",(path+"/data/ExportTrees/weights/TMVAClassification_BDTG_EVEN.weights.xml").c_str());

   cout<<"************************ FINISHED CONFIGURATION**************************************"<<endl;

   }



   //-----------------------------------------------------------------
   const char* cmakePath = std::getenv("DATAPATH");
   std::string dataPath = "";
   if (nullptr != cmakePath){
     std::string tmp(cmakePath);
     dataPath = tmp.substr(0,tmp.find(":"));
   }
   else{
     const char* rootCorePath = std::getenv("ROOTCOREBIN");
     std::string tmp(rootCorePath);
     dataPath = tmp;
     dataPath += "/data";
   }
     
   TFile* fin = new TFile((dataPath+"/ExportTrees/2016-20_7-13TeV-MC15-CDI-2016-11-25_v1.root").c_str());


   

   fin->cd("MV2c10/AntiKt4EMTopoJets/FixedCutBEff_77/B/");
   Analysis::CalibrationDataHistogramContainer* calib= (Analysis::CalibrationDataHistogramContainer*)fin->Get("MV2c10/AntiKt4EMTopoJets/FixedCutBEff_77/B/default_Eff");
   Analysis::CalibrationDataHistogramContainer* calib2= (Analysis::CalibrationDataHistogramContainer*)fin->Get("MV2c10/AntiKt4EMTopoJets/FixedCutBEff_77/B/410021_CalibrationBinned_Eff");
   Analysis::CalibrationDataHistogramContainer* calib3= (Analysis::CalibrationDataHistogramContainer*)fin->Get("MV2c10/AntiKt4EMTopoJets/FixedCutBEff_77/B/410021_Eff");

   hcalib = (TH2D*)calib->GetValue("result");
   std::cout << hcalib << std::endl;
   hcalib2 = (TH2D*)calib2->GetValue("result");
   std::cout << hcalib2 << std::endl;
   hcalib3 = (TH2D*)calib3->GetValue("result");
   std::cout << hcalib3 << std::endl;
   
   TFile* fphoton = new TFile((dataPath+"/ExportTrees/Test_PhotonOut_attempt2.root").c_str());
   hphoton = (TH2D*)fphoton->Get("RatioPhotonZ");
   std::cout << hphoton << std::endl;
   TFile* fphotonAcpt = new TFile((dataPath+"/ExportTrees/photon_effA.root").c_str());
   hphotonAcpt = (TH2D*)fphotonAcpt->Get("hreco");
   std::cout <<  hphotonAcpt  << std::endl;


   //hJet = new TH2D("hJet","hJet",50,-1.2,1.2,50,-1.2,1.2);


   //test histograms for ghost associated tracks
   //htracks = new TH2D("htracks","htracks",100,-5,5,100,-5,5);
   //hjets = new TH2D("hljets","hljets",100,-5,5,100,-5,5);
   //hbjets = new TH2D("hbjets","hbjets",100,-5,5,100,-5,5);

   //htracks = new TH2D("htracks","htracks",100,0,500,100,-5,5);
   //hjets = new TH2D("hjets","hjets",100,0,500,100,-5,5);

     
   //load up the events and weighted events    
   TH1F *events = (TH1F*) f_in->Get(entries_name.c_str());
   TH1F *eventsweighted = (TH1F*) f_in->Get(counter_name.c_str());
   eventsweighted->SetName(counter_name_new.c_str());
   //if we are using Pythia8 as the ME generator (not PS) then we need to use the nevents for weighting
   if(isPythia)
     {
       std::cout << "ispythia so using unwgted nevents" << std::endl;
       eventsweighted = (TH1F*) f_in->Get(entries_name.c_str());
       eventsweighted->SetName(entries_name_new.c_str());
     }
   long long int nev = t_old->GetEntries();
   if(nev==0){
     std::cout << "nevents==0!!!!!!!" <<std::endl;
     continue;
   }
   
   cout << "_____________________________________" << endl << " Number of entries to process " << nev << endl << "_____________________________________" << endl;
   
   //_____________________________________
   //Let's start with getting the runnumber so we can find the xsec (if not data)
   int runnumber=-1;
   t_old->SetBranchAddress("RunNumber",&runnumber);
   t_old->GetEntry(0);
   stringstream ss;//create a stringstream
   ss << runnumber;//add number to the stream
   
   if (dataPath=="")
     {
       cout << "You need to set up RootCore first - ROOTCOREBIN not defined " << endl;
       exit(1);
     }
   //! should be ROOTCOREBIN! string dataPath!
   double xsec = getXsec(runnumber,(dataPath+"/ExportTrees/susy_crosssections_13TeV.txt").c_str());

   if (xsec==0)
     xsec=getXsec(runnumber,"/home/macdonald/SUSY_May_2016/RootCoreBin/data/SUSYTools/mc15_13TeV/MGPy8EG_N30LO_A14N23LO_DM_BBscalar_pX_cY.txt");

   if (xsec==0)
     xsec=getXsec(runnumber,(dataPath+"/ExportTrees/MGPy8EG_A14N_BB_direct.txt").c_str());
   if (xsec==0)
     xsec=getXsec(runnumber,(dataPath+"/ExportTrees/MGPy8EG_A14N23LO_BB_onestepN2hN1.txt").c_str());

   bool isData=false;
   if(xsec==0)
     {
       cout << "can't find xsecs " << runnumber << " " << in_file_name << endl;
       if(in_file_name.find("stopSMR")!=std::string::npos)smr=true;

       if(in_file_name.find(".data.")!=std::string::npos || in_file_name.find("physics_Main")!=std::string::npos)
	 {
	   cout << "is data!!!! " << endl;
	   isData=true;
	 }
       else if(smr){
	  cout << "is smr data!!!! " << endl;
	  isData=true;
       }
       else
	 {
	   std::cout << "\n\n\n" <<std::endl;
	   std::cout << "-------------------------- " << std::endl;
	   std::cout << "can't find anything" << std::endl;
	   isData=false;
	   xsec=0.;
	   std::cout << "-------------------------- " << std::endl;
	   //nev=0;
	 }
     }
   double unfilterednev = eventsweighted->GetBinContent(1);
   double neventsraw = events->GetBinContent(1);
   float lumi=0;
   double LumiWeight = 0;//, weight2;
   
   lumi = 1.0 / (xsec); 
   LumiWeight = 1.0/(lumi*unfilterednev);
   if(isData)
     LumiWeight=1;
     

   cout << "************************************************" << endl;
   cout << "runnumber= " << runnumber << endl;
   cout << "unfiltered events wgt= " << unfilterednev << endl;
   cout << "unfiltered events t= " << neventsraw << endl;
   cout << "Lumi weight= " << LumiWeight << endl;
   cout << "************************************************" << endl;    
   
   double fraclast=0;
   int nprocessed=0;
       
   std::cout << " events to process=" <<nev << " events in the inputntuple=" << inputntuple->fChain->GetEntries() << std::endl;
   
   if(k==0 && m_usexAODJets){
     std::cout << " Using CollectionTree!! " << std::endl;
     if(xAODevent->getEntries()!=inputntuple->fChain->GetEntries()){
       std::cout << "aborting because xAOD collection tree doesn't have the same nevents as the inputntuple " << std::endl;
       abort();
     }
     std::cout << " xAOD Collection Tree is good... using this to read in xAOD jets " << std::endl;
   }

   //______________________________________________________________________
   // Start looping over the events in the trees!!
   //______________________________________________________________________
   //
   for(long long int i=0;i<nev;i++){
     
     nprocessed++;


     if(m_maxevents>0 && i>m_maxevents)break;

     xAOD::TStore store;

     inputntuple->GetEntry(i);
     if(m_usexAODJets){
       xAODevent->getEntry(i);
     }
     
     outputntuple->clean();

     if(int(100*i/nev) > fraclast)
       {
	 cout << i << " / " << nev << " events processed ( " << 100*i/nev << " % ) "  << nev << " tree=" << k << " filled=" << outputntuple->GetNFilled() << endl;
	 fraclast=int(100*i/nev);
       }
     


     //prepare variables related to the truth level met
     double filtmet = 0;
     double filtht = 0;

     //optional to filter the MET200 slice so doesn't overlap with MET300

     if(&inputntuple->GenFiltMET!=nullptr)
       {
	 filtmet=inputntuple->GenFiltMET;
	 filtht=inputntuple->GenFiltHT;
       }
     
     //filter the ttbar/StWt 4100* sampels so we don't double count met tails
     bool isttbarMET200_OK=true;
     if((runnumber==410000 || runnumber==410013 || runnumber==410501 || runnumber==410014) && filtmet>200){
       isttbarMET200_OK=false;
     }
     outputntuple->isttbarMET200_OK = isttbarMET200_OK;
 
     bool isttbarMET300_OK=true;
     if(runnumber==407012 && filtmet>300) {
       isttbarMET300_OK=false;
     }     
     outputntuple->isttbarMET300_OK = isttbarMET300_OK;

     bool isttbarHT600_OK=true;
     if((runnumber==410000||runnumber==410501)  && filtht>600) {
       isttbarHT600_OK=false;
     }     
     outputntuple->isttbarHT600_OK = isttbarHT600_OK;

     bool isWtHT500_OK=true;
     if((runnumber==410013 || runnumber==410014) && filtht>500) {
       isWtHT500_OK=false;
     }     
     outputntuple->isWtHT500_OK = isWtHT500_OK;



     // event wide flags
     outputntuple->SCTerror=inputntuple->SCTerror;
     outputntuple->CoreFlag=inputntuple->CoreFlag;
     outputntuple->EventNumber=inputntuple->EventNumber;
     outputntuple->RunNumber=runnumber;//inputntuple->RunNumber;
     outputntuple->lbn=inputntuple->lbn;
     outputntuple->averageIntPerXing= inputntuple->averageIntPerXing;
     outputntuple->averageIntPerXingCorr= inputntuple->averageIntPerXingCorr;
	   
     outputntuple->treatAsYear = inputntuple->year;
     if(isData){
       if(runnumber>=296939)
	 outputntuple->treatAsYear=2016;
       else
	 outputntuple->treatAsYear=2015;
     }
     outputntuple->analysisflag = inputntuple->anaflag;
     outputntuple->isTruthAnalysis = inputntuple->anatruth;
          
     bool MEPhOverlapOK=true;     
     if ((runnumber==410000 || runnumber==407012 || runnumber==407322) && inputntuple->hasMEphoton==1) MEPhOverlapOK=false;
     else if (((runnumber>=410082 && runnumber<=410089) || runnumber==407320) && inputntuple->hasMEphoton==0) MEPhOverlapOK=false;     
     outputntuple->isMEPhOverlapOK = MEPhOverlapOK;



     //event weights, lumiweight, analysis weight etc.
     
     //Jets
     m_jets = 0;
     GetJets(m_jets);
     //resort jets to be sure -> leptons might have been added to be treated as a jet
     std::sort(m_jets->begin(),m_jets->end(),ptsorter);   
       

     //TrackJets
     m_trackjets = 0;
     GetTrackJets(m_trackjets);
     //resort trackjets to be sure -> leptons might have been added to be treated as a jet
     std::sort(m_trackjets->begin(),m_trackjets->end(),ptsorter);   
       


     //FatJets
     m_fatjets_kt8 = 0;
     GetFatJets( m_fatjets_kt8,"kt8");
     m_fatjets_kt12 = 0;
     GetFatJets( m_fatjets_kt12,"kt12");
     m_fatjets_st = 0;
     GetFatJets(m_fatjets_st,"st");


   
     if(m_usexAODJets){
       resetJets();
       m_jets = 0;
       GetxAODJets(m_jets);
       resetFatJets();
       m_fatjets_st = 0;
     }

   
     //Photons
     m_photons   =0;
     GetPhotons(m_photons);

     // Leptons
     m_electrons=0;
     GetElectrons( m_electrons);

     m_muons=0;
     GetMuons(m_muons);
     
     //Taus
     m_taus=0;
     GetTaus(m_taus);
 
     m_leptons = new Particles();
     for(const auto &mu: *m_muons){
       m_leptons->push_back(mu);
     }
     m_leptons->insert(m_leptons->end(),m_electrons->begin(),m_electrons->end());
     std::sort(m_leptons->begin(),m_leptons->end(),ptsorter);   
  
     double TriggerWeight=inputntuple->TriggerWeight2;
     FillEventWeights(LumiWeight,TriggerWeight);
     
     //make approximation of Z boson
     
     if(m_electrons->size()>=2 && m_muons->size()==0) 
       m_Z=new Particle( (*m_electrons->at(0)) + (*m_electrons->at(1)) ); 
     else if(m_muons->size()>=2 && m_electrons->size()==0) 
       m_Z=new Particle((*m_muons->at(0)) + (*m_muons->at(1)) ); 
     else if(m_leptons->size()==2)
       m_Z=new Particle((*m_leptons->at(0)) + (*m_leptons->at(1)) ); 
     else if(m_leptons->size()>2){
       std::vector<Particle> tmplist;
       for(unsigned int iii=0; iii<(m_leptons->size()); iii++){
	 for(unsigned int jjj=iii+1; jjj<(m_leptons->size()); jjj++){
	   Particle tmp1((*m_leptons->at(iii)));
	   Particle tmp2((*m_leptons->at(jjj)));
           if(tmp1.flav == tmp2.flav && tmp1.charge!=tmp2.charge && (tmp1+tmp2).M()<101 && (tmp1+tmp2).M()>81){
	     tmplist.push_back(tmp1+tmp2);
	   }
	 }
       }
       if(tmplist.empty()){
	 m_Z=new Particle((*m_leptons->at(0)) + (*m_leptons->at(1)) );
       }
       else{
	 unsigned int myindex = 0;
	 for(unsigned int ii=1; ii<tmplist.size(); ii++)
	   {
	     float mass1 = tmplist[ii].M()-91.1876;
	     float mass2 = tmplist[myindex].M()-91.1876;
	     if(mass1 < mass2)
	       myindex = ii;
	   }
	 m_Z=new Particle(tmplist[myindex]);
       }
     }
     else
       m_Z = new Particle(0,0,0,0);
    
     //make temporary photon
     
     m_photon = new Particle(0,0,0,0);
     if(m_photons->size()>=1){
       if((*m_photons->at(0)).Pt()>100){
	 delete m_photon;
	 m_photon=new Particle((*m_photons->at(0)));
       }
     }
     //Fill Vboson 
     if(m_photon->Pt()>0){
       //outputntuple->pT_V=m_photon->Pt();
       outputntuple->pT_B=m_photon->Pt();
       outputntuple->phi_V=m_photon->Phi();
       outputntuple->eta_V=m_photon->Eta();
       outputntuple->E_V=m_photon->Et();
     }
     else{
       //outputntuple->pT_B=m_photon->Pt();
       //outputntuple->pT_V=inputntuple->VbosonPt;
       //outputntuple->phi_V=inputntuple->VbosonPhi;
       //outputntuple->eta_V=inputntuple->VbosonEta;
       //outputntuple->E_V=inputntuple->VbosonE;
     }
     m_ljets = new Jets();
     m_bjets = new Jets();
     m_bjets60 = new Jets();
     m_bjets85 = new Jets();
     
     for(const auto &jet: *m_jets){
       if(jet->flav==5 && fabs(jet->Eta())<2.5){m_bjets->push_back(jet);} // 77% EMTopo
       else{m_ljets->push_back(jet);} 
       if(jet->MV2>0.934906){m_bjets60->push_back(jet);} // 60% EMTopo
       if(jet->MV2>0.1758475){m_bjets85->push_back(jet);} // 85% EMTopo
     }
    

     m_trackljets = new Jets(); // nothing at the moment
     m_trackbjets60 = new Jets();
     m_trackbjets70 = new Jets();
     m_trackbjets77 = new Jets();
     m_trackbjets85 = new Jets();
         
     for(const auto &jet: *m_trackjets){
       if(jet->MV2>0.9025 ){m_trackbjets60->push_back(jet);}
       if(jet->MV2>0.7475 ){m_trackbjets70->push_back(jet);}
       if(jet->MV2>0.5140 ){m_trackbjets77->push_back(jet);}
       if(jet->MV2>-0.0168 ){m_trackbjets85->push_back(jet);}
     }
    

     


     //   Triggers           
     FillTriggers(isData);

     //Fill Vboson 
     if(m_photon->Pt()>0){
       //outputntuple->pT_V=m_photon->Pt();
       outputntuple->pT_B=m_photon->Pt();
       outputntuple->phi_V=m_photon->Phi();
       outputntuple->eta_V=m_photon->Eta();
       outputntuple->E_V=m_photon->Et();
     }
     else{
       //outputntuple->pT_B=m_photon->Pt();
       //outputntuple->pT_V=inputntuple->VbosonPt;
       //outputntuple->phi_V=inputntuple->VbosonPhi;
       //outputntuple->eta_V=inputntuple->VbosonEta;
       //outputntuple->E_V=inputntuple->VbosonE;
     }

     //_____________________________________________________________________________________
     // Met
     m_met = 0;
     GetMet(m_met);
     m_met_fake =0;
     GetFakeMet(m_met_fake);
     
     m_met_track = 0;
     GetTrackMet(m_met_track);
     m_met_NonInt = 0;
     GetNonIntMet(m_met_NonInt);
     m_met_simple=0;
     GetSimpleMet(m_met_simple);

     //_____________________________________________________________________________________
       
     //if not jetsmearing fill as normal....
     
     if(!smr){
       FillVariables();
     }
     else{ // for jetsmearing....
       FillSmrVariables();
     }
     reset();
    
     store.clear();

   }
   std::cout << "outputntuple size=" << outputntuple->GetEntries() << std::endl;
   outputntuple->write();
   //delete outputntuple;
   //delete inputntuple;
 }
 std::cout << "file status " << f_in->IsOpen() << std::endl;
 f_in->Close();
  
}


void ExportTrees::FillSmrVariables(){

  double HT=0;
 
  for(const auto &jet: *m_jets){
    HT+=jet->Pt();
  }
  
  double avPt = HT/m_jets->size();
  double met_avPt_seed = (*m_met).Mod()/avPt;
  outputntuple->nbjets_seed=m_bjets->size();
  outputntuple->njets_seed=m_jets->size();
  outputntuple->met_avPt_seed=met_avPt_seed;
  outputntuple->eT_miss_seed=(*m_met).Mod();
  outputntuple->sumet_seed=inputntuple->sumet/1000.;

  if(m_jets->size()>0)
    outputntuple->pT_1jet_seed=m_jets->at(0)->Pt();
  else
    outputntuple->pT_1jet_seed=0;
  if(m_jets->size()>1)
    outputntuple->pT_2jet_seed=m_jets->at(1)->Pt();
  else
    outputntuple->pT_2jet_seed=0;         
     
  
  /*
  Jets* m_jets_seed = new Jets();
  for(unsigned int i=0; i<m_jets->size();i++){
    Jet* theJet = new Jet(m_jets->at(i)->Px(), m_jets->at(i)->Py(), m_jets->at(i)->Pz(), m_jets->at(i)->E());
    m_jets_seed->push_back(theJet);
  }
  */
 

 //seed selection
  bool failSeedSelection =  ((outputntuple->eT_miss_seed - 8) / sqrt(outputntuple->sumet_seed)) > (0.3 + 0.1*m_bjets->size());
  if(failSeedSelection) return;
  
  unsigned int tracker=0;
  //unsigned int trackerkt10=0;
  //unsigned int trackerkt12=0;

  resetJets();
  m_jets = new Jets();
  m_ljets = new Jets();
  m_bjets = new Jets();
  m_bjets60 = new Jets();
  m_bjets85 = new Jets();
    
  resetKt8Jets();
  m_fatjets_kt8 = new Jets();
  
  resetKt12Jets();

  m_fatjets_kt12 = new Jets();
    



  for(unsigned int ii=0;ii<inputntuple->smrMET_pt->size();ii++){
    double met_pt = (*inputntuple->smrMET_pt)[ii];
    double met_phi = (*inputntuple->smrMET_phi)[ii];
    //double sumet = (*inputntuple->smrsumet)[ii];
    TVector2 etmiss2D;
    etmiss2D.SetMagPhi(met_pt,met_phi);
        

    if(m_met)
      delete m_met;
    m_met=new TVector2(etmiss2D);


    if(m_met_fake)
      delete m_met_fake;
    m_met_fake=new TVector2(etmiss2D);
    
 

    int nJets=0;
    //reset the  jets
    
    
    for(unsigned int jj=tracker; jj< inputntuple->smrjet_pt->size();jj++)
      {
	int smr_index = (*inputntuple->smrjet_index)[jj];
	if(smr_index==0 && jj!=tracker){
	  tracker=jj;
	  break;
	}
	
	double pt = (*inputntuple->smrjet_pt)[jj]/1000.;
	double eta = (*inputntuple->smrjet_eta)[jj];
	double phi = (*inputntuple->smrjet_phi)[jj];
 	double e = (*inputntuple->smrjet_e)[jj]/1000.;
	//int flav=(*inputntuple->smrjet_flav)[jj];
	double MV2=(*inputntuple->smrjet_MV2)[jj];
	//double R = (*inputntuple->smrjet_R)[jj];
	Jet* tmpJet = new Jet(pt,eta,phi,e);
	tmpJet->MV2=MV2;
	//tmpJet->flav=flav;
	//tmpJet->R=R;
	
	if(tmpJet->Pt()<20){
	 delete tmpJet;
	 continue;
	}
	
	m_jets->push_back(tmpJet );
	
	nJets++;
      }
    
    std::sort(m_jets->begin(),m_jets->end(),ptsorter);   
    for(const auto &jet: *m_jets){
      
      if(jet->MV2>0.6459){
	jet->flav=5;
	m_bjets->push_back(jet);
      }
      else{
	jet->flav=0;
	m_ljets->push_back(jet);
      }
      if(jet->MV2>0.934906){m_bjets60->push_back(jet);}
      if(jet->MV2>0.1758475){m_bjets85->push_back(jet);}
    }
    
    
    FillVariables();
    resetJets();
    m_jets = new Jets();
    m_ljets = new Jets();
    m_bjets = new Jets();
    m_bjets60 = new Jets();
    m_bjets85 = new Jets();
    
    resetKt8Jets();
    m_fatjets_kt8 = new Jets();
    
    resetKt12Jets();
    m_fatjets_kt12 = new Jets();
    
    //}//loop over smeared events
  }
  

}


void ExportTrees::FillVariables(){
  
  FillMETVariables();
  FillJetVariables();
  FillFatJetVariables();

  if (MVAFlag)
    {
      FillMVAVariables();
    }

  FillLeptonVariables();
  FillZVariables();
  FillTauVariables();
  FillTrackJetVariables();
  if(passFilter()){

    //_____________________________________________________________________________________
    // Do JET+MET variables, e.g mctcorr2b, meff, MTbmin
    FillJetMETVariables();

    //_____________________________________________________________________________________
    //_____________________________________________________________________________________
    // Do Lepton+MET variables, e.g. amt2
    //
    FillLeptonMETVariables();
    //_____________________________________________________________________________________
    
    //_____________________________________________________________________________________
    //Fill more complicated variables
    //
    FillComplexVariables();

    //_____________________________________________________________________________________
    //fill SRC razor variables?
    FillCompressedAnalysis();

    //_____________________________________________________________________________________
    //fill all channels e.g. btag1echannel
    //

    FillChannels();
    //_____________________________________________________________________________________
    

    //_____________________________________________________________________________________
    //dump the output to the ntuple
    //if(passComplexFilter())
    outputntuple->dump();
    //_____________________________________________________________________________________

  }
  
  //_____________________________________________________________________________________
  // CLEANUP MEMORY... THIS COULD BE A LOT BETTER, unique_ptr?? 
  //_____________________________________________________________________________________
  
}



