#include <ExportTrees/Export.h>
#include "CPAnalysisExamples/errorcheck.h"
#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"




bool ExportTrees :: GetJetToolRunner(JetToolRunner *& tool, double jetradius, std::string inputcontainer, std::string outputcontainer)
{
  const char* APP_NAME = "ExportTrees :: GetJetToolRunner";
  
  Info( APP_NAME,"Setting up JetReclusteringTool for %s",outputcontainer.c_str());

  tool = 0;

  ToolHandleArray<IPseudoJetGetter> hgets;
  ToolHandleArray<IJetExecuteTool> hrecs;

  PseudoJetGetter* plcget = new PseudoJetGetter(("mylcget"+outputcontainer).c_str());
  CHECK(plcget->setProperty("InputContainer", inputcontainer));
  CHECK(plcget->setProperty("OutputContainer", "Reclustered"+outputcontainer));
  CHECK(plcget->setProperty("Label", "Tower"));
  CHECK(plcget->setProperty("SkipNegativeEnergy", true));
  CHECK(plcget->setProperty("GhostScale", 0.0));
  Info( APP_NAME,"finished properties PseudoJetGetter %s",outputcontainer.c_str());
  //CHECK(asg::ToolStore::put(plcget));
  ToolHandle<IPseudoJetGetter> hlcget(plcget);
  hgets.push_back(hlcget);
  //if ( asg::ToolStore::get("lcget") == 0 ) {
  //  Error( APP_NAME,"Failed to retrieved lcget");
  //  return false;
  // }

  Info( APP_NAME,"Setup PseudoJetGetter");

  JetFromPseudojet* pbuild = new JetFromPseudojet(("myjetbuild"+outputcontainer).c_str());
  ToolHandle<IJetFromPseudojet> hbuild(pbuild);
  /*  NameList jetbuildatts;
  jetbuildatts.push_back("ActiveArea");
  jetbuildatts.push_back("ActiveAreaFourVector");
  pbuild->setProperty("Attributes", jetbuildatts);*/
  
  //asg::ToolStore::put(pbuild);
  CHECK(pbuild->initialize());
  
  JetFinder* pfind = new JetFinder("myjetfind");
  CHECK(pfind->setProperty("JetAlgorithm", "AntiKt"));
  CHECK(pfind->setProperty("JetRadius", jetradius));
  CHECK(pfind->setProperty("PtMin", 15000.0));
  CHECK(pfind->setProperty("GhostArea", 0.00));
  CHECK(pfind->setProperty("RandomOption", 1));
  CHECK(pfind->setProperty("JetBuilder", hbuild));
  ToolHandle<IJetFinder> hfind(pfind);
  //asg::ToolStore::put(pfind);
  CHECK(pfind->initialize());
  
  Info( APP_NAME,"Setup JetFinder");

  JetRecTool* pjrf = new JetRecTool("myjrfind");
  CHECK(pjrf->setProperty("OutputContainer", outputcontainer));
  CHECK(pjrf->setProperty("PseudoJetGetters", hgets));
  CHECK(pjrf->setProperty("JetFinder", hfind));
  CHECK(pjrf->initialize());
  ToolHandle<IJetExecuteTool> hjrf(pjrf);
  hrecs.push_back(pjrf);

 
  tool = new JetToolRunner("jetrunner");
  CHECK(tool->setProperty("Tools", hrecs));
  Info( APP_NAME,"Initialising JetReclusteringTool(s)");
  CHECK(tool->initialize());
  tool->print();
  Info( APP_NAME,"done.....");

  
  return true;
} 
