#include "ExportTrees/Export.h"
using namespace std;


double ExportTrees::GetTriggerWeight(string trigger, int runnumber){

   
  double trigweight =0;
  //------------------------------------------------------------
  //Prescales for trigger HLT_g35_loose_L1EM15 
  //------------------------------------------------------------
  if(trigger=="HLT_g35_loose_L1EM15")
    {
      if (runnumber == 276262) trigweight = 80.997 ; // for D1 
      else if (runnumber == 276329) trigweight = 257.992 ; // for D1 
      else if (runnumber == 276336) trigweight = 240.900 ; // for D1 
      else if (runnumber == 276416) trigweight = 240.900 ; // for D1 
      else if (runnumber == 276511) trigweight = 307.855 ; // for D1 
      else if (runnumber == 276689) trigweight = 399.301 ; // for D1 
      else if (runnumber == 276778) trigweight = 251.966 ; // for D1 
      else if (runnumber == 276790) trigweight = 190.259 ; // for D1 
      else if (runnumber == 276952) trigweight = 239.607 ; // for D1 
      else if (runnumber == 276954) trigweight = 198.854 ; // for D1 
      else if (runnumber == 278880) trigweight = 194.474 ; // for D1 
      else if (runnumber == 278912) trigweight = 223.232 ; // for D1 
      else if (runnumber == 278968) trigweight = 336.627 ; // for D1 
      else if (runnumber == 279169) trigweight = 231.349 ; // for D1 
      else if (runnumber == 279259) trigweight = 358.544 ; // for D1 
      else if (runnumber == 279279) trigweight = 362.072 ; // for D1 
      else if (runnumber == 279284) trigweight = 291.625 ; // for  
      else if (runnumber == 279345) trigweight = 367.350 ; // for  
      else if (runnumber == 279515) trigweight = 982.417 ; // for  
      else if (runnumber == 279598) trigweight = 451.061 ; // for  
      else if (runnumber == 279685) trigweight = 413.040 ; // for  
      else if (runnumber == 279764) trigweight = 522.125 ; // for  
      else if (runnumber == 279813) trigweight = 377.450 ; // for  
      else if (runnumber == 279867) trigweight = 553.207 ; // for  
      else if (runnumber == 279928) trigweight = 982.413 ; // for  
      else if (runnumber == 279932) trigweight = 475.631 ; // for  
      else if (runnumber == 279984) trigweight = 556.671 ; // for  
      else if (runnumber == 280231) trigweight = 537.670 ; // for  
      else if (runnumber == 280319) trigweight = 314.261 ; // for  
      else if (runnumber == 280368) trigweight = 416.274 ; // for  
      else if (runnumber == 280423) trigweight = 340.684 ; // for  
      else if (runnumber == 280464) trigweight = 372.312 ; // for  
      else if (runnumber == 280500) trigweight = 272.000 ; // for  
      else if (runnumber == 280520) trigweight = 270.583 ; // for  
      else if (runnumber == 280614) trigweight = 353.042 ; // for  
      else if (runnumber == 280673) trigweight = 320.699 ; // for  
      else if (runnumber == 280753) trigweight = 395.209 ; // for  
      else if (runnumber == 280853) trigweight = 338.872 ; // for  
      else if (runnumber == 280862) trigweight = 317.963 ; // for  
      else if (runnumber == 280950) trigweight = 306.509 ; // for  
      else if (runnumber == 280977) trigweight = 390.987 ; // for  
      else if (runnumber == 281070) trigweight = 457.924 ; // for  
      else if (runnumber == 281074) trigweight = 389.517 ; // for  
      else if (runnumber == 281075) trigweight = 544.002 ; // for  
      else if (runnumber == 281317) trigweight = 492.348 ; // for  
      else if (runnumber == 281385) trigweight = 360.485 ; // for  
      else if (runnumber == 281411) trigweight = 502.307 ; // for  
      else if (runnumber == 282625) trigweight = 172.896 ; // for  
      else if (runnumber == 282631) trigweight = 380.986 ; // for  
      else if (runnumber == 282712) trigweight = 444.524 ; // for  
      else if (runnumber == 282784) trigweight = 380.800 ; // for  
      else if (runnumber == 282992) trigweight = 405.311 ; // for  
      else if (runnumber == 283074) trigweight = 447.804 ; // for  
      else if (runnumber == 283155) trigweight = 459.403 ; // for  
      else if (runnumber == 283270) trigweight = 523.167 ; // for  
      else if (runnumber == 283429) trigweight = 408.966 ; // for  
      else if (runnumber == 283608) trigweight = 326.401 ; // for  
      else if (runnumber == 283780) trigweight = 411.821 ; // for  
      else if (runnumber == 284006) trigweight = 540.362 ; // for  
      else if (runnumber == 284154) trigweight = 390.828 ; // for  
      else if (runnumber == 284213) trigweight = 438.965 ; // for  
      else if (runnumber == 284285) trigweight = 471.237 ; // for  
      else if (runnumber == 284420) trigweight = 554.157 ; // for  
      else if (runnumber == 284427) trigweight = 553.551 ; // for  
      else if (runnumber == 284484) trigweight = 499.345 ; // for  
      else std::cout << "Warning No runnumber found for this trigger! Prescale set to 1" << std::endl; 

    }

  //------------------------------------------------------------
  //Prescales for trigger HLT_g40_loose_L1EM15 
  //------------------------------------------------------------
  if(trigger=="HLT_g40_loose_L1EM15"){
    if (runnumber == 276262) trigweight = 53.991 ; // for D1 
    else if (runnumber == 276329) trigweight = 164.177 ; // for D1 
    else if (runnumber == 276336) trigweight = 153.300 ; // for D1 
    else if (runnumber == 276416) trigweight = 153.300 ; // for D1 
    else if (runnumber == 276511) trigweight = 201.987 ; // for D1 
    else if (runnumber == 276689) trigweight = 266.201 ; // for D1 
    else if (runnumber == 276778) trigweight = 160.342 ; // for D1 
    else if (runnumber == 276790) trigweight = 123.633 ; // for D1 
    else if (runnumber == 276952) trigweight = 157.209 ; // for D1 
    else if (runnumber == 276954) trigweight = 129.393 ; // for D1 
    else if (runnumber == 278880) trigweight = 127.624 ; // for D1 
    else if (runnumber == 278912) trigweight = 146.496 ; // for D1 
    else if (runnumber == 278968) trigweight = 220.911 ; // for D1 
    else if (runnumber == 279169) trigweight = 151.098 ; // for D1 
    else if (runnumber == 279259) trigweight = 235.294 ; // for D1 
    else if (runnumber == 279279) trigweight = 237.609 ; // for D1 
    else if (runnumber == 279284) trigweight = 191.378 ; // for  
    else if (runnumber == 279345) trigweight = 241.073 ; // for  
    else if (runnumber == 279515) trigweight = 644.711 ; // for  
    else if (runnumber == 279598) trigweight = 296.009 ; // for  
    else if (runnumber == 279685) trigweight = 271.057 ; // for  
    else if (runnumber == 279764) trigweight = 342.646 ; // for  
    else if (runnumber == 279813) trigweight = 247.701 ; // for  
    else if (runnumber == 279867) trigweight = 363.042 ; // for  
    else if (runnumber == 279928) trigweight = 644.707 ; // for  
    else if (runnumber == 279932) trigweight = 312.133 ; // for  
    else if (runnumber == 279984) trigweight = 365.314 ; // for  
    else if (runnumber == 280231) trigweight = 352.847 ; // for  
    else if (runnumber == 280319) trigweight = 206.233 ; // for  
    else if (runnumber == 280368) trigweight = 273.180 ; // for  
    else if (runnumber == 280423) trigweight = 223.573 ; // for  
    else if (runnumber == 280464) trigweight = 244.329 ; // for  
    else if (runnumber == 280500) trigweight = 178.500 ; // for  
    else if (runnumber == 280520) trigweight = 177.570 ; // for  
    else if (runnumber == 280614) trigweight = 231.684 ; // for  
    else if (runnumber == 280673) trigweight = 210.459 ; // for  
    else if (runnumber == 280753) trigweight = 259.356 ; // for  
    else if (runnumber == 280853) trigweight = 222.385 ; // for  
    else if (runnumber == 280862) trigweight = 208.663 ; // for  
    else if (runnumber == 280950) trigweight = 201.146 ; // for  
    else if (runnumber == 280977) trigweight = 256.586 ; // for  
    else if (runnumber == 281070) trigweight = 300.513 ; // for  
    else if (runnumber == 281074) trigweight = 255.621 ; // for  
    else if (runnumber == 281075) trigweight = 357.001 ; // for  
    else if (runnumber == 281317) trigweight = 323.103 ; // for  
    else if (runnumber == 281385) trigweight = 236.568 ; // for  
    else if (runnumber == 281411) trigweight = 329.639 ; // for  
    else if (runnumber == 282625) trigweight = 113.463 ; // for  
    else if (runnumber == 282631) trigweight = 250.021 ; // for  
    else if (runnumber == 282712) trigweight = 291.719 ; // for  
    else if (runnumber == 282784) trigweight = 249.900 ; // for  
    else if (runnumber == 282992) trigweight = 265.985 ; // for  
    else if (runnumber == 283074) trigweight = 293.870 ; // for  
    else if (runnumber == 283155) trigweight = 301.484 ; // for  
    else if (runnumber == 283270) trigweight = 341.959 ; // for  
    else if (runnumber == 283429) trigweight = 268.384 ; // for  
    else if (runnumber == 283608) trigweight = 214.201 ; // for  
    else if (runnumber == 283780) trigweight = 270.257 ; // for  
    else if (runnumber == 284006) trigweight = 354.612 ; // for  
    else if (runnumber == 284154) trigweight = 256.481 ; // for  
    else if (runnumber == 284213) trigweight = 288.071 ; // for  
    else if (runnumber == 284285) trigweight = 309.248 ; // for  
    else if (runnumber == 284420) trigweight = 363.666 ; // for  
    else if (runnumber == 284427) trigweight = 363.268 ; // for  
    else if (runnumber == 284484) trigweight = 327.695 ; // for  
    else std::cout << "Warning No runnumber found for this trigger! Prescale set to 1" << std::endl; 
  }

  //------------------------------------------------------------
  //Prescales for trigger HLT_g45_loose_L1EM15 
  //------------------------------------------------------------
  if(trigger=="HLT_g45_loose_L1EM15"){
    if (runnumber == 276262) trigweight = 34.362 ; // for D1 
    else if (runnumber == 276329) trigweight = 109.451 ; // for D1 
    else if (runnumber == 276336) trigweight = 102.200 ; // for D1 
    else if (runnumber == 276416) trigweight = 102.200 ; // for D1 
    else if (runnumber == 276511) trigweight = 129.691 ; // for D1 
    else if (runnumber == 276689) trigweight = 169.401 ; // for D1 
    else if (runnumber == 276778) trigweight = 106.895 ; // for D1 
    else if (runnumber == 276790) trigweight = 78.676 ; // for D1 
    else if (runnumber == 276952) trigweight = 100.096 ; // for D1 
    else if (runnumber == 276954) trigweight = 82.341 ; // for D1 
    else if (runnumber == 278880) trigweight = 85.082 ; // for D1 
    else if (runnumber == 278912) trigweight = 97.664 ; // for D1 
    else if (runnumber == 278968) trigweight = 147.274 ; // for D1 
    else if (runnumber == 279169) trigweight = 100.732 ; // for D1 
    else if (runnumber == 279259) trigweight = 156.863 ; // for D1 
    else if (runnumber == 279279) trigweight = 158.407 ; // for D1 
    else if (runnumber == 279284) trigweight = 127.585 ; // for  
    else if (runnumber == 279345) trigweight = 160.715 ; // for  
    else if (runnumber == 279515) trigweight = 429.807 ; // for  
    else if (runnumber == 279598) trigweight = 197.340 ; // for  
    else if (runnumber == 279685) trigweight = 180.705 ; // for  
    else if (runnumber == 279764) trigweight = 228.430 ; // for  
    else if (runnumber == 279813) trigweight = 165.135 ; // for  
    else if (runnumber == 279867) trigweight = 242.029 ; // for  
    else if (runnumber == 279928) trigweight = 429.806 ; // for  
    else if (runnumber == 279932) trigweight = 208.089 ; // for  
    else if (runnumber == 279984) trigweight = 243.542 ; // for  
    else if (runnumber == 280231) trigweight = 235.231 ; // for  
    else if (runnumber == 280319) trigweight = 137.489 ; // for  
    else if (runnumber == 280368) trigweight = 182.120 ; // for  
    else if (runnumber == 280423) trigweight = 149.049 ; // for  
    else if (runnumber == 280464) trigweight = 162.886 ; // for  
    else if (runnumber == 280500) trigweight = 119.000 ; // for  
    else if (runnumber == 280520) trigweight = 118.380 ; // for  
    else if (runnumber == 280614) trigweight = 154.455 ; // for  
    else if (runnumber == 280673) trigweight = 140.305 ; // for  
    else if (runnumber == 280753) trigweight = 172.904 ; // for  
    else if (runnumber == 280853) trigweight = 148.257 ; // for  
    else if (runnumber == 280862) trigweight = 139.109 ; // for  
    else if (runnumber == 280950) trigweight = 134.097 ; // for  
    else if (runnumber == 280977) trigweight = 171.057 ; // for  
    else if (runnumber == 281070) trigweight = 200.342 ; // for  
    else if (runnumber == 281074) trigweight = 170.414 ; // for  
    else if (runnumber == 281075) trigweight = 238.001 ; // for  
    else if (runnumber == 281317) trigweight = 215.402 ; // for  
    else if (runnumber == 281385) trigweight = 157.712 ; // for  
    else if (runnumber == 281411) trigweight = 219.759 ; // for  
    else if (runnumber == 282625) trigweight = 75.642 ; // for  
    else if (runnumber == 282631) trigweight = 166.681 ; // for  
    else if (runnumber == 282712) trigweight = 194.479 ; // for  
    else if (runnumber == 282784) trigweight = 166.600 ; // for  
    else if (runnumber == 282992) trigweight = 177.323 ; // for  
    else if (runnumber == 283074) trigweight = 195.914 ; // for  
    else if (runnumber == 283155) trigweight = 200.989 ; // for  
    else if (runnumber == 283270) trigweight = 227.972 ; // for  
    else if (runnumber == 283429) trigweight = 178.923 ; // for  
    else if (runnumber == 283608) trigweight = 142.800 ; // for  
    else if (runnumber == 283780) trigweight = 180.171 ; // for  
    else if (runnumber == 284006) trigweight = 236.408 ; // for  
    else if (runnumber == 284154) trigweight = 170.988 ; // for  
    else if (runnumber == 284213) trigweight = 192.048 ; // for  
    else if (runnumber == 284285) trigweight = 206.166 ; // for  
    else if (runnumber == 284420) trigweight = 242.444 ; // for  
    else if (runnumber == 284427) trigweight = 242.178 ; // for  
    else if (runnumber == 284484) trigweight = 218.464 ; // for  
    else std::cout << "Warning No runnumber found for this trigger! Prescale set to 1" << std::endl; 
  }
  //------------------------------------------------------------
  //Prescales for trigger HLT_g50_loose_L1EM15 
  //------------------------------------------------------------
  if(trigger=="HLT_g50_loose_L1EM15"){
    if (runnumber == 276262) trigweight = 24.545 ; // for D1 
    else if (runnumber == 276329) trigweight = 78.179 ; // for D1 
    else if (runnumber == 276336) trigweight = 73.000 ; // for D1 
    else if (runnumber == 276416) trigweight = 73.000 ; // for D1 
    else if (runnumber == 276511) trigweight = 92.636 ; // for D1 
    else if (runnumber == 276689) trigweight = 121.000 ; // for D1 
    else if (runnumber == 276778) trigweight = 76.353 ; // for D1 
    else if (runnumber == 276790) trigweight = 56.197 ; // for D1 
    else if (runnumber == 276952) trigweight = 71.497 ; // for D1 
    else if (runnumber == 276954) trigweight = 58.815 ; // for D1 
    else if (runnumber == 278880) trigweight = 60.773 ; // for D1 
    else if (runnumber == 278912) trigweight = 69.760 ; // for D1 
    else if (runnumber == 278968) trigweight = 105.196 ; // for D1 
    else if (runnumber == 279169) trigweight = 71.951 ; // for D1 
    else if (runnumber == 279259) trigweight = 112.045 ; // for D1 
    else if (runnumber == 279279) trigweight = 113.147 ; // for D1 
    else if (runnumber == 279284) trigweight = 91.132 ; // for  
    else if (runnumber == 279345) trigweight = 114.797 ; // for  
    else if (runnumber == 279515) trigweight = 307.005 ; // for  
    else if (runnumber == 279598) trigweight = 140.957 ; // for  
    else if (runnumber == 279685) trigweight = 129.075 ; // for  
    else if (runnumber == 279764) trigweight = 163.165 ; // for  
    else if (runnumber == 279813) trigweight = 117.953 ; // for  
    else if (runnumber == 279867) trigweight = 172.877 ; // for  
    else if (runnumber == 279928) trigweight = 307.004 ; // for  
    else if (runnumber == 279932) trigweight = 148.634 ; // for  
    else if (runnumber == 279984) trigweight = 173.959 ; // for  
    else if (runnumber == 280231) trigweight = 168.022 ; // for  
    else if (runnumber == 280319) trigweight = 98.207 ; // for  
    else if (runnumber == 280368) trigweight = 130.086 ; // for  
    else if (runnumber == 280423) trigweight = 106.464 ; // for  
    else if (runnumber == 280464) trigweight = 116.347 ; // for  
    else if (runnumber == 280500) trigweight = 85.000 ; // for  
    else if (runnumber == 280520) trigweight = 84.557 ; // for  
    else if (runnumber == 280614) trigweight = 110.326 ; // for  
    else if (runnumber == 280673) trigweight = 100.218 ; // for  
    else if (runnumber == 280753) trigweight = 123.503 ; // for  
    else if (runnumber == 280853) trigweight = 105.898 ; // for  
    else if (runnumber == 280862) trigweight = 99.363 ; // for  
    else if (runnumber == 280950) trigweight = 95.784 ; // for  
    else if (runnumber == 280977) trigweight = 122.183 ; // for  
    else if (runnumber == 281070) trigweight = 143.101 ; // for  
    else if (runnumber == 281074) trigweight = 121.724 ; // for  
    else if (runnumber == 281075) trigweight = 170.001 ; // for  
    else if (runnumber == 281317) trigweight = 153.859 ; // for  
    else if (runnumber == 281385) trigweight = 112.652 ; // for  
    else if (runnumber == 281411) trigweight = 156.971 ; // for  
    else if (runnumber == 282625) trigweight = 54.030 ; // for  
    else if (runnumber == 282631) trigweight = 119.058 ; // for  
    else if (runnumber == 282712) trigweight = 138.914 ; // for  
    else if (runnumber == 282784) trigweight = 119.000 ; // for  
    else if (runnumber == 282992) trigweight = 126.660 ; // for  
    else if (runnumber == 283074) trigweight = 139.938 ; // for  
    else if (runnumber == 283155) trigweight = 143.564 ; // for  
    else if (runnumber == 283270) trigweight = 162.838 ; // for  
    else if (runnumber == 283429) trigweight = 127.802 ; // for  
    else if (runnumber == 283608) trigweight = 102.001 ; // for  
    else if (runnumber == 283780) trigweight = 128.693 ; // for  
    else if (runnumber == 284006) trigweight = 168.863 ; // for  
    else if (runnumber == 284154) trigweight = 122.134 ; // for  
    else if (runnumber == 284213) trigweight = 137.177 ; // for  
    else if (runnumber == 284285) trigweight = 147.261 ; // for  
    else if (runnumber == 284420) trigweight = 173.174 ; // for  
    else if (runnumber == 284427) trigweight = 172.985 ; // for  
    else if (runnumber == 284484) trigweight = 156.046 ; // for  
    else std::cout << "Warning No runnumber found for this trigger! Prescale set to 1" << std::endl; 
    //------------------------------------------------------------
  }

  //std::cout << trigweight << std::endl;
  return trigweight;

}
