#include "ExportTrees/JetSubStructure.h"
#include <JetSubStructureUtils/Pull.h>


                   
Particles*  JSS::GetJetConstituents(const xAOD::Jet *jet){
  Particles* caloCells = new Particles();
  //retrieve the 
  xAOD::JetConstituentVector constituents = jet->getConstituents();
  if (constituents.isValid()){
    for( auto c : constituents ) {
      const xAOD::IParticle* rawObj = c->rawConstituent();
      //assume the jet is EMTopo, needs a fix for PFlow (PFO)
      if( rawObj->type() == xAOD::Type::CaloCluster ) {
	const xAOD::CaloCluster* cluster = static_cast< const xAOD::CaloCluster* >( rawObj );
	
	Particle* particle = new Particle(cluster->p4()*0.001);
	//particle->charge=cluster->charge();
	caloCells->push_back(particle);
      }
    }
  }

  return caloCells;

}

Particles* JSS::GetJetGhostTracks(const xAOD::Jet *jet){
  Particles* tracks = new Particles();
  std::vector<const xAOD::IParticle*> jtracks;
  jet->getAssociatedObjects<xAOD::IParticle>( xAOD::JetAttribute::GhostTrack,jtracks);

  for(size_t iConst=0; iConst<jtracks.size(); ++iConst) {
    const xAOD::TrackParticle* pTrk = static_cast<const xAOD::TrackParticle*>(jtracks[iConst]);
    if(!pTrk)
      continue;
    
    Particle* particle = new Particle(pTrk->p4()*0.001);
    particle->charge=pTrk->charge();
    tracks->push_back(particle);
  }
  
  return tracks;
}


TVector2 JSS::CalcJetPull(Particles* cons, Particle* ref){

  TVector2 r_pull(0,0);
  double jeta = ref->Rapidity();

  for(const auto &con: *cons){
    double dphi = TVector2::Phi_mpi_pi(ref->Phi() - con->Phi()); 
    double ceta = con->Rapidity();
    
    TVector2 r_i(ceta-jeta, dphi);  
    r_i *= (con->Pt() * r_i.Mod());
    r_pull += r_i;
  }

  TVector2 pull;
  if(ref->Pt() != 0) {
    double mag = r_pull.Mod()/ref->Pt();
    double phi = TVector2::Phi_mpi_pi(r_pull.Phi());
      
    pull.SetMagPhi(mag,phi);
  }



  /*
  double pT_ref = ref->Pt();
  TVector2 J(ref->Rapidity(),ref->Phi());

  
  TVector2 pull(0,0);
  for(const auto &con: *cons){
    TVector2 ci(con->Rapidity(),con->Phi());
    TVector2 ri = ci - J;
    TVector2 con_pull = (con->Pt()/pT_ref)*fabs(ri.Mod())*ri;
    pull+=con_pull;

  }
  */
  return pull;

}



// Doesn't work because of no protection against missing constituent links!!

TVector2 JSS::CalcJetPull(const xAOD::Jet *jet){

  TVector2 pull(-99,-99);
  if(!jet->getConstituents().isValid())
    return pull;


  JetSubStructureUtils::Pull jss_tool;
  map<string, double> pull_map = jss_tool.result(*jet);
    
  pull.SetMagPhi(pull_map["PullMag"],pull_map["PullPhi"]);
  return pull;
}


double ptww(Particles* cons, Particle* ref){
  double retval=0.;
  double sum=0;
  double sum2=0;
  for(const auto &con: *cons){
    double dR = con->DeltaR(*ref);
    double pT = con->Pt();
    sum+=(pT*dR);
    sum2+=pT;
  }
  if(sum2>0){
    retval= sum/sum2;
  }

  return retval;
}

double etww(Particles* cons, Particle* ref){
  double retval=0.;
  double sum=0;
  double sum2=0;
  for(const auto &con: *cons){
    double dR = con->DeltaR(*ref);
    double et = con->Et();
    sum+=(et*dR);
    sum2+=et;
  }
  if(sum2>0){
    retval= sum/sum2;
  }

  return retval;
}


double flargest(Particles* cons, Particle* ref){
  std::sort(cons->begin(),cons->end(),ptsorter);
  auto largest = *cons->begin();
  return largest->E()/ref->E();
}


double cbeta(Particles* cons){
  double sum=0;
  double sum2=0;
  for(const auto &con1: *cons){
    for(const auto &con2: *cons){
      double dR = con1->DeltaR(*con2);
      double product = con1->Et()*con2->Et()*dR;
      sum+=product;
    }
    sum2+=con1->Et();
  }
  double retval=0;
  if(sum2>0)
    retval=sum/(sum2*sum2);
  return retval;

}



double JSS::charge(Particles* cons, Particle* ref,double k){
  double sum=0;
  for(const auto &con: *cons){
    double charge = con->charge;
    double pT = con->Pt();
    sum+= charge*std::pow(pT,k);
  }

  double retval = (1./std::pow(ref->Pt(),k)) * sum;

  return retval;

}

