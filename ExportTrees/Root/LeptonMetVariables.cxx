
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//               leptonMET variables
//____________________________________________________
//
void ExportTrees::FillLeptonMETVariables(){
  Particle l1;
  Particle l2;
  Particle l3;
  
  Particles leptons = (*m_leptons);
  
  int nLep = leptons.size();
  if(nLep>=1){
    l1=(*leptons[0]);
    if(nLep>=2){
      l2=(*leptons[1]);
      if(nLep>=3){
	l3=(*leptons[2]);
      }
    }
  }
  
  TVector2 met_nom  = (*m_met);
  TVector2 met_fake = (*m_met_fake);


  // calculate MT with visible leptons!
  outputntuple->MT_orig =  sqrt(2*(met_nom.Mod())*l1.Et() - 2*((met_nom.Px())*l1.Px() + (met_nom.Py())*l1.Py()));
  outputntuple->MT =  sqrt(2*(met_fake.Mod())*l1.Et() - 2*((met_fake.Px())*l1.Px() + (met_fake.Py())*l1.Py()));

  if(outputntuple->EventNumber==1328314)
    std::cout << outputntuple->MT << "" "" << sqrt(2*l1.Pt() *met_nom.Mod() * (1-cos(TVector2::Phi_mpi_pi( met_nom.Phi() )- l1.Phi()))) << std::endl;



}
