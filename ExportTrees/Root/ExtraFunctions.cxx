
#include <ExportTrees/Export.h>



using namespace std;

float ExportTrees::dPhiBadTile(Jets jets, float MET_phi)
{
  float minDPhi = 999.;
  for ( auto jet : jets ){
    float phi = jet->Phi();
    float eta = jet->Eta();
    if ( (phi>0.8 && phi<1.0 && eta>0. && eta<0.9 ) ||
	 (phi>1.9 && phi<2.1 && eta>-1.6 && eta<-0.9 ) ) {
      // jet points to a dead tile
      float dphi = std::acos(std::cos(phi-MET_phi));
      if ( dphi < minDPhi ) minDPhi = dphi;
    }
  }
  
  return minDPhi;
}


bool ExportTrees::badTileVeto(Jets jets, float MET_phi)
{
  bool isDeadTile=false;   
  for ( unsigned int i=0; i<jets.size();i++){
    double jet_pt = jets[i]->Pt();
    if(jet_pt<50.) continue;
    double jet_phi = jets[i]->Phi();
    double  jet_BCH_CORR_JET = jets[i]->BCH_CORR_CELL; 
    if(std::acos(std::cos( jet_phi-MET_phi )  )<0.3 && jet_BCH_CORR_JET>0.05) isDeadTile=true;
  }
  return isDeadTile ;
}


