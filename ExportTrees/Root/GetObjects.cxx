#include <ExportTrees/Export.h>

void  ExportTrees::GetJets(Jets*& jets){
  TLorentzVector tmpTLV;
  double pt=0;
  double eta=0;
  double phi=0;
  double E=0;
  int flav =-1;
  int tflav =-1;
  double MV2=0;

  double BCH_CORR_CELL=-99;
  double fem = -99;
  double fch = -99;
  double fmax = -99;

 
  


  jets = new Jets();

  for(unsigned int j=0; j< inputntuple->jet_pt->size();j++)
    {
      pt=(*inputntuple->jet_pt)[j]/1000.;
      eta=(*inputntuple->jet_eta)[j];
      phi=(*inputntuple->jet_phi)[j];
      E=(*inputntuple->jet_e)[j]/1000.;
      Jet* tmpjet = new Jet(pt,eta,phi,E);

      
      flav=(*inputntuple->jet_flav)[j];
      tflav=(*inputntuple->jet_truthflav)[j];
      MV2=(*inputntuple->jet_MV2c10)[j];
      //set flavour/MV2
      tmpjet->MV2=MV2;
      tmpjet->flav=flav;
      //truth jet flavour label
      tmpjet->tflav=tflav;
      //
      // add in protection for extra variables that might not be present or filled
      //
      
      /*
      if(nullptr != &inputntuple->jet_BCH_CORR_CELL){
	if(inputntuple->jet_BCH_CORR_CELL->size()==inputntuple->jet_pt->size())
	  BCH_CORR_CELL=(*inputntuple->jet_BCH_CORR_CELL)[j];
      }
      if(nullptr != &inputntuple->jet_emfrac){
	if(inputntuple->jet_emfrac->size()==inputntuple->jet_pt->size())
	  fem=(*inputntuple->jet_emfrac)[j];
      }
      if(nullptr != &inputntuple->jet_chf){
	if(inputntuple->jet_chf->size()==inputntuple->jet_pt->size())
	  fch=(*inputntuple->jet_chf)[j];
      }
      if(nullptr != &inputntuple->jet_fmax){
	if(inputntuple->jet_fmax->size()==inputntuple->jet_pt->size())
	  fmax=(*inputntuple->jet_fmax)[j];
      }
      */
      tmpjet->BCH_CORR_CELL=BCH_CORR_CELL;
      tmpjet->fem=fem;
      tmpjet->fch=fch;
      tmpjet->fmax=fmax;
      

      
      if(nullptr != inputntuple->jet_ntracks){
	tmpjet->ntracks=(*inputntuple->jet_ntracks)[j];
      }

      /*
      double npartons = 0;
      double wpartons = 0;
      std::string type = "None";


      //more truth stuff
      if(nullptr != &inputntuple->jet_npartons){
	npartons=(*inputntuple->jet_npartons)[j];
      }
      if(nullptr != &inputntuple->jet_wpartons){
	wpartons=(*inputntuple->jet_wpartons)[j];
      }
      if(nullptr != &inputntuple->jet_type){
	type=(*inputntuple->jet_type)[j];
      }

      tmpjet->npartons=npartons;
      tmpjet->wpartons=wpartons;
      tmpjet->origin=type;
      */



      if(tmpjet->Pt()<20 || (true && fabs(tmpjet->Eta())>2.8)){
	delete tmpjet;
	continue;
      }
      
      
      
      jets->push_back(tmpjet);
     
    }
  

   
}


void  ExportTrees::GetTrackJets(Jets*& jets){
  

  TLorentzVector tmpTLV;
  double pt=0;
  double eta=0;
  double phi=0;
  double E=0;
  int tflav =-1;
  double MV2=0;

  
  jets = new Jets();

  //check if track jets are even in the minintuple
  if(inputntuple->trackjet_pt == nullptr) return;

  for(unsigned int j=0; j< inputntuple->trackjet_pt->size();j++)
    {
      pt=(*inputntuple->trackjet_pt)[j]/1000.;
      eta=(*inputntuple->trackjet_eta)[j];
      phi=(*inputntuple->trackjet_phi)[j];
      E=(*inputntuple->trackjet_e)[j]/1000.;
      Jet* tmpjet = new Jet(pt,eta,phi,E);

     
      tflav=(*inputntuple->trackjet_truthflav)[j];
      MV2=(*inputntuple->trackjet_MV2c10)[j];
      //set flavour/MV2
      tmpjet->MV2=MV2;
      //truth jet flavour label
      tmpjet->tflav=tflav;
         
      /*
	if(pt<7 || fabs(eta)>2.5){
	delete tmpjet;
	continue;
	}
      */
      jets->push_back(tmpjet);
     
    }
  

   
}






void  ExportTrees::GetFatJets(Jets*& jets, string name){
  TLorentzVector tmpTLV;
  double pt=0;
  double eta=0;
  double phi=0;
  double E=0;
  
  jets = new Jets();
  
  if(name=="kt8"){


    if(inputntuple->rcjet_kt8_pt == nullptr )return;


    for(unsigned int j=0; j< inputntuple->rcjet_kt8_pt->size();j++)
      {
    	pt=(*inputntuple->rcjet_kt8_pt)[j]/1000.;
    	eta=(*inputntuple->rcjet_kt8_eta)[j];
    	phi=(*inputntuple->rcjet_kt8_phi)[j];
    	E=(*inputntuple->rcjet_kt8_e)[j]/1000.;
    	Jet* tmpjet = new Jet(pt,eta,phi,E);
    	//no tagging for these fatjets
    	//set flavour/MV2
    	tmpjet->MV2=-999;
    	tmpjet->flav=-999;
    	if(tmpjet->Pt()<20 || (true && fabs(tmpjet->Eta())>2.8)){
    	  delete tmpjet;
    	  continue;
    	}
    	jets->push_back(tmpjet);
      }
  }
  else if(name=="kt12"){

    if(inputntuple->rcjet_kt12_pt == nullptr)return;


    for(unsigned int j=0; j< inputntuple->rcjet_kt12_pt->size();j++)
      {
	pt=(*inputntuple->rcjet_kt12_pt)[j]/1000.;
	eta=(*inputntuple->rcjet_kt12_eta)[j];
	phi=(*inputntuple->rcjet_kt12_phi)[j];
	E=(*inputntuple->rcjet_kt12_e)[j]/1000.;
	Jet* tmpjet = new Jet(pt,eta,phi,E);
	//no tagging for these fatjets
	//set flavour/MV2
	tmpjet->MV2=-999;
	tmpjet->flav=-999;

	if(tmpjet->Pt()<20 || (true && fabs(tmpjet->Eta())>2.8)){
	  delete tmpjet;
	  continue;
	}
	jets->push_back(tmpjet);
	
      }
  }
  else if(name=="st"){
    
    if(inputntuple->fatjet_kt10_pt == nullptr)return;


    //fat jets from susytools
    for(unsigned int j=0; j< inputntuple->fatjet_kt10_pt->size();j++)
      {
	pt=(*inputntuple->fatjet_kt10_pt)[j]/1000.;
	eta=(*inputntuple->fatjet_kt10_eta)[j];
	phi=(*inputntuple->fatjet_kt10_phi)[j];
	E=(*inputntuple->fatjet_kt10_e)[j]/1000.;
	Jet* tmpjet = new Jet(pt,eta,phi,E);
	//no tagging for these fatjets
	//set flavour/MV2
	tmpjet->MV2=-999;
	tmpjet->flav=-999;
	
        tmpjet->ntrkjets=(*inputntuple->fatjet_kt10_ntrkjets)[j];
        tmpjet->nghostbhad=(*inputntuple->fatjet_kt10_nghostbhad)[j];
        tmpjet->Split12=(*inputntuple->fatjet_kt10_Split12)[j];
        tmpjet->Split23=(*inputntuple->fatjet_kt10_Split23)[j];
        tmpjet->Split34=(*inputntuple->fatjet_kt10_Split34)[j];
        tmpjet->Tau1=(*inputntuple->fatjet_kt10_Tau1)[j];
        tmpjet->Tau2=(*inputntuple->fatjet_kt10_Tau2)[j];
        tmpjet->Tau3=(*inputntuple->fatjet_kt10_Tau3)[j];
        tmpjet->Tau32=(*inputntuple->fatjet_kt10_Tau32)[j];
        tmpjet->Qw=(*inputntuple->fatjet_kt10_Qw)[j];

        tmpjet->w50=(*inputntuple->fatjet_kt10_w50)[j];
        tmpjet->w80=(*inputntuple->fatjet_kt10_w80)[j];
        tmpjet->z50=(*inputntuple->fatjet_kt10_z50)[j];
        tmpjet->z80=(*inputntuple->fatjet_kt10_z80)[j];
	// tmpjet->W50res=(*inputntuple->nt_fatjet_kt10_W50res)[j];
        //tmpjet->W80res=(*inputntuple->nt_fatjet_kt10_W80res)[j];
        //tmpjet->Z50res=(*inputntuple->nt_fatjet_kt10_Z50res)[j];
        //tmpjet->Z80res=(*inputntuple->nt_fatjet_kt10_Z80res)[j];
        //tmpjet->WLowWMassCut50=(*inputntuple->nt_fatjet_kt10_WLowWMassCut50)[j];
        //tmpjet->WLowWMassCut80=(*inputntuple->nt_fatjet_kt10_WLowWMassCut80)[j];
        //tmpjet->ZLowWMassCut50=(*inputntuple->nt_fatjet_kt10_ZLowWMassCut50)[j];
        //tmpjet->ZLowWMassCut80=(*inputntuple->nt_fatjet_kt10_ZLowWMassCut80)[j];
        //tmpjet->WHighWMassCut50=(*inputntuple->nt_fatjet_kt10_WHighWMassCut50)[j];
        //tmpjet->WHighWMassCut80=(*inputntuple->nt_fatjet_kt10_WHighWMassCut80)[j];
        //tmpjet->ZHighWMassCut50=(*inputntuple->nt_fatjet_kt10_ZHighWMassCut50)[j];
        //tmpjet->ZHighWMassCut80=(*inputntuple->nt_fatjet_kt10_ZHighWMassCut80)[j];
        //tmpjet->WD2Cut50=(*inputntuple->nt_fatjet_kt10_WD2Cut50)[j];
        //tmpjet->WD2Cut80=(*inputntuple->nt_fatjet_kt10_WD2Cut80)[j];
        //tmpjet->ZD2Cut50=(*inputntuple->nt_fatjet_kt10_ZD2Cut50)[j];
        //tmpjet->ZD2Cut80=(*inputntuple->nt_fatjet_kt10_ZD2Cut80)[j];
        //tmpjet->top50=(*inputntuple->fatjet_kt10_top50)[j];
        //tmpjet->top80=(*inputntuple->fatjet_kt10_top80)[j];
        //tmpjet->top50res=(*inputntuple->nt_fatjet_kt10_top50res)[j];
        //tmpjet->top80res=(*inputntuple->nt_fatjet_kt10_top80res)[j];
        //tmpjet->TopTagTau32Cut50=(*inputntuple->nt_fatjet_kt10_TopTagTau32Cut50)[j];
        //tmpjet->TopTagTau32Cut80=(*inputntuple->nt_fatjet_kt10_TopTagTau32Cut80)[j];
        //tmpjet->TopTagSplit23Cut50=(*inputntuple->nt_fatjet_kt10_TopTagSplit23Cut50)[j];
        //tmpjet->TopTagSplit23Cut80=(*inputntuple->nt_fatjet_kt10_TopTagSplit23Cut80)[j];


	if(tmpjet->Pt()<20 || (true && fabs(tmpjet->Eta())>2.8)){
	  delete tmpjet;
	  continue;
	}
	jets->push_back(tmpjet);
      }
  }
  
}



void  ExportTrees::GetElectrons(Particles*& electrons){
  electrons = new Particles();
  for(unsigned int j=0; j< inputntuple->el_pt->size();j++)
    {
      double pt=(*inputntuple->el_pt)[j]/1000.;
      double eta=(*inputntuple->el_eta)[j];
      double phi=(*inputntuple->el_phi)[j];
      double E=(*inputntuple->el_e)[j]/1000.;
      
      Particle* el = new Particle(pt,eta,phi,E);
      el->charge=(*inputntuple->el_charge)[j];
      el->flav=11;
      electrons->push_back(el);
    }
}  

void ExportTrees::GetMuons(Particles*& muons){
  muons = new Particles();
  for(unsigned int j=0; j< inputntuple->mu_pt->size();j++)
    {
      double pt=(*inputntuple->mu_pt)[j]/1000.;
      double eta=(*inputntuple->mu_eta)[j];
      double phi=(*inputntuple->mu_phi)[j];
      double E=(*inputntuple->mu_e)[j]/1000.;
      
      Particle* mu = new Particle(pt,eta,phi,E);
      mu->charge=(*inputntuple->mu_charge)[j];
      mu->flav=13;
      muons->push_back(mu);
    }
}  

void ExportTrees::GetPhotons(Particles*& photons){
  photons = new Particles();
  for(unsigned int j=0; j< inputntuple->ph_pt->size();j++)
    {
      double pt=(*inputntuple->ph_pt)[j]/1000.;
      double eta=(*inputntuple->ph_eta)[j];
      double phi=(*inputntuple->ph_phi)[j];
      double E=(*inputntuple->ph_e)[j]/1000.;
      Particle* ph = new Particle(pt,eta,phi,E);

      double topoetcone40=-99;
      double ptvarcone20=-99;
      
      if(nullptr != &inputntuple->ph_topoetcone40){
	if(inputntuple->ph_topoetcone40->size()==inputntuple->ph_pt->size())
	  topoetcone40=(*inputntuple->ph_topoetcone40)[j];
      }
      if(nullptr != &inputntuple->ptvarcone20){
	if(inputntuple->ptvarcone20->size()==inputntuple->ph_pt->size())
	  ptvarcone20=(*inputntuple->ptvarcone20)[j];
      }

      ph->topoetcone40=topoetcone40;
      ph->ptvarcone20=ptvarcone20;

      photons->push_back(ph);
    }
}  

void ExportTrees::GetTaus(Particles*& taus){
  taus=new Particles();
  for (unsigned int j=0; j<inputntuple->tau_pt->size(); j++)
    {
      double taupt=(*inputntuple->tau_pt)[j]/1000.;
     
      double taueta=(*inputntuple->tau_eta)[j];
     
      double tauphi=(*inputntuple->tau_phi)[j];
     
      double tauE=(*inputntuple->tau_e)[j]/1000;
      
      Particle* tau= new Particle(taupt,taueta,tauphi,tauE);
      //tau->nPi0=(*inputntuple->tau_nPi0)[j];
      //tau->truthJet_matched_tau=(*inputntuple->tau_nPi0)[j];
      //std::cout<<"Tau matched to truthjet_index:"<<tau->truthJet_matched_tau<<std::endl;
      
      
      tau->bdtJet=(*inputntuple->tau_bdtJet)[j];
      tau->bdtEl=(*inputntuple->tau_bdtEl)[j];
      tau->nH=(*inputntuple->tau_nH)[j];
      //      tau->nWTracks=(*inputntuple->tau_nWTracks)[j];
      tau->nTracks=(*inputntuple->tau_nTracks)[j];
      tau->nPi0=(*inputntuple->tau_nPi0)[j];
      tau->nCharged=(*inputntuple->tau_nCharged)[j];
      tau->nNeut=(*inputntuple->tau_nNeut)[j];


      tau->flav=15;

      
      
      // NO TAU CHARGE (ONLY DOING KINEMATIC STUFF ON TAU)
      taus->push_back(tau);
      
	
      }
}






void  ExportTrees::GetTruthPhotons(Particles*& photons){
  photons = new Particles();
  /*
  for(unsigned int j=0; j< inputntuple->truthPh_pt->size();j++)
    {
      double pt=(*inputntuple->truthPh_pt)[j]/1000.;
      double eta=(*inputntuple->truthPh_eta)[j];
      double phi=(*inputntuple->truthPh_phi)[j];
      double E=(*inputntuple->truthPh_e)[j]/1000.;
      
      Particle* ph = new Particle(pt,eta,phi,E);
      photons->push_back(ph);
    }
  */
}  

void ExportTrees::GetMet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_pt/1000.,inputntuple->MET_phi);
  //Jets jets = (*m_jets);
  /*
    for(unsigned int i =0;i<jets.size();i++){
    Jet j1 = (*jets[i]);
    if(j1.Pt()<35)
    (*etmiss2D)+= TVector2(j1.Pt(),j1.Py());
    }
  */
}

void ExportTrees::GetSimpleMet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  //etmiss2D->Set(inputntuple->simpleMET_px/1000.,inputntuple->simpleMET_py/1000.);
}

void ExportTrees::GetMetEl(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_el_pt/1000.,inputntuple->MET_el_phi);
}


void ExportTrees::GetMetMu(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_mu_pt/1000.,inputntuple->MET_mu_phi);
}

void ExportTrees::GetSimpleMetMu(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  //etmiss2D->Set(inputntuple->simpleMET_mu_px/1000.,inputntuple->simpleMET_mu_py/1000.);
}

void ExportTrees::GetMetY(TVector2*& etmiss2D){
   etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_y_pt/1000.,inputntuple->MET_y_phi);
}

void ExportTrees::GetMetSoft(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_softTrk_pt/1000.,inputntuple->MET_softTrk_phi);
}


void ExportTrees::GetMetJet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_jet_pt/1000.,inputntuple->MET_jet_phi);
}




void ExportTrees::GetFakeMet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_pt_prime/1000.,inputntuple->MET_phi_prime);
}
void ExportTrees::GetTrackMet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_track_pt/1000.,inputntuple->MET_track_phi);
}

void ExportTrees::GetNonIntMet(TVector2*& etmiss2D){
  etmiss2D = new TVector2();
  etmiss2D->SetMagPhi(inputntuple->MET_NonInt_pt/1000.,inputntuple->MET_NonInt_phi);
}
