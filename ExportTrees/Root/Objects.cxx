#include <ExportTrees/Objects.h>
#include <ExportTrees/MiniNtuple.h>
#include <iostream>


Jet::Jet(double pt, double eta, double phi, double E){
  this->SetPtEtaPhiE(pt,eta,phi,E);
}

Particle::Particle(double pt, double eta, double phi, double E){
  this->SetPtEtaPhiE(pt,eta,phi,E);
}
Particle::Particle(TLorentzVector tlv){
  double pt=tlv.Pt();
  double eta=tlv.Eta();
  double phi=tlv.Phi();
  double E =tlv.E();
  this->SetPtEtaPhiE(pt,eta,phi,E);
}
