
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                   jet variables
//____________________________________________________
//
void ExportTrees::FillJetVariables(){
       

  Jets jets = (*m_jets);

  //variables for jet smearing... don't mind me..
  double average_R=0;
  for(unsigned int i=0;i<jets.size();i++){
    Jet tmpJet=(*jets[i]);
    if(i==0)
      outputntuple->R_1jet=tmpJet.R;
    else if(i==1)
      outputntuple->R_2jet=tmpJet.R;
    else if(i==2)
      outputntuple->R_3jet=tmpJet.R;
    else if(i==3)
      outputntuple->R_4jet=tmpJet.R;
    
    average_R+=tmpJet.R;
  }
  average_R=average_R/jets.size();
  outputntuple->R_av=average_R;


  Jets bjets = (*m_bjets);

  



  Jets bjets60 = (*m_bjets60);
  Jets bjets85 = (*m_bjets85);
  Jets ljets = (*m_ljets);


  Jet j1,j2,j3,j4,j5,j6,j7;
  Jet lj1,lj2;
  Jet b1,b2,b1_85,b2_85,b1_60,b2_60,b_j,b_i;
  
 
  
  int njets = jets.size();
  int nljets = ljets.size();
  int nbjets = bjets.size();
  int nbjets60 = bjets60.size();
  int nbjets85 = bjets85.size();

  vector<int>     bindex;  
  for(unsigned int i=0;i<jets.size();i++){
    Jet tj = (*jets[i]);
    if (tj.flav==5)
      bindex.push_back(i);
  }

  outputntuple->bindex=bindex;


  outputntuple->nj_good=njets;
  outputntuple->num_bjets=nbjets;
    
  int nbjets35=0;
  for(int ii=0;ii<nbjets;ii++){
    if( bjets[ii]->Pt()>35)
      nbjets35++;
  }
  outputntuple->num_bjets35=nbjets35;


  outputntuple->num_bjets60=nbjets60;
  outputntuple->num_bjets85=nbjets85;
	  

  if(njets>0){
    j1 = (*jets[0]);
    if(njets>1){
      j2 = (*jets[1]);
      if(njets>2){
    	j3 = (*jets[2]);
    	if(njets>3){
    	  j4 = (*jets[3]);
          if(njets>4){
            j5 = (*jets[4]);
	    if(njets>5){
	      j6 = (*jets[5]);
	      if(njets>6){
		j7 = (*jets[6]);
	      }
	    }
          }
        }
      }  
    }
  }
  if(nljets>0){
    lj1 = (*ljets[0]);
    if(nljets>1){
      lj2 = (*ljets[1]);
    }
  }
  if(nbjets>0){
    b1 = (*bjets[0]);
    if(nbjets>1){
      b2 = (*bjets[1]);
    }
  }
  if(nbjets85>0){
    b1_85 = (*bjets85[0]);
    if(nbjets85>1){
      b2_85 = (*bjets85[1]);
    }
  }
  if(nbjets60>0){
    b1_60 = (*bjets60[0]);
    if(nbjets60>1){
      b2_60 = (*bjets60[1]);
    }
  }



  
  // maxDR algorithm
  if(nbjets>3){
    double DR = 0;
    double assym = 0;
    double mbb = 0;
    int ntracks=0;
    unsigned int maxI = -1;
    unsigned int maxJ = -1;

    // find the 2 b-jes with maximum DR
    for (unsigned int i = 0; i < bjets.size()-1; ++i)
    {
      for (unsigned int j = i+1; j < bjets.size(); ++j)
      {
        b_i = (*bjets[i]);
        b_j = (*bjets[j]);
        if (b_i.DeltaR(b_j)>DR){
          DR = b_i.DeltaR(b_j);
          assym = fabs((b_i.Pt()-b_j.Pt())/(b_i.Pt()+b_j.Pt()));
          mbb = (b_i+b_j).M();
	  ntracks=b_i.ntracks + b_j.ntracks;
          maxI = i;
          maxJ = j;
        }
      }
    }
    // write their properties
    outputntuple->maxDRbb=DR;
    outputntuple->maxDRassym=assym;
    outputntuple->maxDRmbb=mbb;
    outputntuple->maxDRntracks=ntracks;
    

    // find the 2 b-jets with minimum DR, excluding the above pair
    DR = 999;
    assym = 0;
    mbb = 0;
    for (unsigned int i = 0; i < bjets.size()-1; ++i)
    {
      for (unsigned int j = i+1; j < bjets.size(); ++j)
      {
        b_i = (*bjets[i]);
        b_j = (*bjets[j]);
        if (b_i.DeltaR(b_j)<DR && i!=maxI && i!=maxJ && j!=maxI && j!=maxJ){
          DR = b_i.DeltaR(b_j);
          assym = fabs((b_i.Pt()-b_j.Pt())/(b_i.Pt()+b_j.Pt()));
          mbb = (b_i+b_j).M();
	  ntracks=b_i.ntracks + b_j.ntracks;
        }
      }
      // write their properties
      outputntuple->maxminDRbb=DR;
      outputntuple->maxminDRassym=assym;
      outputntuple->maxminDRmbb=mbb;
      outputntuple->maxminDRntracks=ntracks;
    }
  }

  // minDR algorithm
  if(nbjets>1){
    double DR = 999;
    double assym = 0;
    double mbb = 0;
    int ntracks=0;

    // find the 2 b-jets with minimum DR
    for (unsigned int i = 0; i < bjets.size()-1; ++i)
    {
      for (unsigned int j = i+1; j < bjets.size(); ++j)
      {
        b_i = (*bjets[i]);
        b_j = (*bjets[j]);
        if (b_i.DeltaR(b_j)<DR){
          DR = b_i.DeltaR(b_j);
          assym = fabs((b_i.Pt()-b_j.Pt())/(b_i.Pt()+b_j.Pt()));
          mbb = (b_i+b_j).M();
	  ntracks=b_i.ntracks + b_j.ntracks;
        }
      }
      // write their properties
      outputntuple->minDRbb=DR;
      outputntuple->minDRassym=assym;
      outputntuple->minDRmbb=mbb;
      outputntuple->minDRntracks=ntracks;
    }
  }
  
  
  outputntuple->w_MV2c20_1=j1.MV2;
  outputntuple->w_MV2c20_2=j2.MV2;
  outputntuple->w_MV2c20_3=j3.MV2;
  
  outputntuple->leadb1=(j1.flav==5);
  outputntuple->leadb2=(j2.flav==5);
  outputntuple->leadb1_60=(j1.MV2>0.934906);
  outputntuple->leadb2_60=(j2.MV2>0.934906);
  outputntuple->leadb1_85=(j1.MV2>0.1758475);
  outputntuple->leadb2_85=(j2.MV2>0.1758475);


    
  outputntuple->pT_1bjet=b1.Pt();
  outputntuple->pT_2bjet=b2.Pt();
  
  outputntuple->pT_1jet=j1.Pt();
  outputntuple->pT_2jet=j2.Pt();
  outputntuple->pT_3jet=j3.Pt();
  outputntuple->pT_4jet=j4.Pt();
  outputntuple->pT_5jet=j5.Pt();  
  outputntuple->pT_6jet=j6.Pt();  


  outputntuple->ntracks_1jet=j1.ntracks;
  outputntuple->ntracks_2jet=j2.ntracks;
  outputntuple->ntracks_3jet=j3.ntracks;
  outputntuple->ntracks_4jet=j4.ntracks;
  outputntuple->ntracks_5jet=j5.ntracks;
  outputntuple->ntracks_6jet=j6.ntracks;
  
  outputntuple->jpa_1jet_tracks=j1.jpa_tracks;
  outputntuple->jpa_2jet_tracks=j2.jpa_tracks;
  outputntuple->jpa_3jet_tracks=j3.jpa_tracks;
  outputntuple->jpa_4jet_tracks=j4.jpa_tracks;
  
  outputntuple->jpa_1jet_calo=j1.jpa_calo;
  outputntuple->jpa_2jet_calo=j2.jpa_calo;
  outputntuple->jpa_3jet_calo=j3.jpa_calo;
  outputntuple->jpa_4jet_calo=j4.jpa_calo;
  




  outputntuple->flav_1jet=j1.flav;
  outputntuple->flav_2jet=j2.flav;
  outputntuple->flav_3jet=j3.flav;
  outputntuple->flav_4jet=j4.flav;



  outputntuple->eta_1jet=j1.Eta();
  outputntuple->eta_2jet=j2.Eta();
  outputntuple->eta_3jet=j3.Eta();
  outputntuple->eta_4jet=j4.Eta();
  outputntuple->eta_5jet=j5.Eta();
  outputntuple->eta_6jet=j6.Eta();

  outputntuple->phi_1jet=j1.Phi();
  outputntuple->phi_2jet=j2.Phi();
  outputntuple->phi_3jet=j3.Phi();
  outputntuple->phi_4jet=j4.Phi();
  outputntuple->phi_5jet=j5.Phi();

  outputntuple->mjj=(j1+j2).M();
  outputntuple->mbb=(b1+b2).M();

  /*

  outputntuple->origin_1jet=j1.origin;
  outputntuple->origin_2jet=j2.origin;
  outputntuple->origin_3jet=j3.origin;
  outputntuple->origin_4jet=j4.origin;
  outputntuple->origin_5jet=j5.origin;
  if(njets>5){
    outputntuple->origin_6jet=j6.origin;
    if(njets>6){
      outputntuple->origin_7jet=j7.origin;
    }
  }

  outputntuple->npartons_1jet=j1.npartons;
  outputntuple->npartons_2jet=j2.npartons;
  outputntuple->npartons_3jet=j3.npartons;
  outputntuple->npartons_4jet=j4.npartons;

  outputntuple->wpartons_1jet=j1.wpartons;
  outputntuple->wpartons_2jet=j2.wpartons;
  outputntuple->wpartons_3jet=j3.wpartons;
  outputntuple->wpartons_4jet=j4.wpartons;
  */

  // pt asymmetry based on the MV2 weight
  
  Jets jetsMV2sort = (*m_jets);
  std::sort(jetsMV2sort.begin(),jetsMV2sort.end(),MV2sorter);  
  double pt_asym =0;
  if(jetsMV2sort.size()>=2){
    pt_asym = (jetsMV2sort[0]->Pt() - jetsMV2sort[1]->Pt())/(jetsMV2sort[0]->Pt() + jetsMV2sort[1]->Pt());
  }
  outputntuple->pt_asym = pt_asym;
  





  //pt of the last jet
  outputntuple->pT_lastjet = 0;
  if(njets>0)
    outputntuple->pT_lastjet =jets[njets-1]->Pt();
  // ht3
  double ht3=0;
  for(int ii=3;ii<njets;ii++){
    ht3+=jets[ii]->Pt();
  }
  outputntuple->ht3=ht3;
  
  //HT
  double HT=0;
  double HT_35=0;
  int njets35=0;
  int njets45=0;
  int njets45_0eta240=0;
  
  for(int ii=0;ii<njets;ii++){
    HT+=jets[ii]->Pt();
    if(jets[ii]->Pt()>35){
      HT_35+=jets[ii]->Pt();
      njets35++;
    }
    if(jets[ii]->Pt()>45){
      njets45++;
      if(fabs(jets[ii]->Eta())<2.4) njets45_0eta240++;
    }
    
  }
  outputntuple->HT=HT;
  outputntuple->HT_35=HT_35;
  outputntuple->nj_good35=njets35;
  outputntuple->nj_good45=njets45;
  outputntuple->nj_good45_0eta240=njets45_0eta240;
	   
  
  // mCT variable!
  TMctLib* mcttool = new TMctLib();
  outputntuple->mct2b=mcttool->mct(b1,b2);
  outputntuple->mctjj=mcttool->mct(j1,j2);
  delete mcttool;
  
  
 
  double dRjmax = 0;
  double dRjmin = 10000;
  double dEtajmin = 10000;
  
  outputntuple->dEtab1b2 = fabs(b1.Eta()-b2.Eta());
  
  for( int ii=0;ii<njets;ii++){
    for( int jj=0; jj<njets;jj++){
      if(ii==jj)
  	continue;
      
      double dRtemp = (*jets[ii]).DeltaR((*jets[jj]));
      double dEtatemp = fabs((*jets[ii]).Eta()-(*jets[jj]).Eta());
      if(dRtemp>dRjmax)
  	dRjmax = dRtemp;
      if(dRtemp<dRjmin)
  	dRjmin = dRtemp;
      
      if(dEtatemp<dEtajmin)
  	dEtajmin = dEtatemp;
      
    }
  }
  
  outputntuple->dRjmax = dRjmax;
  outputntuple->dRjmin = dRjmin;
  outputntuple->dEtajmin = dEtajmin;

  
  double A = 0;
  A = (j1.Pt()-j2.Pt()) / (j1.Pt()+j2.Pt());
  double y1 = -999;
  double y2 = -999;
  if(njets>0){
    y1 = j1.Rapidity();
    if(njets>1){
      y2 = j2.Rapidity();
    }
  }

  outputntuple->y_1jet = y1;
  outputntuple->y_2jet = y2;
  outputntuple->A=A;
     
  vector<TLorentzVector> t_TLV;
  
  for(unsigned int i=0;i<m_jets->size();i++){
    t_TLV.push_back((*jets[i])*1000);
  }
   
  
  double Sp = 0;
  double ST = 0;
  double Ap = 0;
  Sphericity sp; 
  sp.SetTLV(t_TLV,t_TLV.size());
  sp.GetSphericity(Sp, ST, Ap);
  
  outputntuple->Sp=Sp;
  outputntuple->ST=ST;
  outputntuple->Ap=Ap;
  
  TLorentzVector top1(0,0,0,0);
  TLorentzVector top2(0,0,0,0);
  TLorentzVector W1(0,0,0,0);
  TLorentzVector W2(0,0,0,0);
     
  double dRb1j1,dRb1j2,dRb2j1,dRb2j2;
     
  if(nljets==1 && nbjets==2){
    dRb1j1 = b1.DeltaR(lj1);
    dRb2j1 = b2.DeltaR(lj1);
    if ( dRb1j1 < dRb2j1)
      W1 = lj1;
    else
      W2 = lj1; 
  }
  else if(nljets==2 && nbjets==2){
    dRb1j1 = b1.DeltaR(lj1);
    dRb2j1 = b2.DeltaR(lj1);
    dRb1j2 = b1.DeltaR(lj2);
    dRb2j2 = b2.DeltaR(lj2);
    
    double minimum = min(min( dRb1j1,dRb2j1),min( dRb1j2,dRb2j2));
    if(minimum==dRb1j1)
      W1=lj1;
    else if(minimum==dRb1j2)
      W1=lj2;
    else if(minimum==dRb2j1)
      W2=lj1;
    else if(minimum==dRb2j2)
      W2=lj2;
    
  }

  double dPhi_top1_sys = -999;
  double dPhi_top2_sys = -999;
  double dPhi_tt_sys = -999;
	
  if(nbjets==2){
    top1 = b1 + W1;
    top2 = b2 + W2;
    TVector2 met_fake = (*m_met_fake);
    dPhi_top1_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - top1.Phi());
    dPhi_top2_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - top2.Phi());
    dPhi_tt_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - (top1+top2).Phi());
  }
  
  
  outputntuple->m_top1 = top1.M();
  outputntuple->m_top2 = top2.M();

  outputntuple->dPhi_top1_sys=dPhi_top1_sys;
  outputntuple->dPhi_top2_sys=dPhi_top2_sys;
  outputntuple->dPhi_tt_sys=dPhi_tt_sys;

  // pt asymmetry based on the MV2 weight
  // std::vector<Jet*> new_jets = jets;
  // std::sort(new_jets.begin(),new_jets.end(),MV2sorter);  
  // double pt_asym =0;
  // if(new_jets.size()>=2){
  //  pt_asym = (new_jets[0]->Pt() - new_jets[1]->Pt())/(new_jets[0]->Pt() + new_jets[1]->Pt());
  // }
  //outputntuple->pt_asym = pt_asym;


  // get some truth jet information based on jet flavour
  bool hastau=false;
  bool hasb=false;
  bool hasc=false;
  
  
  
  for(int ii=0;ii<njets;ii++){
    int tflav = abs(jets[ii]->tflav);
    if(tflav==15)
      hastau=true;
    if(tflav==5)
      hasb=true;
    if(tflav==4)
      hasc=true;
  }




  outputntuple->hastau=hastau;
  outputntuple->hasb=hasb;
  outputntuple->hasc=hasc;

  outputntuple->j1_truthFlav=j1.tflav;
  outputntuple->j2_truthFlav=j2.tflav;
  outputntuple->j3_truthFlav=j3.tflav;
  outputntuple->j4_truthFlav=j4.tflav;

  outputntuple->b1_truthFlav=b1.tflav;
  outputntuple->b2_truthFlav=b2.tflav;

  /*Calum - turn off for now, as the algo is improved

  // Computing mbjj : consider 2 leading b-jets and all other jets (including subleading b-jets),
  // find pair of jets with minimum Delta R, match it with closest b-jet. Do that twice, and get
  // invariant masses to form mbjj_0 and mbjj_1. Additionally, check truth information: each
  // subjet of bjj system that does come from a top gets a score of 1 (so 0-misreconstructed to
  // 3-true top).
  Jets otherjets = (*m_jets);
  for(unsigned int i=0;i<bjets.size();i++){
    if(i==0)b1=(*bjets[i]);
    else if(i==1)b2=(*bjets[i]);
    else otherjets.push_back(bjets[i]);
  }
  int minpair1a,minpair1b,minpair2a,minpair2b;
  minpair1a=minpair1b=minpair2a=minpair2b=-1;
  double minDR1,minDR2;
  minDR1=minDR2=9999.0;
  for(int i=0;i<otherjets.size()-1;i++){
    for(int j=i+1;j<otherjets.size();j++){
      if(i==minpair1a||i==minpair1b||i==minpair2a||i==minpair2b) continue;
      if(j==minpair1a||j==minpair1b||j==minpair2a||j==minpair2b) continue;
	double thisDR=(*otherjets[i]).DeltaR((*otherjets[j]));
	if(thisDR<minDR1){
	  minDR2=minDR1;
	  minpair2a=minpair1a;
	  minpair2b=minpair1b;
	  minDR1=thisDR;
	  minpair1a=i;
	  minpair1b=j;
	}
	else if(thisDR<minDR2){
	  minDR2=thisDR;
	  minpair2a=i;
	  minpair2b=j;
	}
    }
  }
  double minpair1_eta=((*otherjets[minpair1a]).Eta()+(*otherjets[minpair1b]).Eta())/2;
  double minpair1_phi=TVector2::Phi_mpi_pi(((*otherjets[minpair1a]).Phi()+(*otherjets[minpair1b]).Phi())/2);
  TLorentzVector minpair1;
  minpair1.SetPtEtaPhiE(0,minpair1_eta,minpair1_phi,0);
  double m_mbjj_0,m_mbjj_1;
  int Tbjj_0,Tbjj_1;
  int Wjj_0,Wjj_1;
  Tbjj_0=Tbjj_1=Wjj_0=Wjj_1=0;
  if(b1.DeltaR(minpair1)<b2.DeltaR(minpair1)){
    m_mbjj_0=(b1+(*otherjets[minpair1a])+(*otherjets[minpair1b])).M();

    std::cout << "m_mbjj_0=" << m_mbjj_0 << " " << (*otherjets[minpair1a]).charge + (*otherjets[minpair1b]).charge << std::endl;


    if((*otherjets[minpair1a]).origin=="top") ++Wjj_0;
    if((*otherjets[minpair1b]).origin=="top") ++Wjj_0;
    if(minpair2a!=-1){
      m_mbjj_1=(b2+(*otherjets[minpair2a])+(*otherjets[minpair2b])).M();
      if((*otherjets[minpair2a]).origin=="top") ++Wjj_1;
      if((*otherjets[minpair2b]).origin=="top") ++Wjj_1;
    }
    else m_mbjj_1=0.;
    Tbjj_0=Wjj_0;
    Tbjj_1=Wjj_1;
    if(b1.origin=="top") ++Tbjj_0;
    if(b2.origin=="top") ++Tbjj_1;    
  }
  else{
    m_mbjj_0=(b2+(*otherjets[minpair1a])+(*otherjets[minpair1b])).M();
    if((*otherjets[minpair1a]).origin=="top") ++Wjj_0;
    if((*otherjets[minpair1b]).origin=="top") ++Wjj_0;
    if(minpair2a!=-1){
      m_mbjj_1=(b1+(*otherjets[minpair2a])+(*otherjets[minpair2b])).M();
      if((*otherjets[minpair2a]).origin=="top") ++Wjj_1;
      if((*otherjets[minpair2b]).origin=="top") ++Wjj_1;
    }
    else m_mbjj_1=0.;
    Tbjj_0=Wjj_0;
    Tbjj_1=Wjj_1;
    if(b1.origin=="top") ++Tbjj_1;
    if(b2.origin=="top") ++Tbjj_0;
  }
  outputntuple->mbjj_0=m_mbjj_0;
  outputntuple->mbjj_1=m_mbjj_1;
  outputntuple->Tbjj_0=Tbjj_0;
  outputntuple->Tbjj_1=Tbjj_1;
  outputntuple->Wjj_0=Wjj_0;
  outputntuple->Wjj_1=Wjj_1;
  */

}

