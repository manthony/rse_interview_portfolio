#include "ExportTrees/Export.h"
#include <RootCore/Packages.h>
#include "RestFrames/RestFrames.hh"
using namespace std;
using namespace RestFrames;

void ExportTrees::FillCompressedAnalysis()
  {

    Jets jets = (*m_jets);
    //dont bother if there are no jets
    if(jets.size()==0)
      return;



    // RestFrames stuff                                                                                                                                                     

    ////////////// Tree set-up /////////////////                                                                                                                           
    LabRecoFrame*        LAB;
    DecayRecoFrame*      CM;
    DecayRecoFrame*      S;
    VisibleRecoFrame*    ISR;
    VisibleRecoFrame*    V;
    InvisibleRecoFrame*  I;
    InvisibleGroup*      INV;
    SetMassInvJigsaw*    InvMass;
    CombinatoricGroup*   VIS;
    MinMassesCombJigsaw* SplitVis;

    LAB = new LabRecoFrame("LAB", "lab");
    CM = new DecayRecoFrame("CM", "CM");
    S = new DecayRecoFrame("S", "S");
    ISR = new VisibleRecoFrame("ISR", "ISR");
    V = new VisibleRecoFrame("V", "Vis");
    I = new InvisibleRecoFrame("I", "Inv");

    LAB->SetChildFrame(*CM);
    CM->AddChildFrame(*ISR);
    CM->AddChildFrame(*S);
    S->AddChildFrame(*V);
    S->AddChildFrame(*I);

    LAB->InitializeTree();
    ////////////// Tree set-up /////////////////                                                                                                                            

    ////////////// Jigsaw rules set-up /////////////////                                                                                                                    
    INV = new InvisibleGroup("INV", "Invisible System");
    INV->AddFrame(*I);

    VIS = new CombinatoricGroup("VIS", "Visible Objects");
    VIS->AddFrame(*ISR);
    VIS->SetNElementsForFrame(*ISR, 1, false);
    VIS->AddFrame(*V);
    VIS->SetNElementsForFrame(*V, 0, false);

    // set the invisible system mass to zero                                                                                                                                
    InvMass = new SetMassInvJigsaw("InvMass", "Invisible system mass Jigsaw");
    INV->AddJigsaw(*InvMass);

    // define the rule for partitioning objects between "ISR" and "V"                                                                                                       
    SplitVis = new MinMassesCombJigsaw("SplitVis", "Minimize M_{ISR} and M_{S} Jigsaw");
    VIS->AddJigsaw(*SplitVis);
    // "0" group (ISR)                                                                                                                                                      
    SplitVis->AddFrame(*ISR, 0);
    // "1" group (V + I)                                                                                                                                                    
    SplitVis->AddFrame(*V, 1);
    SplitVis->AddFrame(*I, 1);

    LAB->InitializeAnalysis();


    double m_PTISR,m_RISR,m_MS,m_MV,m_dphiISRI,m_pTbV1,m_NbV,m_NjV,m_pTjV4;
    //    double m_cosS,m_MISR,m_dphiCMI,m_pTjV1,m_pTjV2,m_pTjV3,m_pTjV5,m_pTjV6,m_pTbV2
    double m_NbISR,m_NjISR;

    m_PTISR = -999;
    m_RISR = -999;
    m_MS = -999;
    m_MV = -999;
    m_dphiISRI = -999;
    m_pTbV1 = -999;
    m_NbV = -999;
    m_NjV = -999;
    m_pTjV4 = -999;

    //return;
    ////////////// Jigsaw rules set-up /////////////////
   
    // analyze event in RestFrames tree
    LAB->ClearEvent();
    vector<RFKey> jetID;
   
    TVector2 met_fake = (*m_met_fake);
    TVector3 met(met_fake.Px(), met_fake.Py(), 0); 
    TLorentzVector jet(0,0,0,0);
    auto ijet=0;
    for( auto& Jet : jets ){  //jet loop
    // for (Int_t jetIndex = 0; jetIndex < inputJets.GetEntries(); jetIndex++)
    //{
    //auto jet = *static_cast<TLorentzVector*>(inputJets.At(jetIndex));
      // transverse view of jet 4-vectors
      //jet.SetPtEtaPhiM(jet.Pt(), 0.0, jet.Phi(), jet.M());
      jet.SetPtEtaPhiM(Jet->Pt(),0.0,Jet->Phi(),Jet->M());
      jetID.push_back(VIS->AddLabFrameFourVector(jet));
      ijet++; 
     }
    INV->SetLabFrameThreeVector(met);
   

    if(! (outputntuple->RunNumber==305723 && outputntuple->EventNumber==595517120)){
      if (!LAB->AnalyzeEvent()) cout << "Something went wrong..." << endl;
    }
    



    // Compressed variables from tree
    m_NjV = 0;
    m_NbV = 0;
    m_NjISR = 0;
    m_NbISR = 0;
    //    m_pTjV1 = 0.;
    //    m_pTjV2 = 0.;
    //    m_pTjV3 = 0.;
    m_pTjV4 = 0.;
    //    m_pTjV5 = 0.;
    //    m_pTjV6 = 0.;
    m_pTbV1 = 0.;
    //    m_pTbV2 = 0.;
    //for (int i = 0; i < int(inputJets.GetEntries()); i++) {
    //auto jet = static_cast<Stop0LUtils::Jet*>(inputJets.At(i));

    ijet=0;
    for( auto& Jet : jets ){  //jet loop   
      if (VIS->GetFrame(jetID[ijet]) == *V) { // sparticle group
        m_NjV++;
	//        if (m_NjV == 1)
	//	  m_pTjV1 = Jet->Pt();
	//        if (m_NjV == 2)
	//	  m_pTjV2 = Jet->Pt();
	//        if (m_NjV == 3)
	//	  m_pTjV3 = Jet->Pt();
        if (m_NjV == 4)
	  m_pTjV4 = Jet->Pt();
	//        if (m_NjV == 5)
	//	  m_pTjV5 = Jet->Pt();
	//        if (m_NjV == 6)
	//	  m_pTjV6 = Jet->Pt();

        if (abs(Jet->flav)==5) {
          m_NbV++;
          if (m_NbV == 1)
            m_pTbV1 = Jet->Pt();
	  //          if (m_NbV == 2)
	  //            m_pTbV2 = Jet->Pt();
        }
	
      }
      else {
        m_NjISR++;
        if (abs(Jet->flav)==5)
      	  m_NbISR++;
      }
      ijet++;
    }
   
 
    // need at least one jet associated with sparticle-side of event
    if (m_NjV < 1) {
      m_PTISR = 0.;
      m_RISR = 0.;
      //      m_cosS = 0.;
      m_MS = 0.;
      m_MV = 0.;
      //      m_MISR = 0.;
      //      m_dphiCMI = 0.;
      m_dphiISRI = 0.;
    }
    else {
               
      TVector3 vP_ISR = ISR->GetFourVector(*CM).Vect();
      TVector3 vP_I   = I->GetFourVector(*CM).Vect();
               
      m_PTISR = vP_ISR.Mag();
      m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
      //      m_cosS = S->GetCosDecayAngle();
      m_MS = S->GetMass();
      m_MV = V->GetMass();
      //      m_MISR = ISR->GetMass();
      //      m_dphiCMI = acos(-1.)-fabs(CM->GetDeltaPhiBoostVisible());
      m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));
    }

  

    outputntuple->m_PTISR=m_PTISR;
    outputntuple->m_RISR=m_RISR;
    outputntuple->m_MS=m_MS;
    outputntuple->m_MV=m_MV;
    outputntuple->m_dphiISRI=m_dphiISRI;
    outputntuple->m_pTbV1=m_pTbV1;
    outputntuple->m_NbV=m_NbV;
    outputntuple->m_NjV=m_NjV;
    outputntuple->m_pTjV4=m_pTjV4;

    outputntuple->m_NbISR=m_NbISR;
    outputntuple->m_NjISR=m_NjISR;

    delete LAB;
    delete CM;
    delete S;
    delete ISR;
    delete V;
    delete I;
    delete INV;
    delete InvMass;
    delete VIS;
    delete SplitVis;

  }
