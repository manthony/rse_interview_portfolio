#include "ExportTrees/Export.h"
using namespace std;




void ExportTrees::FillPhotonTriggerWeights(int runnumber){
  Particle photon = (*m_photon);
  double TriggerWeight=1.0;
  
  if(inputntuple->HLT_g120_loose && photon.Pt()>125)
    TriggerWeight=1.0;//GetTriggerWeight("HLT_g120_loose",runnumber);
  else if ( inputntuple->HLT_g50_loose_L1EM15 && photon.Pt()>55)
    TriggerWeight=GetTriggerWeight("HLT_g50_loose_L1EM15",runnumber);
  else if ( inputntuple->HLT_g45_loose_L1EM15 && photon.Pt()>47)
    TriggerWeight=GetTriggerWeight("HLT_g45_loose_L1EM15",runnumber);
  else if ( inputntuple->HLT_g40_loose_L1EM15 && photon.Pt()>42)
    TriggerWeight=GetTriggerWeight("HLT_g40_loose_L1EM15",runnumber);
  else if ( inputntuple->HLT_g35_loose_L1EM15 && photon.Pt()>37)
    TriggerWeight=GetTriggerWeight("HLT_g35_loose_L1EM15",runnumber);
  else
    TriggerWeight=0.0;
  
  outputntuple->TriggerWeight=TriggerWeight;
}



//____________________________________________________
//                       FILL 
//                  photon variables
//____________________________________________________
//
void ExportTrees::FillPhotonVariables(){

  


  outputntuple->nbaselinePh=inputntuple->nbaselinePh;
  
  Particle Z = (*m_Z);
  Particles photons = (*m_photons);
  //Particles truthphotons = (*m_truthphotons);


  TVector2 met_fake=(*m_met_fake);
  TVector2 met=(*m_met);

  double RMPF = -999;
  double photon_weight=1.0;
  double photon_acceptance_weight=1.0;
  if(photons.size()>0){
 
   
    //photon_weight = hphoton->GetBinContent(hphoton->GetXaxis()->FindBin(photons[0]->Pt()),hphoton->GetYaxis()->FindBin(fabs(photons[0]->Eta())));
    double temp = hphotonAcpt->GetBinContent(hphotonAcpt->GetXaxis()->FindBin(photons[0]->Pt()),hphotonAcpt->GetYaxis()->FindBin(photons[0]->Eta()));

    if(temp>0)
      photon_acceptance_weight=1.0/temp;
    else
      photon_acceptance_weight=0.0;
    
    if(photons[0]->Pt()>150){
      TH1F* htemp = (TH1F*)hphoton->ProjectionX("px",hphoton->GetYaxis()->FindBin(fabs(photons[0]->Eta())),hphoton->GetYaxis()->FindBin(fabs(photons[0]->Eta()))+1);
      photon_weight = htemp->GetBinContent(htemp->GetXaxis()->FindBin(photons[0]->Pt()));
      //std::cout << photon_weight << " " <<  photons[0]->Pt() << " " << fabs(photons[0]->Eta()) << std::endl;
      delete htemp;

    }
  
    
    RMPF=1 + (photons[0]->Px()*met.Px() + photons[0]->Py()*met.Py())/(photons[0]->Pt()*photons[0]->Pt()); 
  }
  outputntuple->PhotonReWeight=photon_weight;
  outputntuple->PhotonAcptWeight=photon_acceptance_weight;

 


  if(Z.Pt()>0)
    RMPF=1 + (Z.Px()*met.Px() + Z.Py()*met.Py())/(Z.Pt()*Z.Pt());
  
  outputntuple->RMPF=RMPF;
    
  double pT_1ph=0;
  double eta_1ph=0;
  double phi_1ph=0;
  double dPhi_1ph=-999;
  double topoetcone40_1ph=-99;
  double ptvarcone20_1ph=-99;
  
  if(photons.size()>=1){
    pT_1ph=(*photons[0]).Pt();
    eta_1ph=(*photons[0]).Eta();
    phi_1ph=(*photons[0]).Phi();
    topoetcone40_1ph=(*photons[0]).topoetcone40;
    ptvarcone20_1ph=(*photons[0]).ptvarcone20;
    
    dPhi_1ph=TVector2::Phi_mpi_pi(met_fake.Phi() - (*photons[0]).Phi());	     
  }
     
  outputntuple->topoetcone40_1ph=topoetcone40_1ph;
  outputntuple->ptvarcone20_1ph=ptvarcone20_1ph;


  outputntuple->pT_1ph=pT_1ph;
  outputntuple->phi_1ph=phi_1ph;
  outputntuple->dPhi_1ph=dPhi_1ph;
  outputntuple->eta_1ph=eta_1ph;
  outputntuple->nPh=photons.size();
  
  /*
  double pT_1truthph=0;
  double eta_1truthph=0;
  double phi_1truthph=0;
  double dPhi_1truthph=-999;
  if(truthphotons.size()>=1){
    pT_1truthph=(*truthphotons[0]).Pt();
    eta_1truthph=(*truthphotons[0]).Eta();
    phi_1truthph=(*truthphotons[0]).Phi();
    dPhi_1truthph=TVector2::Phi_mpi_pi(met_fake.Phi() - (*truthphotons[0]).Phi());	     
  }
     
  outputntuple->pT_1truthph=pT_1truthph;
  outputntuple->phi_1truthph=phi_1truthph;
  outputntuple->dPhi_1truthph=dPhi_1truthph;
  outputntuple->eta_1truthph=eta_1truthph;
  outputntuple->ntruthPh=truthphotons.size();
  */

}
