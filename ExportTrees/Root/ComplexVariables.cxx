#include <sstream>

#include "NewMT2/MT2.h"
#include "ExportTrees/Export.h"
using namespace std;


double ExportTrees::amt2_calc(TLorentzVector b1v, TLorentzVector b2v, TLorentzVector lepton,TLorentzVector EtMissVec, double cut) {

  //amt2_calc(b1v, b2v, lepton, EtMissVec);
  //ComputeMT2 mycalc;// = ComputeMT2(b1v,b2v,EtMissVec,0,0);
  //mt2=mycalc.Compute();
  
  TLorentzVector lepb1 = b1v + lepton;
  TLorentzVector lepb2 = b2v + lepton;
  double amt2=0;

  
  if((lepb1.M()>cut)&&(lepb2.M()>cut)) 
    {
      amt2=0;
    }
  else if ((lepb1.M()<cut)&&(lepb2.M()>cut)) 
    {
      ComputeMT2 mycalc(lepb1,b2v,EtMissVec,0,0);
      amt2=mycalc.Compute();
      //amt2=amt2tool->MT2calc(lepb1,b2v,vds,etmiss2D);
      
    }
  else if ((lepb1.M()>cut)&&(lepb2.M()<cut)) 
    {
      ComputeMT2 mycalc(lepb2,b1v,EtMissVec,0,0);
      amt2=mycalc.Compute();
      //amt2=amt2tool->MT2calc(lepb2,b1v,vds,etmiss2D);
    }
  else 
    {
      ComputeMT2 mycalc(lepb1,b2v,EtMissVec,0,0);
      double tmp1=mycalc.Compute();
      ComputeMT2 mycalc2(lepb2,b1v,EtMissVec,0,0);
      double tmp2=mycalc2.Compute();
      amt2=min(tmp1,tmp2);
      //delete mycalc;
      //amt2= min(amt2tool->MT2calc(lepb1,b2v,vds,etmiss2D),amt2tool->MT2calc(lepb2,b1v,vds,etmiss2D));
    }
  return amt2;
  
}


bool ExportTrees::chi2Top(TLorentzVector &topCand0, TLorentzVector &topCand1, TLorentzVector &WCand0, TLorentzVector &WCand1, double &Chi2, int b1Index, int b2Index, Jets m_jets){
  if(m_jets.size() < 4) return false;
  float realWMass = 80.385;
  float realTopMass = 173.210;

  //Chi2 method
  double Chi2min = DBL_MAX;
  int W1j1_low = -1,W1j2_low = -1,W2j1_low = -1,W2j2_low = -1,b1_low = -1,b2_low = -1;

  // First separate light from b-jets
  std::vector<TLorentzVector> bjets;  //fill with TLorentzVectors of the two signal jets with the highest b-tag
  std::vector<TLorentzVector> ljets;  //fill with TLorentzVectors of ALL OTHER signal jets (even if they are b-tagged)

  for(unsigned int j=0;j<m_jets.size();j++){
    TLorentzVector tempTLV(0,0,0,0);
    tempTLV.SetPtEtaPhiM(m_jets[j]->Pt(),m_jets[j]->Eta(),m_jets[j]->Phi(),m_jets[j]->M());
    if((int)j==b1Index || (int)j==b2Index){
      bjets.push_back((tempTLV));
    }
    else{
      ljets.push_back((tempTLV));
    }
  }
  for(int W1j1=0; W1j1<(int)ljets.size(); W1j1++) {// <------------------This lines has to be replaced
    for(int W2j1=0;W2j1<(int)ljets.size(); W2j1++) {// <------------------This lines has to be replaced
      if (W2j1==W1j1) continue;// <------------------This lines has to be added
      //      for(int W1j1=0; W1j1<(int)ljets.size()-1; W1j1++) {
      //      for(int W2j1=W1j1+1;W2j1<(int)ljets.size(); W2j1++) {
      for(int b1=0;b1<(int)bjets.size();b1++){
	for(int b2=0;b2<(int)bjets.size();b2++){
	  if(b2==b1) continue;
	  double chi21, chi22, mW1, mW2, mt1, mt2;
	  //try bl,bl top candidates
	  if(W2j1>W1j1){
	    mW1 = ljets.at(W1j1).M();
	    mW2 = ljets.at(W2j1).M();
	    mt1 = (ljets.at(W1j1) + bjets.at(b1)).M();
	    mt2 = (ljets.at(W2j1) + bjets.at(b2)).M();
	    chi21 = (mW1-realWMass)*(mW1-realWMass)/realWMass + (mt1-realTopMass)*(mt1-realTopMass)/realTopMass;
	    chi22 = (mW2-realWMass)*(mW2-realWMass)/realWMass + (mt2-realTopMass)*(mt2-realTopMass)/realTopMass;
	    if(Chi2min > (chi21 + chi22)){
	      Chi2min = chi21 + chi22;
	      if(chi21 < chi22){
		W1j1_low = W1j1;
		W1j2_low = -1;
		W2j1_low = W2j1;
		W2j2_low = -1;
		b1_low = b1;
		b2_low = b2;
	      }
	      else{
		W2j1_low = W1j1;
		W2j2_low = -1;
		W1j1_low = W2j1;
		W1j2_low = -1;
		b2_low = b1;
		b1_low = b2;
	      }
	    }
	  }
	  if(ljets.size() < 3) continue;
	  for(int W1j2=W1j1+1;W1j2<(int)ljets.size(); W1j2++) {
	    if(W1j2==W2j1) continue;
	        
	    //try bll,bl top candidates
	    mW1 = (ljets.at(W1j1) + ljets.at(W1j2)).M();
	    mW2 = ljets.at(W2j1).M();
	    mt1 = (ljets.at(W1j1) + ljets.at(W1j2) + bjets.at(b1)).M();
	    mt2 = (ljets.at(W2j1) + bjets.at(b2)).M();
	    chi21 = (mW1-realWMass)*(mW1-realWMass)/realWMass + (mt1-realTopMass)*(mt1-realTopMass)/realTopMass;
	    chi22 = (mW2-realWMass)*(mW2-realWMass)/realWMass + (mt2-realTopMass)*(mt2-realTopMass)/realTopMass;
	    if(Chi2min > (chi21 + chi22)){
	      Chi2min = chi21 + chi22;
	      if(chi21 < chi22){
		W1j1_low = W1j1;
		W1j2_low = W1j2;
		W2j1_low = W2j1;
		W2j2_low = -1;
		b1_low = b1;
		b2_low = b2;
	      }
	      else{
		W2j1_low = W1j1;
		W2j2_low = W1j2;
		W1j1_low = W2j1;
		W1j2_low = -1;
		b2_low = b1;
		b1_low = b2;
	      }
	    }
	    if(ljets.size() < 4)continue;
	    //try bll, bll top candidates
	    for(int W2j2=W2j1+1;W2j2<(int)ljets.size(); W2j2++){
	      if((W2j2==W1j1) || (W2j2==W1j2)) continue;
	      if(W2j1<W1j1) continue;  //runtime reasons, we don't want combinations checked twice <--------------------This line should be added
	      mW1 = (ljets.at(W1j1) + ljets.at(W1j2)).M();
	      mW2 = (ljets.at(W2j1) + ljets.at(W2j2)).M();
	      mt1 = (ljets.at(W1j1) + ljets.at(W1j2) + bjets.at(b1)).M();
	      mt2 = (ljets.at(W2j1) + ljets.at(W2j2) + bjets.at(b2)).M();
	      chi21 = (mW1-realWMass)*(mW1-realWMass)/realWMass + (mt1-realTopMass)*(mt1-realTopMass)/realTopMass;
	      chi22 = (mW2-realWMass)*(mW2-realWMass)/realWMass + (mt2-realTopMass)*(mt2-realTopMass)/realTopMass;
	      if(Chi2min > (chi21 + chi22)){
		Chi2min = chi21 + chi22;
		if(chi21 < chi22){
		  W1j1_low = W1j1;
		  W1j2_low = W1j2;
		  W2j1_low = W2j1;
		  W2j2_low = W2j2;
		  b1_low = b1;
		  b2_low = b2;
		}
		else{
		  W2j1_low = W1j1;
		  W2j2_low = W1j2;
		  W1j1_low = W2j1;
		  W1j2_low = W2j2;
		  b2_low = b1;
		  b1_low = b2;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  if(W1j2_low == -1) WCand0 = ljets.at(W1j1_low);
  else WCand0 = ljets.at(W1j1_low) + ljets.at(W1j2_low);
  topCand0 = WCand0 + bjets.at(b1_low);
  if(W2j2_low == -1) WCand1 = ljets.at(W2j1_low);
  else WCand1 = ljets.at(W2j1_low) + ljets.at(W2j2_low);
  topCand1 = WCand1 + bjets.at(b2_low);
  Chi2 = Chi2min;
  return true;
}


std::pair <double,double>  ExportTrees::RecoHadTops(int ibtop1, int ibtop2, int nbjets, Jets jets){    
 
  //==== Hadronic top reconstruction ====
  // DRmin implemented as in https://cds.cern.ch/record/1570992/files/ATL-COM-PHYS-2013-1092.pdf
  // 1- select two highest btag-weights bjets
  // 2- two closest light jets --> W1_had --> add closest bjet from before --> t1_had
  // 3- next two closest light jets --> W2_had --> add remaining bjet from before --> t2_had
  //
  // Update: by default the leading good electron and/or muon are added 'as' jets. Although no effect is obviously expected
  //         in the SRs (since we apply lepton veto), it is needed for the 1-lepton CR afterwards.
  //         This way we avoid doubling the number of variables.
  //
  
  std::pair <double,double> Top_Reco_minDR;
  double m_top_minDR1=-99; 
  double m_top_minDR2=-99;
  int Wjet1, Wjet2;
  int Wjet3, Wjet4;
  int bjet1 = -1;
  int bjet2 = -1;
 
  //if(ibtop1<0){
  //  m_top_minDR
  //  m_top_minDR2 
  //}
  
  if (nbjets>=1 && jets.size()>=3){
       
    Wjet1=Wjet2=-1;
       
    double mindr = 1000.;
       
    for (unsigned int i = 0 ; i < jets.size() ; i++){
      for (unsigned int j = i+1 ; j < jets.size() ; j++)
	{
	  if((jets.size()>3 && (int)i!=ibtop1 && (int)i!=ibtop2 && (int)j!=ibtop1 && (int)j!=ibtop2) || (jets.size()==3 && (int)i!=ibtop1 && (int)j!=ibtop1)){
	    double curr_dr = (*jets[i]).DeltaR(*jets[j]);
 
	    if (curr_dr<mindr)
	      {
		Wjet1 = i;
		Wjet2 = j;
		mindr = curr_dr;
	      }
	  }
	}
    }
 
 
    //book first W
    TLorentzVector W1candidate = (*jets[Wjet1])+(*jets[Wjet2]);
 
    mindr = 1000.;
    for (unsigned int i = 0 ; i < jets.size() ; i++)
      {
	if((jets.size()>3 && ((int)i==ibtop1 || (int)i==ibtop2)) || (jets.size()==3 &&  (int)i==ibtop1)){         
	  double curr_dr = (*jets[i]).DeltaR(W1candidate);
             
	  if (curr_dr<mindr)
	    {
	      bjet1 = i;
	      mindr = curr_dr;
	    }
	}
      }
 
    if(bjet1<0){
      m_top_minDR1 = 0;
      m_top_minDR2 = 0;
     
    }
 
    //book first top
    TLorentzVector top1candidate = (*jets[bjet1])+W1candidate;
     
     
 
    if (nbjets>=2 && jets.size()>=6){    
      Wjet3=Wjet4=-1;
      mindr = 1000.;
      for (unsigned int i = 0 ; i < jets.size() ; i++){
        for (unsigned int j = i+1 ; j < jets.size() ; j++){
         
          if((int)i!=ibtop1 && (int)i!=ibtop2 && (int)j!=ibtop1 && (int)j!=ibtop2){    
            if ((int)i!=Wjet1 && (int)i!=Wjet2 && (int)j!=Wjet1 && (int)j!=Wjet2)
              {
                double curr_dr = (*jets[i]).DeltaR(*jets[j]);
               
                if (curr_dr<mindr)
                  {
                    Wjet3 = i;
                    Wjet4 = j;
                    mindr = curr_dr;
                  }
              }
          }
        }      
      }
 
      m_top_minDR1 = top1candidate.M();
     
      //book second W
      TLorentzVector W2candidate = (*jets[Wjet3])+(*jets[Wjet4]);
 
      mindr = 1000.;      
      for (unsigned int i = 0 ; i < jets.size() ; i++){
        if ((int)i!=bjet1 && ((int)i==ibtop1 || (int)i==ibtop2))
          {
            double curr_dr = (*jets[i]).DeltaR(W2candidate);
           
            if (curr_dr<mindr)
              {
                bjet2 = i;
                mindr = curr_dr;
              }
          }
      }
     
      //book second top
      if(bjet2>=0){
        TLorentzVector top2candidate = (*jets[bjet2])+W2candidate;
        m_top_minDR2 = top2candidate.M();
      }
 
    }
  }
  Top_Reco_minDR = std::make_pair(m_top_minDR1,m_top_minDR2); 
  return Top_Reco_minDR;

}

//____________________________________________________
//                    FILL 
//               complex variables
//____________________________________________________
//
void ExportTrees::FillComplexVariables(){

  Particles photons = (*m_photons);
  Particles leptons = (*m_leptons);

  TVector2 met_fake = (*m_met_fake);
  Particle l1;
  Particle l2;
  Particle l3;
  int nLep = leptons.size();
  if(nLep>=1){
    l1=(*leptons[0]);
    if(nLep>=2){
      l2=(*leptons[1]);
      if(nLep>=3){
	l3=(*leptons[2]);
      }
    }
  }
  

  Jet j1,j2,j3,j4;
  Jet lj1,lj2,lj3;
  Jet b1,b2,b1_85,b2_85,b1_60,b2_60;
  Jet b3,b4;

  Jets jets = (*m_jets);
  Jets bjets = (*m_bjets);
   
  
  int njets = jets.size();
  int nbjets = bjets.size();

	  

  if(njets>0){
    j1 = (*jets[0]);
    if(njets>1){
      j2 = (*jets[1]);
      if(njets>2){
    	j3 = (*jets[2]);
    	if(njets>3){
    	  j4 = (*jets[3]);
    	}
      }
    }
  }
 
  if(nbjets>0){
    b1 = (*bjets[0]);
    if(nbjets>1){
      b2 = (*bjets[1]);
    }
  }



  double dPhi_1ph1jet = -999;
  Particle y1;
  if(photons.size()>0)
    y1=(*photons[0]);
  if(y1.Pt()>0 && j1.Pt()>0)
    dPhi_1ph1jet=TVector2::Phi_mpi_pi(y1.Phi() - j1.Phi());	    
  outputntuple->dPhi_1ph1jet=dPhi_1ph1jet;

  double mjy =0;
  if(photons.size()>0 && njets>0){
    mjy=((*photons[0])+j1).M();
  }
  outputntuple->mjy=mjy;



  double amt2[4]={0.,0.,0.,0.};
  double mt2[4]={0.,0.,0.,0.};
  TLorentzVector EtMissVec;
  EtMissVec.SetPxPyPzE(met_fake.Px(),met_fake.Py(),0,met_fake.Mod());
  
  double MTb1l=0;
  double MTb2l=0;
  double topness =0;
  double centrality=0;
  
  ComputeMT2 mycalc(j1,j2,EtMissVec,0,0);
  mt2[0]=mycalc.Compute();
  
  ComputeMT2 mycalc2(b1,b2,EtMissVec,0,0);
  mt2[1]=mycalc2.Compute();
  
  amt2[0]=amt2_calc(b1, b2, l1,EtMissVec, 160);
  amt2[1]=amt2_calc(b1, b2, l1,EtMissVec, 170);
  amt2[2]=amt2_calc(b1, b2, l1,EtMissVec, 180);
  amt2[3]=amt2_calc(b1, b2, l1,EtMissVec, 200);

  ComputeMT2 mycalc3(j1+l1,j2+l2,EtMissVec,0,0);
  mt2[2]=mycalc3.Compute();
  ComputeMT2 mycalc4(j2+l1,j1+l2,EtMissVec,0,0);
  mt2[3]=mycalc4.Compute();

  // TopnessTool TopTool(l1,met_fake,j1,j2,true);
  // topness = TopTool.topness();
  // centrality = max( 
  // 		   fabs(TVector2::Phi_mpi_pi(j1.Phi() - (j2+l1+EtMissVec).Phi())),
  // 		   fabs(TVector2::Phi_mpi_pi(j2.Phi() - (j1+l1+EtMissVec).Phi()))
  // 		    );
  
  double mlb1=(l1+b1).M();
  double mlb2=(l1+b2).M();
  
  MTb1l = sqrt(2*(met_fake.Mod())*(l1+b1).Et() - 2*((met_fake.Px())*(l1+b1).Px() + (met_fake.Py())*(l1+b1).Py()));
  MTb2l = sqrt(2*(met_fake.Mod())*(l1+b2).Et() - 2*((met_fake.Px())*(l1+b2).Px() + (met_fake.Py())*(l1+b2).Py()));
  
  double m_MT2ll=0;
  if(nLep>=2){
    ComputeMT2 mycalcll(l1, l2, EtMissVec, 0, 0);
    m_MT2ll=mycalcll.Compute();
  }
  outputntuple->m_MT2ll=m_MT2ll;

  outputntuple->MTblmin=min(MTb1l,MTb2l);
  outputntuple->topness = topness;
  
  outputntuple->centrality = centrality;
  
  outputntuple->mlbmin= min(mlb1,mlb2);
  
  outputntuple->amt2.clear();
  outputntuple->amt2.push_back(amt2[0]);
  outputntuple->amt2.push_back(amt2[1]);
  outputntuple->amt2.push_back(amt2[2]);
  outputntuple->amt2.push_back(amt2[3]);
  
  outputntuple->mt2.clear();
  outputntuple->mt2.push_back(mt2[0]);
  outputntuple->mt2.push_back(mt2[1]);
  outputntuple->mt2.push_back(mt2[2]);
  outputntuple->mt2.push_back(mt2[3]);
  
  //outputntuple->mvh=mvh;

  double dRj1j2,dRlj1,dRlj2;
  dRj1j2=dRlj1=dRlj2=0;
  dRj1j2 = j1.DeltaR(j2);
  dRlj1 = l1.DeltaR(j1);
  dRlj2 = l1.DeltaR(j2);

  outputntuple->dRj1j2=dRj1j2;
  outputntuple->dRlj1=dRlj1;
  outputntuple->dRlj2=dRlj2;
  outputntuple->dRljmin=min(dRlj1,dRlj2);
  outputntuple->dRljmax=max(dRlj1,dRlj2);

  // ------> Reconstruction of top candidates using the minimum DR method

  int ibtop1=-1; //book two highest btag-weight jets for later use
  int ibtop2=-1;
  int iblead1=-1;
  int iblead2=-1;
  float maxbw1=-99;
  float maxbw2=-99;
  auto ijet=0;
    for( auto& jet : jets ){  //jet loop
 
      //if( jet->flav==5 && abs(jet->Eta())<2.5){
      if (abs(jet->Eta())<2.5) {

      if(iblead1<0)//leadings
      iblead1=ijet;
      else if(iblead2<0)
      iblead2=ijet;
 
      float locbw = jet->MV2; //book high bweight jets
      if(locbw > maxbw1){
      maxbw2 = maxbw1;     
      ibtop2 = ibtop1;
      ibtop1 = ijet;
      maxbw1 = locbw;
      }
      else if(locbw > maxbw2){
      ibtop2 = ijet;
      maxbw2 = locbw;
      }
    }
 
    ijet++;
 
  }

  std::pair <double,double>  Top_Reco_minDR;
  if (ibtop1 > 0 && ibtop2 > 0)
    Top_Reco_minDR = RecoHadTops(ibtop1, ibtop2, nbjets, jets);

  outputntuple->m_top_minDR1=Top_Reco_minDR.first;
  outputntuple->m_top_minDR2=Top_Reco_minDR.second;
  
  double dRb1b2 =0;
  double pT_1leadbtag = -99;
  double pT_2leadbtag =-99;
  double dRlb1,dRlb2,mindR;
  dRlb1=dRlb2=mindR=999;

  if(ibtop1>=0) {
    pT_1leadbtag=(*jets[ibtop1]).Pt();
    if(ibtop2>=0) {
      pT_2leadbtag=(*jets[ibtop2]).Pt();
      dRb1b2 = (*jets[ibtop1]).DeltaR((*jets[ibtop2]));
      dRlb1=(*jets[ibtop1]).DeltaR(l1);
      dRlb2=(*jets[ibtop2]).DeltaR(l1);
      mindR = dRlb1;
      if (dRlb1 > dRlb2){
        mindR = dRlb2;
      }
    }
  }
  outputntuple->dRb1b2=dRb1b2;
  outputntuple->dRlbmin=mindR;
  outputntuple->pT_1leadbtag=pT_1leadbtag;
  outputntuple->pT_2leadbtag=pT_2leadbtag;

  //  double m_top0Chi2Pt = -99;
  //  double m_top0Chi2Phi = -99;
  //  double m_top0Chi2Eta = -99;
  double m_top0Chi2M = -99;
  //  double m_top1Chi2Pt = -99;
  //  double m_top1Chi2Phi = -99;
  //  double m_top1Chi2Eta = -99;
  double m_top1Chi2M = -99;
  //  double m_w0Chi2Pt = -99;
  //  double m_w0Chi2Phi = -99;
  //  double m_w0Chi2Eta = -99;
  double m_w0Chi2M = -99;
  //  double m_w1Chi2Pt = -99;
  //  double m_w1Chi2Phi = -99;
  //  double m_w1Chi2Eta = -99;
  double m_w1Chi2M = -99;
  double m_mt2Chi2 = -99;
  double m_chi2 = -1.;

  // Chi2 top
  TLorentzVector top0Chi2;
  TLorentzVector top1Chi2;
  TLorentzVector w0Chi2;
  TLorentzVector w1Chi2;

  TLorentzVector m_top0;
  TLorentzVector m_top1;

  if (ibtop1 >= 0 && ibtop2 >= 0){  
    bool chi2top = chi2Top(top0Chi2, top1Chi2, w0Chi2, w1Chi2, m_chi2, ibtop1, ibtop2, jets);
  
    if(chi2top){
      //      m_top0Chi2Pt  = top0Chi2.Pt();
      //      m_top0Chi2Eta = top0Chi2.Eta();
      //  m_top0Chi2Phi = top0Chi2.Phi();
      m_top0Chi2M   = top0Chi2.M();

      //  m_top1Chi2Pt  = top1Chi2.Pt();
      //  m_top1Chi2Eta = top1Chi2.Eta();
      //  m_top1Chi2Phi = top1Chi2.Phi();
      m_top1Chi2M   = top1Chi2.M();

      //  m_w0Chi2Pt  = w0Chi2.Pt();
      //  m_w0Chi2Eta = w0Chi2.Eta();
      //  m_w0Chi2Phi = w0Chi2.Phi();
      m_w0Chi2M   = w0Chi2.M();

      //  m_w1Chi2Pt  = w1Chi2.Pt();
      //  m_w1Chi2Eta = w1Chi2.Eta();
      //  m_w1Chi2Phi = w1Chi2.Phi();
      m_w1Chi2M   = w1Chi2.M();
      
      m_top0.SetPtEtaPhiM(top0Chi2.Pt(),0,top0Chi2.Phi(),173.210);
      m_top1.SetPtEtaPhiM(top1Chi2.Pt(),0,top1Chi2.Phi(),173.210);

    //Calculate MT2

        ComputeMT2 mt2class = ComputeMT2(m_top0, m_top1, EtMissVec);
        m_mt2Chi2 = mt2class.Compute();
      
   }
  }
  outputntuple->m_top0Chi2M=m_top0Chi2M;
  outputntuple->m_top1Chi2M=m_top1Chi2M;
  outputntuple->m_w0Chi2M=m_w0Chi2M;
  outputntuple->m_w1Chi2M=m_w1Chi2M;
  outputntuple->m_mt2Chi2=m_mt2Chi2;
  outputntuple->m_chi2=m_chi2;


}
