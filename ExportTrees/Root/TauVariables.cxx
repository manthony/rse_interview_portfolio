#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                  Reco Tau variables
//____________________________________________________
//
void ExportTrees::FillTauVariables(){
 
  //outputntuple->tau_pt=inputntuple->tau_pt;
  // outputntuple->tau_eta=inputntuple->tau_eta;
  // outputntuple->tau_phi=inputntuple->tau_phi;
  //outputntuple->tau_e=inputntuple->tau_e;
  //outputntuple->tau_nPi0=inputntuple->tau_nPi0;
  
  outputntuple->pt_tau=0;
  outputntuple->eta_tau=-9;
  outputntuple->phi_tau=-9;
  outputntuple->e_tau=0;
  outputntuple->nPi0_tau=-1;

  outputntuple->bdtJet_tau=0;
  outputntuple->bdtEl_tau=0;

  outputntuple->nH_tau=-1;
  outputntuple->nWTracks_tau=-1;
  outputntuple->nTracks_tau=-1;
  outputntuple->nPi0_tau=-1;
  outputntuple->nCharged_tau=-1;
  outputntuple->nNeut_tau=-1;
  

  
    
  Particles taus= (*m_taus);
  if (taus.size()>0){
    outputntuple->pt_tau=(*taus[0]).Pt();
    outputntuple->eta_tau=(*taus[0]).Eta();
    outputntuple->phi_tau=(*taus[0]).Phi();
    outputntuple->e_tau=(*taus[0]).E();
   
    outputntuple->bdtJet_tau=(*taus[0]).bdtJet;
    outputntuple->bdtEl_tau=(*taus[0]).bdtEl;
    outputntuple->nH_tau=(*taus[0]).nH;
    outputntuple->nWTracks_tau=(*taus[0]).nWTracks;
    outputntuple->nTracks_tau=(*taus[0]).nTracks;
    outputntuple->nPi0_tau=(*taus[0]).nPi0;
    outputntuple->nCharged_tau=(*taus[0]).nCharged;
    outputntuple->nNeut_tau=(*taus[0]).nNeut;

  }
  
  //number of reco taus
  outputntuple->n_Taus=m_taus->size();

  double maxbdtTauJet=0;
  for(const auto &tau: *m_taus){
    if(tau->bdtJet>maxbdtTauJet) maxbdtTauJet=tau->bdtJet;
  }
  
  outputntuple->maxbdtTauJet=maxbdtTauJet;
  
  
  // reco taus, from MasterShef conf: pt>20 GeV, |eta|<2.5  
  
  
  int nTruthTaus=0;
  int nTruthTaus_inA=0;
  int nTruthTaus_outA=0;
  
  int nRecoTaus_matched=0;
  
  int nRecoTaus_matched_TruthTaus=0;

  if(inputntuple->truthJet_pt != nullptr){
    //loop over truth jets
    for(unsigned int j=0; j< inputntuple->truthJet_pt->size();j++){
        // get their pt, flav and which reco tau index they may/may not be matched to
        double pt=(*inputntuple->truthJet_pt)[j]/1000.;
        double eta=(*inputntuple->truthJet_eta)[j];
	//double phi=(*inputnutple->truthJet_phi)[j];
        int flav=(*inputntuple->truthJet_flav)[j];
        int matched_tau = -1;
        if(inputntuple->truthJet_matched_tau != nullptr) {matched_tau=(*inputntuple->truthJet_matched_tau)[j];} // POINTER CHECK!

	if(matched_tau>=0)nRecoTaus_matched++;
	if(matched_tau>=0 && flav==15){
	  nRecoTaus_matched_TruthTaus++;
	}


        if(flav==15){
            nTruthTaus++;
            if(pt>20 && fabs(eta)<2.5) nTruthTaus_inA++; // TAU ID REQUIREMENTS!
            else nTruthTaus_outA++;
        }   
    }//end of loop over truth jets
    
  }//pointer check
  



  int nFakeTaus = m_taus->size() - nRecoTaus_matched; //   DJT calls fake news on these 
 

  //OUTPUTNTUPLE_DUMP STEPS

  outputntuple->nTruthTaus=nTruthTaus;
  outputntuple->nFakeTaus=nFakeTaus;
  outputntuple->nTruthTaus_inA=nTruthTaus_inA;
  outputntuple->nTruthTaus_outA=nTruthTaus_outA;
  outputntuple->nRecoTaus_matched=nRecoTaus_matched;
  outputntuple->nRecoTaus_matched_TruthTaus=nRecoTaus_matched_TruthTaus;
  
  
  
}

