#include <FourMomUtils/xAODP4Helpers.h>
#include "ExportTrees/Export.h"

using namespace std;

//____________________________________________________
//                       FILL 
//                jetMET variables
//____________________________________________________
//
void ExportTrees::FillJetMETVariables(){
  
  Jet j1,j2,j3,j4;
  Jet lj1,lj2;
  Jet b1,b2,b1_85,b2_85,b1_60,b2_60;
  Jet b3,b4;

  Jets jets = (*m_jets);
  Jets bjets = (*m_bjets);
   
  
  int njets = jets.size();
  int nbjets = bjets.size();

  int nbjets35=outputntuple->num_bjets35;
  	  

  if(njets>0){
    j1 = (*jets[0]);
    if(njets>1){
      j2 = (*jets[1]);
      if(njets>2){
    	j3 = (*jets[2]);
    	if(njets>3){
    	  j4 = (*jets[3]);
    	}
      }
    }
  }
 
  if(nbjets>0){
    b1 = (*bjets[0]);
    if(nbjets>1){
      b2 = (*bjets[1]);
    }
  }
  

  TVector2 met_nom  = (*m_met);
  TVector2 met_fake = (*m_met_fake);
  TVector2 met_simple = (*m_met_simple);
  
  int isPlateau=0;
  if  (j1.Pt()>80 && (met_nom.Mod())>((155*j1.Pt()-8000)/(j1.Pt()-60)))
    isPlateau = 1;

  outputntuple->TrigAtPlat=isPlateau;
 


  double dPhi_1jet =-999; 
  double dPhi_2jet =-999; 
  double dPhi_3jet =-999; 
  double dPhi_4jet =-999; 
  
  double dPhi_1bjet =-999;
  double dPhi_2bjet =-999;

  double dPhi_1jet_simple =-999;
  double dPhi_2jet_simple =-999;
  double dPhi_3jet_simple =-999;
  double dPhi_4jet_simple =-999;

  double dPhi_1bjet_simple =-999;
  double dPhi_2bjet_simple =-999;
  
  double dPhi_1j2j = -999;
  double dPhi_2j3j = -999;
  double dPhi_1j3j = -999;

  double dPhi_4jet_sys = -999;
  double dPhi_2bjet_sys = -999;
  
  if(njets>=1){
    dPhi_1jet = TVector2::Phi_mpi_pi(met_fake.Phi() - j1.Phi());
    dPhi_1jet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - j1.Phi());
  }
  if(njets>=2)
    {
      dPhi_2jet = TVector2::Phi_mpi_pi(met_fake.Phi() - j2.Phi());
      dPhi_2jet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - j2.Phi());
      dPhi_1j2j = TVector2::Phi_mpi_pi(j1.Phi() - j2.Phi());
    }
  if(njets>=3)
    {
      dPhi_3jet = TVector2::Phi_mpi_pi(met_fake.Phi() - j3.Phi());
      dPhi_3jet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - j3.Phi());
      dPhi_2j3j = TVector2::Phi_mpi_pi(j2.Phi() - j3.Phi());
      dPhi_1j3j = TVector2::Phi_mpi_pi(j1.Phi() - j3.Phi());
    }
  if(njets>=4)
    {
      dPhi_4jet = TVector2::Phi_mpi_pi(met_fake.Phi() - j4.Phi());
      dPhi_4jet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - j4.Phi());
      dPhi_4jet_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - (j1+j2+j3+j4).Phi());
    }
  
  if(nbjets>=1 )
    {
      dPhi_1bjet = TVector2::Phi_mpi_pi(met_fake.Phi() - b1.Phi());
      dPhi_1bjet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - b1.Phi());
    }

  if(nbjets>=2)
    {
      dPhi_2bjet = TVector2::Phi_mpi_pi(met_fake.Phi() - b2.Phi());
      dPhi_2bjet_simple = TVector2::Phi_mpi_pi(met_simple.Phi() - b2.Phi());
      dPhi_2bjet_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - (b1+b2).Phi());
    }
  
  outputntuple->dPhi_1jet=dPhi_1jet;
  outputntuple->dPhi_2jet=dPhi_2jet;
  outputntuple->dPhi_3jet=dPhi_3jet;
  outputntuple->dPhi_4jet=dPhi_4jet;
  
  outputntuple->dPhi_1bjet=dPhi_1bjet;
  outputntuple->dPhi_2bjet=dPhi_2bjet;
  
  outputntuple->dPhi_4jet_sys=dPhi_4jet_sys;
  outputntuple->dPhi_2bjet_sys=dPhi_2bjet_sys;

  outputntuple->dPhi_1jet_simple=dPhi_1jet_simple;
  outputntuple->dPhi_2jet_simple=dPhi_2jet_simple;
  outputntuple->dPhi_3jet_simple=dPhi_3jet_simple;
  outputntuple->dPhi_4jet_simple=dPhi_4jet_simple;

  outputntuple->dPhi_1bjet_simple=dPhi_1bjet_simple;
  outputntuple->dPhi_2bjet_simple=dPhi_2bjet_simple;

  outputntuple->dPhi_1j2j=dPhi_1j2j;
  outputntuple->dPhi_2j3j=dPhi_2j3j;
  outputntuple->dPhi_1j3j=dPhi_1j3j;


  outputntuple->dPhi_b1b2=TVector2::Phi_mpi_pi(b1.Phi() - b2.Phi());
  
  outputntuple->dphimin4 = min ( min(fabs(dPhi_1jet),fabs(dPhi_2jet)),min(fabs(dPhi_3jet),fabs(dPhi_4jet)));
  outputntuple->dphimin3 = min ( min(fabs(dPhi_1jet),fabs(dPhi_2jet)),fabs(dPhi_3jet));
  outputntuple->dphimin2 = min (fabs(dPhi_1jet),fabs(dPhi_2jet));

  outputntuple->dphimin4_simple = min ( min(fabs(dPhi_1jet_simple),fabs(dPhi_2jet_simple)),min(fabs(dPhi_3jet_simple),fabs(dPhi_4jet_simple)));
  outputntuple->dphimin3_simple = min ( min(fabs(dPhi_1jet_simple),fabs(dPhi_2jet_simple)),fabs(dPhi_3jet_simple));
  outputntuple->dphimin2_simple = min (fabs(dPhi_1jet_simple),fabs(dPhi_2jet_simple));   


  double dphimin4_35=999;
  for(unsigned int ii=0;ii<jets.size();ii++){
    if( (*jets[ii]).Pt() < 35.)
      continue;
    if(ii>3)
      break;
    
    double dphi= fabs(TVector2::Phi_mpi_pi((*jets[ii]).Phi() - met_fake.Phi()));
    if(dphi<dphimin4_35)
      dphimin4_35=dphi;
  }
  outputntuple->dphimin4_35=dphimin4_35;
    



  
  dPhi_1jet =-999; 
  dPhi_2jet =-999; 
  dPhi_3jet =-999; 
  dPhi_4jet =-999; 
  
  dPhi_1bjet =-999;
  dPhi_2bjet =-999;
  
  if(njets>=1)
    dPhi_1jet = TVector2::Phi_mpi_pi(met_nom.Phi() - j1.Phi());
  if(njets>=2)
    dPhi_2jet = TVector2::Phi_mpi_pi(met_nom.Phi() - j2.Phi());
  if(njets>=3)
    dPhi_3jet = TVector2::Phi_mpi_pi(met_nom.Phi() - j3.Phi());
  if(njets>=4)
    dPhi_4jet = TVector2::Phi_mpi_pi(met_nom.Phi() - j4.Phi());
  
  
  if(nbjets>=1 )
   dPhi_1bjet = TVector2::Phi_mpi_pi(met_nom.Phi() - b1.Phi());
  
  if(nbjets>=2)
   dPhi_2bjet = TVector2::Phi_mpi_pi(met_nom.Phi() - b2.Phi());
  
  
  outputntuple->dPhi_1jet_orig=dPhi_1jet;
  outputntuple->dPhi_2jet_orig=dPhi_2jet;
  outputntuple->dPhi_3jet_orig=dPhi_3jet;
  outputntuple->dPhi_4jet_orig=dPhi_4jet;
  
  outputntuple->dPhi_1bjet_orig=dPhi_1bjet;
  outputntuple->dPhi_2bjet_orig=dPhi_2bjet;
  
  outputntuple->dphimin4_orig = min ( min(fabs(dPhi_1jet),fabs(dPhi_2jet)),min(fabs(dPhi_3jet),fabs(dPhi_4jet)));
  outputntuple->dphimin2_orig = min(fabs(dPhi_1jet),fabs(dPhi_2jet));
 
  double MTbmin_orig=0;
  double MTbmax_orig=0;
 
  if(nbjets>=1)
    {
      int ref_min=0;
      int ref_max=0;
      double dPhi_bjet;
      double dPhi_min=999;
      double dPhi_max=0;

       for(int ii=0;ii<nbjets;ii++){
         dPhi_bjet = TVector2::Phi_mpi_pi((*bjets[ii]).Phi() - met_nom.Phi());
         if (fabs(dPhi_bjet) < fabs(dPhi_min) )  {
           dPhi_min = dPhi_bjet;
           ref_min = ii;
         }
         if (fabs(dPhi_bjet) > fabs(dPhi_max) )  {
           dPhi_max = dPhi_bjet;
           ref_max = ii;
         }
       }

       MTbmin_orig = sqrt(2.*(met_nom.Mod())*(*bjets[ref_min]).Pt() *(1 - TMath::Cos(dPhi_min)));
       MTbmax_orig = sqrt(2.*(met_nom.Mod())*(*bjets[ref_max]).Pt() *(1 - TMath::Cos(dPhi_max)));

     }


  
  // //______________________________________________________
  // // vector downstream for mct,mt2 etc. 
  // // we just set to 0 tlorentzvector (aka no boost)
  // //______________________________________________________
   TLorentzVector vds;                   
   vds.SetPxPyPzE(0,0,0,0);
  
  // //
  // // all these variables made with fake met only!!! aka fake met from photon or Z
  // //
  

   double meff2j,meff3j;
   meff2j=meff3j=0;
  
     meff2j=met_fake.Mod() + j1.Pt() + j2.Pt();
    meff3j=meff2j + j3.Pt();
   
    outputntuple->meff2j=meff2j;
    outputntuple->meff3j=meff3j;
   
   double meff = outputntuple->HT;
   meff+=met_fake.Mod();
   outputntuple->meff = meff;
   outputntuple->MET_sig = met_fake.Mod()/meff; 
     
  // // mCT variable!
   //TMctLib* mcttool = new TMctLib();
  // //boostcorrected	
   //double mctcorr2b = 0; 
  // if(nbjets>=2)
  //   {
  //     mctcorr2b=mcttool->mctcorr(b1,b2,vds,met_fake,14000.0);
  //   }
  // double mctcorrjj= 0;
  // if(njets>=2)
  //   {
  //     mctcorrjj=mcttool->mctcorr(j1,j2,vds,met_fake,14000.0);
  //   }
  // delete mcttool;
     
     
  // outputntuple->mctcorr2b=mctcorr2b;
  // outputntuple->mctcorrjj=mctcorrjj;


   double MTj1=0;
   double MTj2=0;
   double MTj3=0;
   double MTj4=0;
  
  
   double MTjmin=0;
   if(njets>=2)
     {
       MTj1 = sqrt(2*(met_fake.Mod())*j1.Et() - 2*((met_fake.Px())*j1.Px() + (met_fake.Py())*j1.Py()));
       MTj2 = sqrt(2*(met_fake.Mod())*j2.Et() - 2*((met_fake.Px())*j2.Px() + (met_fake.Py())*j2.Py()));
       MTjmin=min(MTj1,MTj2);
      }
  if(njets>=3)
     {
       MTj3 = sqrt(2*(met_fake.Mod())*j3.Et() - 2*((met_fake.Px())*j3.Px() + (met_fake.Py())*j3.Py()));
       MTjmin=min(MTj1,min(MTj2,MTj3));
     }
   if(njets>=4)
     {
       MTj4 = sqrt(2*(met_fake.Mod())*j4.Et() - 2*((met_fake.Px())*j4.Px() + (met_fake.Py())*j4.Py()));
       MTjmin=min(min(MTj1,MTj4),min(MTj2,MTj3));
     }

   outputntuple->MTjmin=MTjmin;
   outputntuple->MTj1=MTj1;
   outputntuple->MTj2=MTj2;
   outputntuple->MTj3=MTj3;
   outputntuple->MTj4=MTj4;
  
  
   double mtbmin=0;
   double mtb1=0;
   double mtb2=0;
   if(nbjets35>=2)
     {
       mtb1 = sqrt(2*(met_fake.Mod())*b1.Et() - 2*((met_fake.Px())*b1.Px() + (met_fake.Py())*b1.Py()));
       mtb2 = sqrt(2*(met_fake.Mod())*b2.Et() - 2*((met_fake.Px())*b2.Px() + (met_fake.Py())*b2.Py()));
       mtbmin=min(mtb1,mtb2);
      }

   outputntuple->mtbmin=mtbmin;


   double MTbmin=0;
   double MTbmax=0;  
   if(nbjets>=1)
     {
       int ref_min=0;
       int ref_max=0;
       double dPhi_bjet;
       double dPhi_min=999;
       double dPhi_max=0;
      
      for(int ii=0;ii<nbjets;ii++){
   	dPhi_bjet = TVector2::Phi_mpi_pi((*bjets[ii]).Phi() - met_fake.Phi());
   	if (fabs(dPhi_bjet) < fabs(dPhi_min) )  {
   	  dPhi_min = dPhi_bjet;
   	  ref_min = ii;
   	}
   	if (fabs(dPhi_bjet) > fabs(dPhi_max) )  {
   	  dPhi_max = dPhi_bjet;
   	  ref_max = ii;
   	}
       }
    
       MTbmin = sqrt(2.*(met_fake.Mod())*(*bjets[ref_min]).Pt() *(1 - TMath::Cos(dPhi_min)));
       MTbmax = sqrt(2.*(met_fake.Mod())*(*bjets[ref_max]).Pt() *(1 - TMath::Cos(dPhi_max)));
      
     }

   double MTbmin_simple=0;
   double MTbmax_simple=0;
   if(nbjets>=1)
     {
       int ref_min=0;
       int ref_max=0;
       double dPhi_bjet;
       double dPhi_min=999;
       double dPhi_max=0;

       for(int ii=0;ii<nbjets;ii++){
	 dPhi_bjet = TVector2::Phi_mpi_pi((*bjets[ii]).Phi() - met_simple.Phi());
	 if (fabs(dPhi_bjet) < fabs(dPhi_min) )  {
	   dPhi_min = dPhi_bjet;
	   ref_min = ii;
	 }
	 if (fabs(dPhi_bjet) > fabs(dPhi_max) )  {
	   dPhi_max = dPhi_bjet;
	   ref_max = ii;
	 }
       }

       MTbmin_simple = sqrt(2.*(met_simple.Mod())*(*bjets[ref_min]).Pt() *(1 - TMath::Cos(dPhi_min)));
       MTbmax_simple = sqrt(2.*(met_simple.Mod())*(*bjets[ref_max]).Pt() *(1 - TMath::Cos(dPhi_max)));

     }

   outputntuple->MTbmax=MTbmax;
   outputntuple->MTbmin=MTbmin;
   outputntuple->MTbmin_simple=MTbmin_simple;
   outputntuple->MTbmax_simple=MTbmax_simple;
   outputntuple->MTbmax_orig=MTbmax_orig;
   outputntuple->MTbmin_orig=MTbmin_orig;
  

   
   //jetcleaning

   Jets tempjets;
   if(njets>0)
     tempjets.push_back(jets[0]);

   bool passcleaning=true;
   double bad_dphi=dPhiBadTile(tempjets, met_nom.Phi());

  
   if(bad_dphi<0.3)
     passcleaning=false;
   bool tileVeto = badTileVeto(tempjets,met_nom.Phi());
   if(tileVeto)
      passcleaning=false;
     
   bool isbad=false;
   /*
   if(fabs(jets[0]->Eta())<2 && jets[0]->Pt()>1000){
     if(jets[0]->fch<0.02)isbad=true;
     if(jets[0]->fch<0.05 && jets[0]->fem > 0.9 ) isbad=true;

   }
   */
   if(njets>0){
     if(fabs(jets[0]->Eta())<2.4 && (jets[0]->fch / jets[0]->fmax)<0.1)
       isbad=true;
   }

   //if(jets[0]->fmax , 
   



   outputntuple->passcleaning=passcleaning || isbad;
   



}



