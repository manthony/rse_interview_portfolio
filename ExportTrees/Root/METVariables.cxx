
#include "ExportTrees/Export.h"
using namespace std;


//____________________________________________________________
//                     FILL 
//                  met variables
//
//   Modify 'met' , adding in fake met with leptons, photons
//____________________________________________________________
//
void ExportTrees::FillMETVariables(){

  //dereference z,photon
  Particle Z = (*m_Z);
  Particle gamma = (*m_photon);
  //dereference nominal tst met
  TVector2 met_nom = (*m_met);
  //Fake met from 2 leptons
  TVector2 met_lep(met_nom.Px()+Z.Px(),met_nom.Py()+Z.Py());
  //Fake met from photons
  TVector2 met_ph(met_nom.Px()+gamma.Px(),met_nom.Py()+gamma.Py());
  //Simple met
  //TVector2 met_simple = (*m_met_simple);

  TVector2 met_prime = (*m_met_fake);
  TVector2 met_track = (*m_met_track);
   
  //m_metlep = new TVector2(met_lep);
  //m_metph  = new TVector2(met_ph);
  
  //keep original met
  outputntuple->eT_miss_orig=met_nom.Mod();
  outputntuple->phi_met_orig=met_nom.Phi();
  
    
  outputntuple->eT_miss=m_met_fake->Mod();
  outputntuple->eT_miss_track=met_track.Mod();

  outputntuple->dphi_track = abs(TVector2::Phi_mpi_pi(met_prime.Phi() - met_track.Phi()));
  outputntuple->dphi_track_orig = abs(TVector2::Phi_mpi_pi(met_nom.Phi() - met_track.Phi()));


  outputntuple->eT_miss_NonInt=m_met_NonInt->Mod();

 
  if(&inputntuple->metsig != nullptr)
    outputntuple->metsig = inputntuple->metsig;
  if(&inputntuple->metsigET != nullptr)
    outputntuple->metsigET = inputntuple->metsigET;
  if(&inputntuple->metsigHT != nullptr)
    outputntuple->metsigHT = inputntuple->metsigHT;
 

  outputntuple->sumet=inputntuple->sumet/1000.;
  outputntuple->sumet_jet=inputntuple->sumet_jet/1000.;
  outputntuple->sumet_el=inputntuple->sumet_el/1000.;
  outputntuple->sumet_y=inputntuple->sumet_y/1000.;
  outputntuple->sumet_mu=inputntuple->sumet_mu/1000.;
  outputntuple->sumet_tau=inputntuple->sumet_tau/1000.;
  outputntuple->sumet_softTrk=inputntuple->sumet_softTrk/1000.;
  outputntuple->sumet_softClu=inputntuple->sumet_softClu/1000.;

  outputntuple->eT_miss_jet=inputntuple->MET_jet_pt/1000.;
  outputntuple->eT_miss_el=inputntuple->MET_el_pt/1000.;
  outputntuple->eT_miss_mu=inputntuple->MET_mu_pt/1000.;
  outputntuple->eT_miss_y=inputntuple->MET_y_pt/1000.;
  outputntuple->eT_miss_tau=inputntuple->MET_tau_pt/1000.;
  outputntuple->eT_miss_softTrk=inputntuple->MET_softTrk_pt/1000.;

  TVector2 truth_met_nonInt;
  truth_met_nonInt.SetMagPhi(inputntuple->TruthNonInt_pt/*don't need /1000., fix later!*/,inputntuple->TruthNonInt_phi);
  TVector2 reco_met_RefJets;
  reco_met_RefJets.SetMagPhi(inputntuple->MET_jet_pt/1000.,inputntuple->MET_jet_phi);  
  TVector2 reco_met;
  reco_met.SetMagPhi(inputntuple->MET_pt/1000.,inputntuple->MET_phi);

  TVector2 AllTruthJets(0,0);
  TVector2 MatchedTruthJets(0,0);
  bool allTruthGood=true;


  if(inputntuple->truthJet_pt != nullptr){
    for(unsigned int j=0; j< inputntuple->truthJet_pt->size();j++){
      
      double pt=(*inputntuple->truthJet_pt)[j]/1000.;
      
      double eta=(*inputntuple->truthJet_eta)[j];
      double phi=(*inputntuple->truthJet_phi)[j];
      double E=(*inputntuple->truthJet_e)[j]/1000.;
      Jet* tmpjet = new Jet(pt,eta,phi,E);
      int flav=(*inputntuple->truthJet_flav)[j];
      //set flavour/MV2
      tmpjet->flav=flav;


      AllTruthJets+=TVector2(tmpjet->Px(),tmpjet->Py());
      
      // if matched == -1 then the jet isn't matched to any reco jet, if -2 then it's matched to more than 1 "
      if((*inputntuple->truthJet_matched)[j]>=0 && m_jets->size()>0){
	
	MatchedTruthJets+=TVector2(tmpjet->Px(),tmpjet->Py());
	Jet* tmp = m_jets->at((*inputntuple->truthJet_matched)[j]);
	MatchedTruthJets-=TVector2(tmp->Px(),tmp->Py());
	
	
	

	
      }//END truthjet/recojet match
      // if((*inputntuple->truthJet_matched_tau)[j]>=0 &&  inputntuple->tau_pt->size()>0){
      //	tmpjet->truthJet_matched_tau=inputntuple->truthJet_matched_tau[j];
	

      // } //END Truthjet_m
      else{
	allTruthGood=false;
      }
      delete tmpjet;
    }
  }

  outputntuple->allTruthGood=allTruthGood;
  
  TVector2 reco_met_all(reco_met);
  TVector2 reco_met_RefJets_all(reco_met_RefJets);
  reco_met_all-=AllTruthJets;
  
  TVector2 reco_met_matched(reco_met);
  TVector2 reco_met_RefJets_matched(reco_met_RefJets);
  reco_met_matched-=MatchedTruthJets;


  
  for(const auto &jet: *m_jets){
    reco_met_all+=TVector2(jet->Px(),jet->Py());
    reco_met_RefJets_all+=TVector2(jet->Px(),jet->Py());
  }

  
  outputntuple->eT_miss_all_truth=reco_met_all.Mod();
  outputntuple->eT_miss_jets_all_truth=reco_met_RefJets_all.Mod();
  outputntuple->eT_miss_matched_truth=reco_met_matched.Mod();
  outputntuple->eT_miss_jets_matched_truth=reco_met_RefJets_matched.Mod();
  outputntuple->eT_miss_truth=truth_met_nonInt.Mod();
  outputntuple->eT_miss_jets=reco_met_RefJets.Mod();
  

}

