#include "ExportTrees/Export.h"
using namespace std;


void ExportTrees::FillChannels( ){
  
  int btag1e_channel,btag1mu_channel,btag2e_channel,btag2mu_channel,btagemu_channel;
  btag1e_channel=btag1mu_channel=btag2e_channel=btag2mu_channel=btagemu_channel=0;

  int nEl=outputntuple->nEl;
  int nMu=outputntuple->nMu;
  int nBL=outputntuple->nbaselineLep;
  int nBJets=outputntuple->num_bjets;
  int pass1etriggers=outputntuple->pass1etriggers;
  int pass1mutriggers=outputntuple->pass1mutriggers;
  
  btag1e_channel =(nEl==1 && nMu==0 && pass1etriggers && nBL==1 && nBJets>0);
  btag1mu_channel=(nEl==0 && nMu==1 && pass1mutriggers && nBL==1 && nBJets>0);
  btag2e_channel =(nEl==2 && nMu==0 && pass1etriggers && nBL==2 && nBJets>0);
  btag2mu_channel=(nEl==0 && nMu==2 && pass1mutriggers && nBL==2 && nBJets>0);
  btagemu_channel=(nEl==1 && nMu==1 && (pass1mutriggers||pass1etriggers) && nBL==2 && nBJets>0);
  
  

  outputntuple->btag1e_channel=btag1e_channel;
  outputntuple->btag1mu_channel=btag1mu_channel;
  outputntuple->btagemu_channel=btagemu_channel;
  outputntuple->btag2mu_channel=btag2mu_channel;
  outputntuple->btag2e_channel=btag2e_channel;
  
  
  outputntuple->btagzerolep_channel= ( (inputntuple->nbaselineEl + inputntuple->nbaselineMu)==0  && nBJets>0);
  // outputntuple->btag0l_channel=((nEl + nMu)==0);
}      

