
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                 fat jet variables
//____________________________________________________
//
void ExportTrees::FillFatJetVariables(){
       
  Jets jetsKt8 = (*m_fatjets_kt8);
  Jets jetsKt12 = (*m_fatjets_kt12);
  Jets jetsSt = (*m_fatjets_st);
    

  Jet j1kt8,j2kt8,j3kt8;
  Jet j1kt12,j2kt12,j3kt12;  
  Jet j1st,j2st,j3st;

  int njets = jetsKt8.size();
  
  if(njets>0){
    j1kt8 = (*jetsKt8[0]);
    if(njets>1){
      j2kt8 = (*jetsKt8[1]);
      if(njets>2){
	j3kt8 = (*jetsKt8[2]);
      }
    }
  }
  
  outputntuple->m_1fatjet_kt8=j1kt8.M();
  outputntuple->m_2fatjet_kt8=j2kt8.M();
  outputntuple->m_3fatjet_kt8=j3kt8.M();
  
  njets = jetsKt12.size();
  if(njets>0){
    j1kt12 = (*jetsKt12[0]);
    if(njets>1){
      j2kt12 = (*jetsKt12[1]);
      if(njets>2){
	j3kt12 = (*jetsKt12[2]);
      }
    }
  }
  
  outputntuple->m_1fatjet_kt12=j1kt12.M();
  outputntuple->m_2fatjet_kt12=j2kt12.M();
  outputntuple->m_3fatjet_kt12=j3kt12.M();
  
  TVector2 met_fake = (*m_met_fake);
  double dPhi_fat1kt8_sys=-999, dPhi_fat2kt8_sys=-999, dPhi_fat1kt12_sys=-999, dPhi_fat2kt12_sys=-999, dPhi_2fatkt8_sys=-999, dPhi_2fatkt12_sys=-999;
  dPhi_fat1kt8_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - j1kt8.Phi());
  dPhi_fat2kt8_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - j2kt8.Phi());
  dPhi_fat1kt12_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - j1kt12.Phi());
  dPhi_fat2kt12_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - j2kt12.Phi());
  dPhi_2fatkt8_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - (j1kt8+j2kt8).Phi());
  dPhi_2fatkt12_sys = TVector2::Phi_mpi_pi(met_fake.Phi() - (j1kt12+j2kt12).Phi());
  outputntuple->dPhi_fat1kt8_sys=dPhi_fat1kt8_sys;
  outputntuple->dPhi_fat2kt8_sys=dPhi_fat2kt8_sys;
  outputntuple->dPhi_fat1kt12_sys=dPhi_fat1kt12_sys;
  outputntuple->dPhi_fat2kt12_sys=dPhi_fat2kt12_sys;
  outputntuple->dPhi_2fatkt8_sys=dPhi_2fatkt8_sys;
  outputntuple->dPhi_2fatkt12_sys=dPhi_2fatkt12_sys;



  njets = jetsSt.size();
  if(njets>0){
    j1st = (*jetsSt[0]);
    if(njets>1){
      j2st = (*jetsSt[1]);
      if(njets>2){
	j3st = (*jetsSt[2]);
      }
    }
  }
  outputntuple->m_1fatjet_st=j1st.M();
  outputntuple->m_2fatjet_st=j2st.M();
  outputntuple->m_3fatjet_st=j3st.M();
  outputntuple->pt_1fatjet_st=j1st.Pt();
  outputntuple->pt_2fatjet_st=j2st.Pt();
  outputntuple->pt_3fatjet_st=j3st.Pt();
  outputntuple->eta_1fatjet_st=j1st.Eta();
  outputntuple->eta_2fatjet_st=j2st.Eta();
  outputntuple->eta_3fatjet_st=j3st.Eta();
  

  outputntuple->NFatJetsSt=inputntuple->nFatJetsSt;

  //outputntuple->NFatJetsKt10=inputntuple->nFatJetsKt10;

  outputntuple->ntrkjets_1fatjet_st=j1st.ntrkjets;
  outputntuple->ntrkjets_2fatjet_st=j2st.ntrkjets;
  outputntuple->ntrkjets_3fatjet_st=j3st.ntrkjets;
  outputntuple->nghostbhad_1fatjet_st=j1st.nghostbhad;
  outputntuple->nghostbhad_2fatjet_st=j2st.nghostbhad;
  outputntuple->nghostbhad_3fatjet_st=j3st.nghostbhad;
  outputntuple->Split12_1fatjet_st=j1st.Split12/1000.;
  outputntuple->Split12_2fatjet_st=j2st.Split12/1000.;
  outputntuple->Split12_3fatjet_st=j3st.Split12/1000.;
  outputntuple->Split23_1fatjet_st=j1st.Split23/1000.;
  outputntuple->Split23_2fatjet_st=j2st.Split23/1000.;
  outputntuple->Split23_3fatjet_st=j3st.Split23/1000.;
  outputntuple->Split34_1fatjet_st=j1st.Split34/1000.;
  outputntuple->Split34_2fatjet_st=j2st.Split34/1000.;
  outputntuple->Split34_3fatjet_st=j3st.Split34/1000.;
  outputntuple->Tau1_1fatjet_st=j1st.Tau1;
  outputntuple->Tau1_2fatjet_st=j2st.Tau1;
  outputntuple->Tau1_3fatjet_st=j3st.Tau1;
  outputntuple->Tau2_1fatjet_st=j1st.Tau2;
  outputntuple->Tau2_2fatjet_st=j2st.Tau2;
  outputntuple->Tau2_3fatjet_st=j3st.Tau2;
  outputntuple->Tau3_1fatjet_st=j1st.Tau3;
  outputntuple->Tau3_2fatjet_st=j2st.Tau3;
  outputntuple->Tau3_3fatjet_st=j3st.Tau3;

  if (outputntuple->Tau2_1fatjet_st!=0)
    {
      outputntuple->Tau32_1fatjet_st=j1st.Tau32;
    }
   if (outputntuple->Tau2_2fatjet_st!=0)
    {
      outputntuple->Tau32_2fatjet_st=j2st.Tau32;
    }
     if (outputntuple->Tau2_3fatjet_st!=0)
    {
      outputntuple->Tau32_3fatjet_st=j3st.Tau32;
    }
  outputntuple->Qw_1fatjet_st=j1st.Qw;
  outputntuple->Qw_2fatjet_st=j2st.Qw;
  outputntuple->Qw_3fatjet_st=j3st.Qw;

  outputntuple->w50_1fatjet_st=j1st.w50;
  outputntuple->w50_2fatjet_st=j2st.w50;
  outputntuple->w50_3fatjet_st=j3st.w50;
  outputntuple->w80_1fatjet_st=j1st.w80;
  outputntuple->w80_2fatjet_st=j2st.w80;
  outputntuple->w80_3fatjet_st=j3st.w80;
  outputntuple->z50_1fatjet_st=j1st.z50;
  outputntuple->z50_2fatjet_st=j2st.z50;
  outputntuple->z50_3fatjet_st=j3st.z50;
  outputntuple->z80_1fatjet_st=j1st.z80;
  outputntuple->z80_2fatjet_st=j2st.z80;
  outputntuple->z80_3fatjet_st=j3st.z80;
  outputntuple->W50res_1fatjet_st=j1st.W50res;
  outputntuple->W50res_2fatjet_st=j2st.W50res;
  outputntuple->W50res_3fatjet_st=j3st.W50res;
  outputntuple->W80res_1fatjet_st=j1st.W80res;
  outputntuple->W80res_2fatjet_st=j2st.W80res;
  outputntuple->W80res_3fatjet_st=j3st.W80res;
  outputntuple->Z50res_1fatjet_st=j1st.Z50res;
  outputntuple->Z50res_2fatjet_st=j2st.Z50res;
  outputntuple->Z50res_3fatjet_st=j3st.Z50res;
  outputntuple->Z80res_1fatjet_st=j1st.Z80res;
  outputntuple->Z80res_2fatjet_st=j2st.Z80res;
  outputntuple->Z80res_3fatjet_st=j3st.Z80res;
  outputntuple->WLowWMassCut50_1fatjet_st=j1st.WLowWMassCut50;
  outputntuple->WLowWMassCut50_2fatjet_st=j2st.WLowWMassCut50;
  outputntuple->WLowWMassCut50_3fatjet_st=j3st.WLowWMassCut50;
  outputntuple->WLowWMassCut80_1fatjet_st=j1st.WLowWMassCut80;
  outputntuple->WLowWMassCut80_2fatjet_st=j2st.WLowWMassCut80;
  outputntuple->WLowWMassCut80_3fatjet_st=j3st.WLowWMassCut80;
  outputntuple->ZLowWMassCut50_1fatjet_st=j1st.ZLowWMassCut50;
  outputntuple->ZLowWMassCut50_2fatjet_st=j2st.ZLowWMassCut50;
  outputntuple->ZLowWMassCut50_3fatjet_st=j3st.ZLowWMassCut50;
  outputntuple->ZLowWMassCut80_1fatjet_st=j1st.ZLowWMassCut80;
  outputntuple->ZLowWMassCut80_2fatjet_st=j2st.ZLowWMassCut80;
  outputntuple->ZLowWMassCut80_3fatjet_st=j3st.ZLowWMassCut80;
  outputntuple->WHighWMassCut50_1fatjet_st=j1st.WHighWMassCut50;
  outputntuple->WHighWMassCut50_2fatjet_st=j2st.WHighWMassCut50;
  outputntuple->WHighWMassCut50_3fatjet_st=j3st.WHighWMassCut50;
  outputntuple->WHighWMassCut80_1fatjet_st=j1st.WHighWMassCut80;
  outputntuple->WHighWMassCut80_2fatjet_st=j2st.WHighWMassCut80;
  outputntuple->WHighWMassCut80_3fatjet_st=j3st.WHighWMassCut80;
  outputntuple->ZHighWMassCut50_1fatjet_st=j1st.ZHighWMassCut50;
  outputntuple->ZHighWMassCut50_2fatjet_st=j2st.ZHighWMassCut50;
  outputntuple->ZHighWMassCut50_3fatjet_st=j3st.ZHighWMassCut50;
  outputntuple->ZHighWMassCut80_1fatjet_st=j1st.ZHighWMassCut80;
  outputntuple->ZHighWMassCut80_2fatjet_st=j2st.ZHighWMassCut80;
  outputntuple->ZHighWMassCut80_3fatjet_st=j3st.ZHighWMassCut80;
  outputntuple->WD2Cut50_1fatjet_st=j1st.WD2Cut50;
  outputntuple->WD2Cut50_2fatjet_st=j2st.WD2Cut50;
  outputntuple->WD2Cut50_3fatjet_st=j3st.WD2Cut50;
  outputntuple->WD2Cut80_1fatjet_st=j1st.WD2Cut80;
  outputntuple->WD2Cut80_2fatjet_st=j2st.WD2Cut80;
  outputntuple->WD2Cut80_3fatjet_st=j3st.WD2Cut80;
  outputntuple->ZD2Cut50_1fatjet_st=j1st.ZD2Cut50;
  outputntuple->ZD2Cut50_2fatjet_st=j2st.ZD2Cut50;
  outputntuple->ZD2Cut50_3fatjet_st=j3st.ZD2Cut50;
  outputntuple->ZD2Cut80_1fatjet_st=j1st.ZD2Cut80;
  outputntuple->ZD2Cut80_2fatjet_st=j2st.ZD2Cut80;
  outputntuple->ZD2Cut80_3fatjet_st=j3st.ZD2Cut80;
  outputntuple->top50_1fatjet_st=j1st.top50;
  outputntuple->top50_2fatjet_st=j2st.top50;
  outputntuple->top50_3fatjet_st=j3st.top50;
  outputntuple->top80_1fatjet_st=j1st.top80;
  outputntuple->top80_2fatjet_st=j2st.top80;
  outputntuple->top80_3fatjet_st=j3st.top80;
  outputntuple->top50res_1fatjet_st=j1st.top50res;
  outputntuple->top50res_2fatjet_st=j2st.top50res;
  outputntuple->top50res_3fatjet_st=j3st.top50res;
  outputntuple->top80res_1fatjet_st=j1st.top80res;
  outputntuple->top80res_2fatjet_st=j2st.top80res;
  outputntuple->top80res_3fatjet_st=j3st.top80res;
  outputntuple->TopTagTau32Cut50_1fatjet_st=j1st.TopTagTau32Cut50;
  outputntuple->TopTagTau32Cut50_2fatjet_st=j2st.TopTagTau32Cut50;
  outputntuple->TopTagTau32Cut50_3fatjet_st=j3st.TopTagTau32Cut50;
  outputntuple->TopTagTau32Cut80_1fatjet_st=j1st.TopTagTau32Cut80;
  outputntuple->TopTagTau32Cut80_2fatjet_st=j2st.TopTagTau32Cut80;
  outputntuple->TopTagTau32Cut80_3fatjet_st=j3st.TopTagTau32Cut80;
  outputntuple->TopTagSplit23Cut50_1fatjet_st=j1st.TopTagSplit23Cut50;
  outputntuple->TopTagSplit23Cut50_2fatjet_st=j2st.TopTagSplit23Cut50;
  outputntuple->TopTagSplit23Cut50_3fatjet_st=j3st.TopTagSplit23Cut50;
  outputntuple->TopTagSplit23Cut80_1fatjet_st=j1st.TopTagSplit23Cut80;
  outputntuple->TopTagSplit23Cut80_2fatjet_st=j2st.TopTagSplit23Cut80;
  outputntuple->TopTagSplit23Cut80_3fatjet_st=j3st.TopTagSplit23Cut80;

  outputntuple->pT_1fatjet_st=j1st.Pt();
  outputntuple->phi_1fatjet_st=j1st.Phi();
  outputntuple->y_1fatjet_st=j1st.Rapidity();

  /*

  //addining in constituents of fat jets!!
  outputntuple->con_pT_1fatjet_st.clear();
  outputntuple->con_phi_1fatjet_st.clear();
  outputntuple->con_y_1fatjet_st.clear();
  //use calo clusters
  //for(const auto& con : *j1st.caloCells){
  
  for(const auto& con : *j1st.tracks){
    double y=con->Rapidity();
    double phi=con->Phi();
    double pt=con->Pt();
    outputntuple->con_pT_1fatjet_st.push_back(pt);
    outputntuple->con_phi_1fatjet_st.push_back(phi);
    outputntuple->con_y_1fatjet_st.push_back(y);
  }

  //addining in subjets of the leading fat jet! anti-kt=0.2 subjets
  outputntuple->sub_pT_1fatjet_st.clear();
  outputntuple->sub_phi_1fatjet_st.clear();
  outputntuple->sub_y_1fatjet_st.clear();
  for(const auto& sub : *j1st.subJets){
    double y=sub->Rapidity();
    double phi=sub->Phi();
    double pt=sub->Pt();
    
    outputntuple->sub_pT_1fatjet_st.push_back(pt);
    outputntuple->sub_phi_1fatjet_st.push_back(phi);
    outputntuple->sub_y_1fatjet_st.push_back(y);  
  }

  */
}

