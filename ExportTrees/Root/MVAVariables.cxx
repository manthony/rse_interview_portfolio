
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                mva  variables
//____________________________________________________
//
void ExportTrees::FillMVAVariables(){

  m_BDTvar1 = Float_t(outputntuple->Tau32_1fatjet_st);
  m_BDTvar2 = Float_t(outputntuple->Tau32_2fatjet_st);
  m_BDTvar3 = Float_t(outputntuple->m_1fatjet_st);
  m_BDTvar4 = Float_t(outputntuple->m_2fatjet_st);
  m_BDTvar5 = Float_t(outputntuple->Split23_1fatjet_st);
  m_BDTvar6=Float_t(outputntuple->Split23_2fatjet_st);
  m_BDTvar7=Float_t(outputntuple->eT_miss_orig);
  m_BDTvar8=Float_t(outputntuple->MTbmin_orig);
  m_BDTvar9=Float_t(outputntuple->dRb1b2);
  m_BDTvar10=Float_t(outputntuple->nj_good);
  m_BDTvar11=Float_t(outputntuple->pt_1fatjet_st);
  m_BDTvar12=Float_t(outputntuple->pt_2fatjet_st);
  m_BDTvar13=Float_t(outputntuple->m_mt2Chi2);
  m_BDTvar14=Float_t(outputntuple->NFatJetsSt);

  if (outputntuple->EventNumber % 2 == 0){
    outputntuple->BDT_highstop=m_reader_odd->EvaluateMVA( "BDT method" );
    outputntuple->BDTG_highstop=m_reader_BDTG_odd->EvaluateMVA( "BDTG method" );
  }
  else {
    outputntuple->BDT_highstop=m_reader_even->EvaluateMVA( "BDT method" );
    outputntuple->BDTG_highstop=m_reader_BDTG_even->EvaluateMVA( "BDTG method" );
  }

  // outputntuple->BDTG_600_300=m_reader1->EvaluateMVA( "BDTG method" );
  // outputntuple->BDT_600_300=m_reader1->EvaluateMVA( "BDT method");

}

