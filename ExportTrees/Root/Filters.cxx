
#include "ExportTrees/Export.h"
using namespace std;



bool  ExportTrees::passFilter(){
 
  if( filter == "passttZFilter" ) return passttZFilter();
  else if( filter == "passSbottomFilter" ) return passSbottomFilter();
  else if( filter == "pass0LFilter" ) return pass0LFilter();
  else if( filter == "pass0baselineLFilter" ) return pass0baselineLFilter();
  else if( filter == "pass0LMETFilter" ) return pass0LMETFilter();
  else if( filter == "pass1LFilter" ) return pass1LFilter();
  else if( filter == "passemuFilter" ) return passemuFilter();
  else if( filter == "pass2LFilter" ) return pass2LFilter();
  else if( filter == "pass2BFilter" ) return pass2BFilter();
  else if( filter == "pass1PhFilter" ) return pass1PhFilter();
  else if( filter == "passStop0LFilter" ) return passStop0LFilter();
  else if( filter == "passTightStop0LFilter" ) return passTightStop0LFilter();
  else if( filter == "" ) return true;
  else {std::cout << "Filter argument unknown!! Filter=" << filter  << std::endl; abort();} 
}

bool  ExportTrees::passComplexFilter(){
  if (filter == "passStop0LFilter" ) return passTightStop0LFilter();
  else { return true;}
  
}


bool ExportTrees::passemuFilter(){
  
  if(outputntuple->nEl==1 && outputntuple->nMu==1)
    return true;
  else
    return false;
}



bool ExportTrees::passttZFilter(){
  //ttgamma || ttZ filter
  
  //std::cout << outputntuple->num_bjets << " " << 
  if(outputntuple->num_bjets==2 && ( (outputntuple->nEl+outputntuple->nMu)>=1 || outputntuple->nPh==1))
    return true;
  else
    return false;
}
bool ExportTrees::passStop0LFilter(){
  //int nPh=outputntuple->nPh;
  //int nLep=outputntuple->nEl+outputntuple->nMu;
  int nJets=outputntuple->nj_good;
  int nbaselineLep=outputntuple->nbaselineLep;
  int nB=outputntuple->num_bjets;
  double met = outputntuple->eT_miss;
  double pT_4jet= outputntuple->pT_4jet;
 

  //if( (nPh>=1 && nB>=2) || (nLep==1 && nB>=1) || (nLep==0 && nB>=2 && met>200.) || nLep>=2)
    // return true;
  if ( met > 200 && nbaselineLep==0 && nJets>=4 && pT_4jet>40 && nB>=1)
    return true;
  else
    return false;

}

bool ExportTrees::passTightStop0LFilter(){

  if( outputntuple->num_bjets>=2 || (outputntuple->m_NbV>=1 && outputntuple->m_PTISR>400)) 
    return true;
  else
    return false;
  /*
  int nPh=outputntuple->nPh;
  int nLep=outputntuple->nEl+outputntuple->nMu;
  double pTl1=outputntuple->pT_1lep;
  double pTl2=outputntuple->pT_2lep;
  double pTy=outputntuple->pT_1ph;
  int nB=outputntuple->num_bjets;
  double met = outputntuple->eT_miss_orig;
  
  if( (nPh==1 && nB>=2 && pTy>30) || (nLep==1 && pTl1>20 && nB>=1 && met>90.) || (nLep==0 && nB>=2 && met>200.) || (nLep==2&&pTl2>20))
    return true;
  else
    return false;
  */

}



bool ExportTrees::passSbottomFilter(){
  int nPh=outputntuple->nPh;
  int nLep=outputntuple->nEl+outputntuple->nMu;
  double pTl1=outputntuple->pT_1lep;
  double pTl2=outputntuple->pT_2lep;
  double pTy=outputntuple->pT_1ph;
  int nB=outputntuple->num_bjets;
  int nB35=outputntuple->num_bjets35;
  double met = outputntuple->eT_miss_orig;
  double metll = outputntuple->eT_miss;
  

  bool onePhExp  = (nPh==1 && pTy>150 && metll>250 && nB>=2);
  bool oneLepExp = (nLep==1 && pTl1>27 && nB35>=1 && met>100.);
  bool twoLepExp = (nLep==2&& pTl2>20 && pTl1>27  && ( (nB>=2 && metll>100)||(metll>250)));
  bool zeroLepExp= (nLep==0 && nB>=2 && met>250.);

  if( onePhExp || oneLepExp || zeroLepExp || twoLepExp)
    return true;
  else
    return false;

}

bool ExportTrees::pass0LFilter(){
  int nPh=inputntuple->nPh;
  int nLep=inputntuple->nEl+inputntuple->nMu;
  
  if( nLep==0 && nPh==0)
    return true;
  else
    return false;

}

bool ExportTrees::pass0baselineLFilter(){
  int nLep=inputntuple->nbaselineEl+inputntuple->nbaselineMu;
  
  if( nLep==0)
    return true;
  else
    return false;

}


bool ExportTrees::pass0LMETFilter(){
  int nLep=outputntuple->nEl+inputntuple->nMu;
  double met = outputntuple->eT_miss;
  if( nLep==0 && met>70)
    return true;
  else
    return false;

}



bool ExportTrees::pass1LFilter(){
  int nPh=inputntuple->nPh;
  int nLep=inputntuple->nEl+inputntuple->nMu;
    
  if( nLep==1 && nPh==0)
    return true;
  else
    return false;

}

bool ExportTrees::pass2LFilter(){
  int nPh=inputntuple->nPh;
  int nLep=inputntuple->nEl+inputntuple->nMu;
    
  if( nLep>=2 && nPh==0)
    return true;
  else
    return false;

}


bool ExportTrees::pass2BFilter(){
  int nB=outputntuple->num_bjets;
  if( nB>=2)
    return true;
  else
    return false;

}

bool ExportTrees::pass1PhFilter(){
  int nPh=inputntuple->nPh;

  if( nPh==1)
    return true;
  else
    return false;

}
