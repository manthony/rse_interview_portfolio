#include <iostream>
#include <string>
#include <vector>
#include "TTree.h"
#include "TFile.h"
#include "ExportTrees/myNtupleDumper.h"

//myNtupleDumper::myNtupleDumper(TFile* file, std::string treename, int i){
myNtupleDumper::myNtupleDumper(string filename, std::string treename, int i){
//  gROOT->ProcessLine("#include <vector>");
  clean();
  
  if(i==0 ) m_file=new TFile( filename.c_str(), "RECREATE" );
  else m_file=new TFile( filename.c_str(), "UPDATE" );

  //m_file=file;
  
  m_tree = new TTree(treename.c_str(),treename.c_str());
  std::cout << "m_file=" <<  m_file << " " << i << std::endl;
  m_tree->SetDirectory(m_file);
  
}

void  myNtupleDumper::init(){
  //m_tree->Branch("el_TrigMatched",&el_TrigMatched);
  //m_tree->Branch("mu_TrigMatched",&mu_TrigMatched);

  m_tree->Branch("SCTerror",&SCTerror);
  m_tree->Branch("CoreFlag",&CoreFlag);
  m_tree->Branch("treatAsYear",&treatAsYear);
  m_tree->Branch("analysisflag",&analysisflag);
  m_tree->Branch("isTruthAnalysis",&isTruthAnalysis);

  m_tree->Branch("RMPF",&RMPF);
  m_tree->Branch("RG",&RG);

  m_tree->Branch("TrigAtPlat",&TrigAtPlat);
  m_tree->Branch("isMEPhOverlapOK",&isMEPhOverlapOK);
  m_tree->Branch("isttbarMET200_OK",&isttbarMET200_OK);
  m_tree->Branch("isttbarMET300_OK",&isttbarMET300_OK);
  m_tree->Branch("isWtHT500_OK",&isWtHT500_OK);
  m_tree->Branch("isttbarHT600_OK",&isttbarHT600_OK);
  

  m_tree->Branch("m_1fatjet_kt8",&m_1fatjet_kt8);
  m_tree->Branch("m_2fatjet_kt8",&m_2fatjet_kt8);
  m_tree->Branch("m_3fatjet_kt8",&m_3fatjet_kt8);

  m_tree->Branch("m_1fatjet_kt12",&m_1fatjet_kt12);
  m_tree->Branch("m_2fatjet_kt12",&m_2fatjet_kt12);
  m_tree->Branch("m_3fatjet_kt12",&m_3fatjet_kt12);
  
  m_tree->Branch("m_1fatjet_st",&m_1fatjet_st);
  m_tree->Branch("m_2fatjet_st",&m_2fatjet_st);
  m_tree->Branch("m_3fatjet_st",&m_3fatjet_st);
  m_tree->Branch("pt_1fatjet_st",&pt_1fatjet_st);
  m_tree->Branch("pt_2fatjet_st",&pt_2fatjet_st);
  m_tree->Branch("pt_3fatjet_st",&pt_3fatjet_st);
  m_tree->Branch("eta_1fatjet_st",&eta_1fatjet_st);
  m_tree->Branch("eta_2fatjet_st",&eta_2fatjet_st);
  m_tree->Branch("eta_3fatjet_st",&eta_3fatjet_st);


  m_tree->Branch("NFatJetsSt",&NFatJetsSt);
  

  m_tree->Branch("NFatJetsKt10",&NFatJetsKt10);


  m_tree->Branch("ntrkjets_1fatjet",&ntrkjets_1fatjet_st);
  m_tree->Branch("ntrkjets_2fatjet",&ntrkjets_2fatjet_st);
  m_tree->Branch("ntrkjets_3fatjet",&ntrkjets_3fatjet_st);
  m_tree->Branch("nghostbhad_1fatjet",&nghostbhad_1fatjet_st);
  m_tree->Branch("nghostbhad_2fatjet",&nghostbhad_2fatjet_st);
  m_tree->Branch("nghostbhad_3fatjet",&nghostbhad_3fatjet_st);
  m_tree->Branch("Split12_1fatjet",&Split12_1fatjet_st);
  m_tree->Branch("Split12_2fatjet",&Split12_2fatjet_st);
  m_tree->Branch("Split12_3fatjet",&Split12_3fatjet_st);
  m_tree->Branch("Split23_1fatjet",&Split23_1fatjet_st);
  m_tree->Branch("Split23_2fatjet",&Split23_2fatjet_st);
  m_tree->Branch("Split23_3fatjet",&Split23_3fatjet_st);
  m_tree->Branch("Split34_1fatjet",&Split34_1fatjet_st);
  m_tree->Branch("Split34_2fatjet",&Split34_2fatjet_st);
  m_tree->Branch("Split34_3fatjet",&Split34_3fatjet_st);
  m_tree->Branch("Qw_1fatjet",&Qw_1fatjet_st);
  m_tree->Branch("Qw_2fatjet",&Qw_2fatjet_st);
  m_tree->Branch("Qw_3fatjet",&Qw_3fatjet_st);
  m_tree->Branch("Tau32_1fatjet",&Tau32_1fatjet_st);
  m_tree->Branch("Tau32_2fatjet",&Tau32_2fatjet_st);
  m_tree->Branch("Tau32_3fatjet",&Tau32_3fatjet_st);
  m_tree->Branch("Tau1_1fatjet",&Tau1_1fatjet_st);
  m_tree->Branch("Tau1_2fatjet",&Tau1_2fatjet_st);
  m_tree->Branch("Tau1_3fatjet",&Tau1_3fatjet_st);
  m_tree->Branch("Tau2_1fatjet",&Tau2_1fatjet_st);
  m_tree->Branch("Tau2_2fatjet",&Tau2_2fatjet_st);
  m_tree->Branch("Tau2_3fatjet",&Tau2_3fatjet_st);
  m_tree->Branch("Tau3_1fatjet",&Tau3_1fatjet_st);
  m_tree->Branch("Tau3_2fatjet",&Tau3_2fatjet_st);
  m_tree->Branch("Tau3_3fatjet",&Tau3_3fatjet_st);  

  m_tree->Branch("w50_1fatjet",&w50_1fatjet_st);
  m_tree->Branch("w50_2fatjet",&w50_2fatjet_st);
  m_tree->Branch("w50_3fatjet",&w50_3fatjet_st);
  m_tree->Branch("w80_1fatjet",&w80_1fatjet_st);
  m_tree->Branch("w80_2fatjet",&w80_2fatjet_st);
  m_tree->Branch("w80_3fatjet",&w80_3fatjet_st);
  m_tree->Branch("z50_1fatjet",&z50_1fatjet_st);
  m_tree->Branch("z50_2fatjet",&z50_2fatjet_st);
  m_tree->Branch("z50_3fatjet",&z50_3fatjet_st);
  m_tree->Branch("z80_1fatjet",&z80_1fatjet_st);
  m_tree->Branch("z80_2fatjet",&z80_2fatjet_st);
  m_tree->Branch("z80_3fatjet",&z80_3fatjet_st);
  m_tree->Branch("W50res_1fatjet",&W50res_1fatjet_st);
  m_tree->Branch("W50res_2fatjet",&W50res_2fatjet_st);
  m_tree->Branch("W50res_3fatjet",&W50res_3fatjet_st);
  m_tree->Branch("W80res_1fatjet",&W80res_1fatjet_st);
  m_tree->Branch("W80res_2fatjet",&W80res_2fatjet_st);
  m_tree->Branch("W80res_3fatjet",&W80res_3fatjet_st);
  m_tree->Branch("Z50res_1fatjet",&Z50res_1fatjet_st);
  m_tree->Branch("Z50res_2fatjet",&Z50res_2fatjet_st);
  m_tree->Branch("Z50res_3fatjet",&Z50res_3fatjet_st);
  m_tree->Branch("Z80res_1fatjet",&Z80res_1fatjet_st);
  m_tree->Branch("Z80res_2fatjet",&Z80res_2fatjet_st);
  m_tree->Branch("Z80res_3fatjet",&Z80res_3fatjet_st);
  m_tree->Branch("WLowWMassCut50_1fatjet",&WLowWMassCut50_1fatjet_st);
  m_tree->Branch("WLowWMassCut50_2fatjet",&WLowWMassCut50_2fatjet_st);
  m_tree->Branch("WLowWMassCut50_3fatjet",&WLowWMassCut50_3fatjet_st);
  m_tree->Branch("WLowWMassCut80_1fatjet",&WLowWMassCut80_1fatjet_st);
  m_tree->Branch("WLowWMassCut80_2fatjet",&WLowWMassCut80_2fatjet_st);
  m_tree->Branch("WLowWMassCut80_3fatjet",&WLowWMassCut80_3fatjet_st);
  m_tree->Branch("ZLowWMassCut50_1fatjet",&ZLowWMassCut50_1fatjet_st);
  m_tree->Branch("ZLowWMassCut50_2fatjet",&ZLowWMassCut50_2fatjet_st);
  m_tree->Branch("ZLowWMassCut50_3fatjet",&ZLowWMassCut50_3fatjet_st);
  m_tree->Branch("ZLowWMassCut80_1fatjet",&ZLowWMassCut80_1fatjet_st);
  m_tree->Branch("ZLowWMassCut80_2fatjet",&ZLowWMassCut80_2fatjet_st);
  m_tree->Branch("ZLowWMassCut80_3fatjet",&ZLowWMassCut80_3fatjet_st);
  m_tree->Branch("WHighWMassCut50_1fatjet",&WHighWMassCut50_1fatjet_st);
  m_tree->Branch("WHighWMassCut50_2fatjet",&WHighWMassCut50_2fatjet_st);
  m_tree->Branch("WHighWMassCut50_3fatjet",&WHighWMassCut50_3fatjet_st);
  m_tree->Branch("WHighWMassCut80_1fatjet",&WHighWMassCut80_1fatjet_st);
  m_tree->Branch("WHighWMassCut80_2fatjet",&WHighWMassCut80_2fatjet_st);
  m_tree->Branch("WHighWMassCut80_3fatjet",&WHighWMassCut80_3fatjet_st);
  m_tree->Branch("ZHighWMassCut50_1fatjet",&ZHighWMassCut50_1fatjet_st);
  m_tree->Branch("ZHighWMassCut50_2fatjet",&ZHighWMassCut50_2fatjet_st);
  m_tree->Branch("ZHighWMassCut50_3fatjet",&ZHighWMassCut50_3fatjet_st);
  m_tree->Branch("ZHighWMassCut80_1fatjet",&ZHighWMassCut80_1fatjet_st);
  m_tree->Branch("ZHighWMassCut80_2fatjet",&ZHighWMassCut80_2fatjet_st);
  m_tree->Branch("ZHighWMassCut80_3fatjet",&ZHighWMassCut80_3fatjet_st);
  m_tree->Branch("WD2Cut50_1fatjet",&WD2Cut50_1fatjet_st);
  m_tree->Branch("WD2Cut50_2fatjet",&WD2Cut50_2fatjet_st);
  m_tree->Branch("WD2Cut50_3fatjet",&WD2Cut50_3fatjet_st);
  m_tree->Branch("WD2Cut80_1fatjet",&WD2Cut80_1fatjet_st);
  m_tree->Branch("WD2Cut80_2fatjet",&WD2Cut80_2fatjet_st);
  m_tree->Branch("WD2Cut80_3fatjet",&WD2Cut80_3fatjet_st);
  m_tree->Branch("ZD2Cut50_1fatjet",&ZD2Cut50_1fatjet_st);
  m_tree->Branch("ZD2Cut50_2fatjet",&ZD2Cut50_2fatjet_st);
  m_tree->Branch("ZD2Cut50_3fatjet",&ZD2Cut50_3fatjet_st);
  m_tree->Branch("ZD2Cut80_1fatjet",&ZD2Cut80_1fatjet_st);
  m_tree->Branch("ZD2Cut80_2fatjet",&ZD2Cut80_2fatjet_st);
  m_tree->Branch("ZD2Cut80_3fatjet",&ZD2Cut80_3fatjet_st);
  m_tree->Branch("WD2Cut50_1fatjet",&WD2Cut50_1fatjet_st);
  m_tree->Branch("WD2Cut50_2fatjet",&WD2Cut50_2fatjet_st);
  m_tree->Branch("WD2Cut50_3fatjet",&WD2Cut50_3fatjet_st);
  m_tree->Branch("WD2Cut80_1fatjet",&WD2Cut80_1fatjet_st);
  m_tree->Branch("WD2Cut80_2fatjet",&WD2Cut80_2fatjet_st);
  m_tree->Branch("WD2Cut80_3fatjet",&WD2Cut80_3fatjet_st);
  m_tree->Branch("ZD2Cut50_1fatjet",&ZD2Cut50_1fatjet_st);
  m_tree->Branch("ZD2Cut50_2fatjet",&ZD2Cut50_2fatjet_st);
  m_tree->Branch("ZD2Cut50_3fatjet",&ZD2Cut50_3fatjet_st);
  m_tree->Branch("ZD2Cut80_1fatjet",&ZD2Cut80_1fatjet_st);
  m_tree->Branch("ZD2Cut80_2fatjet",&ZD2Cut80_2fatjet_st);
  m_tree->Branch("ZD2Cut80_3fatjet",&ZD2Cut80_3fatjet_st);
  m_tree->Branch("top50_1fatjet",&top50_1fatjet_st);
  m_tree->Branch("top50_2fatjet",&top50_2fatjet_st);
  m_tree->Branch("top50_3fatjet",&top50_3fatjet_st);
  m_tree->Branch("top80_1fatjet",&top80_1fatjet_st);
  m_tree->Branch("top80_2fatjet",&top80_2fatjet_st);
  m_tree->Branch("top80_3fatjet",&top80_3fatjet_st);
  m_tree->Branch("top50res_1fatjet",&top50res_1fatjet_st);
  m_tree->Branch("top50res_2fatjet",&top50res_2fatjet_st);
  m_tree->Branch("top50res_3fatjet",&top50res_3fatjet_st);
  m_tree->Branch("top80res_1fatjet",&top80res_1fatjet_st);
  m_tree->Branch("top80res_2fatjet",&top80res_2fatjet_st);
  m_tree->Branch("top80res_3fatjet",&top80res_3fatjet_st);
  m_tree->Branch("TopTagTau32Cut50_1fatjet",&TopTagTau32Cut50_1fatjet_st);
  m_tree->Branch("TopTagTau32Cut50_2fatjet",&TopTagTau32Cut50_2fatjet_st);
  m_tree->Branch("TopTagTau32Cut50_3fatjet",&TopTagTau32Cut50_3fatjet_st);
  m_tree->Branch("TopTagTau32Cut80_1fatjet",&TopTagTau32Cut80_1fatjet_st);
  m_tree->Branch("TopTagTau32Cut80_2fatjet",&TopTagTau32Cut80_2fatjet_st);
  m_tree->Branch("TopTagTau32Cut80_3fatjet",&TopTagTau32Cut80_3fatjet_st);
  m_tree->Branch("TopTagSplit23Cut50_1fatjet",&TopTagSplit23Cut50_1fatjet_st);
  m_tree->Branch("TopTagSplit23Cut50_2fatjet",&TopTagSplit23Cut50_2fatjet_st);
  m_tree->Branch("TopTagSplit23Cut50_3fatjet",&TopTagSplit23Cut50_3fatjet_st);
  m_tree->Branch("TopTagSplit23Cut80_1fatjet",&TopTagSplit23Cut80_1fatjet_st);
  m_tree->Branch("TopTagSplit23Cut80_2fatjet",&TopTagSplit23Cut80_2fatjet_st);
  m_tree->Branch("TopTagSplit23Cut80_3fatjet",&TopTagSplit23Cut80_3fatjet_st);


  m_tree->Branch("pT_1fatjet_st",&pT_1fatjet_st);
  m_tree->Branch("phi_1fatjet_st",&phi_1fatjet_st);
  m_tree->Branch("y_1fatjet_st",&y_1fatjet_st);

  m_tree->Branch("con_pT_1fatjet_st",&con_pT_1fatjet_st);
  m_tree->Branch("con_phi_1fatjet_st",&con_phi_1fatjet_st);
  m_tree->Branch("con_y_1fatjet_st",&con_y_1fatjet_st);
  

  m_tree->Branch("sub_pT_1fatjet_st",&sub_pT_1fatjet_st);
  m_tree->Branch("sub_phi_1fatjet_st",&sub_phi_1fatjet_st);
  m_tree->Branch("sub_y_1fatjet_st",&sub_y_1fatjet_st);
  


  //mtchi2
  m_tree->Branch("Top0Chi2M", &m_top0Chi2M);
  m_tree->Branch("Top1Chi2M", &m_top1Chi2M);                     
  m_tree->Branch("W0Chi2M", &m_w0Chi2M);
  m_tree->Branch("W1Chi2M", &m_w1Chi2M);
  m_tree->Branch("chi2", &m_chi2);
  m_tree->Branch("MT2Chi2", &m_mt2Chi2);
  
  // Jigsaw
  m_tree->Branch("PTISR", &m_PTISR);
  m_tree->Branch("RISR", &m_RISR);
  m_tree->Branch("MS",&m_MS);
  m_tree->Branch("MV",&m_MV);
  m_tree->Branch("dphiISRI",&m_dphiISRI);
  m_tree->Branch("pTbV1",&m_pTbV1);
  m_tree->Branch("NbV",&m_NbV);
  m_tree->Branch("NjV",&m_NjV);
  m_tree->Branch("pTjV4",&m_pTjV4);
  m_tree->Branch("NbISR",&m_NbISR);
  m_tree->Branch("NjISR",&m_NjISR);

  // m_tree->Branch("truthpt",&truthpt);
  m_tree->Branch("pass1Ltriggers",&pass1Ltriggers);
  m_tree->Branch("pass1etriggers",&pass1etriggers);
  m_tree->Branch("pass1mutriggers",&pass1mutriggers);
  m_tree->Branch("pass1phtriggers",&pass1phtriggers);
  m_tree->Branch("passMETtriggers",&passMETtriggers);

  m_tree->Branch("passMETtriggers",&passMETtriggers);



  // m_tree->Branch("passSeedSelection",&passSeedSelection);
  // m_tree->Branch("passHTSeedSelection",&passHTSeedSelection);
  // m_tree->Branch("passETSeedSelection",&passETSeedSelection);
  // m_tree->Branch("passETSeedSelectionv2",&passETSeedSelectionv2);
 
  m_tree->Branch("passTightCleaning",&passcleaning);
 




  // m_tree->Branch("passL1_J12",&passL1_J12);
  // m_tree->Branch("passL1_J50",&passL1_J50);
  // m_tree->Branch("passL1_EM10",&passL1_EM10);
  // m_tree->Branch("passL1_2EM7",&passL1_2EM7);
  // m_tree->Branch("passL1_MU6",&passL1_MU6);
  // m_tree->Branch("passL1_2MU6",&passL1_2MU6);

  // m_tree->Branch("passHLT_xe35",&passHLT_xe35);
  // m_tree->Branch("passHLT_xe50",&passHLT_xe50);
  // m_tree->Branch("passHLT_xe80",&passHLT_xe80);
  
  //triggers

  
  m_tree->Branch("HLT_g120_loose",&HLT_g120_loose);
  m_tree->Branch("HLT_g140_loose",&HLT_g140_loose);

 
  m_tree->Branch("HLT_g200_etcut",&HLT_g200_etcut);
  m_tree->Branch("HLT_g35_loose_L1EM15",&HLT_g35_loose_L1EM15);
  m_tree->Branch("HLT_g40_loose_L1EM15",&HLT_g40_loose_L1EM15);
  m_tree->Branch("HLT_g45_loose_L1EM15",&HLT_g45_loose_L1EM15);
  m_tree->Branch("HLT_g50_loose_L1EM15",&HLT_g50_loose_L1EM15);



  m_tree->Branch("HLT_e24_lhmedium_L1EM18VH",&HLT_e24_lhmedium_L1EM18VH);
  m_tree->Branch("HLT_e24_lhmedium_L1EM20VH",&HLT_e24_lhmedium_L1EM20VH);
  m_tree->Branch("HLT_e20_medium",&HLT_e20_medium);
  m_tree->Branch("HLT_2e17_loose",&HLT_2e17_loose);
  m_tree->Branch("HLT_mu20_iloose_L1MU15",&HLT_mu20_iloose_L1MU15);
  m_tree->Branch("HLT_mu26",&HLT_mu26);
  m_tree->Branch("HLT_2mu6",&HLT_2mu6);
  m_tree->Branch("HLT_xe35",&HLT_xe35);
  m_tree->Branch("HLT_xe60",&HLT_xe60);
  m_tree->Branch("HLT_xe70",&HLT_xe70);
  m_tree->Branch("HLT_xe70_tc_lcw",&HLT_xe70_tc_lcw);
  m_tree->Branch("HLT_xe70_mht",&HLT_xe70_mht);
  m_tree->Branch("HLT_xe80",&HLT_xe80);
  m_tree->Branch("HLT_xe90_mht_wEFMu_L1XE50",&HLT_xe90_mht_wEFMu_L1XE50);
  m_tree->Branch("HLT_xe90_mht_L1XE50",&HLT_xe90_mht_L1XE50);
  m_tree->Branch("HLT_xe100",&HLT_xe100);
  m_tree->Branch("HLT_xe100_mht_L1XE50",&HLT_xe100_mht_L1XE50);
  m_tree->Branch("HLT_xe110_mht_L1XE50",&HLT_xe110_mht_L1XE50);
  m_tree->Branch("HLT_j100_xe80",&HLT_j100_xe80);
  m_tree->Branch("HLT_j80_xe80",&HLT_j80_xe80);
  m_tree->Branch("HLT_j400",&HLT_j400);
  m_tree->Branch("HLT_j360",&HLT_j360);


  m_tree->Branch("HLT_6j45_0eta240",&HLT_6j45_0eta240);
  m_tree->Branch("HLT_6j60",&HLT_6j60);


  
  m_tree->Branch("HLT_e60_lhmedium",&HLT_e60_lhmedium);
  m_tree->Branch("HLT_mu50",&HLT_mu50);
  //m_tree->Branch("HLT_e24_lhmedium_L1EM18VHORHLT_e60_lhmedium",&HLT_e24_lhmedium_L1EM18VHORHLT_e60_lhmedium);
  //m_tree->Branch("HLT_e24_lhmedium_L1EM20VHORHLT_e60_lhmedium",&HLT_e24_lhmedium_L1EM20VHORHLT_e60_lhmedium);

  //m_tree->Branch("HLT_mu20_iloose_L1MU15ORHLT_mu50",&HLT_mu20_iloose_L1MU15ORHLT_mu50);



  m_tree->Branch("j1_truthFlav",&j1_truthFlav);
  m_tree->Branch("j2_truthFlav",&j2_truthFlav);
  m_tree->Branch("j3_truthFlav",&j3_truthFlav);
  m_tree->Branch("j4_truthFlav",&j4_truthFlav);
  
  m_tree->Branch("b1_truthFlav",&b1_truthFlav);
  m_tree->Branch("b2_truthFlav",&b2_truthFlav);


  m_tree->Branch("leadb1",&leadb1);
  m_tree->Branch("leadb2",&leadb2);
  m_tree->Branch("leadb1_60",&leadb1_60);
  m_tree->Branch("leadb2_60",&leadb2_60);
  m_tree->Branch("leadb1_85",&leadb1_85);
  m_tree->Branch("leadb2_85",&leadb2_85);
   

  // m_tree->Branch("whichb",&whichb);
  // m_tree->Branch("whichB",&whichB);



  m_tree->Branch("mctcorrjj",&mctcorrjj);
  m_tree->Branch("mctjj",&mctjj);

  m_tree->Branch("mctcorr2b",&mctcorr2b);
  m_tree->Branch("mct2b",&mct2b);
  


  // m_tree->Branch("mlb1",&mlb1);
  // m_tree->Branch("mlb2",&mlb2);
  m_tree->Branch("mlbmin",&mlbmin);

  
  // m_tree->Branch("mlj1",&mlj1);
  // m_tree->Branch("mlj2",&mlj2);
  
  m_tree->Branch("bindex",&bindex);

  // m_tree->Branch("dRb1b2",&dRb1b2); 
  // m_tree->Branch("dRlb1",&dRlb1);
  // m_tree->Branch("dRlb2",&dRlb2);
  // m_tree->Branch("dRlbmin",&dRlbmin);
  // m_tree->Branch("dRlbmax",&dRlbmax);
   
  m_tree->Branch("dRl1l2",&dRl1l2); 
  m_tree->Branch("dRj1j2",&dRj1j2); 
  m_tree->Branch("dRb1b2",&dRb1b2); 
  m_tree->Branch("pT_1leadbtag",&pT_1leadbtag);
  m_tree->Branch("pT_2leadbtag",&pT_2leadbtag);
  m_tree->Branch("dRlj1",&dRlj1);
  m_tree->Branch("dRlj2",&dRlj2);
  m_tree->Branch("dRljmin",&dRljmin);
  m_tree->Branch("dRljmax",&dRljmax);
  m_tree->Branch("dRjmin",&dRjmin);
  m_tree->Branch("dRjmax",&dRjmax);
  m_tree->Branch("dRlbmin",&dRlbmin);
  m_tree->Branch("dEtajmin",&dEtajmin);
  m_tree->Branch("dEtab1b2",&dEtab1b2);
  m_tree->Branch("m_top_minDR1",&m_top_minDR1);
  m_tree->Branch("m_top_minDR2",&m_top_minDR2);

  //m_tree->Branch("mjy",&mjy);
  m_tree->Branch("mjj",&mjj);
  m_tree->Branch("meff",&meff);
  //m_tree->Branch("MET_sig",&MET_sig);

  m_tree->Branch("ht3",&ht3);
  m_tree->Branch("HT",&HT);
  m_tree->Branch("HT_35",&HT_35);

  // m_tree->Branch("phi_MET",&phi_MET);


  //m_tree->Branch("jet1_bad",&jet1_bad);
  // m_tree->Branch("jet1_fmax",&jet1_fmax);
  // m_tree->Branch("jet1_fch",&jet1_fch);
  // m_tree->Branch("jet1_fem",&jet1_fem);
  // m_tree->Branch("jet1_jvtxf",&jet1_jvtxf);
  // m_tree->Branch("jet2_fmax",&jet1_fmax);
  // m_tree->Branch("jet2_fch",&jet1_fch);
  // m_tree->Branch("jet2_fem",&jet1_fem);
  // m_tree->Branch("jet2_jvtxf",&jet1_jvtxf);
  

  
  m_tree->Branch("nPh",&nPh);
  m_tree->Branch("topoetcone40_1ph",&topoetcone40_1ph);
  m_tree->Branch("ptvarcone20_1ph",&ptvarcone20_1ph);
  m_tree->Branch("pT_1ph",&pT_1ph);
  m_tree->Branch("eta_1ph",&eta_1ph);
  m_tree->Branch("phi_1ph",&phi_1ph);
  m_tree->Branch("dPhi_1ph",&dPhi_1ph);
  m_tree->Branch("dPhi_1ph1jet",&dPhi_1ph1jet);


  //m_tree->Branch("ntruthPh",&ntruthPh);
  //m_tree->Branch("pT_1truthph",&pT_1truthph);
  //m_tree->Branch("eta_1truthph",&eta_1truthph);
  //m_tree->Branch("phi_1truthph",&phi_1truthph);
  //m_tree->Branch("dPhi_1truthph",&dPhi_1truthph);

  
  
  m_tree->Branch("eta_1bjet_Z",&eta_1bjet_Z);
  m_tree->Branch("eta_2bjet_Z",&eta_2bjet_Z);
  m_tree->Branch("dR_1bjet_Z",&dR_1bjet_Z);
  m_tree->Branch("dR_2bjet_Z",&dR_2bjet_Z);
  m_tree->Branch("dPhi_1jet_Z",&dPhi_1jet_Z);
  m_tree->Branch("dPhi_2jet_Z",&dPhi_2jet_Z);
  


  m_tree->Branch("pT_B",&pT_B);

  m_tree->Branch("pT_V",&pT_V);
  m_tree->Branch("E_V",&E_V);
  m_tree->Branch("phi_V",&phi_V);
  m_tree->Branch("eta_V",&eta_V);

  m_tree->Branch("TruthNuMET",&TruthNuMET);

  // m_tree->Branch("charge_1e",&charge_1e);
  // m_tree->Branch("charge_2e",&charge_2e);
  // m_tree->Branch("charge_1mu",&charge_1mu);
  // m_tree->Branch("charge_2mu",&charge_2mu);;
  
  m_tree->Branch("charge_1l",&charge_1l);
  m_tree->Branch("charge_2l",&charge_2l);
  m_tree->Branch("charge_3l",&charge_3l);
   
  m_tree->Branch("flav_1lep",&flav_1lep);
  m_tree->Branch("flav_2lep",&flav_2lep);
  m_tree->Branch("flav_3lep",&flav_3lep);



  m_tree->Branch("pT_1lep",&pT_1lep);
  m_tree->Branch("pT_2lep",&pT_2lep);
  m_tree->Branch("pT_3lep",&pT_3lep);
  m_tree->Branch("eta_1lep",&eta_1lep);
  m_tree->Branch("eta_2lep",&eta_2lep);
  m_tree->Branch("eta_3lep",&eta_3lep);





  // m_tree->Branch("pT_1mu_truth",&pT_1mu_truth);
  // m_tree->Branch("eta_1mu_truth",&eta_1mu_truth);
  // m_tree->Branch("phi_1mu_truth",&phi_1mu_truth);
  // m_tree->Branch("truthMu_signal_matched",&truthMu_signal_matched);
  // m_tree->Branch("truthMu_baseline_matched",&truthMu_baseline_matched);
  // m_tree->Branch("truthMu_preOR_matched",&truthMu_preOR_matched);
  // m_tree->Branch("truthMu_inA",&truthMu_inA );
  // m_tree->Branch("ntruthMu",&ntruthMu );
  




  //m_tree->Branch("dPhi_1lep",&dPhi_1lep);
  //m_tree->Branch("dPhi_2lep",&dPhi_2lep);

  

  m_tree->Branch("pT_1bjet",&pT_1bjet);
  m_tree->Branch("pT_2bjet",&pT_2bjet);


  m_tree->Branch("pT_1trackbjet60",&pT_1trackbjet60);
  m_tree->Branch("pT_1trackbjet70",&pT_1trackbjet70);
  m_tree->Branch("pT_1trackbjet77",&pT_1trackbjet77);
  m_tree->Branch("pT_1trackbjet85",&pT_1trackbjet85);

  m_tree->Branch("pT_1trackbjet60_unmatched",&pT_1trackbjet60_unmatched);
  m_tree->Branch("pT_1trackbjet70_unmatched",&pT_1trackbjet70_unmatched);
  m_tree->Branch("pT_1trackbjet77_unmatched",&pT_1trackbjet77_unmatched);
  m_tree->Branch("pT_1trackbjet85_unmatched",&pT_1trackbjet85_unmatched);


  m_tree->Branch("y_1jet",&y_1jet);
  m_tree->Branch("y_2jet",&y_2jet);


  m_tree->Branch("ntracks_1jet",&ntracks_1jet);
  m_tree->Branch("ntracks_2jet",&ntracks_2jet);
  m_tree->Branch("ntracks_3jet",&ntracks_3jet);
  m_tree->Branch("ntracks_4jet",&ntracks_4jet);
  m_tree->Branch("ntracks_5jet",&ntracks_5jet);
  m_tree->Branch("ntracks_6jet",&ntracks_6jet);

  m_tree->Branch("jpa_1jet_calo",&jpa_1jet_calo);
  m_tree->Branch("jpa_2jet_calo",&jpa_2jet_calo);
  m_tree->Branch("jpa_3jet_calo",&jpa_3jet_calo);
  m_tree->Branch("jpa_4jet_calo",&jpa_4jet_calo);

  m_tree->Branch("jpa_1jet_tracks",&jpa_1jet_tracks);
  m_tree->Branch("jpa_2jet_tracks",&jpa_2jet_tracks);
  m_tree->Branch("jpa_3jet_tracks",&jpa_3jet_tracks);
  m_tree->Branch("jpa_4jet_tracks",&jpa_4jet_tracks);


  m_tree->Branch("pT_1jet",&pT_1jet);
  m_tree->Branch("pT_2jet",&pT_2jet);
  m_tree->Branch("pT_3jet",&pT_3jet);
  m_tree->Branch("pT_4jet",&pT_4jet);
  m_tree->Branch("pT_5jet",&pT_5jet);
  m_tree->Branch("pT_6jet",&pT_6jet);

  m_tree->Branch("pT_lastjet",&pT_lastjet);


  m_tree->Branch("flav_1jet",&flav_1jet);
  m_tree->Branch("flav_2jet",&flav_2jet);
  m_tree->Branch("flav_3jet",&flav_3jet);
  m_tree->Branch("flav_4jet",&flav_4jet);

  /* if(m_ExtraTruth){
    m_tree->Branch("origin_1jet",&origin_1jet);
    m_tree->Branch("origin_2jet",&origin_2jet);
    m_tree->Branch("origin_3jet",&origin_3jet);
    m_tree->Branch("origin_4jet",&origin_4jet);
    m_tree->Branch("origin_5jet",&origin_5jet);
    m_tree->Branch("origin_6jet",&origin_6jet);
    m_tree->Branch("origin_7jet",&origin_7jet);
    
    
    m_tree->Branch("npartons_1jet",&npartons_1jet);
    m_tree->Branch("npartons_2jet",&npartons_2jet);
    m_tree->Branch("npartons_3jet",&npartons_3jet);
    m_tree->Branch("npartons_4jet",&npartons_4jet);
    
    m_tree->Branch("wpartons_1jet",&wpartons_1jet);
    m_tree->Branch("wpartons_2jet",&wpartons_2jet);
    m_tree->Branch("wpartons_3jet",&wpartons_3jet);
    m_tree->Branch("wpartons_4jet",&wpartons_4jet);
  }
  */


  
  m_tree->Branch("hastau",&hastau);
  m_tree->Branch("hasb",&hasb);
  m_tree->Branch("hasc",&hasc);


  m_tree->Branch("pT_1jet_truth",&pT_1jet_truth);


 
  //m_tree->Branch("weight2", &weight_new, "weight_new2/D");
  //m_tree->Branch("weight3", &weight, "weight_new/D");
  m_tree->Branch("j1_bweight", &j1_bweight);
  m_tree->Branch("j2_bweight", &j2_bweight);
  

  m_tree->Branch("RunNumber",&RunNumber);
  m_tree->Branch("averageIntPerXing",&averageIntPerXing);
  m_tree->Branch("averageIntPerXingCorr",&averageIntPerXingCorr);
  m_tree->Branch("lbn",&lbn);
  m_tree->Branch("EventNumber",&EventNumber);
  //m_tree->Branch("numGoodVtx",&numGoodVtx);

  
  m_tree->Branch("nj_good",&nj_good);
  m_tree->Branch("nj_good35",&nj_good35);
  m_tree->Branch("nj_good45",&nj_good45);
  m_tree->Branch("nj_good45_0eta240",&nj_good45_0eta240);
  m_tree->Branch("MT",&MT,"MT/D");
  m_tree->Branch("MT_orig",&MT_orig,"MT_orig/D");
  m_tree->Branch("MTjmin",&MTjmin,"MTjmin/D");

  m_tree->Branch("mtbmin",&mtbmin,"mtbmin/D");

  m_tree->Branch("MTbmin",&MTbmin,"MTbmin/D");
  m_tree->Branch("MTbmax",&MTbmax,"MTbmax/D");
  m_tree->Branch("MTbmin_orig",&MTbmin_orig,"MTbmin_orig/D");
  m_tree->Branch("MTbmax_orig",&MTbmax_orig,"MTbmax_orig/D");
  //m_tree->Branch("MTjmax",&MTjmax,"MTjmax/D");

 m_tree->Branch("MTj1",&MTj1,"MTj1/D");
 m_tree->Branch("MTj2",&MTj2,"MTj2/D");
 m_tree->Branch("MTj3",&MTj3,"MTj3/D");
 m_tree->Branch("MTj4",&MTj4,"MTj4/D");
  //m_tree->Branch("MTblmin",&MTblmin,"MTblmin/D");

  m_tree->Branch("m_top1",&m_top1);
  m_tree->Branch("m_top2",&m_top2);

  
  m_tree->Branch("topness",&topness);
  m_tree->Branch("pt_asym",&pt_asym);
  m_tree->Branch("A",&A);
  m_tree->Branch("centrality",&centrality);

  
  m_tree->Branch("Sp",&Sp);
  m_tree->Branch("ST",&ST);
  m_tree->Branch("Ap",&Ap);
	   


  
  // m_tree->Branch("EventWeight",&EventWeight);

 

  m_tree->Branch("jvtweight", &jvtweight, "jvtweight/D");
  m_tree->Branch("jvtweightUP", &jvtweightUP, "jvtweightUP/D");
  m_tree->Branch("jvtweightDOWN", &jvtweightDOWN, "jvtweightDOWN/D");
  m_tree->Branch("SherpaWeight",&SherpaWeight);
  m_tree->Branch("btagweight", &btagweight, "btagweight/D");
  m_tree->Branch("btagweightBUP", &btagweightBUP, "btagweightBUP/D");
  m_tree->Branch("btagweightBDOWN", &btagweightBDOWN, "btagweightBDOWN/D");
  m_tree->Branch("btagweightCUP", &btagweightCUP, "btagweightCUP/D");
  m_tree->Branch("btagweightCDOWN", &btagweightCDOWN, "btagweightCDOWN/D");
  m_tree->Branch("btagweightLUP", &btagweightLUP, "btagweightLUP/D");
  m_tree->Branch("btagweightLDOWN", &btagweightLDOWN, "btagweightLDOWN/D");
  m_tree->Branch("btagweightExUP", &btagweightExUP, "btagweightExUP/D");
  m_tree->Branch("btagweightExDOWN", &btagweightExDOWN, "btagweightExDOWN/D");
  m_tree->Branch("btagweightExCUP", &btagweightExCUP, "btagweightExCUP/D");
  m_tree->Branch("btagweightExCDOWN", &btagweightExCDOWN, "btagweightExCDOWN/D");
  m_tree->Branch("pileupweight", &pileupweight, "pileupweight/D");
  m_tree->Branch("pileupweightUP", &pileupweightUP, "pileupweightUP/D");
  m_tree->Branch("pileupweightDOWN", &pileupweightDOWN, "pileupweightDOWN/D");
  m_tree->Branch("AnalysisWeight",&AnalysisWeight, "AnalysisWeight/D");
  m_tree->Branch("LeptonWeight",&LeptonWeight, "LeptonWeight/D");
  //m_tree->Branch("LostLeptonWeight",&LostLeptonWeight, "LostLeptonWeight/D");
  m_tree->Branch("LumiWeight", &LumiWeight, "LumiWeight/D");
  //m_tree->Branch("LumiWeightFix", &LumiWeightOld, "LumiWeightFix/D");
  m_tree->Branch("TriggerWeight", &TriggerWeight,"TriggerWeight/D");

  m_tree->Branch("ElecWeight",&ElecWeight);
  m_tree->Branch("MuonWeight",&MuonWeight);
  m_tree->Branch("ElecWeightReco",&ElecWeightReco);
  m_tree->Branch("MuonWeightReco",&MuonWeightReco);
  m_tree->Branch("ElecWeightTrig",&ElecWeightTrig);
  m_tree->Branch("MuonWeightTrig",&MuonWeightTrig);
  m_tree->Branch("PhotonWeight",&PhotonWeight);
  m_tree->Branch("PhotonReWeight",&PhotonReWeight);
  m_tree->Branch("PhotonAcptWeight",&PhotonAcptWeight);


  m_tree->Branch("ElecWeightIDUP",&ElecWeightIDUP);
  m_tree->Branch("ElecWeightIDDOWN",&ElecWeightIDDOWN);   
  m_tree->Branch("ElecWeightISOUP",&ElecWeightISOUP); 
  m_tree->Branch("ElecWeightISODOWN",&ElecWeightISODOWN) ;
  m_tree->Branch("ElecWeightRECOUP",&ElecWeightRECOUP);   
  m_tree->Branch("ElecWeightRECODOWN",&ElecWeightRECODOWN); 
  m_tree->Branch("ElecWeightTRIGUP",&ElecWeightTRIGUP);   
  m_tree->Branch("ElecWeightTRIGDOWN",&ElecWeightTRIGDOWN); 
  m_tree->Branch("ElecWeightTRIGEFFUP",&ElecWeightTRIGEFFUP);
  m_tree->Branch("ElecWeightTRIGEFFDOWN",&ElecWeightTRIGEFFDOWN);


  m_tree->Branch("MuonWeightSTATUP",&MuonWeightSTATUP);
  m_tree->Branch("MuonWeightSTATDOWN",&MuonWeightSTATDOWN);
  m_tree->Branch("MuonWeightSYSUP",&MuonWeightSYSUP);
  m_tree->Branch("MuonWeightSYSDOWN",&MuonWeightSYSDOWN);
  m_tree->Branch("MuonWeightTRIGSTATUP",&MuonWeightTRIGSTATUP);
  m_tree->Branch("MuonWeightTRIGSTATDOWN",&MuonWeightTRIGSTATDOWN);
  m_tree->Branch("MuonWeightTRIGSYSTUP",&MuonWeightTRIGSYSTUP);
  m_tree->Branch("MuonWeightTRIGSYSTDOWN",&MuonWeightTRIGSYSTDOWN);
  m_tree->Branch("MuonWeightISOSTATUP",&MuonWeightISOSTATUP);
  m_tree->Branch("MuonWeightISOSTATDOWN",&MuonWeightISOSTATDOWN);
  m_tree->Branch("MuonWeightISOSYSTUP",&MuonWeightISOSYSTUP);
  m_tree->Branch("MuonWeightISOSYSTDOWN",&MuonWeightISOSYSTDOWN);
  m_tree->Branch("MuonWeightTTVASTATUP",&MuonWeightTTVASTATUP);
  m_tree->Branch("MuonWeightTTVASTATDOWN",&MuonWeightTTVASTATDOWN);
  m_tree->Branch("MuonWeightTTVASYSTUP",&MuonWeightTTVASYSTUP);
  m_tree->Branch("MuonWeightTTVASYSTDOWN",&MuonWeightTTVASYSTDOWN);



  m_tree->Branch("passtauveto",&passtauveto);


  m_tree->Branch("nbaselineLep",&nbaselineLep);



  m_tree->Branch("nbaselinePh",&nbaselinePh);
  m_tree->Branch("btagzerolep_channel",&btagzerolep_channel);
  //m_tree->Branch("btag0l_channel",&btag0l_channel);
  m_tree->Branch("btag1e_channel",&btag1e_channel,"btag1e_channel/I");  
  m_tree->Branch("btag1mu_channel",&btag1mu_channel,"btag1mu_channel/I");  
  m_tree->Branch("btag2e_channel",&btag2e_channel,"btag2e_channel/I");  
  m_tree->Branch("btag2mu_channel",&btag2mu_channel,"btag2mu_channel/I");  
  m_tree->Branch("btagemu_channel",&btagemu_channel,"btagemu_channel/I");  
  
  m_tree->Branch("w_MV2c20_1",&w_MV2c20_1,"w_MV2c20_1/D");
  m_tree->Branch("w_MV2c20_2",&w_MV2c20_2,"w_MV2c20_2/D");
  m_tree->Branch("w_MV2c20_3",&w_MV2c20_3,"w_MV2c20_3/D");

  
 

  m_tree->Branch("num_bjets",&num_bjets);
  m_tree->Branch("num_bjets35",&num_bjets35);
  m_tree->Branch("num_bjets60",&num_bjets60);
  m_tree->Branch("num_bjets85",&num_bjets85);


  
  m_tree->Branch("ntrackjets",&ntrackjets);
  m_tree->Branch("nbtrackjets60",&nbtrackjets60);
  m_tree->Branch("nbtrackjets70",&nbtrackjets70);
  m_tree->Branch("nbtrackjets77",&nbtrackjets77);
  m_tree->Branch("nbtrackjets85",&nbtrackjets85);

 
  m_tree->Branch("nbtrackjets60_unmatched",&nbtrackjets60_unmatched);
  m_tree->Branch("nbtrackjets70_unmatched",&nbtrackjets70_unmatched);
  m_tree->Branch("nbtrackjets77_unmatched",&nbtrackjets77_unmatched);
  m_tree->Branch("nbtrackjets85_unmatched",&nbtrackjets85_unmatched);




  m_tree->Branch("eT_miss",&eT_miss);
  //m_tree->Branch("eT_miss",&eT_miss);
  m_tree->Branch("eT_miss_jet",&eT_miss_jet);
  m_tree->Branch("eT_miss_softTrk",&eT_miss_softTrk);
  m_tree->Branch("eT_miss_el",&eT_miss_el);
  m_tree->Branch("eT_miss_mu",&eT_miss_mu);
  m_tree->Branch("eT_miss_y",&eT_miss_y);
  m_tree->Branch("eT_miss_tau",&eT_miss_tau);
  m_tree->Branch("eT_miss_NonInt",&eT_miss_NonInt);
  m_tree->Branch("eT_miss_tau",&eT_miss_NonInt);

  m_tree->Branch("metsig",&metsig);
  m_tree->Branch("metsigET",&metsigET);
  m_tree->Branch("metsigHT",&metsigHT);


  m_tree->Branch("eT_miss_track",&eT_miss_track);
  m_tree->Branch("eT_miss_inv",&eT_miss_inv);
  m_tree->Branch("eT_miss_lep",&eT_miss_lep);
  m_tree->Branch("eT_miss_orig",&eT_miss_orig);
  m_tree->Branch("phi_met_orig",&phi_met_orig);
  m_tree->Branch("sumet",&sumet);
  m_tree->Branch("sumet_jet",&sumet_jet);
  m_tree->Branch("sumet_el",&sumet_el);
  m_tree->Branch("sumet_y",&sumet_y);
  m_tree->Branch("sumet_my",&sumet_mu);
  m_tree->Branch("sumet_tau",&sumet_tau);
  m_tree->Branch("sumet_softTrk",&sumet_softTrk);
  m_tree->Branch("sumet_softClu",&sumet_softClu);
  
  m_tree->Branch("eT_miss_truth",&eT_miss_truth);
  m_tree->Branch("eT_miss_jets",&eT_miss_jets);
  
  m_tree->Branch("eT_miss_ghost",&eT_miss_ghost);
  m_tree->Branch("eT_miss_jets_ghost",&eT_miss_jets_ghost);
  
  m_tree->Branch("eT_miss_all_truth",&eT_miss_all_truth);
  m_tree->Branch("eT_miss_jets_all_truth",&eT_miss_jets_all_truth);


  m_tree->Branch("eT_miss_matched_truth",&eT_miss_matched_truth);
  m_tree->Branch("eT_miss_jets_matched_truth",&eT_miss_jets_matched_truth);




  
  m_tree->Branch("allGhostGood",&allGhostGood);
  m_tree->Branch("allTruthGood",&allTruthGood);
  






  m_tree->Branch("eT_miss_seed",&eT_miss_seed);
  m_tree->Branch("sumet_seed",&sumet_seed);
  m_tree->Branch("sumet_refjet_seed",&sumet_refjet_seed);
 
  m_tree->Branch("nbjets_seed",&nbjets_seed);
  m_tree->Branch("njets_seed",&njets_seed);
  m_tree->Branch("met_avPt_seed",&met_avPt_seed);

  m_tree->Branch("pT_1jet_seed",&pT_1jet_seed);
  m_tree->Branch("pT_2jet_seed",&pT_2jet_seed);


  m_tree->Branch("R_1jet",&R_1jet);
  m_tree->Branch("R_2jet",&R_2jet);
  m_tree->Branch("R_3jet",&R_3jet);
  m_tree->Branch("R_4jet",&R_4jet);

  m_tree->Branch("R_av",&R_av);

  m_tree->Branch("maxDRbb",&maxDRbb);
  m_tree->Branch("maxDRassym",&maxDRassym);
  m_tree->Branch("maxDRmbb",&maxDRmbb);
  m_tree->Branch("maxDRntracks",&maxDRntracks);
  m_tree->Branch("maxminDRbb",&maxminDRbb);
  m_tree->Branch("maxminDRassym",&maxminDRassym);
  m_tree->Branch("maxminDRmbb",&maxminDRmbb);
  m_tree->Branch("maxminDRntracks",&maxminDRntracks);
  m_tree->Branch("minDRbb",&minDRbb);
  m_tree->Branch("minDRassym",&minDRassym);
  m_tree->Branch("minDRmbb",&minDRmbb);
  m_tree->Branch("minDRntracks",&minDRntracks);


  //m_tree->Branch("etlep",&etlep);

 
 
  m_tree->Branch("mt2",&mt2);
  m_tree->Branch("amt2",&amt2);
  // m_tree->Branch("amt2alt",&amt2alt);
  // m_tree->Branch("amt2new",&amt2new);
  // m_tree->Branch("amt2new2",&amt2new2);
 
  m_tree->Branch("mbb",&mbb);
  m_tree->Branch("mll",&mll);
  //m_tree->Branch("mvh",&mvh);
  m_tree->Branch("pT_ll",&pT_ll);
  m_tree->Branch("meff2j",&meff2j);
  m_tree->Branch("meff3j",&meff3j);
  
  m_tree->Branch("dphi_track",&dphi_track);
  m_tree->Branch("dphi_track_orig",&dphi_track_orig);

  m_tree->Branch("dPhi_1jet",&dPhi_1jet);
  m_tree->Branch("dPhi_2jet",&dPhi_2jet);
  m_tree->Branch("dPhi_3jet",&dPhi_3jet);
  m_tree->Branch("dPhi_4jet",&dPhi_4jet);
  m_tree->Branch("dphimin4",&dphimin4);
  m_tree->Branch("dphimin4_35",&dphimin4_35);
  m_tree->Branch("dphimin3",&dphimin3);
  m_tree->Branch("dphimin2",&dphimin2);
  m_tree->Branch("dphimin4_lep",&dphimin4_lep);

  m_tree->Branch("dPhi_1jet_orig",&dPhi_1jet_orig);
  m_tree->Branch("dPhi_2jet_orig",&dPhi_2jet_orig);
  m_tree->Branch("dPhi_3jet_orig",&dPhi_3jet_orig);
  m_tree->Branch("dPhi_4jet_orig",&dPhi_4jet_orig);
  m_tree->Branch("dphimin4_orig",&dphimin4_orig);
  m_tree->Branch("dphimin2_orig",&dphimin2_orig);

  m_tree->Branch("dPhi_1j2j",&dPhi_1j2j);
  m_tree->Branch("dPhi_2j3j",&dPhi_2j3j);
  m_tree->Branch("dPhi_1j3j",&dPhi_1j3j);

  m_tree->Branch("dPhi_b1b2",&dPhi_b1b2);

  m_tree->Branch("eta_1jet",&eta_1jet);
  m_tree->Branch("eta_2jet",&eta_2jet);
  m_tree->Branch("eta_3jet",&eta_3jet);
  m_tree->Branch("eta_4jet",&eta_4jet);
  m_tree->Branch("eta_5jet",&eta_5jet);
  m_tree->Branch("eta_6jet",&eta_6jet);

  m_tree->Branch("phi_1jet",&phi_1jet);
  m_tree->Branch("phi_2jet",&phi_2jet);
  m_tree->Branch("phi_3jet",&phi_3jet);
  m_tree->Branch("phi_4jet",&phi_4jet);
  m_tree->Branch("phi_5jet",&phi_5jet);

  m_tree->Branch("MT2ll",&m_MT2ll);
 

  // m_tree->Branch("dPhi_min",&dPhi_min);

  m_tree->Branch("dPhi_1bjet",&dPhi_1bjet);
  m_tree->Branch("dPhi_2bjet",&dPhi_2bjet);


  // m_tree->Branch("btagSFCentral",&btagSFCent,"bEffSFCent/D");


  m_tree->Branch("nEl",&nEl); 
  m_tree->Branch("nMu",&nMu);



  // m_tree->Branch("pT_1djet",&pT_1djet);
  // m_tree->Branch("pT_2djet",&pT_2djet);
  // m_tree->Branch("pT_3djet",&pT_3djet);
  // m_tree->Branch("pT_4djet",&pT_4djet);
  // m_tree->Branch("mdd",&mdd);
  // m_tree->Branch("mctdd",&mctdd);
  // m_tree->Branch("nd_good",&nd_good);
  // m_tree->Branch("ddphimin4",&ddphimin4);
  // m_tree->Branch("eT_miss_dd",&eT_miss_dd);
   
  m_tree->Branch("eT_miss_simple",&eT_miss_simple);
  m_tree->Branch("dphi_track_simple",&dphi_track_simple);
  m_tree->Branch("sumet_simpleTST",&sumet_simpleTST);
  m_tree->Branch("sumet_simpleTST_jet",&sumet_jet_simpleTST);
  m_tree->Branch("sumet_simpleTST_mu",&sumet_mu_simpleTST);
  m_tree->Branch("sumet_simpleTST_softTrk",&sumet_softTrk_simpleTST);
  m_tree->Branch("sumet_simpleCST",&sumet_simpleCST);
  m_tree->Branch("sumet_simpleCST_jet",&sumet_jet_simpleCST);
  m_tree->Branch("sumet_simpleCST_mu",&sumet_mu_simpleCST);
  m_tree->Branch("sumet_simpleCST_softClus",&sumet_softClus_simpleCST);
  m_tree->Branch("eT_miss_simple_jet",&eT_miss_simple_jet);
  m_tree->Branch("eT_miss_simple_mu",&eT_miss_simple_mu);
  m_tree->Branch("eT_miss_simple_softTrk",&eT_miss_simple_softTrk);
  m_tree->Branch("dPhi_1jet_simple",&dPhi_1jet_simple);
  m_tree->Branch("dPhi_2jet_simple",&dPhi_2jet_simple);
  m_tree->Branch("dPhi_3jet_simple",&dPhi_3jet_simple);
  m_tree->Branch("dPhi_4jet_simple",&dPhi_4jet_simple);
  m_tree->Branch("dPhi_1bjet_simple",&dPhi_1bjet_simple);
  m_tree->Branch("dPhi_2bjet_simple",&dPhi_2bjet_simple);
  m_tree->Branch("dphimin4_simple",&dphimin4_simple);
  m_tree->Branch("dphimin3_simple",&dphimin3_simple);
  m_tree->Branch("dphimin2_simple",&dphimin2_simple);
  m_tree->Branch("MTbmin_simple",&MTbmin_simple);
  m_tree->Branch("MTbmax_simple",&MTbmax_simple);


  //m_tree->Branch("BDTG_1000_1",&BDTG_1000_1);
  m_tree->Branch("BDT_highstop",&BDT_highstop);
  m_tree->Branch("BDTG_highstop",&BDTG_highstop);
  //m_tree->Branch("BDTG_600_300",&BDTG_600_300);
  //m_tree->Branch("BDT_600_300",&BDT_600_300);


  m_tree->Branch("ttbar_class",&ttbar_class);
  m_tree->Branch("mbjj_0",&mbjj_0);
  m_tree->Branch("mbjj_1",&mbjj_1);
  m_tree->Branch("Tbjj_0",&Tbjj_0);
  m_tree->Branch("Tbjj_1",&Tbjj_1);
  m_tree->Branch("Wjj_0",&Wjj_0);
  m_tree->Branch("Wjj_1",&Wjj_1);


  m_tree->Branch("dPhi_4jet_sys",&dPhi_4jet_sys);
  m_tree->Branch("dPhi_2bjet_sys",&dPhi_2bjet_sys);
  m_tree->Branch("dPhi_top1_sys",&dPhi_top1_sys);
  m_tree->Branch("dPhi_top2_sys",&dPhi_top2_sys);
  m_tree->Branch("dPhi_tt_sys",&dPhi_tt_sys);
  m_tree->Branch("dPhi_fat1kt8_sys",&dPhi_fat1kt8_sys);
  m_tree->Branch("dPhi_fat1kt12_sys",&dPhi_fat1kt12_sys);
  m_tree->Branch("dPhi_fat2kt8_sys",&dPhi_fat2kt8_sys);
  m_tree->Branch("dPhi_fat2kt12_sys",&dPhi_fat2kt12_sys);
  m_tree->Branch("dPhi_2fatkt8_sys",&dPhi_2fatkt8_sys);
  m_tree->Branch("dPhi_2fatkt12_sys",&dPhi_2fatkt12_sys);




  /*
  //Branches: Jets
  
  m_tree->Branch("nJets",&nJets);
  m_tree->Branch("jet_px",&jet_px);
  m_tree->Branch("jet_py",&jet_py);
  m_tree->Branch("jet_pz",&jet_pz);
  m_tree->Branch("jet_e",&jet_e);
  

  // Branches: Electrons
  
 
  m_tree->Branch("el_px",&el_px);  
  m_tree->Branch("el_py",&el_py);
  m_tree->Branch("el_pz",&el_pz);
  m_tree->Branch("el_e",&el_e);

  // Branches: Muons

  m_tree->Branch("nMu",&nMu);
  m_tree->Branch("mu_px",&mu_px);
  m_tree->Branch("mu_py",&mu_py);
  m_tree->Branch("mu_pz",&mu_pz);
  m_tree->Branch("mu_e",&mu_e);
  */

  //BRANCHES:Taus
  m_tree->Branch("nPi0_tau",&nPi0_tau);
 
  m_tree->Branch("maxbdtTauJet",&maxbdtTauJet);

  m_tree->Branch("bdtJet_tau",&bdtJet_tau);
  m_tree->Branch("bdtEl_tau",&bdtEl_tau);
   
  m_tree->Branch("nH_tau",&nH_tau);
  m_tree->Branch("nWTracks_tau",&nWTracks_tau);
  m_tree->Branch("nTracks_tau",&nTracks_tau);
  m_tree->Branch("nCharged_tau",&nCharged_tau);
  m_tree->Branch("nNeut_tau",&nCharged_tau);



  m_tree->Branch("pT_tau",&pt_tau);
  m_tree->Branch("eta_tau",&eta_tau);
  m_tree->Branch("phi_tau",&phi_tau);
  m_tree->Branch("E_tau",&e_tau);
  m_tree->Branch("truthJet_matched_tau",&truthJet_matched_tau);
  m_tree->Branch("nTruthTaus",&nTruthTaus);
  m_tree->Branch("nTaus",&n_Taus);
  m_tree->Branch("nTruthTaus_inA",&nTruthTaus_inA);
  m_tree->Branch("nTruthTaus_outA",&nTruthTaus_outA);
  m_tree->Branch("nFakeTaus",&nFakeTaus); // #FakeNews
  m_tree->Branch("nRecoTaus_matched",&nRecoTaus_matched);
  m_tree->Branch("nRecoTaus_matched_TruthTaus",&nRecoTaus_matched_TruthTaus);
}

void myNtupleDumper::dump(){
  m_tree->Fill();
  m_nfilled++;
}

void myNtupleDumper::write(){
  std::cout << "going to cd to the file..." << std::endl;
  std::cout << "mfile is= " << m_file << std::endl;
  m_file->cd("");
  std::cout << "now writing...." << std::endl;
  m_tree->Write();

  std::cout << "now done...." << std::endl;
  
 
  /*
  HCounter->Write();
  HCounterNoWeight->Write();
  
  HEntries->Write();
  HEntWgt->Write();
  HEntWgtPU->Write();
  */
  //m_file->Close();
  
  //if(m_file)
  //  delete m_file;
  //if(m_tree)
  //  delete m_tree;

}

myNtupleDumper::~myNtupleDumper()
{
  if(m_file) m_file->Close();

  if(m_tree)
    delete m_tree->GetCurrentFile();
}



void myNtupleDumper::clean(){
  
  
  SCTerror = 0;
  CoreFlag = 0;
  treatAsYear = 0;

  TrigAtPlat = 0;

  passSeedSelection = 0;
  passHTSeedSelection = 0;
  passETSeedSelection = 0;
  passETSeedSelectionv2 = 0;

  pT_1mu_truth  = 0;
  eta_1mu_truth  = 0;
  phi_1mu_truth  = 0;
  truthMu_signal_matched  = 0;
  truthMu_baseline_matched  = 0;
  truthMu_preOR_matched  = 0;
  truthMu_inA  = 0;
  ntruthMu  = 0;

  m_1fatjet_kt12  = 0;
  m_2fatjet_kt12  = 0;
  m_3fatjet_kt12  = 0;
  m_1fatjet_kt8  = 0;
  m_2fatjet_kt8  = 0;
  m_3fatjet_kt8  = 0;
  m_1fatjet_st  = 0;
  m_2fatjet_st  = 0;
  m_3fatjet_st  = 0;
  pt_1fatjet_st  = 0;
  pt_2fatjet_st  = 0;
  pt_3fatjet_st  = 0;
  eta_1fatjet_st  = 0;
  eta_2fatjet_st  = 0;
  eta_3fatjet_st  = 0;  
  NFatJetsKt10=0 ;
  ntrkjets_1fatjet_st=0 ;
  ntrkjets_2fatjet_st=0 ;
  ntrkjets_3fatjet_st=0 ;
  nghostbhad_1fatjet_st=0 ;
  nghostbhad_2fatjet_st=0 ;
  nghostbhad_3fatjet_st=0 ;
  Split12_1fatjet_st=0. ;
  Split12_2fatjet_st=0. ;
  Split12_3fatjet_st=0. ;
  Split23_1fatjet_st=0. ;
  Split23_2fatjet_st=0. ;
  Split23_3fatjet_st=0. ;
  Split34_1fatjet_st=0. ;
  Split34_2fatjet_st=0. ;
  Split34_3fatjet_st=0. ;
  Qw_1fatjet_st=0. ;
  Qw_2fatjet_st=0. ;
  Qw_3fatjet_st=0. ;
  Tau1_1fatjet_st=0. ;
  Tau1_2fatjet_st=0. ;
  Tau1_3fatjet_st=0. ;
  Tau2_1fatjet_st=0. ;
  Tau2_2fatjet_st=0. ;
  Tau2_3fatjet_st=0. ;
  Tau3_1fatjet_st=0. ;
  Tau3_2fatjet_st=0. ;
  Tau3_3fatjet_st=0. ;
  Tau32_1fatjet_st=0. ;
  Tau32_2fatjet_st=0. ;
  Tau32_3fatjet_st=0. ;
  w50_1fatjet_st=false ;
  w50_2fatjet_st=false ;
  w50_3fatjet_st=false ;   
  w80_1fatjet_st=false ;
  w80_2fatjet_st=false ;
  w80_3fatjet_st=false ;
  z50_1fatjet_st=false ;
  z50_2fatjet_st=false ;
  z50_3fatjet_st=false ;
  z80_1fatjet_st=false ;
  z80_2fatjet_st=false ;
  z80_3fatjet_st=false ;
  W50res_1fatjet_st=-99 ;
  W50res_2fatjet_st=-99 ;
  W50res_3fatjet_st=-99 ;
  W80res_1fatjet_st=-99 ;
  W80res_2fatjet_st=-99 ;
  W80res_3fatjet_st=-99 ;
  Z50res_1fatjet_st=-99 ;
  Z50res_2fatjet_st=-99 ;
  Z50res_3fatjet_st=-99 ;
  Z80res_1fatjet_st=-99 ;
  Z80res_2fatjet_st=-99 ;
  Z80res_3fatjet_st=-99 ;
  WLowWMassCut50_1fatjet_st=0. ;
  WLowWMassCut50_2fatjet_st =0. ;
  WLowWMassCut50_3fatjet_st =0. ;
  WLowWMassCut80_1fatjet_st =0. ;
  WLowWMassCut80_2fatjet_st =0. ;
  WLowWMassCut80_3fatjet_st =0. ;
  ZLowWMassCut50_1fatjet_st =0. ;
  ZLowWMassCut50_2fatjet_st =0. ;
  ZLowWMassCut50_3fatjet_st =0. ;
  ZLowWMassCut80_1fatjet_st =0. ;
  ZLowWMassCut80_2fatjet_st =0. ;
  ZLowWMassCut80_3fatjet_st =0. ;
  WHighWMassCut50_1fatjet_st =0. ;
  WHighWMassCut50_2fatjet_st =0. ;
  WHighWMassCut50_3fatjet_st =0. ;
  WHighWMassCut80_1fatjet_st =0. ;
  WHighWMassCut80_2fatjet_st=0. ;
  WHighWMassCut80_3fatjet_st =0. ;
  ZHighWMassCut50_1fatjet_st =0. ;
  ZHighWMassCut50_2fatjet_st =0. ;
  ZHighWMassCut50_3fatjet_st =0. ;
  ZHighWMassCut80_1fatjet_st =0. ;
  ZHighWMassCut80_2fatjet_st =0. ;
  ZHighWMassCut80_3fatjet_st =0. ;
  WD2Cut50_1fatjet_st =0. ;
  WD2Cut50_2fatjet_st =0. ;
  WD2Cut50_3fatjet_st =0. ;
  WD2Cut80_1fatjet_st =0. ;
  WD2Cut80_2fatjet_st =0. ;
  WD2Cut80_3fatjet_st =0. ;
  ZD2Cut50_1fatjet_st =0. ;
  ZD2Cut50_2fatjet_st =0. ;
  ZD2Cut50_3fatjet_st =0. ;
  ZD2Cut80_1fatjet_st =0. ;
  ZD2Cut80_2fatjet_st =0. ;
  ZD2Cut80_3fatjet_st =0. ;
  top50_1fatjet_st=false ;
  top50_2fatjet_st=false ;
  top50_3fatjet_st=false ;
  top80_1fatjet_st=false ;
  top80_2fatjet_st=false ;
  top80_3fatjet_st=false ;
  top50res_1fatjet_st=-99 ;
  top50res_2fatjet_st=-99 ;
  top50res_3fatjet_st=-99 ;
  top80res_1fatjet_st=-99 ;
  top80res_2fatjet_st=-99 ;
  top80res_3fatjet_st=-99 ;
  TopTagTau32Cut50_1fatjet_st =0. ;
  TopTagTau32Cut50_2fatjet_st =0. ;
  TopTagTau32Cut50_3fatjet_st =0. ;
  TopTagTau32Cut80_1fatjet_st =0. ;
  TopTagTau32Cut80_2fatjet_st =0. ;
  TopTagTau32Cut80_3fatjet_st =0. ;
  TopTagSplit23Cut50_1fatjet_st =0. ;
  TopTagSplit23Cut50_2fatjet_st =0. ;
  TopTagSplit23Cut50_3fatjet_st =0. ;
  TopTagSplit23Cut80_1fatjet_st =0. ;
  TopTagSplit23Cut80_2fatjet_st =0. ;
  TopTagSplit23Cut80_3fatjet_st =0. ;
  
  m_top0Chi2M = 0;
  m_top1Chi2M = 0;
  m_w0Chi2M = 0;
  m_w1Chi2M = 0;
  m_chi2 = -1;
  m_mt2Chi2 = 0;

  RMPF=RG = 0;

  isMEPhOverlapOK = true;

  passfilter=passL1_J12=passL1_J50=passL1_MU6=passL1_2MU6=passL1_EM10=passL1_2EM7=passHLT_xe35=passHLT_xe50=passHLT_xe80 = 0;

  pass1Ltriggers=pass1etriggers=pass1mutriggers=pass1phtriggers = passMETtriggers=0;

  passcleaning = 0;

  el_TrigMatched=mu_TrigMatched = 0;

  HLT_g200_etcut = 0;
  HLT_g35_loose_L1EM15 = 0;
  HLT_g40_loose_L1EM15 = 0;
  HLT_g45_loose_L1EM15 = 0;
  HLT_g50_loose_L1EM15 = 0;

  charge_1e=charge_1mu=charge_2e=charge_2mu = 0;
  charge_1l=charge_2l = 0;

  sumet = 0;
  sumet_jet = 0;
  sumet_el = 0;
  sumet_y = 0;
  sumet_mu = 0;
  sumet_softTrk = 0;
  sumet_softClu = 0;


  leadb1=leadb2 = 0;
  leadb1_60=leadb2_60 = 0;
  leadb1_85=leadb2_85 = 0;
  nbaselineLep = 0;
  

  jet1_bad = 0;
  jet1_fmax=jet1_fch=jet1_fchfmax=jet1_fem=jet1_jvtxf = 0;
  jet2_fmax=jet2_fch=jet2_fchfmax=jet2_fem=jet2_jvtxf = 0;

  mrel=mst=msz=ms = 0;
  truthpt = 0;
  whichb = 0;
  whichB = 0;
  dphimin3 = 0;
  dphimin4=dphimin4_35=dphimin4_lep=dphimin4_orig = dphimin2 = dphimin2_orig=0;

  mdeltaR= costhetaR=costhetaRp1=shatR=Rpt=gaminvRp1 = 0;
  
  maxDRbb = 0;
  maxDRassym = 0;
  maxDRmbb = 0;
  maxminDRbb = 0;
  maxminDRassym = 0;
  maxminDRmbb = 0;
  minDRbb = 0;
  minDRassym = 0;
  minDRmbb = 0;

  nextrajets50 = 0;

  phi_MET = 0;

  ntruthjets30 = 0;

  HEntries= HEntWgt= HEntWgtPU = 0;

  // added calum
  test= meff= MET_sig= mjj=mjy = 0;
  MET_px=MET_py=MET_pt = 0;
  asym_bjets = 0;

  //m_1bjj = 0;
  m_2bjj = 0;

  //lbn = 0;


  weight_new_filt = 0;
  weight_new = 0;
  LumiWeight=LumiWeightOld = 0;
  jvtweight = 0;
  jvtweightUP = 0;
  jvtweightDOWN = 0;
  SherpaWeight = 0;
  btagweight=btagweightBUP=btagweightBDOWN = 0;
  btagweightCUP=btagweightCDOWN = 0;
  btagweightLUP=btagweightLDOWN = 0;
  btagweightExUP=btagweightExDOWN = 0;
  btagweightExCUP=btagweightExCDOWN = 0;
  

  processtype = 0;

  RunNumber = 0;
  EventNumber = 0;

  numVtx= numGoodVtx = 0;

  eT_miss=eT_miss_lep= eT_miss_orig= etmll=etlep=pT_ll=mvh = eT_miss_NonInt =0;
  eT_miss_inv = eT_miss_track=0;
  phi_met_orig=-999;

  eT_miss_seed=sumet_seed=sumet_refjet_seed=HT_seed=sumet_soft_seed = 0;
  nbjets_seed=met_avPt_seed = 0;
  pT_1jet_seed=pT_2jet_seed = 0;
  mlb1=mlb2=mlbmin = 0;

  mlj1=mlj2 = 0;

  dRlb1=dRlb2=dRljmin=dRljmax = 0;
  dRb1b2 = 0;
  pT_1leadbtag=pT_2leadbtag = 0;


  dRl1l2 = 0;
  dRj1j2=dRlj1=dRlj2 = 0;
  dRjmax=dRjmin = 0;
  dRlbmin=0;
  dEtajmin = 0;
  m_top_minDR1 =-99;
  m_top_minDR2 =-99;


  ht2= ht3=HT = 0;

  eT_miss_phi = 0;

  track_eT_miss= track_eT_miss_phi= track_eT_miss_px= track_eT_miss_py = 0;

  dphimets = dphi_track = dphi_track_orig = 0;

  metsig=metsigET=metsigHT=0;

  pT_1jet= pT_2jet= pT_3jet= pT_4jet= pT_5jet= eta_1jet= eta_2jet= eta_3jet= eta_4jet= eta_5jet= pT_lep= eta_lep = 0; 
  pT_lastjet = 0;
  
  passtauveto=0;


  flav_1jet=flav_2jet=flav_3jet=flav_4jet=-1;

  hastau=hasb=hasc=-1;

  A=Ap=ST=Sp = 0;
  y_1jet=y_2jet = 0;


  eta_1bjet_Z  = 0;
  eta_2bjet_Z  = 0;
  dR_1bjet_Z  = 0;
  dR_2bjet_Z  = 0;
  
  ttbar_class = -999;

  nPh = 0;
  pT_1ph = 0;
  eta_1ph = 0;
  phi_1ph = 0;
  dPhi_1ph = 0;
  dPhi_1ph1jet = 0;
  dPhi_1jet_Z = 0;
  dPhi_2jet_Z = 0;

  bindex.shrink_to_fit();
  bindex.clear();

  pT_V=eta_V=phi_V=E_V = 0;
  pT_B = 0;
  TruthNuMET.shrink_to_fit();
  TruthNuMET.clear();

  pT_1lep= pT_2lep = pT_3lep = 0;
    
  phi_1lep= phi_2lep = phi_3lep = 0;

  eta_1lep= eta_2lep = eta_3lep = 0;

  charge_1lep= charge_2lep = charge_3lep = 0;

  flav_1lep= flav_2lep = flav_3lep = 0;

  pT_1bjet= pT_2bjet= pT_3bjet = 0;

  pT_1lightjet= pT_2lightjet = 0;

  weight = 0;

  EventWeight = 0;

  phi_1jet= phi_2jet= phi_3jet= phi_4jet= phi_5jet= phi_lep = 0;
  dR_1jet= dR_2jet= dR_3jet= dR_4jet = 0;
  dPhi_1jet= dPhi_2jet= dPhi_3jet= dPhi_4jet = 0;
  dPhi_1bjet= dPhi_2bjet = 0;
  dPhi_1jet_orig= dPhi_2jet_orig= dPhi_3jet_orig= dPhi_4jet_orig = 0;
  dPhi_1bjet_orig= dPhi_2bjet_orig = 0;

  dPhi_1j2j  = 0;
  dPhi_2j3j  = 0;
  dPhi_1j3j  = 0;


  dPhi_min = 0;

  w_MV2c20_1= w_MV2c20_2= w_MV2c20_3 = 0;

  W_tr_mass = 0;

  lep1_pt= lep2_pt= mll = 0;

  nj_good=nj_good35=nextrajets = 0;

  btagSFCent = 0;
  bEffUp= bEffDown= cEffUp= cEffDown= lEffUp= lEffDown = 0;

  btagSFCentTTBAR = 0;
  bEffUpTTBAR= bEffDownTTBAR= cEffUpTTBAR= cEffDownTTBAR= lEffUpTTBAR= lEffDownTTBAR = 0;

  btagzerolep_channel= btag1e_channel= btag1mu_channel = 0;
  btag2e_channel= btag2mu_channel = 0;
  btagemu_channel = 0;
  btag0l_channel = 0;  

  meff2j= meff3j= meff4j= meffinc = 0;  

  num_bjets= num_bjets25= num_bjets30= num_bjets35=num_bjets40 = 0;

  num_bjets60= num_bjets85 = 0;

  MT=MT_orig=MTjmin=MTbmin=MTbmax=MTblmin = 0;
  MTj1=MTj2=MTj3=MTj4=MTjmax = 0;
  topness=pt_asym = 0;
  centrality = 0;

  m_top1=m_top2 = 0;

  pileupweight = 0;
  pileupweightUP = 0;
  pileupweightDOWN = 0; 
 
  AnalysisWeight=LeptonWeight=LostLeptonWeight=TriggerWeight = 0;

  ElecWeight=MuonWeight=PhotonWeight = 0;
  ElecWeightIDUP = 0;   
  ElecWeightIDDOWN = 0; 
  ElecWeightISOUP = 0;  
  ElecWeightISODOWN = 0;
  ElecWeightRECOUP = 0; 
  ElecWeightRECODOWN = 0;
  ElecWeightTRIGUP = 0;  
  ElecWeightTRIGDOWN = 0;

  MuonWeightSTATUP = 0;
  MuonWeightSTATDOWN = 0;
  MuonWeightSYSUP = 0;
  MuonWeightSYSDOWN = 0;
  MuonWeightTRIGSTATUP = 0;
  MuonWeightTRIGSTATDOWN = 0;
  MuonWeightTRIGSYSTUP = 0;
  MuonWeightTRIGSYSTDOWN = 0;
  MuonWeightISOSTATUP = 0;
  MuonWeightISOSTATDOWN = 0;
  MuonWeightISOSYSTUP = 0;
  MuonWeightISOSYSTDOWN = 0;
  MuonWeightTTVASTATUP = 0;
  MuonWeightTTVASTATDOWN = 0;
  MuonWeightTTVASYSTUP = 0;
  MuonWeightTTVASYSTDOWN = 0;


  top_hfor_type= top_hfor_type_2 = 0;

  mu = 0;

  nvtx = 0;
  
  mct2b= mctcorr2b= mbb = 0;
  mct1b=mct0b=mctcorr1b=mctcorr0b = 0;
  mctcorr2bfix=mctcorr1bfix=mctcorr0bfix = 0;
  
  mctjj=mctcorrjj = 0;
  amt2alt=amt2new=amt2new2= amt2_zerolep=mt2alt=mt2alt2=mt2alt3=mt2alt4=mt2susy = 0;

  amt2.shrink_to_fit();
  amt2.clear();
    
  mt2.shrink_to_fit();
  mt2.clear();

  dphibb= dphijj= drbb = 0;

  weight_ttbar_powheg = 0;


  averageIntPerXing = 0;
  averageIntPerXingCorr = 0;
  lbn = 0;



  HLT_e24_lhmedium_L1EM20VHORHLT_e60_lhmedium = 0;
  HLT_e24_lhmedium_L1EM18VHORHLT_e60_lhmedium = 0;
  HLT_mu20_iloose_L1MU15ORHLT_mu50 = 0;

  HLT_e24_lhmedium_L1EM20VH = 0;
  HLT_e24_lhmedium_L1EM18VH = 0;
  HLT_mu20_iloose_L1MU15 = 0;
  
  HLT_g120_loose = 0;

  HLT_e24_lhtight_iloose = 0;
  HLT_e20_medium = 0;
  HLT_2e17_loose = 0;
  HLT_mu24_imedium = 0;
  HLT_mu26 = 0;
  HLT_2mu6 = 0;
  HLT_xe35 = 0;
  HLT_xe60 = 0;
  HLT_xe70 = 0;
  HLT_xe70_tc_lcw =0;
  HLT_xe70_mht =0;
  HLT_xe80 = 0;
  HLT_xe90_mht_wEFMu_L1XE50 =0;
  HLT_xe90_mht_L1XE50 =0 ;
  HLT_xe100 = 0;
  HLT_xe100_mht_L1XE50 =0;
  HLT_xe110_mht_L1XE50 = 0;
  HLT_j100_xe80 = 0;
  HLT_j80_xe80 = 0;
  HLT_j400 = 0;
  HLT_j360 = 0;

  HLT_e60_lhmedium = 0;
  HLT_mu50 = 0;
  



  pT_1djet = 0;
  pT_2djet = 0;
  pT_3djet = 0;
  pT_4djet = 0;
  mdd = 0;
  mctdd = 0;
  nd_good = 0;
  ddphimin4 = 0;
  eT_miss_dd = 0;


  m_MT2ll = 0;


  // Variables: JETS
  nJets = 0;
 

  // Variables: Electrons
  nEl = 0;
 

  // Variables: Muons
  nMu = 0;


}




