
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                   Track jet variables
//____________________________________________________
//
void ExportTrees::FillTrackJetVariables(){
       

  Jets emjets = (*m_jets);

  Jets jets = (*m_trackjets);
  Jets bjets60 = (*m_trackbjets60);
  Jets bjets70 = (*m_trackbjets70);
  Jets bjets77 = (*m_trackbjets77);
  Jets bjets85 = (*m_trackbjets85);
 
  int njets = jets.size();
  int nbjets60 = bjets60.size();
  int nbjets70 = bjets70.size();
  int nbjets77 = bjets77.size();
  int nbjets85 = bjets85.size();

 
  int nbjets60_unmatched=0;
  int nbjets70_unmatched=0;
  int nbjets77_unmatched=0;
  int nbjets85_unmatched=0;
  

  Jet b60,b70,b77,b85;
  



  //loop over emjets and check for matched to track jets dR=0.3
  double dR=0.3;
  for(unsigned int j=0;j<jets.size();j++){
      Jet jet = (*jets[j]);
      bool ismatched=false;
      for(unsigned int i=0;i<emjets.size();i++){
	Jet tj = (*emjets[i]);
    	//dR match
	ismatched = tj.DeltaR(jet) < dR;
	if(ismatched)break;
      }//end loop over emtopo jets

      //count unmatched b-tagged track jets
      if(!ismatched){
      	if(jet.MV2>0.9025 )  {
	  nbjets60_unmatched++;
	  if(b60.Pt()==0)b60=jet;
	}
	if(jet.MV2>0.7475 ) {
	  nbjets70_unmatched++;
	  if(b70.Pt()==0)b70=jet;
	}
	if(jet.MV2>0.5140 ) {
	  nbjets77_unmatched++;
	  if(b77.Pt()==0)b77=jet;
	}
	if(jet.MV2>-0.0168) {
	  nbjets85_unmatched++;
	  if(b85.Pt()==0)b85=jet;
	}
      }
  }//end loop over trackjets
  
  outputntuple->pT_1trackbjet60_unmatched=b60.Pt();
  outputntuple->pT_1trackbjet70_unmatched=b70.Pt();
  outputntuple->pT_1trackbjet77_unmatched=b77.Pt();
  outputntuple->pT_1trackbjet85_unmatched=b85.Pt();


  if(nbjets60>0)b60=(*bjets60[0]);
  if(nbjets70>0)b70=(*bjets70[0]);
  if(nbjets77>0)b77=(*bjets77[0]);
  if(nbjets85>0)b85=(*bjets85[0]);

  outputntuple->pT_1trackbjet60=b60.Pt();
  outputntuple->pT_1trackbjet70=b70.Pt();
  outputntuple->pT_1trackbjet77=b77.Pt();
  outputntuple->pT_1trackbjet85=b85.Pt();
 




 
  outputntuple->ntrackjets    = njets;
  outputntuple->nbtrackjets60 = nbjets60;
  outputntuple->nbtrackjets70 = nbjets70;
  outputntuple->nbtrackjets77 = nbjets77;
  outputntuple->nbtrackjets85 = nbjets85;

 
  outputntuple->nbtrackjets60_unmatched=nbjets60_unmatched;
  outputntuple->nbtrackjets70_unmatched=nbjets70_unmatched;
  outputntuple->nbtrackjets77_unmatched=nbjets77_unmatched;
  outputntuple->nbtrackjets85_unmatched=nbjets85_unmatched;



}

