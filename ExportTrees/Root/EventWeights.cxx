#include "ExportTrees/Export.h"
#include "TH2D.h"

using namespace std;


void ExportTrees::FillEventWeights(double LumiWeight, double TriggerWeight){


  
  Jet j1,j2;
  Jets jets = (*m_jets);
  int njets = jets.size();
  if(njets>0){
    j1 = (*jets[0]);  
    if(njets>1){
      j2 = (*jets[1]);
    }
  }
  
 
  outputntuple->j1_bweight.clear();
  outputntuple->j2_bweight.clear();
  outputntuple->j1_bweight.push_back(hcalib->GetBinContent(hcalib->GetXaxis()->FindBin(fabs(j1.Eta())),hcalib->GetYaxis()->FindBin(fabs(j1.Pt()))));
  outputntuple->j2_bweight.push_back(hcalib->GetBinContent(hcalib->GetXaxis()->FindBin(fabs(j2.Eta())),hcalib->GetYaxis()->FindBin(fabs(j2.Pt()))));
  outputntuple->j1_bweight.push_back(hcalib2->GetBinContent(hcalib2->GetXaxis()->FindBin(fabs(j1.Eta())),hcalib2->GetYaxis()->FindBin(fabs(j1.Pt()))));
  outputntuple->j2_bweight.push_back(hcalib2->GetBinContent(hcalib2->GetXaxis()->FindBin(fabs(j2.Eta())),hcalib2->GetYaxis()->FindBin(fabs(j2.Pt()))));
  outputntuple->j1_bweight.push_back(hcalib3->GetBinContent(hcalib3->GetXaxis()->FindBin(fabs(j1.Eta())),hcalib3->GetYaxis()->FindBin(fabs(j1.Pt()))));
  outputntuple->j2_bweight.push_back(hcalib3->GetBinContent(hcalib3->GetXaxis()->FindBin(fabs(j2.Eta())),hcalib3->GetYaxis()->FindBin(fabs(j2.Pt()))));
  


  outputntuple->TriggerWeight= inputntuple->TriggerWeight2;
  outputntuple->LumiWeight = LumiWeight;
  double pileupweight = inputntuple->pileupweight;
  if(!isfinite(pileupweight))
    pileupweight=1.0;

  outputntuple->SherpaWeight=inputntuple->SherpaWeight;

  outputntuple->pileupweight=pileupweight;
  outputntuple->pileupweightUP=inputntuple->pileupweightUP;
  outputntuple->pileupweightDOWN=inputntuple->pileupweightDOWN;
  outputntuple->AnalysisWeight=inputntuple->AnalysisWeight;
  outputntuple->TriggerWeight=TriggerWeight;
  outputntuple->btagweight=inputntuple->btagweight;
  outputntuple->jvtweight=inputntuple->jvtweight;
  outputntuple->jvtweightUP=inputntuple->jvtweightUP;
  outputntuple->jvtweightDOWN=inputntuple->jvtweightDOWN;
  outputntuple->btagweightBUP=inputntuple->btagweightBUP;
  outputntuple->btagweightBDOWN=inputntuple->btagweightBDOWN;
  outputntuple->btagweightCUP=inputntuple->btagweightCUP;
  outputntuple->btagweightCDOWN=inputntuple->btagweightCDOWN;
  outputntuple->btagweightLUP=inputntuple->btagweightLUP;
  outputntuple->btagweightLDOWN=inputntuple->btagweightLDOWN;
  outputntuple->btagweightExUP=inputntuple->btagweightExUP;
  outputntuple->btagweightExDOWN=inputntuple->btagweightExDOWN;
  outputntuple->btagweightExCUP=inputntuple->btagweightExCUP;
  outputntuple->btagweightExCDOWN=inputntuple->btagweightExCDOWN;
  
  //outputntuple->LeptonWeight=inputntuple->ElecWeight*inputntuple->MuonWeight;
  outputntuple->ElecWeight=inputntuple->ElecWeightReco*inputntuple->ElecWeightTrigger;
  Particles muons = (*m_muons);
  if(muons.size()>0)
    outputntuple->MuonWeight=inputntuple->MuonWeightReco*inputntuple->MuonWeightTrigger;
  else
    outputntuple->MuonWeight=1;
  
  outputntuple->ElecWeightReco=inputntuple->ElecWeightReco;
  outputntuple->MuonWeightReco=inputntuple->MuonWeightReco;
  outputntuple->ElecWeightTrig=inputntuple->ElecWeightTrigger;
  outputntuple->MuonWeightTrig=inputntuple->MuonWeightTrigger;
  
  outputntuple->PhotonWeight=inputntuple->PhotonWeight;
  
  outputntuple->ElecWeightIDUP=inputntuple->ElecWeightIDUP;
  outputntuple->ElecWeightIDDOWN=inputntuple->ElecWeightIDDOWN;
  outputntuple->ElecWeightISOUP=inputntuple->ElecWeightISOUP;
  outputntuple->ElecWeightISODOWN=inputntuple->ElecWeightISODOWN;
  outputntuple->ElecWeightRECOUP=inputntuple->ElecWeightRECOUP;
  outputntuple->ElecWeightRECODOWN=inputntuple->ElecWeightRECODOWN;
  outputntuple->ElecWeightTRIGUP=inputntuple->ElecWeightTRIGUP;
  outputntuple->ElecWeightTRIGDOWN=inputntuple->ElecWeightTRIGDOWN;
  outputntuple->ElecWeightTRIGEFFUP=inputntuple->ElecWeightTRIGEFFUP;
  outputntuple->ElecWeightTRIGEFFDOWN=inputntuple->ElecWeightTRIGEFFDOWN;  
  
  outputntuple->MuonWeightSTATUP=inputntuple->MuonWeightSTATUP;
  outputntuple->MuonWeightSTATDOWN=inputntuple->MuonWeightSTATDOWN;
  outputntuple->MuonWeightSYSUP=inputntuple->MuonWeightSYSUP;
  outputntuple->MuonWeightSYSDOWN=inputntuple->MuonWeightSYSDOWN;
  outputntuple->MuonWeightTRIGSTATUP=inputntuple->MuonWeightTRIGSTATUP;
  outputntuple->MuonWeightTRIGSTATDOWN=inputntuple->MuonWeightTRIGSTATDOWN; 
  outputntuple->MuonWeightTRIGSYSTUP=inputntuple->MuonWeightTRIGSYSTUP;  
  outputntuple->MuonWeightTRIGSYSTDOWN=inputntuple->MuonWeightTRIGSYSTDOWN;
  outputntuple->MuonWeightISOSTATUP=inputntuple->MuonWeightISOSTATUP;
  outputntuple->MuonWeightISOSTATDOWN=inputntuple->MuonWeightISOSTATDOWN; 
  outputntuple->MuonWeightISOSYSTUP=inputntuple->MuonWeightISOSYSTUP;
  outputntuple->MuonWeightISOSYSTDOWN=inputntuple->MuonWeightISOSYSTDOWN;
  outputntuple->MuonWeightTTVASTATUP=inputntuple->MuonWeightTTVASTATUP;
  outputntuple->MuonWeightTTVASTATDOWN=inputntuple->MuonWeightTTVASTATDOWN;
  outputntuple->MuonWeightTTVASYSTUP=inputntuple->MuonWeightTTVASYSTUP;
  outputntuple->MuonWeightTTVASYSTDOWN=inputntuple->MuonWeightTTVASYSTDOWN;
}

