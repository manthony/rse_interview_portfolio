#include "ExportTrees/Export.h"

using namespace std;

void ExportTrees::FillTriggers(bool isData){ 


  Particles electrons = (*m_electrons);
  Particle e1;
  if(electrons.size()>=1){
    e1=(*electrons[0]);
  }
  
  Particles muons = (*m_muons);
  Particle mu1;
  if(muons.size()>=1){
    mu1=(*muons[0]);
  }
  

  int year = outputntuple->treatAsYear;
  outputntuple->passMETtriggers=inputntuple->METTrigPassed;


  outputntuple->HLT_e24_lhmedium_L1EM18VH=inputntuple->HLT_e24_lhmedium_L1EM18VH;
  outputntuple->HLT_e24_lhmedium_L1EM20VH=inputntuple->HLT_e24_lhmedium_L1EM20VH;
  
  outputntuple->HLT_xe35=inputntuple->HLT_xe35;
  outputntuple->HLT_xe60=inputntuple->HLT_xe60;
  
  outputntuple->HLT_xe70=inputntuple->HLT_xe70;
  outputntuple->HLT_xe70_tc_lcw=inputntuple->HLT_xe70_tc_lcw;
  outputntuple->HLT_xe70_mht=inputntuple->HLT_xe70_mht;
  outputntuple->HLT_xe80=inputntuple->HLT_xe80;
  outputntuple->HLT_xe90_mht_wEFMu_L1XE50=inputntuple->HLT_xe90_mht_wEFMu_L1XE50;
  outputntuple->HLT_xe90_mht_L1XE50=inputntuple->HLT_xe90_mht_L1XE50;
  outputntuple->HLT_xe100=inputntuple->HLT_xe100;
  outputntuple->HLT_xe100_mht_L1XE50=inputntuple->HLT_xe100_mht_L1XE50;
  outputntuple->HLT_xe110_mht_L1XE50=inputntuple->HLT_xe110_mht_L1XE50;


  outputntuple->HLT_j100_xe80=inputntuple->HLT_j100_xe80;
  outputntuple->HLT_j80_xe80=inputntuple->HLT_j80_xe80;
  outputntuple->HLT_j360=inputntuple->HLT_j360;
  outputntuple->HLT_j400=inputntuple->HLT_j400;

  outputntuple->HLT_6j45_0eta240=inputntuple->HLT_6j45_0eta240;
  outputntuple->HLT_6j60=inputntuple->HLT_6j60;

  outputntuple->HLT_e60_lhmedium=inputntuple->HLT_e60_lhmedium;

  outputntuple->HLT_g120_loose=inputntuple->HLT_g120_loose && inputntuple->matched_HLT_g120_loose;
  outputntuple->HLT_g140_loose=inputntuple->HLT_g140_loose && inputntuple->matched_HLT_g140_loose;
  outputntuple->HLT_g200_etcut=inputntuple->HLT_g200_etcut;
  outputntuple->HLT_g35_loose_L1EM15=inputntuple->HLT_g35_loose_L1EM15;
  outputntuple->HLT_g40_loose_L1EM15=inputntuple->HLT_g40_loose_L1EM15;
  outputntuple->HLT_g45_loose_L1EM15=inputntuple->HLT_g45_loose_L1EM15;
  outputntuple->HLT_g50_loose_L1EM15=inputntuple->HLT_g50_loose_L1EM15;
  
  
  
  bool pass1etriggers=0; 
  bool pass1mutriggers=0; 
  bool pass1phtriggers=0;
  
  if(year==2015){
    pass1etriggers=( (inputntuple->HLT_e24_lhmedium_L1EM20VH&& inputntuple->matched_HLT_e24_lhmedium_L1EM20VH)
                     || (inputntuple->HLT_e60_lhmedium && inputntuple->matched_HLT_e60_lhmedium)
                     || (inputntuple->HLT_e120_lhloose && inputntuple->matched_HLT_e120_lhloose ));

    
    pass1mutriggers=( (inputntuple->HLT_mu20_iloose_L1MU15 && inputntuple->matched_HLT_mu20_iloose_L1MU15)
                      || (inputntuple->HLT_mu50&& inputntuple->matched_HLT_mu50));

    pass1phtriggers=(inputntuple->HLT_g120_loose && inputntuple->matched_HLT_g120_loose);

  }
  else if(year==2016){
    pass1etriggers=  ( (inputntuple->HLT_e26_lhtight_nod0_ivarloose && inputntuple->matched_HLT_e26_lhtight_nod0_ivarloose)
                       || (inputntuple->HLT_e60_lhmedium_nod0 && inputntuple->matched_HLT_e60_lhmedium_nod0)
                       || (inputntuple->HLT_e140_lhloose_nod0 && inputntuple->matched_HLT_e140_lhloose_nod0));

    pass1mutriggers= ( (inputntuple->HLT_mu26_ivarmedium && inputntuple->matched_HLT_mu26_ivarmedium)
                       || (inputntuple->HLT_mu50 && inputntuple->matched_HLT_mu50));

    pass1phtriggers=(inputntuple->HLT_g140_loose && inputntuple->matched_HLT_g140_loose);


  } 
  
  
  outputntuple->pass1etriggers=pass1etriggers;
  outputntuple->pass1mutriggers=pass1mutriggers;
  outputntuple->pass1phtriggers=pass1phtriggers;

  outputntuple->passMETtriggers=inputntuple->METTrigPassed;

}
