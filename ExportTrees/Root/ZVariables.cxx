
#include "ExportTrees/Export.h"
using namespace std;


//____________________________________________________
//                       FILL 
//                   Z variables
//____________________________________________________
//
void ExportTrees::FillZVariables(){
  Particle Z = (*m_Z);
  
  

  Jets jets = (*m_jets);
  Jets bjets = (*m_bjets);
   

  double pT_ll=0;
  double mvh=0;
  double eta_1bjet_Z =0;
  double eta_2bjet_Z =0;
  double dR_1bjet_Z =0;
  double dR_2bjet_Z =0;
  
  double mll;
  mll=0;
  double dPhi_1jet_Z = -999;
  double dPhi_2jet_Z = -999;
 
  mll=Z.M();
  pT_ll=Z.Pt();
  //Z+bjets variables
  if(bjets.size()>=2){
    mvh = (Z+(*bjets[0])+(*bjets[1])).M();
    eta_1bjet_Z = bjets[0]->Eta() - Z.Eta();
    eta_2bjet_Z = bjets[1]->Eta() - Z.Eta();
    dR_1bjet_Z  = bjets[0]->DeltaR(Z);
    dR_2bjet_Z  = bjets[1]->DeltaR(Z);
  }
  //dPhi Z and leading two jets
  if(jets.size()>0){
    dPhi_1jet_Z = TVector2::Phi_mpi_pi(jets[0]->Phi() - Z.Phi());
  }
  if(jets.size()>1){
    dPhi_2jet_Z = TVector2::Phi_mpi_pi(jets[1]->Phi() - Z.Phi());
  }
  
  outputntuple->mvh=mvh;
  outputntuple->eta_1bjet_Z=eta_1bjet_Z;
  outputntuple->eta_2bjet_Z=eta_2bjet_Z;
  outputntuple->dR_1bjet_Z=dR_1bjet_Z;
  outputntuple->dR_2bjet_Z=dR_2bjet_Z;
  
  outputntuple->dR_1bjet_Z=dR_1bjet_Z;
  outputntuple->dR_1bjet_Z=dR_1bjet_Z;
  
  outputntuple->dPhi_1jet_Z=dPhi_1jet_Z;
  outputntuple->dPhi_2jet_Z=dPhi_2jet_Z;
  
  outputntuple->pT_ll=pT_ll;
  outputntuple->mll=mll;
	   
}
