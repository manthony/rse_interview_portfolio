
#include "ExportTrees/Export.h"
using namespace std;

//____________________________________________________
//                       FILL 
//                  lepton variables
//____________________________________________________
//
void ExportTrees::FillLeptonVariables(){


  outputntuple->passtauveto=inputntuple->passtauveto;

  Particles leptons = (*m_leptons);
  
  Particle l1;
  Particle l2;
  Particle l3; 

  int nLep = leptons.size();
  if(nLep>=1){
    l1=(*leptons[0]);
    if(nLep>=2){
      l2=(*leptons[1]);
      if(nLep>=3){
	l3=(*leptons[2]);
      }
    }
  }
  
  
  outputntuple->pT_1lep=l1.Pt();
  outputntuple->pT_2lep=l2.Pt();
  outputntuple->pT_3lep=l3.Pt();

  outputntuple->eta_1lep=l1.Eta();
  outputntuple->eta_2lep=l2.Eta();
  outputntuple->eta_3lep=l3.Eta();
     
  outputntuple->charge_1l=l1.charge;
  outputntuple->charge_2l=l2.charge;
  outputntuple->charge_3l=l3.charge;

  outputntuple->flav_1lep=l1.flav;
  outputntuple->flav_2lep=l2.flav;
  outputntuple->flav_3lep=l3.flav;

  outputntuple->dRl1l2= l1.DeltaR(l2);
    
  int nEl = inputntuple->nEl;
  int nMu = inputntuple->nMu;
  outputntuple->nEl = nEl;
  outputntuple->nMu = nMu;
  
  int nbaselineEl = inputntuple->nbaselineEl;
  int nbaselineMu = inputntuple->nbaselineMu;
  int nbaselineLep= nbaselineEl+nbaselineMu;
  outputntuple->nbaselineLep=nbaselineLep;
     



}

