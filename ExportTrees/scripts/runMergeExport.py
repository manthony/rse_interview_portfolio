
import argparse

def Merge(FileIn,OutDir):
    name={}
    for f in open("ExportTrees/data/sample_ref.txt"):
        f=f.rstrip().split(".")
        if len(f)>2:
            name[f[1]]=f[2].replace("physics_Main","data")
     
        
        

    folder = sys.argv[1]
    folder=FileIn


    temp= folder.split("/")
    temp[len(temp)-2]="summary_"+temp[len(temp)-2]
    newfolder="/".join(temp)

    print temp
    print newfolder

    add=""
    #if len(sys.argv)>2:
     #   add=sys.argv[2]

    os.system("mkdir -p "+newfolder)
    for f in glob.glob(folder+"*"+add+"*"):
        fout=f
        f = f.split(".")
        print f
        if "output_tree.root" in fout:
            f[4]=f[2]
        print f[4]
        print name[f[4]]
        
        try:
            outname=f[4]+"."+name[f[4]]+"."+f[5]+"."+f[6].replace("/","")+".root"
            isfile = os.path.isfile(newfolder+outname)
            if not isfile:
                os.system(" hadd -f "+newfolder+outname+"  "+fout+"*/*/*root*")
            #os.system(" hadd -f "+newfolder+outname+"  "+fout+"*/*root*")
            else:
                print outname," already exists!!!"
        except KeyError:
            try:
                print "hereadfasioaigh"
                outname=f[2]+"."+name[f[2]]+"."+f[4]+"."+f[5].replace("/","")+".root"
                print outname
                print os.path.isfile(outname)
            #os.system(" hadd -f "+newfolder+outname+"  "+fout+"*/*/*root*")
            except KeyError:
                if "RFinal" in fout or "hmoss" in f:
                    outname=fout.split(folder)[1]
                    os.system("hadd -f "+newfolder+outname+"  "+fout+"*/*/*root*")
                else:
                    print f
                    print "CANNOT MERGE", fout    

    return OutDirName


def main:
    parser  = argparse.ArgumentParser(description='runExport: export trees from a list of files/directory')
    parser.add_argument('--input', action='store', dest='inputlist',
                        default="",
                        help='list of input files ',type=str)
    parser.add_argument('--inDir', action='store', dest='inDir',
                        default="",
                        help='location of input dir ',type=str)
    parser.add_argument('--inFile', action='store', dest='inFile',
                        default="",
                        help='single input file',type=str)
    parser.add_argument('--filter', action='store', dest='filter',
                        default="",
                        help='name of the filter to use..',type=str)
    parser.add_argument('--outDir', action='store', dest='outDir',
                        default="./default_out/",
                        help='location of output dir ',type=str)
    parser.add_argument('--smr', action='store', dest='smr',
                        default=False,
                        help='do jetsmearing ',type=bool)
    parser.add_argument('--nosys', action='store', dest='nosys',
                        default=True,
                        help='turn off systematics ',type=bool)
    parser.add_argument('--isPythia8', action='store', dest='isPythia8',
                        default=False,
                        help='is Pythia8 --> use nevents ',type=bool)
    parser.add_argument('--usexAODJets', action='store', dest='usexAODJets',
                        default=False,
                        help='use xAODJets from collection tree instead for more substrucutre studies ',type=bool)
    parser.add_argument('--maxevents', action='store', dest='maxevents',
                        default=-1,
                        help='set the maximum number of events to run on ',type=float)
    parser.add_argument('--test', action='store_true', dest='test',
                        default=False,
                        help='run a quick test ')
    
    
    m=parser.parse_args()
    
    import sys,os,glob,logging
    import gc
    import ROOT
    from ROOT import gROOT,gSystem,PyConfig
    ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")
    
    PyConfig.IgnoreCommandLineOptions = True
    from ROOT import ExportTrees
    
    logging.basicConfig(level=logging.DEBUG,format='%(name)-12s :: %(levelname)-8s ::       %(message)s')
    msg=logging.getLogger('runExport.py ')
    msg.info('Starting  '+str(__file__))




    msg.debug(m.inDir)
    # For the job to work correctly, need to re-work this step, to make it so that it can accept a directory or a list of files
    in_samples=[]
    if m.inDir!="":
        for sample in glob.glob(m.inDir+"*.root"):
            in_samples.append(sample)
    elif m.inputlist!="":
        for sample in open(m.inputlist):
            in_samples.append(sample.rstrip())
    elif m.inFile!="":
        in_samples.append(m.inFile)
    else:
        msg.error("No in samples found! you need to use --input <filename>.txt or --input <dir> ")
        sys.exit(1)

        if len(in_samples)==0:
            msg.error("No samples found!! exiting...")
            sys.exit(1)

            if not os.path.exists(m.outDir):
                os.system("mkdir -p "+m.outDir)
    
    ##Merging code in ExportTrees in export job
    
    merge_samples=[]
    if m.inDir!="":
        for sample in glob.glob(m.inDir+"*.root"):
            in_samples.append(sample)
    elif m.inputlist!="":
        for sample in open(m.inputlist):
            in_samples.append(sample.rstrip())
    elif m.inFile!="":
        in_samples.append(m.inFile)
    else:
        msg.error("No in samples found! you need to use --input <filename>.txt or --input <dir> ")
        sys.exit(1)

        if len(in_samples)==0:
            msg.error("No samples found!! exiting...")
            sys.exit(1)

            if not os.path.exists(m.outDir):
                os.system("mkdir -p "+m.outDir)
    



    import subprocess
    

    for sample in in_samples:
        if "#" in sample: continue
        temp=sample.split("/")
        temp=temp[len(temp)-1]
        out_sample=m.outDir+"/"+temp
        if "jetjet" in sample:
            isPythia8=True
        else:
            isPythia8=False
            export = ROOT.ExportTrees(sample,out_sample,isPythia8,m.filter,m.nosys,m.smr)
            export.m_usexAODJets=m.usexAODJets
            export.m_maxevents=m.maxevents
            if m.test:
                msg.info("################################################# ")
                msg.info("I'm running a test of this sample! ")
                msg.info("\n\n ")
                export.runTest()
                exit(0)
            export.execute()
            del export

if __name__ == "__main__":
    main()    


