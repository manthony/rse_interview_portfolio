import getpass
user=getpass.getuser()

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  help="list of samples in .txt format, FILE", metavar="FILE")
parser.add_option("-n", "--nFilesPerJob", dest="njobs", default=20,
                  help="number of input samples to run per job")
parser.add_option("-o", "--outDir", dest="outDir",default="/atlas/"+user+"/TEMP/",
                  help="output destination DIR",metavar="DIR")
parser.add_option("--submit", "-s", dest="doSubmit", action='store_true',
                  help="submit the jobs to the cluster? Default is false.")

(options, args) = parser.parse_args()

import sys,os

njobs=int(options.njobs)
f = open(options.filename)

#os.system("rm ExportTrees/jobs/*")


num_lines = sum(1 for line in open(options.filename))
done = False

i=0

here=os.getcwd()
print here
samples=[]
for line in f:
    line=line.rstrip()
    samples.append([os.path.getsize(line),line])




samples=sorted(samples)
re_samples=reversed(samples)

total_size=0
for line in samples:
    total_size=total_size+int(line[0])



    
cumulative_size=0
processed_size=0

GBperJob= total_size/njobs

temp_njobs=0

all_samples={}

for item in re_samples:
    this_size=int(item[0])
    cumulative_size+=this_size
    processed_size+=this_size
        
    
    if cumulative_size > GBperJob:
        temp_njobs+=1
        new_GBperJob=(total_size-processed_size)/(njobs-temp_njobs)
        GBperJob=new_GBperJob
        
        cumulative_size=0


    if temp_njobs not in all_samples:
        all_samples[temp_njobs]=[item[1]]
    else:
        all_samples[temp_njobs].append(item[1])





m_outDir=options.outDir
m_outDirScratch=options.outDir.replace("/atlas/","/scratch/")


j=0
i=0

for i in all_samples:
    
    

  
    outDirScratch=m_outDirScratch+"job"+str(i)+"/"

 
    fname="ExportTrees/jobs/job_"+str(i)+".sh"
    a=open(fname,"w")
    a.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n")
    a.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n ")
    a.write("cd "+here+" \n")
    a.write("mkdir -p "+outDirScratch+" \n")
    a.write("rm -rf "+outDirScratch+"* \n")
    a.write("cp -r . "+outDirScratch+" \n")
    a.write("cd "+outDirScratch+" \n")
    a.write("ls \n")
    
    a.write("lsetup rcsetup \n")
    a.write("rc find_packages\n")
    a.write("rc compile\n")
  
    count = 0
    
    b=open("ExportTrees/jobs/tmp_"+str(i)+".txt","w")

    for sample in all_samples[i]:
        j+=1
        b.write(sample+"\n")
    
    #while (count<=count_max):
    #    if j==len(all_samples):
    #        done=True
    #        break
    #    line1=all_samples[j]
    #    j+=1
    #    b.write(line1+"\n")
    #    
    #    count += 1
    
    a.write("python ExportTrees/scripts/runExport.py  --input ExportTrees/jobs/tmp_"+str(i)+".txt --outDir=Out"+str(i)+"/ --MVA=1 \n")

    a.write("mkdir -p "+m_outDir+" \n")
    a.write("mv "+outDirScratch+"Out"+str(i)+"/* "+m_outDir+" \n")

    a.write("rm -rf "+outDirScratch+" \n")
    i+=1
    print "finished making job...",fname
    os.system("chmod u+x "+fname)
    if options.doSubmit:
        os.system("condor_qsub `pwd`/"+fname)

