
import argparse


parser  = argparse.ArgumentParser(description='runExport: export trees from a list of files/directory')
parser.add_argument('--input', action='store', dest='inputlist',
               default="",
               help='list of input files ',type=str)
parser.add_argument('--inDir', action='store', dest='inDir',
               default="",
               help='location of input dir ',type=str)
parser.add_argument('--inFile', action='store', dest='inFile',
               default="",
               help='single input file',type=str)
parser.add_argument('--filter', action='store', dest='filter',
               default="",
               help='name of the filter to use..',type=str)
parser.add_argument('--outDir', action='store', dest='outDir',
               default="./default_out/",
               help='location of output dir ',type=str)
parser.add_argument('--smr', action='store', dest='smr',
               default=False,
               help='do jetsmearing ',type=bool)
parser.add_argument('--nosys', action='store', dest='nosys',
               default=True,
               help='turn off systematics ',type=bool)
parser.add_argument('--isPythia8', action='store', dest='isPythia8',
               default=False,
               help='is Pythia8 --> use nevents ',type=bool)
parser.add_argument('--usexAODJets', action='store', dest='usexAODJets',
               default=False,
               help='use xAODJets from collection tree instead for more substrucutre studies ',type=bool)
parser.add_argument('--maxevents', action='store', dest='maxevents',
               default=-1,
               help='set the maximum number of events to run on ',type=float)
parser.add_argument('--test', action='store_true', dest='test',
               default=False,
               help='run a quick test ')

parser.add_argument('--MVA', action='store', dest='MVAFlag',
               default=False,
               help='Apply TMVA (Only use after training has been done) ',type=bool)

m=parser.parse_args()

import sys,os,glob,logging
import gc
import ROOT
from ROOT import gROOT,gSystem,PyConfig
ROOT.gROOT.Macro(os.environ['ROOTCOREDIR']+"/scripts/load_packages.C")

PyConfig.IgnoreCommandLineOptions = True
from ROOT import ExportTrees

logging.basicConfig(level=logging.DEBUG,format='%(name)-12s :: %(levelname)-8s ::       %(message)s')
msg=logging.getLogger('runExport.py ')
msg.info('Starting  '+str(__file__))




msg.debug(m.inDir)

in_samples=[]
if m.inDir!="":
    for sample in glob.glob(m.inDir+"*.root"):
        in_samples.append(sample)
elif m.inputlist!="":
    for sample in open(m.inputlist):
        in_samples.append(sample.rstrip())
elif m.inFile!="":
    in_samples.append(m.inFile)
else:
    msg.error("No in samples found! you need to use --input <filename>.txt or --input <dir> ")
    sys.exit(1)

if len(in_samples)==0:
    msg.error("No samples found!! exiting...")
    sys.exit(1)

if not os.path.exists(m.outDir):
    os.system("mkdir -p "+m.outDir)

import subprocess


for sample in in_samples:
    if "#" in sample: continue
    temp=sample.split("/")
    temp=temp[len(temp)-1]
    out_sample=m.outDir+"/"+temp
    if "jetjet" in sample:
        isPythia8=True
    else:
        isPythia8=False
<<<<<<< HEAD
    #ExportTrees(string in_file_name="", string out_file_name="", bool useProcessedNev = false, bool filter=NULL, bool nosys=true,bool MVAFlag=false, bool smr=false):
    export = ROOT.ExportTrees(sample,out_sample,m.isPythia8,m.filter,m.nosys,m.MVAFlag,m.smr)
    print " initialised export"
=======
    export = ROOT.ExportTrees(sample,out_sample,isPythia8,m.filter,m.nosys,m.smr)
    export.m_usexAODJets=m.usexAODJets
    export.m_maxevents=m.maxevents
    if m.test:
        msg.info("################################################# ")
        msg.info("I'm running a test of this sample! ")
        msg.info("\n\n ")
        export.runTest()
        exit(0)
>>>>>>> af647713f79d4141e0e9852214b29b7e538fa698
    export.execute()
    del export

    


