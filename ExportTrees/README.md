# ExportTrees Code 


## Setup Git/Code


Link your hep account with your cern account if you haven't set up a .ssh key
```
kinit <username>@CERN.CH
```


Git global setup
```
git config --global user.name <user name>
git config --global user.email <email>
```

Retrieve the code from gitlab.cern

```
git clone https://:@gitlab.cern.ch:8443/Shef-Analysis/ExportTrees.git
cd ExportTrees
ls
```
You also need to checkout the RestFrames package:
```
git clone https://github.com/lawrenceleejr/Ext_RestFrames.git
git clone https://:@gitlab.cern.ch:8443/Shef-Analysis/TopnessTool.git
git clone https://:@gitlab.cern.ch:8443/Shef-Analysis/NewMT2.git
```

Now we need to setup the Analysis base, this contains all tools needed in the reconstruction of events.

These first two lines can be added to your .bashrc file, you can find this here /home/<username>/.bashrc.
setupATLAS sets up lot of tools that are needed to submit to the grid, setup the base release, track datasets etc. etc.
```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
#run setupATLAS
setupATLAS
```
We now run rcSetup to get the base release, the first time you should do this, run:
```
rcSetup Base,2.4.20
```
If you have already set up this base, all you need to do next time is
```
rcSetup
```
To change the analysis base to a new release you should do

```
rcSetup -u
rcSetup Base,X.Y.Z
```

Find packages and compile
```
rc find_packages
rc compile
```

**IF you make changes to any of the .h or .cxx files then recompile the pacakges.....**

```
rc compile_pkg AnalysisUtils
```


## Starting your own development branch

To see all branches in your local repository you can do:
```
[macdonald@hep28 ExportTrees]$ git branch
* master
```
which displays the master branch.

You can make your own personal development branch. For example to make a new branch called "test"..
```
git checkout -b test
```
Running again "git branch" you will now see the following:
```
[macdonald@hep28 ExportTrees]$ git branch
  master
* test
```
Now you can start playing with this. 

To switch back to the master you can do:
```
git checkout master
#and then switch back to test
git checkout test
```

If you have made a new file you can check on the status of your local repository by doing:
```
git status
```

To add new files to your local repository do:
```
git add <file>
```
To commit these files to your local repository do:
``` 
git commit -a <file> -m "adding a new file"
```
*or* if you didnt run git add <file> you can do:
```
git commit -m "adding a new file" <file>
```

To then push the branch onto git ( if you really want - master should be flawless ;) )
```
git push origin test
```

** DON'T DO MERGING YET... ASK ME ABOUT THIS LATER **


### MAKING CLUSTER JOBS FOR EXPORT 

As exporting is rather slow and easily separated into batches, we can run the jobs separately:

First, a textfile is needed listing all of the merged pre-export files, which can be made by: 
```
find /PATH/TO/EXPORTED/DIRECTORY/ -name *.root -print &> run.txt
```
Then, make a /jobs/ directory in Export Trees and execute:
```
python ExportTrees/scripts/make_cluster_jobs.py -f run.txt -o <Final output directory>
```
then run one of the jobs on a terminal first (doesnt need to be completed), to check for compilation errors/other weird effects.

Once satisfied, run the command:
```
<<<<<<< HEAD
python ExportTrees/scripts/make_cluster_jobs -f run.txt -o <Final output directory> -s
=======
python ExportTrees/scripts/make_cluster_jobs.py -f run.txt -o <Final output directory> -s
>>>>>>> af647713f79d4141e0e9852214b29b7e538fa698
```
to submit the jobs or use `condor_qsub job_X.sh` on each to submit the jobs to the HEP cluster using condor (and I think LXPLUS imminently).

# Monitoring the jobs

Job progress can be monitored using `condor_q` command and noting the job ID. However, be careful to make sure that the job runs locally first, as faulty jobs may disappear with no prompt from condor (and no stdout is dumped unless the option.
