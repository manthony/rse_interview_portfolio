// Class: ReadRuleFit
// Automatically generated by MethodBase::MakeClass
//

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : RuleFit::RuleFit
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.04/14       [394254]
Creator        : anthony
Date           : Tue Jun 20 12:27:19 2017
Host           : Linux lcgapp-slc6-physical1.cern.ch 2.6.32-573.8.1.el6.x86_64 #1 SMP Wed Nov 11 15:27:45 CET 2015 x86_64 x86_64 x86_64 GNU/Linux
Dir            : /home/anthony/MVA_V2/TMVA
Training events: 2000
Analysis type  : [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
H: "True" [Print method-specific help message]
GDTau: "-1.000000e+00" [Gradient-directed (GD) path: default fit cut-off]
GDTauPrec: "1.000000e-02" [GD path: precision of tau]
GDStep: "1.000000e-02" [GD path: step size]
GDNSteps: "10000" [GD path: number of steps]
GDErrScale: "1.020000e+00" [Stop scan when error > scale*errmin]
fEventsMin: "1.000000e-02" [Minimum fraction of events in a splittable node]
fEventsMax: "5.000000e-01" [Maximum fraction of events in a splittable node]
nTrees: "20" [Number of trees in forest.]
RuleMinDist: "1.000000e-03" [Minimum distance between rules]
MinImp: "1.000000e-03" [Minimum rule importance accepted]
Model: "modrulelinear" [Model to be used]
RuleFitModule: "rftmva" [Which RuleFit module to use]
# Default:
VerbosityLevel: "Default" [Verbosity level]
VarTransform: "None" [List of variable transformations performed before training, e.g., "D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for the given class of events ('AllClasses' denotes all events of all classes, if no class indication is given, 'All' is assumed)"]
CreateMVAPdfs: "False" [Create PDFs for classifier outputs (signal and background)]
IgnoreNegWeightsInTraining: "False" [Events with negative weights are ignored in the training (but are included for testing and performance evaluation)]
LinQuantile: "2.500000e-02" [Quantile of linear terms (removes outliers)]
GDPathEveFrac: "5.000000e-01" [Fraction of events used for the path search]
GDValidEveFrac: "5.000000e-01" [Fraction of events used for the validation]
ForestType: "adaboost" [Method to use for forest generation (AdaBoost or RandomForest)]
RFWorkDir: "./rulefit" [Friedman's RuleFit module (RFF): working dir]
RFNrules: "2000" [RFF: Mximum number of rules]
RFNendnodes: "4" [RFF: Average number of end nodes]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 8
Tau2_1fatjet                  Tau2_1fatjet                  Tau2_1fatjet                  Tau2_1fatjet                                                    'D'    [0,0.397072643042]
Tau2_2fatjet                  Tau2_2fatjet                  Tau2_2fatjet                  Tau2_2fatjet                                                    'D'    [0,0.441416352987]
m_1fatjet_st                  m_1fatjet_st                  m_1fatjet_st                  m_1fatjet_st                                                    'D'    [-0.392838448286,910.695495605]
m_2fatjet_st                  m_2fatjet_st                  m_2fatjet_st                  m_2fatjet_st                                                    'D'    [-0.184998035431,368.340179443]
Qw_1fatjet                    Qw_1fatjet                    Qw_1fatjet                    Qw_1fatjet                                                      'D'    [0,208386.46875]
Qw_2fatjet                    Qw_2fatjet                    Qw_2fatjet                    Qw_2fatjet                                                      'D'    [0,127581.210938]
Split12_1fatjet               Split12_1fatjet               Split12_1fatjet               Split12_1fatjet                                                 'D'    [0,635.636474609]
Split12_2fatjet               Split12_2fatjet               Split12_2fatjet               Split12_2fatjet                                                 'D'    [0,257.611022949]
NSpec 0


============================================================================ */

#include <vector>
#include <cmath>
#include <string>
#include <iostream>

#ifndef IClassifierReader__def
#define IClassifierReader__def

class IClassifierReader {

 public:

   // constructor
   IClassifierReader() : fStatusIsClean( true ) {}
   virtual ~IClassifierReader() {}

   // return classifier response
   virtual double GetMvaValue( const std::vector<double>& inputValues ) const = 0;

   // returns classifier status
   bool IsStatusClean() const { return fStatusIsClean; }

 protected:

   bool fStatusIsClean;
};

#endif

class ReadRuleFit : public IClassifierReader {

 public:

   // constructor
   ReadRuleFit( std::vector<std::string>& theInputVars ) 
      : IClassifierReader(),
        fClassName( "ReadRuleFit" ),
        fNvars( 8 ),
        fIsNormalised( false )
   {      
      // the training input variables
      const char* inputVars[] = { "Tau2_1fatjet", "Tau2_2fatjet", "m_1fatjet_st", "m_2fatjet_st", "Qw_1fatjet", "Qw_2fatjet", "Split12_1fatjet", "Split12_2fatjet" };

      // sanity checks
      if (theInputVars.size() <= 0) {
         std::cout << "Problem in class \"" << fClassName << "\": empty input vector" << std::endl;
         fStatusIsClean = false;
      }

      if (theInputVars.size() != fNvars) {
         std::cout << "Problem in class \"" << fClassName << "\": mismatch in number of input values: "
                   << theInputVars.size() << " != " << fNvars << std::endl;
         fStatusIsClean = false;
      }

      // validate input variables
      for (size_t ivar = 0; ivar < theInputVars.size(); ivar++) {
         if (theInputVars[ivar] != inputVars[ivar]) {
            std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names" << std::endl
                      << " for variable [" << ivar << "]: " << theInputVars[ivar].c_str() << " != " << inputVars[ivar] << std::endl;
            fStatusIsClean = false;
         }
      }

      // initialize min and max vectors (for normalisation)
      fVmin[0] = 0;
      fVmax[0] = 0;
      fVmin[1] = 0;
      fVmax[1] = 0;
      fVmin[2] = 0;
      fVmax[2] = 0;
      fVmin[3] = 0;
      fVmax[3] = 0;
      fVmin[4] = 0;
      fVmax[4] = 0;
      fVmin[5] = 0;
      fVmax[5] = 0;
      fVmin[6] = 0;
      fVmax[6] = 0;
      fVmin[7] = 0;
      fVmax[7] = 0;

      // initialize input variable types
      fType[0] = 'D';
      fType[1] = 'D';
      fType[2] = 'D';
      fType[3] = 'D';
      fType[4] = 'D';
      fType[5] = 'D';
      fType[6] = 'D';
      fType[7] = 'D';

      // initialize constants
      Initialize();

   }

   // destructor
   virtual ~ReadRuleFit() {
      Clear(); // method-specific
   }

   // the classifier response
   // "inputValues" is a vector of input values in the same order as the 
   // variables given to the constructor
   double GetMvaValue( const std::vector<double>& inputValues ) const;

 private:

   // method-specific destructor
   void Clear();

   // common member variables
   const char* fClassName;

   const size_t fNvars;
   size_t GetNvar()           const { return fNvars; }
   char   GetType( int ivar ) const { return fType[ivar]; }

   // normalisation of input variables
   const bool fIsNormalised;
   bool IsNormalised() const { return fIsNormalised; }
   double fVmin[8];
   double fVmax[8];
   double NormVariable( double x, double xmin, double xmax ) const {
      // normalise to output range: [-1, 1]
      return 2*(x - xmin)/(xmax - xmin) - 1.0;
   }

   // type of input variable: 'F' or 'I'
   char   fType[8];

   // initialize internal variables
   void Initialize();
   double GetMvaValue__( const std::vector<double>& inputValues ) const;

   // private members (method specific)
   // not implemented for class: "ReadRuleFit"
};
void   ReadRuleFit::Initialize(){}
void   ReadRuleFit::Clear(){}
double ReadRuleFit::GetMvaValue__( const std::vector<double>& inputValues ) const {
   double rval=-0.2861554353;
   //
   // here follows all rules ordered in importance (most important first)
   // at the end of each line, the relative importance of the rule is given
   //
   if ((inputValues[1]<0.189178437)&&(inputValues[3]<52.4614563)&&(9756.248047<inputValues[4])&&(inputValues[6]<65.14905548)) rval+=-0.3554517083;   // importance = 1.000
   if ((39692.66016<inputValues[4])&&(44.96543503<inputValues[6])) rval+=0.1775235858;   // importance = 0.519
   if ((inputValues[0]<0.06870909035)&&(inputValues[4]<12548.68555)&&(inputValues[5]<20111.40234)&&(inputValues[7]<12.26719189)) rval+=-0.2098119529;   // importance = 0.458
   if ((0.01890822127<inputValues[0])&&(inputValues[1]<0.0487370044)) rval+=0.1403617587;   // importance = 0.423
   if ((12.26719189<inputValues[7])) rval+=0.1278359418;   // importance = 0.415
   if ((6900.106445<inputValues[5])&&(30.26840401<inputValues[6])) rval+=-0.1218772227;   // importance = 0.392
   if ((inputValues[0]<0.06870909035)&&(inputValues[7]<12.26719189)) rval+=-0.1409990499;   // importance = 0.354
   if ((inputValues[1]<0.189178437)&&(52.4614563<inputValues[3])&&(9756.248047<inputValues[4])) rval+=0.1197501804;   // importance = 0.341
   if ((0.1050991341<inputValues[1])&&(9923.165039<inputValues[4])&&(inputValues[4]<38285.42188)) rval+=-0.1478962278;   // importance = 0.314
   if ((18.30791283<inputValues[3])&&(5708.654785<inputValues[5])&&(12.26719189<inputValues[7])) rval+=0.09598583619;   // importance = 0.313
   if ((5708.654785<inputValues[5])&&(12.26719189<inputValues[7])) rval+=0.09386065564;   // importance = 0.306
   if ((inputValues[5]<18225.88672)) rval+=0.1096342396;   // importance = 0.295
   if ((inputValues[5]<18225.88672)&&(8.454108238<inputValues[7])) rval+=0.08190547863;   // importance = 0.266
   if ((inputValues[1]<0.1050991341)) rval+=0.07057381751;   // importance = 0.207
   if ((0.1050991341<inputValues[1])&&(86.37747955<inputValues[2])&&(10696.81543<inputValues[5])) rval+=-0.0730575273;   // importance = 0.164
   if ((inputValues[1]<0.1050991341)&&(9923.165039<inputValues[4])) rval+=0.02716853944;   // importance = 0.089
   if ((inputValues[1]<0.189178437)&&(52.4614563<inputValues[3])&&(9756.248047<inputValues[4])&&(4201.390137<inputValues[5])) rval+=0.01577877568;   // importance = 0.045
   if ((0.05993884802<inputValues[1])&&(inputValues[2]<86.37747955)&&(inputValues[3]<24.93240547)) rval+=-0.02642744362;   // importance = 0.041
   if ((0.01890822127<inputValues[0])) rval+=0.01222137692;   // importance = 0.025
   if ((inputValues[0]<0.06870909035)&&(inputValues[5]<20111.40234)&&(inputValues[7]<12.26719189)) rval+=-0.00791453133;   // importance = 0.020
   if ((inputValues[1]<0.189178437)&&(inputValues[3]<49.89199066)&&(inputValues[4]<9756.248047)) rval+=0.0005379154889;   // importance = 0.001
   //
   // here follows all linear terms
   // at the end of each line, the relative importance of the term is given
   //
   rval+=-0.1342108088*std::min( double(0.2326926887), std::max( double(inputValues[1]), double(0.002998857526)));   // importance = 0.049
   rval+=0.002863766146*std::min( double(110.0824356), std::max( double(inputValues[7]), double(1.462514758)));   // importance = 0.474
   return rval;
}
   inline double ReadRuleFit::GetMvaValue( const std::vector<double>& inputValues ) const
   {
      // classifier response value
      double retval = 0;

      // classifier response, sanity check first
      if (!IsStatusClean()) {
         std::cout << "Problem in class \"" << fClassName << "\": cannot return classifier response"
                   << " because status is dirty" << std::endl;
         retval = 0;
      }
      else {
         if (IsNormalised()) {
            // normalise variables
            std::vector<double> iV;
            iV.reserve(inputValues.size());
            int ivar = 0;
            for (std::vector<double>::const_iterator varIt = inputValues.begin();
                 varIt != inputValues.end(); varIt++, ivar++) {
               iV.push_back(NormVariable( *varIt, fVmin[ivar], fVmax[ivar] ));
            }
            retval = GetMvaValue__( iV );
         }
         else {
            retval = GetMvaValue__( inputValues );
         }
      }

      return retval;
   }
