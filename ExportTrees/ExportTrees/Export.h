#ifndef EXPORT_H
#define EXPORT_H


#include "TF1.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include <TROOT.h>
#include "TLorentzVector.h"
#include "TVector2.h"



#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <iomanip>
#include <regex>
#include <iterator>
#include <algorithm>
#include <map>


#include "TObject.h"
#include "TKey.h"
#include "TIterator.h"
#include "TClass.h"
#include "TList.h"
#include "TEntryList.h"
#include "TEventList.h"
#include "TH2D.h"

#include "ExportTrees/TMctLib.h"
#include "ExportTrees/MiniNtuple.h"
#include "ExportTrees/Sphericity.h" 
#include "ExportTrees/myNtupleDumper.h"
//#include "TopnessTool/TopnessTool.h"

#include <ExportTrees/Objects.h>
#include <ExportTrees/JetSubStructure.h>
#include <CalibrationDataInterface/CalibrationDataContainer.h>


#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"


// ASG includes: 
#include "AsgTools/AsgTool.h" 
#include "AsgTools/IAsgTool.h" 
#include "AsgTools/AsgMessaging.h"
#include "AsgTools/ToolHandle.h" 

#include "AsgTools/SgTEvent.h"

#include "AthContainers/AuxVectorBase.h"

#include "ExportTrees/IExport.h"


#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODRootAccess/Init.h" 
#include "xAODRootAccess/TEvent.h" 


#include <JetSubStructureMomentTools/SubjetFinderTool.h>
#include <JetSubStructureUtils/SubjetFinder.h>

#include "JetRec/JetToolRunner.h"
#include "JetRec/PseudoJetGetter.h"




using namespace std;


class dRsorter {
public:
  dRsorter(const Jet* obj) : m_obj(obj){} ;
  
  bool operator() (const Jet* o1, const Jet* o2) const  {
    TLorentzVector p1(o1->Px(),o1->Py(),o1->Pz(),o1->E());
    TLorentzVector p2(o2->Px(),o2->Py(),o2->Pz(),o2->E());
    TLorentzVector p3(m_obj->Px(),m_obj->Py(),m_obj->Pz(),m_obj->E());

      return  p1.DeltaR(p3) < p2.DeltaR(p3) ;
  }

private:
  const Jet* m_obj;
};


 

class ExportTrees : public virtual IExportTrees, public TObject, public asg::AsgTool {

  ASG_TOOL_CLASS(ExportTrees, IExportTrees)

 public:

 ExportTrees(string in_file_name="", string out_file_name="", bool useProcessedNev = false, string filter="", bool nosys=true, bool MVAFlag=false , bool smr=false):

  in_file_name(in_file_name),
    out_file_name(out_file_name),
    isPythia(useProcessedNev),
    filter(filter),
    nosys(nosys),
    MVAFlag(MVAFlag),
    smr(smr){
    };*/
  
    // ExportTrees(string in_file_name="", string out_file_name="", bool useProcessedNev = false, string filter="", bool nosys=true, bool smr=false);
    

  bool m_usexAODJets = false;
  int m_maxevents=-1;
 

  ~ExportTrees();
  void reset();
  void resetJets();
  void resetFatJets();
  void resetKt8Jets();
  void resetKt12Jets();
  void execute();
  void runTest();
  

 private:

  JetSubStructureUtils::SubjetFinder* m_subjetTool; //!
  JetToolRunner* m_jetRecTool_kt2; //!

  string in_file_name;
  string out_file_name;
  bool isPythia;
  string filter;
  bool nosys;
  bool MVAFlag;
  bool smr;

  TH2D* hJet;

  TH2D* hcalib;
  TH2D* hcalib2;
  TH2D* hcalib3;

  TH2D* hphoton;
  TH2D* hphotonAcpt;

  TH2D* htracks;
  TH2D* hjets;
  TH2D* hbjets;


  //input/output ntuples
  MiniNtuple* inputntuple = 0;
  myNtupleDumper *outputntuple = 0;
  
  myNtupleDumper *outputntuple0L = 0;
  myNtupleDumper *outputntuple1L = 0;
  myNtupleDumper *outputntuple2L = 0;
  myNtupleDumper *outputntuple1Ph = 0;
  
  TMVA::Reader *m_reader_odd = 0;
  TMVA::Reader *m_reader_even=0;
  TMVA::Reader *m_reader_BDTG_odd = 0;
  TMVA::Reader *m_reader_BDTG_even=0;

  Float_t m_BDTvar1;
  Float_t m_BDTvar2;
  Float_t m_BDTvar3;
  Float_t m_BDTvar4;
  Float_t m_BDTvar5;
  Float_t m_BDTvar6;
  Float_t m_BDTvar7;
  Float_t m_BDTvar8;
  Float_t m_BDTvar9;
  Float_t m_BDTvar10;
  Float_t m_BDTvar11;
  Float_t m_BDTvar12;
  Float_t m_BDTvar13;
  Float_t m_BDTvar14;
  Float_t m_BDTvar15;
  Float_t m_BDTvar16;
  Float_t m_BDTvar17;
  Float_t m_BDTvar18;
  Float_t m_BDTvar19;
  Float_t m_BDTvar20;
  Float_t m_BDTvar21;



  //photon, Z for fake met 
  Particle* m_photon = 0;
  Particle* m_Z = 0;
 
  
  
  //met
  TVector2* m_met = 0;
  TVector2* m_metinv = 0;
  TVector2* m_metlep = 0;
  TVector2* m_metph = 0;
  TVector2* m_mettau=0;
  TVector2* m_met_fake = 0;
  TVector2* m_met_track = 0;
  TVector2* m_met_NonInt = 0;
  TVector2* m_met_simple = 0;
  
  //jets
  Jets *m_jets = 0;
  Jets *m_bjets = 0;
  Jets *m_ljets = 0;
  Jets *m_bjets60 = 0;
  Jets *m_bjets85 = 0;
  

  //fat jets
  Jets*  m_fatjets_kt8 = 0;
  Jets*  m_fatjets_kt12 = 0;
  Jets*  m_fatjets_st = 0;

  //trackJets
  Jets *m_trackjets=0;
  Jets *m_trackbjets60=0;
  Jets *m_trackbjets70=0;
  Jets *m_trackbjets77=0;
  Jets *m_trackbjets85=0;
  
  Jets *m_trackljets=0;



  //photons
  Particles *m_photons = 0;
  Particles *m_truthphotons = 0;
  
  //leptons
  Particles *m_leptons = 0;
  Particles *m_muons = 0;
  Particles *m_electrons = 0;
  Particles *m_taus=0;




  void GetMet(TVector2*& met);
  void GetMetEl(TVector2*& met);
  void GetMetMu(TVector2*& met);
  void GetMetY(TVector2*& met);
  void GetMetSoft(TVector2*& met);
  void GetMetJet(TVector2*& met);
  void GetSimpleMet(TVector2*& met);
  void GetSimpleMetMu(TVector2*& met);
  
  void GetNonIntMet(TVector2*& met);
  void GetFakeMet(TVector2*& met);
  void GetTrackMet(TVector2*& met);
  void GetJets(Jets*& jets);
  void GetTrackJets(Jets*& jets);

  void GetFatJets(Jets*& jets,std::string name="kt12");
  void GetElectrons(Particles*& particles);
  void GetMuons(Particles*& particles);
  void GetPhotons(Particles*& particles);
  void GetTruthPhotons(Particles*& particles);
  void GetTaus(Particles*& particles);

  //functions for handling xAOD objects
  void GetxAODJets(Jets*& jets, std::string name="STCalibAntiKt4EMTopoJets");
  
  //function to get JetRec tool
  bool GetJetToolRunner(JetToolRunner *& tool, double jetradius, std::string inputcontainer, std::string outputcontainer);
  

  float dPhiBadTile(std::vector<TLorentzVector> jets, float MET_phi);
  bool badTileVeto(std::vector<TLorentzVector> jets, std::vector<float> jets_BCH_CORR_CELL, float MET_phi);

  float dPhiBadTile(Jets jets, float MET_phi);
  bool badTileVeto(Jets jets, float MET_phi);

  double getXsec(int runnumber,string xsecfile="/home/macdonald/SUSY_July_2015/SUSY.2.3.15/RootCoreBin/data/SUSYTools/susy_crosssections_13TeV.txt"); 
  void Tokenize(const string& str, vector<string>& tokens,const string& delimiters = " \t"); 
  double amt2_calc(TLorentzVector b1v, TLorentzVector b2v, TLorentzVector lepton,TLorentzVector EtMissVec,double cut=170);
  std::pair <double,double> RecoHadTops(int ibtop1, int ibtop2, int nbjets, Jets jets);
  bool chi2Top(TLorentzVector &topCand0, TLorentzVector &topCand1, TLorentzVector &WCand0, TLorentzVector &WCand1, double &Chi2, int b1Index, int b2Index, Jets m_jets);

   
  void FillPhotonTriggerWeights(int runnumber);
  void FillEventWeights(double LumiWeight, double TriggerWeight);
  void FillTriggers(bool isData);
  void FillPhotonVariables();
  void FillLeptonVariables(); 
  void FillMETVariables();
  void FillComplexVariables();
  void FillCompressedAnalysis();
  void FillLeptonMETVariables();
  void FillZVariables();
  void FillJetMETVariables();
  void FillJetVariables();
  void FillFatJetVariables();
  void FillMVAVariables();
  void FillChannels();
  void FillTauVariables();
  void FillTrackJetVariables();
 

  bool passttZFilter();
  bool passSbottomFilter();
  bool pass0LFilter();
  bool pass0baselineLFilter();
  bool pass1LFilter(); 
  bool passemuFilter();
  bool pass2LFilter();
  bool pass2BFilter();
  bool pass1PhFilter();
  bool passStop0LFilter();
  bool passTightStop0LFilter();
  bool pass0LMETFilter();
  bool passFilter();
  bool passComplexFilter();
  double GetTriggerWeight(std::string name, int num);

  void FillVariables();
  void FillSmrVariables();


 public:
  ClassDef(ExportTrees,1);
};

#endif
