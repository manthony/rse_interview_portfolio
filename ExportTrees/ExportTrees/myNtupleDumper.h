
#ifndef MYNTUPLEDUMPER_H
#define MYNTUPLEDUMPER_H

#include <iostream>
#include <string>
#include <vector>
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
using namespace std;

using namespace std;

class myNtupleDumper {

 public:

  myNtupleDumper(TFile*, std::string, int);  
  myNtupleDumper(std::string, std::string, int);
  ~myNtupleDumper();
  void init();
  void dump();
  void write();
  void clean();
  double GetEntries(){return m_tree->GetEntries();};
  int GetNFilled(){return m_nfilled;};
  
  void getName(){std::cout <<  m_tree->GetName() << " " << m_file->GetName() << std::endl; };
  void printFile(){std::cout <<  m_file << std::endl; };

  bool m_ExtraTruth=false;


  Int_t           SCTerror;
  Int_t           CoreFlag;
  Int_t           treatAsYear;
  std::string     *analysisflag;
  Int_t           isTruthAnalysis;

  bool           isMEPhOverlapOK;  
  
  //met or HT filter flags for OR removal
  bool           isttbarMET200_OK;
  bool           isttbarMET300_OK;
  bool           isWtHT500_OK;
  bool           isttbarHT600_OK;



  vector<int>     bindex;

  Int_t TrigAtPlat;


  bool allGhostGood;
  bool allTruthGood;

  Int_t passSeedSelection;
  Int_t passHTSeedSelection;
  Int_t passETSeedSelection;
  Int_t passETSeedSelectionv2;

  double pT_1mu_truth ;
  double eta_1mu_truth ;
  double phi_1mu_truth ;
  bool truthMu_signal_matched ;
  bool truthMu_baseline_matched ;
  bool truthMu_preOR_matched ;
  bool truthMu_inA ;
  int ntruthMu ;
  
  int nTruthTaus;
  int n_Taus;
  int nTruthTaus_inA;
  int nTruthTaus_outA;
  int nFakeTaus;
  int nRecoTaus_matched;
  int nRecoTaus_matched_TruthTaus;


  int hastau;
  int hasb;
  int hasc;

  double m_1fatjet_kt12 ;
  double m_2fatjet_kt12 ;
  double m_3fatjet_kt12 ;
  double m_1fatjet_kt8 ;
  double m_2fatjet_kt8 ;
  double m_3fatjet_kt8 ;

  double m_1fatjet_st ;
  double m_2fatjet_st ;
  double m_3fatjet_st ;


  double pT_1fatjet_st;
  double phi_1fatjet_st;
  double y_1fatjet_st;
  
  std::vector<double> con_pT_1fatjet_st;
  std::vector<double> con_phi_1fatjet_st;
  std::vector<double> con_y_1fatjet_st;
  
  std::vector<double> sub_pT_1fatjet_st;
  std::vector<double> sub_phi_1fatjet_st;
  std::vector<double> sub_y_1fatjet_st;
  

  double pt_1fatjet_st ;
  double pt_2fatjet_st ;
  double pt_3fatjet_st ;
  double eta_1fatjet_st ;
  double eta_2fatjet_st ;
  double eta_3fatjet_st ;

  int ntrkjets_1fatjet_st ;
  int ntrkjets_2fatjet_st ;
  int ntrkjets_3fatjet_st ;
  int nghostbhad_1fatjet_st ;
  int nghostbhad_2fatjet_st ;
  int nghostbhad_3fatjet_st ;
  double Split12_1fatjet_st ;
  double Split12_2fatjet_st ;
  double Split12_3fatjet_st ;
  double Split23_1fatjet_st ;
  double Split23_2fatjet_st ;
  double Split23_3fatjet_st ;
  double Split34_1fatjet_st ;
  double Split34_2fatjet_st ;
  double Split34_3fatjet_st ;

  double Tau1_1fatjet_st ;
  double Tau1_2fatjet_st ;
  double Tau1_3fatjet_st ;
  double Tau2_1fatjet_st ;
  double Tau2_2fatjet_st ;
  double Tau2_3fatjet_st ;
  double Tau3_1fatjet_st ;
  double Tau3_2fatjet_st ;
  double Tau3_3fatjet_st ;
  double Tau32_1fatjet_st ;
  double Tau32_2fatjet_st ;
  double Tau32_3fatjet_st ;

  double Qw_1fatjet_st ;
  double Qw_2fatjet_st ;
  double Qw_3fatjet_st ;

  bool w50_1fatjet_st ;
  bool w50_2fatjet_st ;
  bool w50_3fatjet_st ;
  bool w80_1fatjet_st ;
  bool w80_2fatjet_st ;
  bool w80_3fatjet_st ;
  bool z50_1fatjet_st ;
  bool z50_2fatjet_st ;
  bool z50_3fatjet_st ;
  bool z80_1fatjet_st ;
  bool z80_2fatjet_st ;
  bool z80_3fatjet_st ;
  int W50res_1fatjet_st ;
  int W50res_2fatjet_st ;
  int W50res_3fatjet_st ;
  int W80res_1fatjet_st ;
  int W80res_2fatjet_st ;
  int W80res_3fatjet_st ;
  int Z50res_1fatjet_st ;
  int Z50res_2fatjet_st ;
  int Z50res_3fatjet_st ;
  int Z80res_1fatjet_st ;
  int Z80res_2fatjet_st ;
  int Z80res_3fatjet_st ;
  double WLowWMassCut50_1fatjet_st ;
  double WLowWMassCut50_2fatjet_st ;
  double WLowWMassCut50_3fatjet_st ;
  double WLowWMassCut80_1fatjet_st ;
  double WLowWMassCut80_2fatjet_st ;
  double WLowWMassCut80_3fatjet_st ;
  double ZLowWMassCut50_1fatjet_st ;
  double ZLowWMassCut50_2fatjet_st ;
  double ZLowWMassCut50_3fatjet_st ;
  double ZLowWMassCut80_1fatjet_st ;
  double ZLowWMassCut80_2fatjet_st ;
  double ZLowWMassCut80_3fatjet_st ;
  double WHighWMassCut50_1fatjet_st ;
  double WHighWMassCut50_2fatjet_st ;
  double WHighWMassCut50_3fatjet_st ;
  double WHighWMassCut80_1fatjet_st ;
  double WHighWMassCut80_2fatjet_st;
  double WHighWMassCut80_3fatjet_st ;
  double ZHighWMassCut50_1fatjet_st ;
  double ZHighWMassCut50_2fatjet_st ;
  double ZHighWMassCut50_3fatjet_st ;
  double ZHighWMassCut80_1fatjet_st ;
  double ZHighWMassCut80_2fatjet_st ;
  double ZHighWMassCut80_3fatjet_st ;
  double WD2Cut50_1fatjet_st ;
  double WD2Cut50_2fatjet_st ;
  double WD2Cut50_3fatjet_st ;
  double WD2Cut80_1fatjet_st ;
  double WD2Cut80_2fatjet_st ;
  double WD2Cut80_3fatjet_st ;
  double ZD2Cut50_1fatjet_st ;
  double ZD2Cut50_2fatjet_st ;
  double ZD2Cut50_3fatjet_st ;
  double ZD2Cut80_1fatjet_st ;
  double ZD2Cut80_2fatjet_st ;
  double ZD2Cut80_3fatjet_st ;
  bool top50_1fatjet_st ;
  bool top50_2fatjet_st ;
  bool top50_3fatjet_st ;
  bool top80_1fatjet_st ;
  bool top80_2fatjet_st ;
  bool top80_3fatjet_st ;
  int top50res_1fatjet_st ;
  int top50res_2fatjet_st ;
  int top50res_3fatjet_st ;
  int top80res_1fatjet_st ;
  int top80res_2fatjet_st ;
  int top80res_3fatjet_st ;
  double TopTagTau32Cut50_1fatjet_st ;
  double TopTagTau32Cut50_2fatjet_st ;
  double TopTagTau32Cut50_3fatjet_st ;
  double TopTagTau32Cut80_1fatjet_st ;
  double TopTagTau32Cut80_2fatjet_st ;
  double TopTagTau32Cut80_3fatjet_st ;
  double TopTagSplit23Cut50_1fatjet_st ;
  double TopTagSplit23Cut50_2fatjet_st ;
  double TopTagSplit23Cut50_3fatjet_st ;
  double TopTagSplit23Cut80_1fatjet_st ;
  double TopTagSplit23Cut80_2fatjet_st ;
  double TopTagSplit23Cut80_3fatjet_st ;



  //double BDTG_1000_1;
  double BDT_highstop;
  double BDTG_highstop;
  //double BDTG_600_300;
  //double BDT_600_300;




  // int NFatJetsSt ;

  int NFatJetsKt10 ;


  double m_top0Chi2M ;
  double m_top1Chi2M ;
  double m_w0Chi2M ;
  double m_w1Chi2M ;
  double m_chi2 ;
  double m_mt2Chi2 ;    

  double RMPF,RG;

  int passfilter,passL1_J12,passL1_J50,passL1_MU6,passL1_2MU6,passL1_EM10,passL1_2EM7,passHLT_xe35,passHLT_xe50,passHLT_xe80;

  int pass1Ltriggers,pass1etriggers,pass1mutriggers,pass1phtriggers,passMETtriggers;
  int passcleaning;

  Int_t           el_TrigMatched,mu_TrigMatched;

   Int_t          HLT_g200_etcut;
   Int_t          HLT_g35_loose_L1EM15;
   Int_t          HLT_g40_loose_L1EM15;
   Int_t          HLT_g45_loose_L1EM15;
   Int_t          HLT_g50_loose_L1EM15;

  int charge_1e,charge_1mu,charge_2e,charge_2mu;
  int charge_1l,charge_2l,charge_3l;

  double sumet;
   double         sumet_jet;
   double         sumet_el;
   double         sumet_y;
   double         sumet_mu;
   double         sumet_tau;
   double         sumet_softTrk;
   double         sumet_softClu;

   int j1_truthFlav, j2_truthFlav, j3_truthFlav, j4_truthFlav;
   int b1_truthFlav, b2_truthFlav;



  int leadb1,leadb2;
  int leadb1_60,leadb2_60;
  int leadb1_85,leadb2_85;
  int nbaselineLep;
  int nbaselinePh;

  int jet1_bad;
  double jet1_fmax,jet1_fch,jet1_fchfmax,jet1_fem,jet1_jvtxf;
  double jet2_fmax,jet2_fch,jet2_fchfmax,jet2_fem,jet2_jvtxf;

  double mrel,mst,msz,ms;
  double truthpt;
  int whichb;
  int whichB;
  double dphimin3,dphimin2, dphimin2_orig;
  double dphimin4,dphimin4_lep,dphimin4_orig;
  double dphimin4_35;
  

  double mdeltaR, costhetaR,costhetaRp1,shatR,Rpt,gaminvRp1;

  double maxDRbb;
  double maxDRassym;
  double maxDRmbb;
  double maxminDRbb;
  double maxminDRassym;
  double maxminDRmbb;
  double minDRbb;
  double minDRassym;
  double minDRmbb;
  
  int minDRntracks;
  int maxminDRntracks;
  int maxDRntracks;

  int nextrajets50;

  double phi_MET;

  int ntruthjets30;

  TH1D *HEntries, *HEntWgt, *HEntWgtPU;

  // added calum
  double test, meff, MET_sig, mjj,mjy;
  float MET_px,MET_py,MET_pt;
  double asym_bjets;

  //double m_1bjj;
  double m_2bjj;

  //int lbn;

  std::vector<double> j1_bweight;
  std::vector<double> j2_bweight;
 
  double weight_new_filt;
  double weight_new;
  double LumiWeight,LumiWeightOld;
  double jvtweight,jvtweightUP,jvtweightDOWN;
  double SherpaWeight;
  double btagweight,btagweightBUP,btagweightBDOWN;
  double btagweightCUP,btagweightCDOWN;
  double btagweightLUP,btagweightLDOWN;
  double btagweightExUP,btagweightExDOWN;
  double btagweightExCUP,btagweightExCDOWN;
  

  int processtype;

  int RunNumber;
  unsigned long long int EventNumber;

  int numVtx, numGoodVtx;

  int passtauveto;


  double eT_miss_jet,eT_miss_el,eT_miss_mu,eT_miss_y,eT_miss_softTrk,eT_miss_tau;

  double eT_miss,eT_miss_lep, eT_miss_orig, etmll,etlep,pT_ll,mvh,eT_miss_NonInt;
  double phi_met_orig;
  double eT_miss_inv;
  double eT_miss_track;
  double dphi_track,dphi_track_orig;

  double eT_miss_truth, eT_miss_jets, eT_miss_ghost,eT_miss_jets_ghost;

  double eT_miss_matched_truth,eT_miss_jets_matched_truth;
  double eT_miss_all_truth,eT_miss_jets_all_truth;


  double eT_miss_seed,sumet_seed,sumet_refjet_seed,HT_seed,sumet_soft_seed;
  double nbjets_seed,njets_seed,met_avPt_seed;
  double pT_1jet_seed,pT_2jet_seed;

  double jpa_1jet_calo,jpa_2jet_calo,jpa_3jet_calo,jpa_4jet_calo;
  double jpa_1jet_tracks,jpa_2jet_tracks,jpa_3jet_tracks,jpa_4jet_tracks;

  double R_1jet,R_2jet,R_3jet,R_4jet,R_av;


  double pT_1jet_truth;
  double mlb1,mlb2,mlbmin;

  double mlj1,mlj2;

  double dRlb1,dRlb2,dRljmin,dRljmax;
  
  double dRl1l2;
  double dRj1j2,dRb1b2,dRlj1,dRlj2;
  double pT_1leadbtag,pT_2leadbtag;
  double dRjmax,dRjmin;
  double dRlbmin;
  double dEtajmin,dEtab1b2;
  double m_top_minDR1;
  double m_top_minDR2;

  double ht2, ht3,HT,HT_35;

  double eT_miss_phi;

  double track_eT_miss, track_eT_miss_phi, track_eT_miss_px, track_eT_miss_py;

  double dphimets;

  double metsig, metsigET,metsigHT;

  double pT_1jet, pT_2jet, pT_3jet, pT_4jet, pT_5jet, pT_6jet, eta_1jet, eta_2jet, eta_3jet, eta_4jet, eta_5jet, eta_6jet, pT_lep, eta_lep; 
  double pT_lastjet;
  

  int ntracks_1jet,ntracks_2jet,ntracks_3jet,ntracks_4jet,ntracks_5jet,ntracks_6jet;

  int flav_1jet, flav_2jet, flav_3jet, flav_4jet;


  std::string origin_1jet, origin_2jet, origin_3jet, origin_4jet, origin_5jet, origin_6jet, origin_7jet;
  double npartons_1jet, npartons_2jet, npartons_3jet,npartons_4jet;
  double wpartons_1jet, wpartons_2jet, wpartons_3jet,wpartons_4jet;
  


  double A,Ap,ST,Sp;
  double y_1jet,y_2jet;


  double eta_1bjet_Z ;
  double eta_2bjet_Z ;
  double dR_1bjet_Z ;
  double dR_2bjet_Z ;
  


  int nPh;

  double topoetcone40_1ph,ptvarcone20_1ph;
  double pT_1ph;
  double eta_1ph;
  double phi_1ph;
  double dPhi_1ph;
  double dPhi_1ph1jet;
  double dPhi_1jet_Z;
  double dPhi_2jet_Z;

  int ntruthPh;
  double pT_1truthph;
  double eta_1truthph;
  double phi_1truthph;
  double dPhi_1truthph;
  


  double pT_V,eta_V,phi_V,E_V;
  double pT_B;
  std::vector<double> TruthNuMET;

  double pT_1lep, pT_2lep, pT_3lep;
    
  double phi_1lep, phi_2lep, phi_3lep;

  double eta_1lep, eta_2lep, eta_3lep;

  int charge_1lep, charge_2lep, charge_3lep;
  int flav_1lep, flav_2lep, flav_3lep;

  double pT_1bjet, pT_2bjet, pT_3bjet;
  double pT_1lightjet, pT_2lightjet;

  double pT_1trackbjet60;
  double pT_1trackbjet70;
  double pT_1trackbjet77;
  double pT_1trackbjet85;

  double pT_1trackbjet60_unmatched;
  double pT_1trackbjet70_unmatched;
  double pT_1trackbjet77_unmatched;
  double pT_1trackbjet85_unmatched;




  double weight;

  double EventWeight;

  double phi_1jet, phi_2jet, phi_3jet, phi_4jet, phi_5jet, phi_lep;
  double dR_1jet, dR_2jet, dR_3jet, dR_4jet;
  double dPhi_1jet, dPhi_2jet, dPhi_3jet, dPhi_4jet;
  double dPhi_1bjet, dPhi_2bjet;
  double dPhi_1jet_orig, dPhi_2jet_orig, dPhi_3jet_orig, dPhi_4jet_orig;
  double dPhi_1bjet_orig, dPhi_2bjet_orig;

  double dPhi_1j2j ;
  double dPhi_2j3j ;
  double dPhi_1j3j ;
  double dPhi_b1b2 ;


  double dPhi_min;

  double w_MV2c20_1, w_MV2c20_2, w_MV2c20_3;

  double W_tr_mass;

  double lep1_pt, lep2_pt, mll;



  int nj_good,nj_good35,nextrajets;

  int nj_good45,nj_good45_0eta240;

  double btagSFCent;
  double bEffUp, bEffDown, cEffUp, cEffDown, lEffUp, lEffDown;

  double btagSFCentTTBAR;
  double bEffUpTTBAR, bEffDownTTBAR, cEffUpTTBAR, cEffDownTTBAR, lEffUpTTBAR, lEffDownTTBAR;

  int btagzerolep_channel, btag1e_channel, btag1mu_channel;
  int btag2e_channel, btag2mu_channel;
  int btagemu_channel;
  int btag0l_channel;  

  double meff2j, meff3j, meff4j, meffinc;  

  int num_bjets, num_bjets35,  num_bjets25, num_bjets30, num_bjets40;
  int num_bjets60, num_bjets85;


  int ntrackjets;
  int nbtrackjets60;
  int nbtrackjets70; 
  int nbtrackjets77;
  int nbtrackjets85;
  int nbtrackjets60_unmatched;
  int nbtrackjets70_unmatched;
  int nbtrackjets77_unmatched;
  int nbtrackjets85_unmatched;


  double MT,MT_orig,MTjmin,MTbmin,MTbmax,MTblmin,MTbmax_orig,MTbmin_orig;
  double mtbmin;
  double MTj1,MTj2,MTj3,MTj4,MTjmax;
  double topness,pt_asym;
  double centrality;
  double m_MT2ll;

  double m_top1,m_top2;

  double m_PTISR,m_RISR,m_MS,m_MV,m_dphiISRI,m_pTbV1,m_NbV,m_NjV,m_pTjV4,m_NbISR,m_NjISR;

  double pileupweight,pileupweightUP,pileupweightDOWN;
  
  double AnalysisWeight,LeptonWeight,LostLeptonWeight,TriggerWeight;

  double ElecWeight,MuonWeight,PhotonWeight,PhotonReWeight,PhotonAcptWeight;
  double ElecWeightReco,ElecWeightTrig;
  double MuonWeightReco,MuonWeightTrig;
  double        ElecWeightIDUP;   
  double        ElecWeightIDDOWN; 
  double        ElecWeightISOUP;  
  double        ElecWeightISODOWN;
  double        ElecWeightRECOUP; 
  double        ElecWeightRECODOWN;
  double        ElecWeightTRIGUP;  
  double        ElecWeightTRIGDOWN;
  double        ElecWeightTRIGEFFUP;
  double        ElecWeightTRIGEFFDOWN;

  double         MuonWeightSTATUP;
  double         MuonWeightSTATDOWN;
  double         MuonWeightSYSUP;
  double         MuonWeightSYSDOWN;
  double         MuonWeightTRIGSTATUP;
  double         MuonWeightTRIGSTATDOWN;
  double         MuonWeightTRIGSYSTUP;
  double         MuonWeightTRIGSYSTDOWN;
  double         MuonWeightISOSTATUP;
  double         MuonWeightISOSTATDOWN;
  double         MuonWeightISOSYSTUP;
  double         MuonWeightISOSYSTDOWN;
  double         MuonWeightTTVASTATUP;
  double         MuonWeightTTVASTATDOWN;
  double         MuonWeightTTVASYSTUP;
  double         MuonWeightTTVASYSTDOWN;


  int top_hfor_type, top_hfor_type_2;

  double mu;

  int nvtx;
  
  double mct2b, mctcorr2b, mbb;
  double mct1b,mct0b,mctcorr1b,mctcorr0b;
  double mctcorr2bfix,mctcorr1bfix,mctcorr0bfix;
  
  double mctjj,mctcorrjj;
  double amt2alt,amt2new,amt2new2, amt2_zerolep,mt2alt,mt2alt2,mt2alt3,mt2alt4,mt2susy;

  std::vector<double> amt2;
  std::vector<double> mt2;

  double dphibb, dphijj, drbb;

  double weight_ttbar_powheg;


  double averageIntPerXing;
  double averageIntPerXingCorr;
  double lbn;

  Int_t           ttbar_class;

  int          HLT_e24_lhmedium_L1EM20VHORHLT_e60_lhmedium;
  int          HLT_e24_lhmedium_L1EM18VHORHLT_e60_lhmedium;
  int          HLT_mu20_iloose_L1MU15ORHLT_mu50;

  int          HLT_e24_lhmedium_L1EM20VH;
  int          HLT_e24_lhmedium_L1EM18VH;
  int          HLT_mu20_iloose_L1MU15;
  
  int  HLT_g120_loose,HLT_g140_loose;

  int          HLT_e24_lhtight_iloose;
  int          HLT_e20_medium;
  int          HLT_2e17_loose;
  int          HLT_mu24_imedium;
  int          HLT_mu26;
  int          HLT_2mu6;
  int          HLT_xe35;
  int          HLT_xe60;
  int          HLT_xe70;
  int          HLT_xe70_tc_lcw;
  int          HLT_xe70_mht;
  int          HLT_xe80;
  int          HLT_xe90_mht_wEFMu_L1XE50;
  int          HLT_xe90_mht_L1XE50;
  int          HLT_xe100;
  int          HLT_xe100_mht_L1XE50;
  int          HLT_xe110_mht_L1XE50;
  int          HLT_j100_xe80;
  int          HLT_j80_xe80;
  int          HLT_j400;
  int          HLT_j360;

  int HLT_6j60;
  int HLT_6j45_0eta240;

  int HLT_e60_lhmedium;
  int HLT_mu50;
  



  double pT_1djet;
  double pT_2djet;
  double pT_3djet;
  double pT_4djet;
  double mdd;
  double mctdd;
  int nd_good;
  double ddphimin4;
  double eT_miss_dd;

  double eT_miss_simple;
  double dphi_track_simple;
  double sumet_simpleTST;
  double sumet_jet_simpleTST;
  double sumet_mu_simpleTST;
  double sumet_softTrk_simpleTST;
  double sumet_simpleCST;
  double sumet_jet_simpleCST;
  double sumet_mu_simpleCST;
  double sumet_softClus_simpleCST;
  double eT_miss_simple_jet;
  double eT_miss_simple_mu;
  double eT_miss_simple_softTrk;
  double dPhi_1jet_simple;
  double dPhi_2jet_simple;
  double dPhi_3jet_simple;
  double dPhi_4jet_simple;
  double dPhi_1bjet_simple;
  double dPhi_2bjet_simple;
  double dphimin4_simple;
  double dphimin3_simple;
  double dphimin2_simple;
  double MTbmin_simple;
  double MTbmax_simple;


  double mbjj_0;
  double mbjj_1;
  int Tbjj_0;
  int Tbjj_1;
  int Wjj_0;
  int Wjj_1;

  double dPhi_4jet_sys, dPhi_2bjet_sys, dPhi_top1_sys, dPhi_top2_sys, dPhi_tt_sys, dPhi_fat1kt8_sys, dPhi_fat1kt12_sys, dPhi_fat2kt8_sys, dPhi_fat2kt12_sys, dPhi_2fatkt8_sys, dPhi_2fatkt12_sys;


  // Variables: JETS
  int nJets;
  

  /* std::vector<float> jet_px; */
  /* std::vector<float> jet_py; */
  /* std::vector<float> jet_pz; */
  /* std::vector<float> jet_e; */
  /* std::vector<float> jet_MV1; */
  /* std::vector<float> jet_chf; */
  /* std::vector<float> jet_jvtxf; */
  /* std::vector<int> jet_truthflav; */

  // Variables: Electrons
  int nEl;
  /* std::vector<float> el_px; */
  /* std::vector<float> el_py; */
  /* std::vector<float> el_pz; */
  /* std::vector<float> el_e; */

  // Variables: Muons
  int nMu;
  /* std::vector<float> mu_px; */
  /* std::vector<float> mu_py; */
  /* std::vector<float> mu_pz; */
  /* std::vector<float> mu_e; */

  //These definitions are wrong by output - Needs to be a vector in, and a vector out, not a mapping vector->Float (information lost here)
  double pt_tau; 
  double eta_tau; 
  double phi_tau; 
  double e_tau; 

  double bdtJet_tau;
  double bdtEl_tau;
   
  int nPi0_tau;
  int nH_tau;
  int nWTracks_tau;
  int nTracks_tau;
  int nCharged_tau;
  int nNeut_tau;
  

  double maxbdtTauJet;

  int truthJet_matched_tau;
  


  TH1F *HCounter;
  TH1F *HCounterNoWeight;


 private:
  TTree* m_tree = 0;
  TFile* m_file = 0;  

  int m_nfilled = 0;


} ;
#endif
