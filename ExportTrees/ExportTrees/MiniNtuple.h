//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Sep  3 17:04:13 2017 by ROOT version 6.04/16
// from TTree Nominal/m_tree
// found on file: /atlas/macdonald/NTUP_BjetSUSY/Stop0L/Ntuples_AB-2-4-37/summary_Sep01_taus/364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.DAOD_SUSY1.e5307_s2726_r7772_r7676_p2823_stop0L_tau.root
//////////////////////////////////////////////////////////

#ifndef MiniNtuple_h
#define MiniNtuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "string"
#include "vector"
#include "vector"
#include "vector"

using namespace std;

class MiniNtuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   string          *anaflag;
   Int_t           anatruth;
   Float_t         realtime;
   ULong64_t       EventNumber;
   Int_t           RunNumber;
   Float_t         AnalysisWeight;
   Float_t         pileupweight;
   Float_t         pileupweightUP;
   Float_t         pileupweightDOWN;
   ULong64_t       pileupweightHash;
   Float_t         SherpaWeight;
   Int_t           nJets;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<int>     *jet_flav;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_ntracks;
   vector<float>   *trackjet_pt;
   vector<float>   *trackjet_eta;
   vector<float>   *trackjet_phi;
   vector<float>   *trackjet_e;
   vector<float>   *trackjet_MV2c10;
   vector<int>     *trackjet_truthflav;
   Int_t           nbaselineEl;
   Int_t           nEl;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   Int_t           nbaselineMu;
   Int_t           nMu;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   Int_t           nbaselinePh;
   Int_t           nPh;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;

   vector<float>   *tau_pt;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<float>   *tau_e;
   vector<float>   *tau_bdtJet;
   vector<float>   *tau_bdtEl;
   vector<float>   *tau_nH;
   vector<float>   *tau_nWTracks;
   vector<float>   *tau_nTracks;
   vector<float>   *tau_nPi0;
   vector<float>   *tau_nCharged;
   vector<float>   *tau_nNeut;
   vector<int>     *nRecoTaus_matched_TruthTaus;


   Float_t         MET_pt;
   Float_t         MET_phi;
   Float_t         MET_pt_prime;
   Float_t         MET_phi_prime;
   Float_t         sumet;
   Float_t         metsig;
   Float_t         metsigET;
   Float_t         metsigHT;
   Int_t           year;
   Int_t           SCTerror;
   Int_t           CoreFlag;
   Int_t           lbn;
   Int_t           bcid;
   Float_t         TriggerSF;
   Float_t         MuonWeightReco;
   Float_t         MuonWeightTrigger;
   Float_t         ElecWeightReco;
   Float_t         ElecWeightTrigger;
   Float_t         PhotonWeight;
   Float_t         MuonWeightSTATUP;
   Float_t         MuonWeightSTATDOWN;
   Float_t         MuonWeightSYSUP;
   Float_t         MuonWeightSYSDOWN;
   Float_t         MuonWeightTRIGSTATUP;
   Float_t         MuonWeightTRIGSTATDOWN;
   Float_t         MuonWeightTRIGSYSTUP;
   Float_t         MuonWeightTRIGSYSTDOWN;
   Float_t         MuonWeightISOSTATUP;
   Float_t         MuonWeightISOSTATDOWN;
   Float_t         MuonWeightISOSYSTUP;
   Float_t         MuonWeightISOSYSTDOWN;
   Float_t         MuonWeightTTVASTATUP;
   Float_t         MuonWeightTTVASTATDOWN;
   Float_t         MuonWeightTTVASYSTUP;
   Float_t         MuonWeightTTVASYSTDOWN;
   Float_t         MuonWeightBADMUONSTATUP;
   Float_t         MuonWeightBADMUONSTATDOWN;
   Float_t         MuonWeightBADMUONSYSUP;
   Float_t         MuonWeightBADMUONSYSDOWN;
   Float_t         ElecWeightIDUP;
   Float_t         ElecWeightIDDOWN;
   Float_t         ElecWeightISOUP;
   Float_t         ElecWeightISODOWN;
   Float_t         ElecWeightRECOUP;
   Float_t         ElecWeightRECODOWN;
   Float_t         ElecWeightTRIGUP;
   Float_t         ElecWeightTRIGDOWN;
   Float_t         ElecWeightTRIGEFFUP;
   Float_t         ElecWeightTRIGEFFDOWN;
   Float_t         ElecWeightCHARGEIDSELUP;
   Float_t         ElecWeightCHARGEIDSELDOWN;
   Float_t         PhotonSF_IDUP;
   Float_t         PhotonSF_IDDOWN;
   Float_t         PhotonSF_TRKISOUP;
   Float_t         PhotonSF_TRKISODOWN;
   Float_t         TriggerWeight;
   Float_t         TriggerWeight2;
   Float_t         jvtweight;
   Float_t         jvtweightUP;
   Float_t         jvtweightDOWN;
   Float_t         btagweight;
   Float_t         btagweightBUP;
   Float_t         btagweightBDOWN;
   Float_t         btagweightCUP;
   Float_t         btagweightCDOWN;
   Float_t         btagweightLUP;
   Float_t         btagweightLDOWN;
   Float_t         btagweightExUP;
   Float_t         btagweightExDOWN;
   Float_t         btagweightExCUP;
   Float_t         btagweightExCDOWN;
   Float_t         averageIntPerXing;
   Float_t         averageIntPerXingCorr;
   vector<float>   *jet_MV2c10;
   vector<float>   *jet_chf;
   vector<float>   *jet_jvtxf;
   vector<float>   *jet_BCH_CORR_CELL;
   vector<float>   *jet_emfrac;
   vector<int>     *jet_fmax;


   vector<float>   *el_SF;
   Int_t           ncosmicMu;
   vector<float>   *mu_SF;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_topoetcone40;
   vector<float>   *ptcone20;
   vector<float>   *ptvarcone20;
   Int_t           passtauveto;
   Float_t         MET_jet_pt;
   Float_t         MET_jet_phi;
   Float_t         MET_mu_pt;
   Float_t         MET_mu_phi;
   Float_t         MET_el_pt;
   Float_t         MET_el_phi;
   Float_t         MET_y_pt;
   Float_t         MET_y_phi;
   Float_t         MET_softTrk_pt;
   Float_t         MET_softTrk_phi;
   Float_t         MET_track_pt;
   Float_t         MET_track_phi;
   Float_t         MET_NonInt_pt;
   Float_t         MET_NonInt_phi;
   Float_t         MET_tau_pt;
   Float_t         MET_tau_phi;
   Float_t         sumet_jet;
   Float_t         sumet_el;
   Float_t         sumet_y;
   Float_t         sumet_mu;
   Float_t         sumet_softTrk;
   Float_t         sumet_softClu;
   Float_t         sumet_NonInt;
   Float_t         sumet_tau;
   Float_t         cellMET;
   Float_t         mhtMET;
   Float_t         METTrigPassed;
   Bool_t          HLT_e120_lhloose;
   Bool_t          matched_HLT_e120_lhloose;
   Bool_t          HLT_e60_lhmedium;
   Bool_t          matched_HLT_e60_lhmedium;
   Bool_t          HLT_e24_lhmedium_L1EM20VH;
   Bool_t          matched_HLT_e24_lhmedium_L1EM20VH;
   Bool_t          HLT_e24_lhmedium_L1EM18VH;
   Bool_t          matched_HLT_e24_lhmedium_L1EM18VH;
   Bool_t          HLT_e20_medium;
   Bool_t          matched_HLT_e20_medium;
   Bool_t          HLT_2e17_loose;
   Bool_t          matched_HLT_2e17_loose;
   Bool_t          HLT_mu50;
   Bool_t          matched_HLT_mu50;
   Bool_t          HLT_mu20_iloose_L1MU15;
   Bool_t          matched_HLT_mu20_iloose_L1MU15;
   Bool_t          HLT_mu26;
   Bool_t          matched_HLT_mu26;
   Bool_t          HLT_mu26_ivarmedium;
   Bool_t          matched_HLT_mu26_ivarmedium;
   Bool_t          HLT_2mu6;
   Bool_t          matched_HLT_2mu6;
   Bool_t          HLT_g120_loose;
   Bool_t          matched_HLT_g120_loose;
   Bool_t          HLT_g140_loose;
   Bool_t          matched_HLT_g140_loose;
   Bool_t          HLT_g200_etcut;
   Bool_t          matched_HLT_g200_etcut;
   Bool_t          HLT_g35_loose_L1EM15;
   Bool_t          matched_HLT_g35_loose_L1EM15;
   Bool_t          HLT_g40_loose_L1EM15;
   Bool_t          matched_HLT_g40_loose_L1EM15;
   Bool_t          HLT_g45_loose_L1EM15;
   Bool_t          matched_HLT_g45_loose_L1EM15;
   Bool_t          HLT_g50_loose_L1EM15;
   Bool_t          matched_HLT_g50_loose_L1EM15;
   Bool_t          HLT_2j35_btight_2j35_L13J25_0ETA23;
   Bool_t          matched_HLT_2j35_btight_2j35_L13J25_0ETA23;
   Bool_t          HLT_2j45_bmedium_2j45_L13J25_0ETA23;
   Bool_t          matched_HLT_2j45_bmedium_2j45_L13J25_0ETA23;
   Bool_t          HLT_6j45_0eta240;
   Bool_t          HLT_6j60;
   Bool_t          HLT_j100_2j55_bmedium;
   Bool_t          matched_HLT_j100_2j55_bmedium;
   Bool_t          HLT_j150_bmedium_j50_bmedium;
   Bool_t          matched_HLT_j150_bmedium_j50_bmedium;
   Bool_t          HLT_2j65_btight_j65;
   Bool_t          matched_HLT_2j65_btight_j65;
   Bool_t          HLT_2j70_bmedium_j70;
   Bool_t          matched_HLT_2j70_bmedium_j70;
   Bool_t          HLT_j225_bloose;
   Bool_t          matched_HLT_j225_bloose;
   Bool_t          HLT_j65_btight_3j65_L13J25_0ETA23;
   Bool_t          matched_HLT_j65_btight_3j65_L13J25_0ETA23;
   Bool_t          HLT_j70_bmedium_3j70_L13J25_0ETA23;
   Bool_t          matched_HLT_j70_bmedium_3j70_L13J25_0ETA23;
   Bool_t          HLT_xe35;
   Bool_t          matched_HLT_xe35;
   Bool_t          HLT_xe60;
   Bool_t          matched_HLT_xe60;
   Bool_t          HLT_xe70;
   Bool_t          matched_HLT_xe70;
   Bool_t          HLT_xe70_tc_lcw;
   Bool_t          matched_HLT_xe70_tc_lcw;
   Bool_t          HLT_xe70_mht;
   Bool_t          matched_HLT_xe70_mht;
   Bool_t          HLT_xe80;
   Bool_t          matched_HLT_xe80;
   Bool_t          HLT_xe80_tc_lcw_L1XE50;
   Bool_t          matched_HLT_xe80_tc_lcw_L1XE50;
   Bool_t          HLT_xe90_mht_L1XE50;
   Bool_t          matched_HLT_xe90_mht_L1XE50;
   Bool_t          HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Bool_t          matched_HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Bool_t          HLT_xe90_mht_wEFMu_L1XE50;
   Bool_t          matched_HLT_xe90_mht_wEFMu_L1XE50;
   Bool_t          HLT_xe100;
   Bool_t          matched_HLT_xe100;
   Bool_t          HLT_xe100_L1XE50;
   Bool_t          matched_HLT_xe100_L1XE50;
   Bool_t          HLT_xe100_tc_em_L1XE50;
   Bool_t          matched_HLT_xe100_tc_em_L1XE50;
   Bool_t          HLT_xe100_mht_L1XE50;
   Bool_t          matched_HLT_xe100_mht_L1XE50;
   Bool_t          HLT_xe110_mht_L1XE50;
   Bool_t          matched_HLT_xe110_mht_L1XE50;
   Bool_t          HLT_xe120_pueta;
   Bool_t          matched_HLT_xe120_pueta;
   Bool_t          HLT_xe120_pufit;
   Bool_t          matched_HLT_xe120_pufit;
   Bool_t          HLT_xe130_mht_L1XE50;
   Bool_t          matched_HLT_xe130_mht_L1XE50;
   Bool_t          HLT_j100_xe80;
   Bool_t          matched_HLT_j100_xe80;
   Bool_t          HLT_j80_xe80;
   Bool_t          matched_HLT_j80_xe80;
   Bool_t          HLT_j400;
   Bool_t          matched_HLT_j400;
   Bool_t          HLT_j360;
   Bool_t          matched_HLT_j360;
   Bool_t          HLT_j320;
   Bool_t          matched_HLT_j320;
   Bool_t          HLT_j260;
   Bool_t          matched_HLT_j260;
   Bool_t          HLT_j200;
   Bool_t          matched_HLT_j200;
   Bool_t          HLT_j175;
   Bool_t          matched_HLT_j175;
   Bool_t          HLT_j150;
   Bool_t          matched_HLT_j150;
   Bool_t          HLT_j110;
   Bool_t          matched_HLT_j110;
   Bool_t          HLT_j85;
   Bool_t          matched_HLT_j85;
   Bool_t          HLT_j60;
   Bool_t          matched_HLT_j60;
   Bool_t          HLT_j25;
   Bool_t          matched_HLT_j25;
   Bool_t          HLT_j15;
   Bool_t          matched_HLT_j15;
   Bool_t          HLT_mu40;
   Bool_t          matched_HLT_mu40;
   Bool_t          HLT_mu24_iloose;
   Bool_t          matched_HLT_mu24_iloose;
   Bool_t          HLT_mu24_ivarloose;
   Bool_t          matched_HLT_mu24_ivarloose;
   Bool_t          HLT_mu24_L1MU15;
   Bool_t          matched_HLT_mu24_L1MU15;
   Bool_t          HLT_mu24_ivarmedium;
   Bool_t          matched_HLT_mu24_ivarmedium;
   Bool_t          HLT_mu24_imedium;
   Bool_t          matched_HLT_mu24_imedium;
   Bool_t          HLT_e24_lhtight_nod0_ivarloose;
   Bool_t          matched_HLT_e24_lhtight_nod0_ivarloose;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          matched_HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          HLT_e60_lhmedium_nod0;
   Bool_t          matched_HLT_e60_lhmedium_nod0;
   Bool_t          HLT_e60_medium;
   Bool_t          matched_HLT_e60_medium;
   Bool_t          HLT_e140_lhloose_nod0;
   Bool_t          matched_HLT_e140_lhloose_nod0;
   Bool_t          HLT_e300_etcut;
   Bool_t          matched_HLT_e300_etcut;
   Int_t           hasMEphoton;
   Int_t           hasMEphoton80;
   Int_t           hasMEphotonMCTC;
   Float_t         TruthNonInt_pt;
   Float_t         TruthNonInt_phi;
   Double_t        GenFiltMET;
   Double_t        GenFiltHT;
    vector<int>     *nsmrJets;
   vector<int>     *smrjet_index;
   vector<float>   *smrjet_pt;
   vector<float>   *smrjet_eta;
   vector<float>   *smrjet_phi;
   vector<float>   *smrjet_e;
   vector<float>   *smrjet_MV2;
   vector<int>     *smrjet_flav;
   vector<float>   *smrMET_pt;
   vector<float>   *smrMET_phi;
     Int_t           nFatJetsKt8;
   vector<float>   *rcjet_kt8_pt;
   vector<float>   *rcjet_kt8_eta;
   vector<float>   *rcjet_kt8_phi;
   vector<float>   *rcjet_kt8_e;
   Int_t           nFatJetsKt12;
   vector<float>   *rcjet_kt12_pt;
   vector<float>   *rcjet_kt12_eta;
   vector<float>   *rcjet_kt12_phi;
   vector<float>   *rcjet_kt12_e;
   Int_t           nFatJetsKt10;
   vector<float>   *fatjet_kt10_pt;
   vector<float>   *fatjet_kt10_eta;
   vector<float>   *fatjet_kt10_phi;
   vector<float>   *fatjet_kt10_e;
   vector<int>     *fatjet_kt10_ntrkjets;
   vector<int>     *fatjet_kt10_nghostbhad;
   vector<float>   *fatjet_kt10_Split12;
   vector<float>   *fatjet_kt10_Split23;
   vector<float>   *fatjet_kt10_Split34;
   vector<float>   *fatjet_kt10_Qw;
   vector<float>   *fatjet_kt10_Tau1;
   vector<float>   *fatjet_kt10_Tau2;
   vector<float>   *fatjet_kt10_Tau3;
   vector<float>   *fatjet_kt10_Tau32;
   vector<bool>    *fatjet_kt10_w50;
   vector<bool>    *fatjet_kt10_w80;
   vector<bool>    *fatjet_kt10_z50;
   vector<bool>    *fatjet_kt10_z80;
   vector<int>     *nt_fatjet_kt10_W50res;
   vector<int>     *nt_fatjet_kt10_W80res;
   vector<int>     *nt_fatjet_kt10_Z50res;
   vector<int>     *nt_fatjet_kt10_Z80res;
   vector<float>   *nt_fatjet_kt10_WLowWMassCut50;
   vector<float>   *nt_fatjet_kt10_WLowWMassCut80;
   vector<float>   *nt_fatjet_kt10_ZLowWMassCut50;
   vector<float>   *nt_fatjet_kt10_ZLowWMassCut80;
   vector<float>   *nt_fatjet_kt10_WHighWMassCut50;
   vector<float>   *nt_fatjet_kt10_WHighWMassCut80;
   vector<float>   *nt_fatjet_kt10_ZHighWMassCut50;
   vector<float>   *nt_fatjet_kt10_ZHighWMassCut80;
   vector<float>   *nt_fatjet_kt10_WD2Cut50;
   vector<float>   *nt_fatjet_kt10_WD2Cut80;
   vector<float>   *nt_fatjet_kt10_ZD2Cut50;
   vector<float>   *nt_fatjet_kt10_ZD2Cut80;
   vector<bool>    *fatjet_kt10_top50;
   vector<bool>    *fatjet_kt10_top80;
   vector<int>     *nt_fatjet_kt10_top50res;
   vector<int>     *nt_fatjet_kt10_top80res;
   vector<float>   *nt_fatjet_kt10_TopTagTau32Cut50;
   vector<float>   *nt_fatjet_kt10_TopTagTau32Cut80;
   vector<float>   *nt_fatjet_kt10_TopTagSplit23Cut50;
   vector<float>   *nt_fatjet_kt10_TopTagSplit23Cut80;
   vector<int>     *jet_truthmatched;
   vector<float>   *truthJet_pt;
   vector<float>   *truthJet_eta;
   vector<float>   *truthJet_phi;
   vector<float>   *truthJet_e;
   vector<float>   *truthJet_flav;
   vector<float>   *truthJet_matched;
   vector<int>     *truthJet_calib_isgood;
   vector<int>     *truthJet_matched_tau;


   vector<float>   *RMapRecoJet_pt;
   vector<float>   *RMapRecoJet_eta;
   vector<float>   *RMapRecoJet_phi;
   vector<float>   *RMapRecoJet_e;
   vector<float>   *RMapRecoJet_flav;

   



   // List of branches
   TBranch        *b_anaflag;   //!
   TBranch        *b_anatruth;   //!
   TBranch        *b_realtime;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_AnalysisWeight;   //!
   TBranch        *b_pileupweight;   //!
   TBranch        *b_pileupweightUP;   //!
   TBranch        *b_pileupweightDOWN;   //!
   TBranch        *b_pileupweightHash;   //!
   TBranch        *b_SherpaWeight;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_flav;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_ntracks;   //!
   TBranch        *b_trackjet_pt;   //!
   TBranch        *b_trackjet_eta;   //!
   TBranch        *b_trackjet_phi;   //!
   TBranch        *b_trackjet_e;   //!
   TBranch        *b_trackjet_MV2c10;   //!
   TBranch        *b_trackjet_truthflav;   //!
   TBranch        *b_nbaselineEl;   //!
   TBranch        *b_nEl;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_nbaselineMu;   //!
   TBranch        *b_nMu;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_nbaselinePh;   //!
   TBranch        *b_nPh;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_e;   //!

   TBranch        *b_tau_nPi0; //!=
   TBranch        *b_tau_bdtJet;   //!
   TBranch        *b_tau_bdtEl;   //!
   TBranch        *b_tau_nH;   //!
   TBranch        *b_tau_nWTracks;   //!
   TBranch        *b_tau_nTracks;   //!
   TBranch        *b_tau_nCharged;   //!
   TBranch        *b_tau_nNeut;   //!

   TBranch        *b_MET_pt;   //!
   TBranch        *b_MET_phi;   //!
   TBranch        *b_MET_pt_prime;   //!
   TBranch        *b_MET_phi_prime;   //!
   TBranch        *b_sumet;   //!
   TBranch        *b_metsig;   //!
   TBranch        *b_metsigET;   //!
   TBranch        *b_metsigHT;   //!
   TBranch        *b_year;   //!
   TBranch        *b_SCTerror;   //!
   TBranch        *b_CoreFlag;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_TriggerSF;   //!
   TBranch        *b_MuonWeightReco;   //!
   TBranch        *b_MuonWeightTrigger;   //!
   TBranch        *b_ElecWeightReco;   //!
   TBranch        *b_ElecWeightTrigger;   //!
   TBranch        *b_PhotonWeight;   //!
   TBranch        *b_MuonWeightSTATUP;   //!
   TBranch        *b_MuonWeightSTATDOWN;   //!
   TBranch        *b_MuonWeightSYSUP;   //!
   TBranch        *b_MuonWeightSYSDOWN;   //!
   TBranch        *b_MuonWeightTRIGSTATUP;   //!
   TBranch        *b_MuonWeightTRIGSTATDOWN;   //!
   TBranch        *b_MuonWeightTRIGSYSTUP;   //!
   TBranch        *b_MuonWeightTRIGSYSTDOWN;   //!
   TBranch        *b_MuonWeightISOSTATUP;   //!
   TBranch        *b_MuonWeightISOSTATDOWN;   //!
   TBranch        *b_MuonWeightISOSYSTUP;   //!
   TBranch        *b_MuonWeightISOSYSTDOWN;   //!
   TBranch        *b_MuonWeightTTVASTATUP;   //!
   TBranch        *b_MuonWeightTTVASTATDOWN;   //!
   TBranch        *b_MuonWeightTTVASYSTUP;   //!
   TBranch        *b_MuonWeightTTVASYSTDOWN;   //!
   TBranch        *b_MuonWeightBADMUONSTATUP;   //!
   TBranch        *b_MuonWeightBADMUONSTATDOWN;   //!
   TBranch        *b_MuonWeightBADMUONSYSUP;   //!
   TBranch        *b_MuonWeightBADMUONSYSDOWN;   //!
   TBranch        *b_ElecWeightIDUP;   //!
   TBranch        *b_ElecWeightIDDOWN;   //!
   TBranch        *b_ElecWeightISOUP;   //!
   TBranch        *b_ElecWeightISODOWN;   //!
   TBranch        *b_ElecWeightRECOUP;   //!
   TBranch        *b_ElecWeightRECODOWN;   //!
   TBranch        *b_ElecWeightTRIGUP;   //!
   TBranch        *b_ElecWeightTRIGDOWN;   //!
   TBranch        *b_ElecWeightTRIGEFFUP;   //!
   TBranch        *b_ElecWeightTRIGEFFDOWN;   //!
   TBranch        *b_ElecWeightCHARGEIDSELUP;   //!
   TBranch        *b_ElecWeightCHARGEIDSELDOWN;   //!
   TBranch        *b_PhotonSF_IDUP;   //!
   TBranch        *b_PhotonSF_IDDOWN;   //!
   TBranch        *b_PhotonSF_TRKISOUP;   //!
   TBranch        *b_PhotonSF_TRKISODOWN;   //!
   TBranch        *b_TriggerWeight;   //!
   TBranch        *b_TriggerWeight2;   //!
   TBranch        *b_jvtweight;   //!
   TBranch        *b_jvtweightUP;   //!
   TBranch        *b_jvtweightDOWN;   //!
   TBranch        *b_btagweight;   //!
   TBranch        *b_btagweightBUP;   //!
   TBranch        *b_btagweightBDOWN;   //!
   TBranch        *b_btagweightCUP;   //!
   TBranch        *b_btagweightCDOWN;   //!
   TBranch        *b_btagweightLUP;   //!
   TBranch        *b_btagweightLDOWN;   //!
   TBranch        *b_btagweightExUP;   //!
   TBranch        *b_btagweightExDOWN;   //!
   TBranch        *b_btagweightExCUP;   //!
   TBranch        *b_btagweightExCDOWN;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_averageIntPerXingCorr;   //!
   TBranch        *b_jet_MV2c10;   //!
   TBranch        *b_jet_chf;   //!
   TBranch        *b_jet_jvtxf;   //!
   TBranch        *b_jet_BCH_CORR_CELL;   //!
   TBranch        *b_jet_emfrac;   //!
   TBranch        *b_jet_fmax;   //!

   TBranch        *b_el_SF;   //!
   TBranch        *b_ncosmicMu;   //!
   TBranch        *b_mu_SF;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ptcone20;   //!
   TBranch        *b_ptvarcone20;   //!
   TBranch        *b_passtauveto;   //!
   TBranch        *b_MET_jet_pt;   //!
   TBranch        *b_MET_jet_phi;   //!
   TBranch        *b_MET_mu_pt;   //!
   TBranch        *b_MET_mu_phi;   //!
   TBranch        *b_MET_el_pt;   //!
   TBranch        *b_MET_el_phi;   //!
   TBranch        *b_MET_y_pt;   //!
   TBranch        *b_MET_y_phi;   //!
   TBranch        *b_MET_softTrk_pt;   //!
   TBranch        *b_MET_softTrk_phi;   //!
   TBranch        *b_MET_track_pt;   //!
   TBranch        *b_MET_track_phi;   //!
   TBranch        *b_MET_NonInt_pt;   //!
   TBranch        *b_MET_NonInt_phi;   //!
   TBranch        *b_MET_tau_pt;   //!
   TBranch        *b_MET_tau_phi;   //!
   TBranch        *b_sumet_jet;   //!
   TBranch        *b_sumet_el;   //!
   TBranch        *b_sumet_y;   //!
   TBranch        *b_sumet_mu;   //!
   TBranch        *b_sumet_softTrk;   //!
   TBranch        *b_sumet_softClu;   //!
   TBranch        *b_sumet_NonInt;   //!
   TBranch        *b_cellMET;   //!
   TBranch        *b_mhtMET;   //!
   TBranch        *b_METTrigPassed;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_matched_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_matched_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_matched_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_matched_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_HLT_e20_medium;   //!
   TBranch        *b_matched_HLT_e20_medium;   //!
   TBranch        *b_HLT_2e17_loose;   //!
   TBranch        *b_matched_HLT_2e17_loose;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_matched_HLT_mu50;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu26;   //!
   TBranch        *b_matched_HLT_mu26;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_2mu6;   //!
   TBranch        *b_matched_HLT_2mu6;   //!
   TBranch        *b_HLT_g120_loose;   //!
   TBranch        *b_matched_HLT_g120_loose;   //!
   TBranch        *b_HLT_g140_loose;   //!
   TBranch        *b_matched_HLT_g140_loose;   //!
   TBranch        *b_HLT_g200_etcut;   //!
   TBranch        *b_matched_HLT_g200_etcut;   //!
   TBranch        *b_HLT_g35_loose_L1EM15;   //!
   TBranch        *b_matched_HLT_g35_loose_L1EM15;   //!
   TBranch        *b_HLT_g40_loose_L1EM15;   //!
   TBranch        *b_matched_HLT_g40_loose_L1EM15;   //!
   TBranch        *b_HLT_g45_loose_L1EM15;   //!
   TBranch        *b_matched_HLT_g45_loose_L1EM15;   //!
   TBranch        *b_HLT_g50_loose_L1EM15;   //!
   TBranch        *b_matched_HLT_g50_loose_L1EM15;   //!
   TBranch        *b_HLT_2j35_btight_2j35_L13J25_0ETA23;   //!
   TBranch        *b_matched_HLT_2j35_btight_2j35_L13J25_0ETA23;   //!
   TBranch        *b_HLT_2j45_bmedium_2j45_L13J25_0ETA23;   //!
   TBranch        *b_matched_HLT_2j45_bmedium_2j45_L13J25_0ETA23;   //!
   TBranch        *b_HLT_6j45_0eta240; //!
   TBranch        *b_HLT_6j60; //!
   TBranch        *b_HLT_j100_2j55_bmedium;   //!
   TBranch        *b_matched_HLT_j100_2j55_bmedium;   //!
   TBranch        *b_HLT_j150_bmedium_j50_bmedium;   //!
   TBranch        *b_matched_HLT_j150_bmedium_j50_bmedium;   //!
   TBranch        *b_HLT_2j65_btight_j65;   //!
   TBranch        *b_matched_HLT_2j65_btight_j65;   //!
   TBranch        *b_HLT_2j70_bmedium_j70;   //!
   TBranch        *b_matched_HLT_2j70_bmedium_j70;   //!
   TBranch        *b_HLT_j225_bloose;   //!
   TBranch        *b_matched_HLT_j225_bloose;   //!
   TBranch        *b_HLT_j65_btight_3j65_L13J25_0ETA23;   //!
   TBranch        *b_matched_HLT_j65_btight_3j65_L13J25_0ETA23;   //!
   TBranch        *b_HLT_j70_bmedium_3j70_L13J25_0ETA23;   //!
   TBranch        *b_matched_HLT_j70_bmedium_3j70_L13J25_0ETA23;   //!
   TBranch        *b_HLT_xe35;   //!
   TBranch        *b_matched_HLT_xe35;   //!
   TBranch        *b_HLT_xe60;   //!
   TBranch        *b_matched_HLT_xe60;   //!
   TBranch        *b_HLT_xe70;   //!
   TBranch        *b_matched_HLT_xe70;   //!
   TBranch        *b_HLT_xe70_tc_lcw;   //!
   TBranch        *b_matched_HLT_xe70_tc_lcw;   //!
   TBranch        *b_HLT_xe70_mht;   //!
   TBranch        *b_matched_HLT_xe70_mht;   //!
   TBranch        *b_HLT_xe80;   //!
   TBranch        *b_matched_HLT_xe80;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_matched_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe100;   //!
   TBranch        *b_matched_HLT_xe100;   //!
   TBranch        *b_HLT_xe100_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_L1XE50;   //!
   TBranch        *b_HLT_xe100_tc_em_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_tc_em_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_xe120_pueta;   //!
   TBranch        *b_matched_HLT_xe120_pueta;   //!
   TBranch        *b_HLT_xe120_pufit;   //!
   TBranch        *b_matched_HLT_xe120_pufit;   //!
   TBranch        *b_HLT_xe130_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe130_mht_L1XE50;   //!
   TBranch        *b_HLT_j100_xe80;   //!
   TBranch        *b_matched_HLT_j100_xe80;   //!
   TBranch        *b_HLT_j80_xe80;   //!
   TBranch        *b_matched_HLT_j80_xe80;   //!
   TBranch        *b_HLT_j400;   //!
   TBranch        *b_matched_HLT_j400;   //!
   TBranch        *b_HLT_j360;   //!
   TBranch        *b_matched_HLT_j360;   //!
   TBranch        *b_HLT_j320;   //!
   TBranch        *b_matched_HLT_j320;   //!
   TBranch        *b_HLT_j260;   //!
   TBranch        *b_matched_HLT_j260;   //!
   TBranch        *b_HLT_j200;   //!
   TBranch        *b_matched_HLT_j200;   //!
   TBranch        *b_HLT_j175;   //!
   TBranch        *b_matched_HLT_j175;   //!
   TBranch        *b_HLT_j150;   //!
   TBranch        *b_matched_HLT_j150;   //!
   TBranch        *b_HLT_j110;   //!
   TBranch        *b_matched_HLT_j110;   //!
   TBranch        *b_HLT_j85;   //!
   TBranch        *b_matched_HLT_j85;   //!
   TBranch        *b_HLT_j60;   //!
   TBranch        *b_matched_HLT_j60;   //!
   TBranch        *b_HLT_j25;   //!
   TBranch        *b_matched_HLT_j25;   //!
   TBranch        *b_HLT_j15;   //!
   TBranch        *b_matched_HLT_j15;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_matched_HLT_mu40;   //!
   TBranch        *b_HLT_mu24_iloose;   //!
   TBranch        *b_matched_HLT_mu24_iloose;   //!
   TBranch        *b_HLT_mu24_ivarloose;   //!
   TBranch        *b_matched_HLT_mu24_ivarloose;   //!
   TBranch        *b_HLT_mu24_L1MU15;   //!
   TBranch        *b_matched_HLT_mu24_L1MU15;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_matched_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu24_imedium;   //!
   TBranch        *b_matched_HLT_mu24_imedium;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_matched_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_matched_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_matched_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e60_medium;   //!
   TBranch        *b_matched_HLT_e60_medium;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_matched_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e300_etcut;   //!
   TBranch        *b_matched_HLT_e300_etcut;   //!
   TBranch        *b_hasMEphoton;   //!
   TBranch        *b_hasMEphoton80;   //!
   TBranch        *b_hasMEphotonMCTC;   //!
   TBranch        *b_TruthNonInt_pt;   //!
   TBranch        *b_TruthNonInt_phi;   //!
   TBranch        *b_GenFiltMET;   //!
   TBranch        *b_GenFiltHT;   //!
   TBranch        *b_nsmrJets;   //!
   TBranch        *b_smrjet_index;   //!
   TBranch        *b_smrjet_pt;   //!
   TBranch        *b_smrjet_eta;   //!
   TBranch        *b_smrjet_phi;   //!
   TBranch        *b_smrjet_e;   //!
   TBranch        *b_smrjet_MV2;   //!
   TBranch        *b_smrjet_flav;   //!
   TBranch        *b_smrMET_pt;   //!
   TBranch        *b_smrMET_phi;   //!
   TBranch        *b_nFatJetsKt8;   //!
   TBranch        *b_rcjet_kt8_pt;   //!
   TBranch        *b_rcjet_kt8_eta;   //!
   TBranch        *b_rcjet_kt8_phi;   //!
   TBranch        *b_rcjet_kt8_e;   //!
   TBranch        *b_nFatJetsKt12;   //!
   TBranch        *b_rcjet_kt12_pt;   //!
   TBranch        *b_rcjet_kt12_eta;   //!
   TBranch        *b_rcjet_kt12_phi;   //!
   TBranch        *b_rcjet_kt12_e;   //!
   TBranch        *b_nFatJetsKt10;   //!
   TBranch        *b_fatjet_kt10_pt;   //!
   TBranch        *b_fatjet_kt10_eta;   //!
   TBranch        *b_fatjet_kt10_phi;   //!
   TBranch        *b_fatjet_kt10_e;   //!
   TBranch        *b_fatjet_kt10_ntrkjets;   //!
   TBranch        *b_fatjet_kt10_nghostbhad;   //!
   TBranch        *b_fatjet_kt10_Split12;   //!
   TBranch        *b_fatjet_kt10_Split23;   //!
   TBranch        *b_fatjet_kt10_Split34;   //!
   TBranch        *b_fatjet_kt10_Qw;   //!
   TBranch        *b_fatjet_kt10_Tau1;   //!
   TBranch        *b_fatjet_kt10_Tau2;   //!
   TBranch        *b_fatjet_kt10_Tau3;   //!
   TBranch        *b_fatjet_kt10_Tau32;   //!
   TBranch        *b_fatjet_kt10_w50;   //!
   TBranch        *b_fatjet_kt10_w80;   //!
   TBranch        *b_fatjet_kt10_z50;   //!
   TBranch        *b_fatjet_kt10_z80;   //!
   TBranch        *b_nt_fatjet_kt10_W50res;   //!
   TBranch        *b_nt_fatjet_kt10_W80res;   //!
   TBranch        *b_nt_fatjet_kt10_Z50res;   //!
   TBranch        *b_nt_fatjet_kt10_Z80res;   //!
   TBranch        *b_nt_fatjet_kt10_WLowWMassCut50;   //!
   TBranch        *b_nt_fatjet_kt10_WLowWMassCut80;   //!
   TBranch        *b_nt_fatjet_kt10_ZLowWMassCut50;   //!
   TBranch        *b_nt_fatjet_kt10_ZLowWMassCut80;   //!
   TBranch        *b_nt_fatjet_kt10_WHighWMassCut50;   //!
   TBranch        *b_nt_fatjet_kt10_WHighWMassCut80;   //!
   TBranch        *b_nt_fatjet_kt10_ZHighWMassCut50;   //!
   TBranch        *b_nt_fatjet_kt10_ZHighWMassCut80;   //!
   TBranch        *b_nt_fatjet_kt10_WD2Cut50;   //!
   TBranch        *b_nt_fatjet_kt10_WD2Cut80;   //!
   TBranch        *b_nt_fatjet_kt10_ZD2Cut50;   //!
   TBranch        *b_nt_fatjet_kt10_ZD2Cut80;   //!
   TBranch        *b_fatjet_kt10_top50;   //!
   TBranch        *b_fatjet_kt10_top80;   //!
   TBranch        *b_nt_fatjet_kt10_top50res;   //!
   TBranch        *b_nt_fatjet_kt10_top80res;   //!
   TBranch        *b_nt_fatjet_kt10_TopTagTau32Cut50;   //!
   TBranch        *b_nt_fatjet_kt10_TopTagTau32Cut80;   //!
   TBranch        *b_nt_fatjet_kt10_TopTagSplit23Cut50;   //!
   TBranch        *b_nt_fatjet_kt10_TopTagSplit23Cut80;   //!
   TBranch        *b_jet_truthmatched;   //!
   TBranch        *b_truthJet_pt;   //!
   TBranch        *b_truthJet_eta;   //!
   TBranch        *b_truthJet_phi;   //!
   TBranch        *b_truthJet_e;   //!
   TBranch        *b_truthJet_flav;   //!
   TBranch        *b_truthJet_matched;   //!
   TBranch        *b_truthJet_calib_isgood;   //!
   TBranch        *b_truthJet_matched_tau;   //!
   TBranch        *b_RMapRecoJet_pt;   //!
   TBranch        *b_RMapRecoJet_eta;   //!
   TBranch        *b_RMapRecoJet_phi;   //!
   TBranch        *b_RMapRecoJet_e;   //!
   TBranch        *b_RMapRecoJet_flav;   //!

   MiniNtuple(TTree *tree=0);
   virtual ~MiniNtuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MiniNtuple_cxx
MiniNtuple::MiniNtuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/atlas/macdonald/NTUP_BjetSUSY/Stop0L/Ntuples_AB-2-4-37/summary_Sep01_taus/364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.DAOD_SUSY1.e5307_s2726_r7772_r7676_p2823_stop0L_tau.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/atlas/macdonald/NTUP_BjetSUSY/Stop0L/Ntuples_AB-2-4-37/summary_Sep01_taus/364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.DAOD_SUSY1.e5307_s2726_r7772_r7676_p2823_stop0L_tau.root");
      }
      f->GetObject("Nominal",tree);

   }
   Init(tree);
}

MiniNtuple::~MiniNtuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MiniNtuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MiniNtuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MiniNtuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   anaflag = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_flav = 0;
   jet_truthflav = 0;
   jet_ntracks = 0;
   trackjet_pt = 0;
   trackjet_eta = 0;
   trackjet_phi = 0;
   trackjet_e = 0;
   trackjet_MV2c10 = 0;
   trackjet_truthflav = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_e = 0;
   tau_pt = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_e = 0;
   tau_bdtJet = 0;
   tau_bdtEl = 0;
   tau_nH = 0;
   tau_nWTracks = 0;
   tau_nTracks = 0;
   tau_nPi0 = 0;
   tau_nCharged = 0;
   tau_nNeut = 0;
   jet_MV2c10 = 0;
   jet_chf = 0;
   jet_jvtxf = 0;
   jet_BCH_CORR_CELL = 0;
   jet_emfrac = 0;
   jet_fmax = 0;
   el_SF = 0;
   mu_SF = 0;
   ph_topoetcone20 = 0;
   ph_topoetcone40 = 0;
   ptcone20 = 0;
   ptvarcone20 = 0;
   nsmrJets = 0;
   smrjet_index = 0;
   smrjet_pt = 0;
   smrjet_eta = 0;
   smrjet_phi = 0;
   smrjet_e = 0;
   smrjet_MV2 = 0;
   smrjet_flav = 0;

   smrMET_pt = 0;
   smrMET_phi = 0;
   rcjet_kt8_pt = 0;
   rcjet_kt8_eta = 0;
   rcjet_kt8_phi = 0;
   rcjet_kt8_e = 0;
   rcjet_kt12_pt = 0;
   rcjet_kt12_eta = 0;
   rcjet_kt12_phi = 0;
   rcjet_kt12_e = 0;
   fatjet_kt10_pt = 0;
   fatjet_kt10_eta = 0;
   fatjet_kt10_phi = 0;
   fatjet_kt10_e = 0;
   fatjet_kt10_ntrkjets = 0;
   fatjet_kt10_nghostbhad = 0;
   fatjet_kt10_Split12 = 0;
   fatjet_kt10_Split23 = 0;
   fatjet_kt10_Split34 = 0;
   fatjet_kt10_Qw = 0;
   fatjet_kt10_Tau1 = 0;
   fatjet_kt10_Tau2 = 0;
   fatjet_kt10_Tau3 = 0;
   fatjet_kt10_Tau32 = 0;
   fatjet_kt10_w50 = 0;
   fatjet_kt10_w80 = 0;
   fatjet_kt10_z50 = 0;
   fatjet_kt10_z80 = 0;
   nt_fatjet_kt10_W50res = 0;
   nt_fatjet_kt10_W80res = 0;
   nt_fatjet_kt10_Z50res = 0;
   nt_fatjet_kt10_Z80res = 0;
   nt_fatjet_kt10_WLowWMassCut50 = 0;
   nt_fatjet_kt10_WLowWMassCut80 = 0;
   nt_fatjet_kt10_ZLowWMassCut50 = 0;
   nt_fatjet_kt10_ZLowWMassCut80 = 0;
   nt_fatjet_kt10_WHighWMassCut50 = 0;
   nt_fatjet_kt10_WHighWMassCut80 = 0;
   nt_fatjet_kt10_ZHighWMassCut50 = 0;
   nt_fatjet_kt10_ZHighWMassCut80 = 0;
   nt_fatjet_kt10_WD2Cut50 = 0;
   nt_fatjet_kt10_WD2Cut80 = 0;
   nt_fatjet_kt10_ZD2Cut50 = 0;
   nt_fatjet_kt10_ZD2Cut80 = 0;
   fatjet_kt10_top50 = 0;
   fatjet_kt10_top80 = 0;
   nt_fatjet_kt10_top50res = 0;
   nt_fatjet_kt10_top80res = 0;
   nt_fatjet_kt10_TopTagTau32Cut50 = 0;
   nt_fatjet_kt10_TopTagTau32Cut80 = 0;
   nt_fatjet_kt10_TopTagSplit23Cut50 = 0;
   nt_fatjet_kt10_TopTagSplit23Cut80 = 0;
   jet_truthmatched = 0;
   truthJet_pt = 0;
   truthJet_eta = 0;
   truthJet_phi = 0;

   truthJet_e = 0;
   truthJet_flav = 0;
   truthJet_matched = 0;
   truthJet_matched_tau=0;
   truthJet_calib_isgood = 0;
   truthJet_matched_tau = 0;
   RMapRecoJet_pt = 0;
   RMapRecoJet_eta = 0;
   RMapRecoJet_phi = 0;
   RMapRecoJet_e = 0;
   RMapRecoJet_flav = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("anaflag", &anaflag, &b_anaflag);
   fChain->SetBranchAddress("anatruth", &anatruth, &b_anatruth);
   fChain->SetBranchAddress("realtime", &realtime, &b_realtime);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("AnalysisWeight", &AnalysisWeight, &b_AnalysisWeight);
   fChain->SetBranchAddress("pileupweight", &pileupweight, &b_pileupweight);
   fChain->SetBranchAddress("pileupweightUP", &pileupweightUP, &b_pileupweightUP);
   fChain->SetBranchAddress("pileupweightDOWN", &pileupweightDOWN, &b_pileupweightDOWN);
   fChain->SetBranchAddress("pileupweightHash", &pileupweightHash, &b_pileupweightHash);
   fChain->SetBranchAddress("SherpaWeight", &SherpaWeight, &b_SherpaWeight);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_flav", &jet_flav, &b_jet_flav);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_ntracks", &jet_ntracks, &b_jet_ntracks);
   fChain->SetBranchAddress("trackjet_pt", &trackjet_pt, &b_trackjet_pt);
   fChain->SetBranchAddress("trackjet_eta", &trackjet_eta, &b_trackjet_eta);
   fChain->SetBranchAddress("trackjet_phi", &trackjet_phi, &b_trackjet_phi);
   fChain->SetBranchAddress("trackjet_e", &trackjet_e, &b_trackjet_e);
   fChain->SetBranchAddress("trackjet_MV2c10", &trackjet_MV2c10, &b_trackjet_MV2c10);
   fChain->SetBranchAddress("trackjet_truthflav", &trackjet_truthflav, &b_trackjet_truthflav);
   fChain->SetBranchAddress("nbaselineEl", &nbaselineEl, &b_nbaselineEl);
   fChain->SetBranchAddress("nEl", &nEl, &b_nEl);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("nbaselineMu", &nbaselineMu, &b_nbaselineMu);
   fChain->SetBranchAddress("nMu", &nMu, &b_nMu);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("nbaselinePh", &nbaselinePh, &b_nbaselinePh);
   fChain->SetBranchAddress("nPh", &nPh, &b_nPh);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);

   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_e", &tau_e, &b_tau_e);
   fChain->SetBranchAddress("tau_bdtJet", &tau_bdtJet, &b_tau_bdtJet);
   fChain->SetBranchAddress("tau_bdtEl", &tau_bdtEl, &b_tau_bdtEl);
   fChain->SetBranchAddress("tau_nH", &tau_nH, &b_tau_nH);
   fChain->SetBranchAddress("tau_nWTracks", &tau_nWTracks, &b_tau_nWTracks);
   fChain->SetBranchAddress("tau_nTracks", &tau_nTracks, &b_tau_nTracks);
   fChain->SetBranchAddress("tau_nPi0", &tau_nPi0, &b_tau_nPi0);
   fChain->SetBranchAddress("tau_nCharged", &tau_nCharged, &b_tau_nCharged);
   fChain->SetBranchAddress("tau_nNeut", &tau_nNeut, &b_tau_nNeut);

   fChain->SetBranchAddress("MET_pt", &MET_pt, &b_MET_pt);
   fChain->SetBranchAddress("MET_phi", &MET_phi, &b_MET_phi);
   fChain->SetBranchAddress("MET_pt_prime", &MET_pt_prime, &b_MET_pt_prime);
   fChain->SetBranchAddress("MET_phi_prime", &MET_phi_prime, &b_MET_phi_prime);
   fChain->SetBranchAddress("sumet", &sumet, &b_sumet);
   fChain->SetBranchAddress("metsig", &metsig, &b_metsig);
   fChain->SetBranchAddress("metsigET", &metsigET, &b_metsigET);
   fChain->SetBranchAddress("metsigHT", &metsigHT, &b_metsigHT);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("SCTerror", &SCTerror, &b_SCTerror);
   fChain->SetBranchAddress("CoreFlag", &CoreFlag, &b_CoreFlag);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("TriggerSF", &TriggerSF, &b_TriggerSF);
   fChain->SetBranchAddress("MuonWeightReco", &MuonWeightReco, &b_MuonWeightReco);
   fChain->SetBranchAddress("MuonWeightTrigger", &MuonWeightTrigger, &b_MuonWeightTrigger);
   fChain->SetBranchAddress("ElecWeightReco", &ElecWeightReco, &b_ElecWeightReco);
   fChain->SetBranchAddress("ElecWeightTrigger", &ElecWeightTrigger, &b_ElecWeightTrigger);
   fChain->SetBranchAddress("PhotonWeight", &PhotonWeight, &b_PhotonWeight);
   fChain->SetBranchAddress("MuonWeightSTATUP", &MuonWeightSTATUP, &b_MuonWeightSTATUP);
   fChain->SetBranchAddress("MuonWeightSTATDOWN", &MuonWeightSTATDOWN, &b_MuonWeightSTATDOWN);
   fChain->SetBranchAddress("MuonWeightSYSUP", &MuonWeightSYSUP, &b_MuonWeightSYSUP);
   fChain->SetBranchAddress("MuonWeightSYSDOWN", &MuonWeightSYSDOWN, &b_MuonWeightSYSDOWN);
   fChain->SetBranchAddress("MuonWeightTRIGSTATUP", &MuonWeightTRIGSTATUP, &b_MuonWeightTRIGSTATUP);
   fChain->SetBranchAddress("MuonWeightTRIGSTATDOWN", &MuonWeightTRIGSTATDOWN, &b_MuonWeightTRIGSTATDOWN);
   fChain->SetBranchAddress("MuonWeightTRIGSYSTUP", &MuonWeightTRIGSYSTUP, &b_MuonWeightTRIGSYSTUP);
   fChain->SetBranchAddress("MuonWeightTRIGSYSTDOWN", &MuonWeightTRIGSYSTDOWN, &b_MuonWeightTRIGSYSTDOWN);
   fChain->SetBranchAddress("MuonWeightISOSTATUP", &MuonWeightISOSTATUP, &b_MuonWeightISOSTATUP);
   fChain->SetBranchAddress("MuonWeightISOSTATDOWN", &MuonWeightISOSTATDOWN, &b_MuonWeightISOSTATDOWN);
   fChain->SetBranchAddress("MuonWeightISOSYSTUP", &MuonWeightISOSYSTUP, &b_MuonWeightISOSYSTUP);
   fChain->SetBranchAddress("MuonWeightISOSYSTDOWN", &MuonWeightISOSYSTDOWN, &b_MuonWeightISOSYSTDOWN);
   fChain->SetBranchAddress("MuonWeightTTVASTATUP", &MuonWeightTTVASTATUP, &b_MuonWeightTTVASTATUP);
   fChain->SetBranchAddress("MuonWeightTTVASTATDOWN", &MuonWeightTTVASTATDOWN, &b_MuonWeightTTVASTATDOWN);
   fChain->SetBranchAddress("MuonWeightTTVASYSTUP", &MuonWeightTTVASYSTUP, &b_MuonWeightTTVASYSTUP);
   fChain->SetBranchAddress("MuonWeightTTVASYSTDOWN", &MuonWeightTTVASYSTDOWN, &b_MuonWeightTTVASYSTDOWN);
   fChain->SetBranchAddress("MuonWeightBADMUONSTATUP", &MuonWeightBADMUONSTATUP, &b_MuonWeightBADMUONSTATUP);
   fChain->SetBranchAddress("MuonWeightBADMUONSTATDOWN", &MuonWeightBADMUONSTATDOWN, &b_MuonWeightBADMUONSTATDOWN);
   fChain->SetBranchAddress("MuonWeightBADMUONSYSUP", &MuonWeightBADMUONSYSUP, &b_MuonWeightBADMUONSYSUP);
   fChain->SetBranchAddress("MuonWeightBADMUONSYSDOWN", &MuonWeightBADMUONSYSDOWN, &b_MuonWeightBADMUONSYSDOWN);
   fChain->SetBranchAddress("ElecWeightIDUP", &ElecWeightIDUP, &b_ElecWeightIDUP);
   fChain->SetBranchAddress("ElecWeightIDDOWN", &ElecWeightIDDOWN, &b_ElecWeightIDDOWN);
   fChain->SetBranchAddress("ElecWeightISOUP", &ElecWeightISOUP, &b_ElecWeightISOUP);
   fChain->SetBranchAddress("ElecWeightISODOWN", &ElecWeightISODOWN, &b_ElecWeightISODOWN);
   fChain->SetBranchAddress("ElecWeightRECOUP", &ElecWeightRECOUP, &b_ElecWeightRECOUP);
   fChain->SetBranchAddress("ElecWeightRECODOWN", &ElecWeightRECODOWN, &b_ElecWeightRECODOWN);
   fChain->SetBranchAddress("ElecWeightTRIGUP", &ElecWeightTRIGUP, &b_ElecWeightTRIGUP);
   fChain->SetBranchAddress("ElecWeightTRIGDOWN", &ElecWeightTRIGDOWN, &b_ElecWeightTRIGDOWN);
   fChain->SetBranchAddress("ElecWeightTRIGEFFUP", &ElecWeightTRIGEFFUP, &b_ElecWeightTRIGEFFUP);
   fChain->SetBranchAddress("ElecWeightTRIGEFFDOWN", &ElecWeightTRIGEFFDOWN, &b_ElecWeightTRIGEFFDOWN);
   fChain->SetBranchAddress("ElecWeightCHARGEIDSELUP", &ElecWeightCHARGEIDSELUP, &b_ElecWeightCHARGEIDSELUP);
   fChain->SetBranchAddress("ElecWeightCHARGEIDSELDOWN", &ElecWeightCHARGEIDSELDOWN, &b_ElecWeightCHARGEIDSELDOWN);
   fChain->SetBranchAddress("PhotonSF_IDUP", &PhotonSF_IDUP, &b_PhotonSF_IDUP);
   fChain->SetBranchAddress("PhotonSF_IDDOWN", &PhotonSF_IDDOWN, &b_PhotonSF_IDDOWN);
   fChain->SetBranchAddress("PhotonSF_TRKISOUP", &PhotonSF_TRKISOUP, &b_PhotonSF_TRKISOUP);
   fChain->SetBranchAddress("PhotonSF_TRKISODOWN", &PhotonSF_TRKISODOWN, &b_PhotonSF_TRKISODOWN);
   fChain->SetBranchAddress("TriggerWeight", &TriggerWeight, &b_TriggerWeight);
   fChain->SetBranchAddress("TriggerWeight2", &TriggerWeight2, &b_TriggerWeight2);
   fChain->SetBranchAddress("jvtweight", &jvtweight, &b_jvtweight);
   fChain->SetBranchAddress("jvtweightUP", &jvtweightUP, &b_jvtweightUP);
   fChain->SetBranchAddress("jvtweightDOWN", &jvtweightDOWN, &b_jvtweightDOWN);
   fChain->SetBranchAddress("btagweight", &btagweight, &b_btagweight);
   fChain->SetBranchAddress("btagweightBUP", &btagweightBUP, &b_btagweightBUP);
   fChain->SetBranchAddress("btagweightBDOWN", &btagweightBDOWN, &b_btagweightBDOWN);
   fChain->SetBranchAddress("btagweightCUP", &btagweightCUP, &b_btagweightCUP);
   fChain->SetBranchAddress("btagweightCDOWN", &btagweightCDOWN, &b_btagweightCDOWN);
   fChain->SetBranchAddress("btagweightLUP", &btagweightLUP, &b_btagweightLUP);
   fChain->SetBranchAddress("btagweightLDOWN", &btagweightLDOWN, &b_btagweightLDOWN);
   fChain->SetBranchAddress("btagweightExUP", &btagweightExUP, &b_btagweightExUP);
   fChain->SetBranchAddress("btagweightExDOWN", &btagweightExDOWN, &b_btagweightExDOWN);
   fChain->SetBranchAddress("btagweightExCUP", &btagweightExCUP, &b_btagweightExCUP);
   fChain->SetBranchAddress("btagweightExCDOWN", &btagweightExCDOWN, &b_btagweightExCDOWN);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("averageIntPerXingCorr", &averageIntPerXingCorr, &b_averageIntPerXingCorr);
   fChain->SetBranchAddress("jet_MV2c10", &jet_MV2c10, &b_jet_MV2c10);
   fChain->SetBranchAddress("jet_chf", &jet_chf, &b_jet_chf);
   fChain->SetBranchAddress("jet_jvtxf", &jet_jvtxf, &b_jet_jvtxf);
   fChain->SetBranchAddress("jet_BCH_CORR_CELL", &jet_BCH_CORR_CELL, &b_jet_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_emfrac", &jet_emfrac, &b_jet_emfrac);
   fChain->SetBranchAddress("jet_fmax", &jet_fmax, &b_jet_fmax);

   fChain->SetBranchAddress("el_SF", &el_SF, &b_el_SF);
   fChain->SetBranchAddress("ncosmicMu", &ncosmicMu, &b_ncosmicMu);
   fChain->SetBranchAddress("mu_SF", &mu_SF, &b_mu_SF);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ptcone20", &ptcone20, &b_ptcone20);
   fChain->SetBranchAddress("ptvarcone20", &ptvarcone20, &b_ptvarcone20);
   fChain->SetBranchAddress("passtauveto", &passtauveto, &b_passtauveto);
   fChain->SetBranchAddress("MET_jet_pt", &MET_jet_pt, &b_MET_jet_pt);
   fChain->SetBranchAddress("MET_jet_phi", &MET_jet_phi, &b_MET_jet_phi);
   fChain->SetBranchAddress("MET_mu_pt", &MET_mu_pt, &b_MET_mu_pt);
   fChain->SetBranchAddress("MET_mu_phi", &MET_mu_phi, &b_MET_mu_phi);
   fChain->SetBranchAddress("MET_el_pt", &MET_el_pt, &b_MET_el_pt);
   fChain->SetBranchAddress("MET_el_phi", &MET_el_phi, &b_MET_el_phi);
   fChain->SetBranchAddress("MET_y_pt", &MET_y_pt, &b_MET_y_pt);
   fChain->SetBranchAddress("MET_y_phi", &MET_y_phi, &b_MET_y_phi);
   fChain->SetBranchAddress("MET_softTrk_pt", &MET_softTrk_pt, &b_MET_softTrk_pt);
   fChain->SetBranchAddress("MET_softTrk_phi", &MET_softTrk_phi, &b_MET_softTrk_phi);
   fChain->SetBranchAddress("MET_track_pt", &MET_track_pt, &b_MET_track_pt);
   fChain->SetBranchAddress("MET_track_phi", &MET_track_phi, &b_MET_track_phi);
   fChain->SetBranchAddress("MET_NonInt_pt", &MET_NonInt_pt, &b_MET_NonInt_pt);
   fChain->SetBranchAddress("MET_NonInt_phi", &MET_NonInt_phi, &b_MET_NonInt_phi);
   fChain->SetBranchAddress("MET_tau_pt", &MET_tau_pt, &b_MET_tau_pt);
   fChain->SetBranchAddress("MET_tau_phi", &MET_tau_phi, &b_MET_tau_phi);
   fChain->SetBranchAddress("sumet_jet", &sumet_jet, &b_sumet_jet);
   fChain->SetBranchAddress("sumet_el", &sumet_el, &b_sumet_el);
   fChain->SetBranchAddress("sumet_y", &sumet_y, &b_sumet_y);
   fChain->SetBranchAddress("sumet_mu", &sumet_mu, &b_sumet_mu);
   fChain->SetBranchAddress("sumet_softTrk", &sumet_softTrk, &b_sumet_softTrk);
   fChain->SetBranchAddress("sumet_softClu", &sumet_softClu, &b_sumet_softClu);
   fChain->SetBranchAddress("sumet_NonInt", &sumet_NonInt, &b_sumet_NonInt);
   fChain->SetBranchAddress("cellMET", &cellMET, &b_cellMET);
   fChain->SetBranchAddress("mhtMET", &mhtMET, &b_mhtMET);
   fChain->SetBranchAddress("METTrigPassed", &METTrigPassed, &b_METTrigPassed);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("matched_HLT_e120_lhloose", &matched_HLT_e120_lhloose, &b_matched_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("matched_HLT_e60_lhmedium", &matched_HLT_e60_lhmedium, &b_matched_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("matched_HLT_e24_lhmedium_L1EM20VH", &matched_HLT_e24_lhmedium_L1EM20VH, &b_matched_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM18VH", &HLT_e24_lhmedium_L1EM18VH, &b_HLT_e24_lhmedium_L1EM18VH);
   fChain->SetBranchAddress("matched_HLT_e24_lhmedium_L1EM18VH", &matched_HLT_e24_lhmedium_L1EM18VH, &b_matched_HLT_e24_lhmedium_L1EM18VH);
   fChain->SetBranchAddress("HLT_e20_medium", &HLT_e20_medium, &b_HLT_e20_medium);
   fChain->SetBranchAddress("matched_HLT_e20_medium", &matched_HLT_e20_medium, &b_matched_HLT_e20_medium);
   fChain->SetBranchAddress("HLT_2e17_loose", &HLT_2e17_loose, &b_HLT_2e17_loose);
   fChain->SetBranchAddress("matched_HLT_2e17_loose", &matched_HLT_2e17_loose, &b_matched_HLT_2e17_loose);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("matched_HLT_mu50", &matched_HLT_mu50, &b_matched_HLT_mu50);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("matched_HLT_mu20_iloose_L1MU15", &matched_HLT_mu20_iloose_L1MU15, &b_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu26", &HLT_mu26, &b_HLT_mu26);
   fChain->SetBranchAddress("matched_HLT_mu26", &matched_HLT_mu26, &b_matched_HLT_mu26);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("matched_HLT_mu26_ivarmedium", &matched_HLT_mu26_ivarmedium, &b_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_2mu6", &HLT_2mu6, &b_HLT_2mu6);
   fChain->SetBranchAddress("matched_HLT_2mu6", &matched_HLT_2mu6, &b_matched_HLT_2mu6);
   fChain->SetBranchAddress("HLT_g120_loose", &HLT_g120_loose, &b_HLT_g120_loose);
   fChain->SetBranchAddress("matched_HLT_g120_loose", &matched_HLT_g120_loose, &b_matched_HLT_g120_loose);
   fChain->SetBranchAddress("HLT_g140_loose", &HLT_g140_loose, &b_HLT_g140_loose);
   fChain->SetBranchAddress("matched_HLT_g140_loose", &matched_HLT_g140_loose, &b_matched_HLT_g140_loose);
   fChain->SetBranchAddress("HLT_g200_etcut", &HLT_g200_etcut, &b_HLT_g200_etcut);
   fChain->SetBranchAddress("matched_HLT_g200_etcut", &matched_HLT_g200_etcut, &b_matched_HLT_g200_etcut);
   fChain->SetBranchAddress("HLT_g35_loose_L1EM15", &HLT_g35_loose_L1EM15, &b_HLT_g35_loose_L1EM15);
   fChain->SetBranchAddress("matched_HLT_g35_loose_L1EM15", &matched_HLT_g35_loose_L1EM15, &b_matched_HLT_g35_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g40_loose_L1EM15", &HLT_g40_loose_L1EM15, &b_HLT_g40_loose_L1EM15);
   fChain->SetBranchAddress("matched_HLT_g40_loose_L1EM15", &matched_HLT_g40_loose_L1EM15, &b_matched_HLT_g40_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g45_loose_L1EM15", &HLT_g45_loose_L1EM15, &b_HLT_g45_loose_L1EM15);
   fChain->SetBranchAddress("matched_HLT_g45_loose_L1EM15", &matched_HLT_g45_loose_L1EM15, &b_matched_HLT_g45_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g50_loose_L1EM15", &HLT_g50_loose_L1EM15, &b_HLT_g50_loose_L1EM15);
   fChain->SetBranchAddress("matched_HLT_g50_loose_L1EM15", &matched_HLT_g50_loose_L1EM15, &b_matched_HLT_g50_loose_L1EM15);
   fChain->SetBranchAddress("HLT_2j35_btight_2j35_L13J25.0ETA23", &HLT_2j35_btight_2j35_L13J25_0ETA23, &b_HLT_2j35_btight_2j35_L13J25_0ETA23);
   fChain->SetBranchAddress("matched_HLT_2j35_btight_2j35_L13J25.0ETA23", &matched_HLT_2j35_btight_2j35_L13J25_0ETA23, &b_matched_HLT_2j35_btight_2j35_L13J25_0ETA23);
   fChain->SetBranchAddress("HLT_2j45_bmedium_2j45_L13J25.0ETA23", &HLT_2j45_bmedium_2j45_L13J25_0ETA23, &b_HLT_2j45_bmedium_2j45_L13J25_0ETA23);
   fChain->SetBranchAddress("matched_HLT_2j45_bmedium_2j45_L13J25.0ETA23", &matched_HLT_2j45_bmedium_2j45_L13J25_0ETA23, &b_matched_HLT_2j45_bmedium_2j45_L13J25_0ETA23);
   fChain->SetBranchAddress("HLT_6j45_0eta240",&HLT_6j45_0eta240, &b_HLT_6j45_0eta240);
   fChain->SetBranchAddress("HLT_6j60",&HLT_6j60, &b_HLT_6j60);
   fChain->SetBranchAddress("HLT_j100_2j55_bmedium", &HLT_j100_2j55_bmedium, &b_HLT_j100_2j55_bmedium);
   fChain->SetBranchAddress("matched_HLT_j100_2j55_bmedium", &matched_HLT_j100_2j55_bmedium, &b_matched_HLT_j100_2j55_bmedium);
   fChain->SetBranchAddress("HLT_j150_bmedium_j50_bmedium", &HLT_j150_bmedium_j50_bmedium, &b_HLT_j150_bmedium_j50_bmedium);
   fChain->SetBranchAddress("matched_HLT_j150_bmedium_j50_bmedium", &matched_HLT_j150_bmedium_j50_bmedium, &b_matched_HLT_j150_bmedium_j50_bmedium);
   fChain->SetBranchAddress("HLT_2j65_btight_j65", &HLT_2j65_btight_j65, &b_HLT_2j65_btight_j65);
   fChain->SetBranchAddress("matched_HLT_2j65_btight_j65", &matched_HLT_2j65_btight_j65, &b_matched_HLT_2j65_btight_j65);
   fChain->SetBranchAddress("HLT_2j70_bmedium_j70", &HLT_2j70_bmedium_j70, &b_HLT_2j70_bmedium_j70);
   fChain->SetBranchAddress("matched_HLT_2j70_bmedium_j70", &matched_HLT_2j70_bmedium_j70, &b_matched_HLT_2j70_bmedium_j70);
   fChain->SetBranchAddress("HLT_j225_bloose", &HLT_j225_bloose, &b_HLT_j225_bloose);
   fChain->SetBranchAddress("matched_HLT_j225_bloose", &matched_HLT_j225_bloose, &b_matched_HLT_j225_bloose);
   fChain->SetBranchAddress("HLT_j65_btight_3j65_L13J25.0ETA23", &HLT_j65_btight_3j65_L13J25_0ETA23, &b_HLT_j65_btight_3j65_L13J25_0ETA23);
   fChain->SetBranchAddress("matched_HLT_j65_btight_3j65_L13J25.0ETA23", &matched_HLT_j65_btight_3j65_L13J25_0ETA23, &b_matched_HLT_j65_btight_3j65_L13J25_0ETA23);
   fChain->SetBranchAddress("HLT_j70_bmedium_3j70_L13J25.0ETA23", &HLT_j70_bmedium_3j70_L13J25_0ETA23, &b_HLT_j70_bmedium_3j70_L13J25_0ETA23);
   fChain->SetBranchAddress("matched_HLT_j70_bmedium_3j70_L13J25.0ETA23", &matched_HLT_j70_bmedium_3j70_L13J25_0ETA23, &b_matched_HLT_j70_bmedium_3j70_L13J25_0ETA23);
   fChain->SetBranchAddress("HLT_xe35", &HLT_xe35, &b_HLT_xe35);
   fChain->SetBranchAddress("matched_HLT_xe35", &matched_HLT_xe35, &b_matched_HLT_xe35);
   fChain->SetBranchAddress("HLT_xe60", &HLT_xe60, &b_HLT_xe60);
   fChain->SetBranchAddress("matched_HLT_xe60", &matched_HLT_xe60, &b_matched_HLT_xe60);
   fChain->SetBranchAddress("HLT_xe70", &HLT_xe70, &b_HLT_xe70);
   fChain->SetBranchAddress("matched_HLT_xe70", &matched_HLT_xe70, &b_matched_HLT_xe70);
   fChain->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("matched_HLT_xe70_tc_lcw", &matched_HLT_xe70_tc_lcw, &b_matched_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   fChain->SetBranchAddress("matched_HLT_xe70_mht", &matched_HLT_xe70_mht, &b_matched_HLT_xe70_mht);
   fChain->SetBranchAddress("HLT_xe80", &HLT_xe80, &b_HLT_xe80);
   fChain->SetBranchAddress("matched_HLT_xe80", &matched_HLT_xe80, &b_matched_HLT_xe80);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe80_tc_lcw_L1XE50", &matched_HLT_xe80_tc_lcw_L1XE50, &b_matched_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_mht_L1XE50", &matched_HLT_xe90_mht_L1XE50, &b_matched_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_tc_lcw_wEFMu_L1XE50", &HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_tc_lcw_wEFMu_L1XE50", &matched_HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_matched_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_wEFMu_L1XE50", &HLT_xe90_mht_wEFMu_L1XE50, &b_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_mht_wEFMu_L1XE50", &matched_HLT_xe90_mht_wEFMu_L1XE50, &b_matched_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe100", &HLT_xe100, &b_HLT_xe100);
   fChain->SetBranchAddress("matched_HLT_xe100", &matched_HLT_xe100, &b_matched_HLT_xe100);
   fChain->SetBranchAddress("HLT_xe100_L1XE50", &HLT_xe100_L1XE50, &b_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_L1XE50", &matched_HLT_xe100_L1XE50, &b_matched_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_tc_em_L1XE50", &HLT_xe100_tc_em_L1XE50, &b_HLT_xe100_tc_em_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_tc_em_L1XE50", &matched_HLT_xe100_tc_em_L1XE50, &b_matched_HLT_xe100_tc_em_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_mht_L1XE50", &matched_HLT_xe100_mht_L1XE50, &b_matched_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe110_mht_L1XE50", &matched_HLT_xe110_mht_L1XE50, &b_matched_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe120_pueta", &HLT_xe120_pueta, &b_HLT_xe120_pueta);
   fChain->SetBranchAddress("matched_HLT_xe120_pueta", &matched_HLT_xe120_pueta, &b_matched_HLT_xe120_pueta);
   fChain->SetBranchAddress("HLT_xe120_pufit", &HLT_xe120_pufit, &b_HLT_xe120_pufit);
   fChain->SetBranchAddress("matched_HLT_xe120_pufit", &matched_HLT_xe120_pufit, &b_matched_HLT_xe120_pufit);
   fChain->SetBranchAddress("HLT_xe130_mht_L1XE50", &HLT_xe130_mht_L1XE50, &b_HLT_xe130_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe130_mht_L1XE50", &matched_HLT_xe130_mht_L1XE50, &b_matched_HLT_xe130_mht_L1XE50);
   fChain->SetBranchAddress("HLT_j100_xe80", &HLT_j100_xe80, &b_HLT_j100_xe80);
   fChain->SetBranchAddress("matched_HLT_j100_xe80", &matched_HLT_j100_xe80, &b_matched_HLT_j100_xe80);
   fChain->SetBranchAddress("HLT_j80_xe80", &HLT_j80_xe80, &b_HLT_j80_xe80);
   fChain->SetBranchAddress("matched_HLT_j80_xe80", &matched_HLT_j80_xe80, &b_matched_HLT_j80_xe80);
   fChain->SetBranchAddress("HLT_j400", &HLT_j400, &b_HLT_j400);
   fChain->SetBranchAddress("matched_HLT_j400", &matched_HLT_j400, &b_matched_HLT_j400);
   fChain->SetBranchAddress("HLT_j360", &HLT_j360, &b_HLT_j360);
   fChain->SetBranchAddress("matched_HLT_j360", &matched_HLT_j360, &b_matched_HLT_j360);
   fChain->SetBranchAddress("HLT_j320", &HLT_j320, &b_HLT_j320);
   fChain->SetBranchAddress("matched_HLT_j320", &matched_HLT_j320, &b_matched_HLT_j320);
   fChain->SetBranchAddress("HLT_j260", &HLT_j260, &b_HLT_j260);
   fChain->SetBranchAddress("matched_HLT_j260", &matched_HLT_j260, &b_matched_HLT_j260);
   fChain->SetBranchAddress("HLT_j200", &HLT_j200, &b_HLT_j200);
   fChain->SetBranchAddress("matched_HLT_j200", &matched_HLT_j200, &b_matched_HLT_j200);
   fChain->SetBranchAddress("HLT_j175", &HLT_j175, &b_HLT_j175);
   fChain->SetBranchAddress("matched_HLT_j175", &matched_HLT_j175, &b_matched_HLT_j175);
   fChain->SetBranchAddress("HLT_j150", &HLT_j150, &b_HLT_j150);
   fChain->SetBranchAddress("matched_HLT_j150", &matched_HLT_j150, &b_matched_HLT_j150);
   fChain->SetBranchAddress("HLT_j110", &HLT_j110, &b_HLT_j110);
   fChain->SetBranchAddress("matched_HLT_j110", &matched_HLT_j110, &b_matched_HLT_j110);
   fChain->SetBranchAddress("HLT_j85", &HLT_j85, &b_HLT_j85);
   fChain->SetBranchAddress("matched_HLT_j85", &matched_HLT_j85, &b_matched_HLT_j85);
   fChain->SetBranchAddress("HLT_j60", &HLT_j60, &b_HLT_j60);
   fChain->SetBranchAddress("matched_HLT_j60", &matched_HLT_j60, &b_matched_HLT_j60);
   fChain->SetBranchAddress("HLT_j25", &HLT_j25, &b_HLT_j25);
   fChain->SetBranchAddress("matched_HLT_j25", &matched_HLT_j25, &b_matched_HLT_j25);
   fChain->SetBranchAddress("HLT_j15", &HLT_j15, &b_HLT_j15);
   fChain->SetBranchAddress("matched_HLT_j15", &matched_HLT_j15, &b_matched_HLT_j15);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("matched_HLT_mu40", &matched_HLT_mu40, &b_matched_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu24_iloose", &HLT_mu24_iloose, &b_HLT_mu24_iloose);
   fChain->SetBranchAddress("matched_HLT_mu24_iloose", &matched_HLT_mu24_iloose, &b_matched_HLT_mu24_iloose);
   fChain->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("matched_HLT_mu24_ivarloose", &matched_HLT_mu24_ivarloose, &b_matched_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("HLT_mu24_L1MU15", &HLT_mu24_L1MU15, &b_HLT_mu24_L1MU15);
   fChain->SetBranchAddress("matched_HLT_mu24_L1MU15", &matched_HLT_mu24_L1MU15, &b_matched_HLT_mu24_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("matched_HLT_mu24_ivarmedium", &matched_HLT_mu24_ivarmedium, &b_matched_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("HLT_mu24_imedium", &HLT_mu24_imedium, &b_HLT_mu24_imedium);
   fChain->SetBranchAddress("matched_HLT_mu24_imedium", &matched_HLT_mu24_imedium, &b_matched_HLT_mu24_imedium);
   fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("matched_HLT_e24_lhtight_nod0_ivarloose", &matched_HLT_e24_lhtight_nod0_ivarloose, &b_matched_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("matched_HLT_e26_lhtight_nod0_ivarloose", &matched_HLT_e26_lhtight_nod0_ivarloose, &b_matched_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("matched_HLT_e60_lhmedium_nod0", &matched_HLT_e60_lhmedium_nod0, &b_matched_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e60_medium", &HLT_e60_medium, &b_HLT_e60_medium);
   fChain->SetBranchAddress("matched_HLT_e60_medium", &matched_HLT_e60_medium, &b_matched_HLT_e60_medium);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("matched_HLT_e140_lhloose_nod0", &matched_HLT_e140_lhloose_nod0, &b_matched_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e300_etcut", &HLT_e300_etcut, &b_HLT_e300_etcut);
   fChain->SetBranchAddress("matched_HLT_e300_etcut", &matched_HLT_e300_etcut, &b_matched_HLT_e300_etcut);
   fChain->SetBranchAddress("hasMEphoton", &hasMEphoton, &b_hasMEphoton);
   fChain->SetBranchAddress("hasMEphoton80", &hasMEphoton80, &b_hasMEphoton80);
   fChain->SetBranchAddress("hasMEphotonMCTC", &hasMEphotonMCTC, &b_hasMEphotonMCTC);
   fChain->SetBranchAddress("TruthNonInt_pt", &TruthNonInt_pt, &b_TruthNonInt_pt);
   fChain->SetBranchAddress("TruthNonInt_phi", &TruthNonInt_phi, &b_TruthNonInt_phi);
   fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
   fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
   fChain->SetBranchAddress("nsmrJets", &nsmrJets, &b_nsmrJets);
   fChain->SetBranchAddress("smrjet_index", &smrjet_index, &b_smrjet_index);
   fChain->SetBranchAddress("smrjet_pt", &smrjet_pt, &b_smrjet_pt);
   fChain->SetBranchAddress("smrjet_eta", &smrjet_eta, &b_smrjet_eta);
   fChain->SetBranchAddress("smrjet_phi", &smrjet_phi, &b_smrjet_phi);
   fChain->SetBranchAddress("smrjet_e", &smrjet_e, &b_smrjet_e);
   fChain->SetBranchAddress("smrjet_MV2", &smrjet_MV2, &b_smrjet_MV2);
   fChain->SetBranchAddress("smrjet_flav", &smrjet_flav, &b_smrjet_flav);
   fChain->SetBranchAddress("smrMET_pt", &smrMET_pt, &b_smrMET_pt);
   fChain->SetBranchAddress("smrMET_phi", &smrMET_phi, &b_smrMET_phi);
   fChain->SetBranchAddress("nFatJetsKt8", &nFatJetsKt8, &b_nFatJetsKt8);
   fChain->SetBranchAddress("rcjet_kt8_pt", &rcjet_kt8_pt, &b_rcjet_kt8_pt);
   fChain->SetBranchAddress("rcjet_kt8_eta", &rcjet_kt8_eta, &b_rcjet_kt8_eta);
   fChain->SetBranchAddress("rcjet_kt8_phi", &rcjet_kt8_phi, &b_rcjet_kt8_phi);
   fChain->SetBranchAddress("rcjet_kt8_e", &rcjet_kt8_e, &b_rcjet_kt8_e);
   fChain->SetBranchAddress("nFatJetsKt12", &nFatJetsKt12, &b_nFatJetsKt12);
   fChain->SetBranchAddress("rcjet_kt12_pt", &rcjet_kt12_pt, &b_rcjet_kt12_pt);
   fChain->SetBranchAddress("rcjet_kt12_eta", &rcjet_kt12_eta, &b_rcjet_kt12_eta);
   fChain->SetBranchAddress("rcjet_kt12_phi", &rcjet_kt12_phi, &b_rcjet_kt12_phi);
   fChain->SetBranchAddress("rcjet_kt12_e", &rcjet_kt12_e, &b_rcjet_kt12_e);
   fChain->SetBranchAddress("nFatJetsKt10", &nFatJetsKt10, &b_nFatJetsKt10);
   fChain->SetBranchAddress("fatjet_kt10_pt", &fatjet_kt10_pt, &b_fatjet_kt10_pt);
   fChain->SetBranchAddress("fatjet_kt10_eta", &fatjet_kt10_eta, &b_fatjet_kt10_eta);
   fChain->SetBranchAddress("fatjet_kt10_phi", &fatjet_kt10_phi, &b_fatjet_kt10_phi);
   fChain->SetBranchAddress("fatjet_kt10_e", &fatjet_kt10_e, &b_fatjet_kt10_e);
   fChain->SetBranchAddress("fatjet_kt10_ntrkjets", &fatjet_kt10_ntrkjets, &b_fatjet_kt10_ntrkjets);
   fChain->SetBranchAddress("fatjet_kt10_nghostbhad", &fatjet_kt10_nghostbhad, &b_fatjet_kt10_nghostbhad);
   fChain->SetBranchAddress("fatjet_kt10_Split12", &fatjet_kt10_Split12, &b_fatjet_kt10_Split12);
   fChain->SetBranchAddress("fatjet_kt10_Split23", &fatjet_kt10_Split23, &b_fatjet_kt10_Split23);
   fChain->SetBranchAddress("fatjet_kt10_Split34", &fatjet_kt10_Split34, &b_fatjet_kt10_Split34);
   fChain->SetBranchAddress("fatjet_kt10_Qw", &fatjet_kt10_Qw, &b_fatjet_kt10_Qw);
   fChain->SetBranchAddress("fatjet_kt10_Tau1", &fatjet_kt10_Tau1, &b_fatjet_kt10_Tau1);
   fChain->SetBranchAddress("fatjet_kt10_Tau2", &fatjet_kt10_Tau2, &b_fatjet_kt10_Tau2);
   fChain->SetBranchAddress("fatjet_kt10_Tau3", &fatjet_kt10_Tau3, &b_fatjet_kt10_Tau3);
   fChain->SetBranchAddress("fatjet_kt10_Tau32", &fatjet_kt10_Tau32, &b_fatjet_kt10_Tau32);
   fChain->SetBranchAddress("fatjet_kt10_w50", &fatjet_kt10_w50, &b_fatjet_kt10_w50);
   fChain->SetBranchAddress("fatjet_kt10_w80", &fatjet_kt10_w80, &b_fatjet_kt10_w80);
   fChain->SetBranchAddress("fatjet_kt10_z50", &fatjet_kt10_z50, &b_fatjet_kt10_z50);
   fChain->SetBranchAddress("fatjet_kt10_z80", &fatjet_kt10_z80, &b_fatjet_kt10_z80);
   fChain->SetBranchAddress("nt_fatjet_kt10_W50res", &nt_fatjet_kt10_W50res, &b_nt_fatjet_kt10_W50res);
   fChain->SetBranchAddress("nt_fatjet_kt10_W80res", &nt_fatjet_kt10_W80res, &b_nt_fatjet_kt10_W80res);
   fChain->SetBranchAddress("nt_fatjet_kt10_Z50res", &nt_fatjet_kt10_Z50res, &b_nt_fatjet_kt10_Z50res);
   fChain->SetBranchAddress("nt_fatjet_kt10_Z80res", &nt_fatjet_kt10_Z80res, &b_nt_fatjet_kt10_Z80res);
   fChain->SetBranchAddress("nt_fatjet_kt10_WLowWMassCut50", &nt_fatjet_kt10_WLowWMassCut50, &b_nt_fatjet_kt10_WLowWMassCut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_WLowWMassCut80", &nt_fatjet_kt10_WLowWMassCut80, &b_nt_fatjet_kt10_WLowWMassCut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZLowWMassCut50", &nt_fatjet_kt10_ZLowWMassCut50, &b_nt_fatjet_kt10_ZLowWMassCut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZLowWMassCut80", &nt_fatjet_kt10_ZLowWMassCut80, &b_nt_fatjet_kt10_ZLowWMassCut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_WHighWMassCut50", &nt_fatjet_kt10_WHighWMassCut50, &b_nt_fatjet_kt10_WHighWMassCut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_WHighWMassCut80", &nt_fatjet_kt10_WHighWMassCut80, &b_nt_fatjet_kt10_WHighWMassCut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZHighWMassCut50", &nt_fatjet_kt10_ZHighWMassCut50, &b_nt_fatjet_kt10_ZHighWMassCut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZHighWMassCut80", &nt_fatjet_kt10_ZHighWMassCut80, &b_nt_fatjet_kt10_ZHighWMassCut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_WD2Cut50", &nt_fatjet_kt10_WD2Cut50, &b_nt_fatjet_kt10_WD2Cut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_WD2Cut80", &nt_fatjet_kt10_WD2Cut80, &b_nt_fatjet_kt10_WD2Cut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZD2Cut50", &nt_fatjet_kt10_ZD2Cut50, &b_nt_fatjet_kt10_ZD2Cut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_ZD2Cut80", &nt_fatjet_kt10_ZD2Cut80, &b_nt_fatjet_kt10_ZD2Cut80);
   fChain->SetBranchAddress("fatjet_kt10_top50", &fatjet_kt10_top50, &b_fatjet_kt10_top50);
   fChain->SetBranchAddress("fatjet_kt10_top80", &fatjet_kt10_top80, &b_fatjet_kt10_top80);
   fChain->SetBranchAddress("nt_fatjet_kt10_top50res", &nt_fatjet_kt10_top50res, &b_nt_fatjet_kt10_top50res);
   fChain->SetBranchAddress("nt_fatjet_kt10_top80res", &nt_fatjet_kt10_top80res, &b_nt_fatjet_kt10_top80res);
   fChain->SetBranchAddress("nt_fatjet_kt10_TopTagTau32Cut50", &nt_fatjet_kt10_TopTagTau32Cut50, &b_nt_fatjet_kt10_TopTagTau32Cut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_TopTagTau32Cut80", &nt_fatjet_kt10_TopTagTau32Cut80, &b_nt_fatjet_kt10_TopTagTau32Cut80);
   fChain->SetBranchAddress("nt_fatjet_kt10_TopTagSplit23Cut50", &nt_fatjet_kt10_TopTagSplit23Cut50, &b_nt_fatjet_kt10_TopTagSplit23Cut50);
   fChain->SetBranchAddress("nt_fatjet_kt10_TopTagSplit23Cut80", &nt_fatjet_kt10_TopTagSplit23Cut80, &b_nt_fatjet_kt10_TopTagSplit23Cut80);
   fChain->SetBranchAddress("jet_truthmatched", &jet_truthmatched, &b_jet_truthmatched);
   fChain->SetBranchAddress("truthJet_pt", &truthJet_pt, &b_truthJet_pt);
   fChain->SetBranchAddress("truthJet_eta", &truthJet_eta, &b_truthJet_eta);
   fChain->SetBranchAddress("truthJet_phi", &truthJet_phi, &b_truthJet_phi);
   fChain->SetBranchAddress("truthJet_e", &truthJet_e, &b_truthJet_e);
   fChain->SetBranchAddress("truthJet_flav", &truthJet_flav, &b_truthJet_flav);
   fChain->SetBranchAddress("truthJet_matched", &truthJet_matched, &b_truthJet_matched);
   fChain->SetBranchAddress("truthJet_calib_isgood", &truthJet_calib_isgood, &b_truthJet_calib_isgood);
   fChain->SetBranchAddress("truthJet_matched_tau", &truthJet_matched_tau, &b_truthJet_matched_tau);
   fChain->SetBranchAddress("RMapRecoJet_pt", &RMapRecoJet_pt, &b_RMapRecoJet_pt);
   fChain->SetBranchAddress("RMapRecoJet_eta", &RMapRecoJet_eta, &b_RMapRecoJet_eta);
   fChain->SetBranchAddress("RMapRecoJet_phi", &RMapRecoJet_phi, &b_RMapRecoJet_phi);
   fChain->SetBranchAddress("RMapRecoJet_e", &RMapRecoJet_e, &b_RMapRecoJet_e);
   fChain->SetBranchAddress("RMapRecoJet_flav", &RMapRecoJet_flav, &b_RMapRecoJet_flav);
  
   Notify();
}

Bool_t MiniNtuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MiniNtuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MiniNtuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MiniNtuple_cxx
