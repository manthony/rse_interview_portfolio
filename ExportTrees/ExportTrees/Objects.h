#ifndef OBJECTS_H
#define OBJECTS_H

#include "TLorentzVector.h"
#include "ExportTrees/MiniNtuple.h"
#include <map>

using namespace std;



class Particle: public TLorentzVector{
 public:
  Particle(double pt=0.,double eta=0.,double phi=0., double e=0.);
  Particle(TLorentzVector tlv);
  double charge=-99;
  double flav=-99;
  double SF=1.;
  //extra photon variables
  double topoetcone40=-99;
  double ptvarcone20=-99;
  int truthJet_matched_tau=-99;

  //extra tau variables
  int nPi0=-99;
  double bdtJet=-99;
  double bdtEl=-99;
  int nH=-99;
  int nWTracks=-99;
  int nTracks=-99;
  int nCharged=-99;
  int nNeut=-99;

};


typedef std::vector<Particle*> Particles;

class Jet: public Particle{
 public:
  Jet(double pt=0., double eta=0., double phi=0., double e=0.);
  Jet(TLorentzVector p4){ this->SetPtEtaPhiE(p4.Pt(),p4.Eta(),p4.Phi(),p4.E());  };
  double MV2=-99;
  double BCH_CORR_CELL=-99;
  double fch=-999;
  double fem=-999;
  double fmax=-999;
  int flav=-99;
  int tflav=-99;
  int ntrkjets=0;
  int nghostbhad=0;
  float Split12=0;
  float Split23=0;
  float Split34=0;
  float Tau1=0;
  float Tau2=0;
  float Tau3=0;
  float Tau32=0;
  float Qw=0;
  bool w50=false;
  bool w80=false;
  bool z50=false;
  bool z80=false;
  int W50res=-99;
  int W80res=-99;
  int Z50res=-99;
  int Z80res=-99;
  float WLowWMassCut50=0.;
  float WLowWMassCut80=0.;
  float ZLowWMassCut50=0.;
  float ZLowWMassCut80=0.;
  float WHighWMassCut50=0.;
  float WHighWMassCut80=0.;
  float ZHighWMassCut50=0.;
  float ZHighWMassCut80=0.;
  float WD2Cut50=0.;
  float WD2Cut80=0.;
  float ZD2Cut50=0.;
  float ZD2Cut80=0.;
  bool top50=false;
  bool top80=false;
  int top50res=-99;
  int top80res=-99;
  float TopTagTau32Cut50=0.;
  float TopTagTau32Cut80=0.;
  float TopTagSplit23Cut50=0.;
  float TopTagSplit23Cut80=0.;

  double npartons=0;
  double wpartons=0;
  std::string origin="None";




  double R=-1;
  Jet* theGhost;
  void SetGhost(Jet* ghost){theGhost=ghost;};
  TVector2 pull;
  int ntracks=-1;
  double jpa_tracks=-99;
  double jpa_calo=-99;
  
  Particles* caloCells;
  Particles* tracks;
  Particles* subJets;
    
};



 
typedef std::vector<Jet*> Jets;


auto ptsorter=[](const Particle* j1, const Particle* j2) { return j1->Pt()>j2->Pt();};
auto MV2sorter=[](const Jet* j1, const Jet* j2) { return j1->MV2>j2->MV2;};


/*
std::map<TString, std::vector<Jet*> > GetJets( MiniNtuple *inputntuple);
std::map<TString, std::vector<Jet*> > GetFatJets( MiniNtuple *inputntuple);
std::vector<Particle*> GetElectrons( MiniNtuple *inputntuple);
std::vector<Particle*> GetMuons( MiniNtuple *inputntuple);
std::vector<Particle*> GetPhotons( MiniNtuple *inputntuple);
std::map<TString, TVector2*> GetMet( MiniNtuple *inputntuple);

std::vector<Particle*> GetTaus( MiniNtuple *inputntuple);
*/

#endif



