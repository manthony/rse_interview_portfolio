#ifndef JETSUBSTRUCTURE_H
#define JETSUBSTRUCTURE_H

#include "xAODCaloEvent/CaloCluster.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODRootAccess/Init.h" 
#include "xAODRootAccess/TEvent.h" 

#include <ExportTrees/Objects.h>

namespace JSS{

  //get jet calo constituents
  Particles* GetJetConstituents(const xAOD::Jet *jet); 
  //get tracks from Ghost Association
  Particles* GetJetGhostTracks(const xAOD::Jet *jet);   


  TVector2 CalcJetPull(Particles* cons, Particle* ref);
  TVector2 CalcJetPull(const xAOD::Jet *jet);
  //pt weighted with of the jets
  double ptww(Particles* cons, Particle* ref);
  //et weighted with of the jets
  double etww(Particles* cons, Particle* ref);
  //fraction of energy carried by largest energy constituent
  double flargest(Particles* cons, Particle* ref);
  //two point energy correlation
  double cbeta(Particles* cons);
  //retrieve the charge associated with a refrence object
  double charge(Particles* cons, Particle* ref,double k=0.3);

}
#endif
