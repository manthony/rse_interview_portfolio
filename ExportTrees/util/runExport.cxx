#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <iomanip>

#include "ExportTrees/Export.h"


//#include "amt2/MyAMT2.h"
//#include "RJigsaw/TRJigsaw.h"






void ExportMyTree(string tmp_file,string skim,bool nosys,bool smr){
 ifstream in0;
 std::cout <<  "imput file is : "  << tmp_file << std::endl;
 in0.open(tmp_file.c_str());
 string in_file, out_file;
 
 while(true)
   {
     in0 >> in_file >> out_file;
     bool isPythia = false;
     if(!in0.good()) break; 
     if(in_file.find("jetjet")!=std::string::npos)
       {
	 std::cout << "dijet found " << std::endl;
	 isPythia = true;
       }
     ExportTrees* exp = new ExportTrees(in_file,out_file,isPythia,skim,nosys,smr);
     exp->execute();
     if(exp)
       delete exp;
   }
 in0.close();

}

int main(int argc, char* argv[])
{
  string skim=NULL;
  //bool skim = false;
  bool nosys=false;
  bool smr = false;
  for(int j=2; j<argc; j++) {                                                  
    string arg=argv[j];
    if(arg.find("--skim")!= std::string::npos)
      {
	//skim="blah";
	cout << "skimming...." << endl;
      }
    else if(arg.find("--nosys")!= std::string::npos) 
      {
	nosys=true;
	cout << "no systematics... " << endl;
      }
     else if(arg.find("--smr")!= std::string::npos) 
      {
	smr=true;
	cout << "smearing ntuple... " << endl;
      }
    else
      {
	cout << "unknown arguments....." << endl;
	cout << "./ExportTrees <input list>" << endl;
	cout << "./ExportTrees <input list> --skim" << endl;
      }
  }
  ExportMyTree(argv[1],skim,nosys,smr);
  
  return 0;
 
}
